# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time
import pytz
class  WizStockValuationDate(models.TransientModel):
    _name = 'stock.valuation.ondate.report.itshore'
    company_id = fields.Many2one('res.company' ,required=True, string='Company',default=lambda  s : s.env["res.users"].sudo().search([('id','=',s.env.user.id)]).company_id)
    warehouse_ids = fields.Many2many('stock.warehouse', string='warehouse')

    location_id = fields.Many2one('stock.location', string='Location')
    start_date = fields.Date(
         string='Date à partir de 8h',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
    end_date = fields.Date(
         string='date à partir de 8h',
        
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
  
    point_vente_id=fields.Many2one('gakd.point.vente', string='Point de  Vente')
    filter_product_ids = fields.Many2many('product.product', string='Products')
    filter_product_categ_ids = fields.Many2many(
        'product.category',
        string='Categories'
        )
    info_s = fields.Char(string='',compute='get_dates_info')
    
    @api.onchange('start_date')
    def _onchange_start_date(self):
        dt=datetime.strptime(self.start_date, '%Y-%m-%d')
       
        tm=  date2 = datetime.strptime('08:00:00', '%H:%M:%S').time()
      
    @api.depends
    def get_dates_info(self):
        dt=datetime.strptime(self.start_date, '%Y-%m-%d')
        tm=  date2 = datetime.strptime('09:00:00', '%H:%M:%S').time()
        tm2=datetime.strptime('08:59:59', '%H:%M:%S').time()
        val1=str(datetime.combine(dt, tm2)+timedelta(days=1))
        val2=str(datetime.combine(dt, tm2)+timedelta(days=1))
        return  'De '+ val1+' Jusqu\'à ' +val2
    @api.onchange('company_id')
    def onchange_company_id(self):
     
        warehouse_ids = self.env['stock.warehouse'].sudo().search([])
        if self.company_id:
            warehouse_ids = self.env['stock.warehouse'].sudo().search([('company_id', '=', self.company_id.id)])
        return {
                'domain':
                        {
                         'warehouse_ids': [('id', 'in', [x.id for x in warehouse_ids])]
                         },
                'value':
                        {
                         'warehouse_ids': False
                        }
                }

    @api.onchange('warehouse_ids', 'company_id')
    def onchange_warehouse(self):
        """
        Make warehouse compatible with company
        """
        location_obj = self.env['stock.location']
        location_ids = location_obj.search([('usage', '=', 'internal')])
        total_warehouses = self.warehouse_ids
        if total_warehouses:
            addtional_ids = []
            for warehouse in total_warehouses:
                store_location_id = warehouse.view_location_id.id
                addtional_ids.extend([y.id for y in location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])])
            location_ids = addtional_ids
        elif self.company_id:
            total_warehouses = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id)])
            addtional_ids = []
            for warehouse in total_warehouses:
                store_location_id = warehouse.view_location_id.id
                addtional_ids.extend([y.id for y in location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])])
            location_ids = addtional_ids
        else:
            location_ids = [p.id for p in location_ids]
        return {
                  'domain':
                            {
                             'location_id': [('company_id', '=', self.company_id.id),('typedeEmp','=','point')]
                             },
                  'value':
                        {
                            'location_id': False
                        }
                }


    

    def get_location_by_point_vente(self):
      
        print self.location_id.name
        print self.location_id.id
       
    @api.multi
    def print_report(self):
        print ' 0 html report --*------'
        """
            Print report either by warehouse or product-category
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        dt=datetime.strptime(self.start_date, '%Y-%m-%d')
        tm=  date2 = datetime.strptime('07:00:00', '%H:%M:%S').time()
        tm2=datetime.strptime('06:59:59', '%H:%M:%S').time()
        datas = {
                 'form':
                    {
                        'company_id': self.company_id and [self.company_id.id] or [],
                        'company_name': self.company_id and [self.company_id.name] or [],
                        'warehouse_ids': [y.id for y in self.warehouse_ids],
                        'location_id': self.location_id and self.location_id.id or False,
                        'point_vente_id':self.point_vente_id and  self.point_vente_id.id or False,
                        'start_date': self.start_date,
                        'start_date_time':str(datetime.combine(dt,tm)),
                        'end_date':str((dt+timedelta(days=1)).date()),
                        'end_date_time':str(datetime.combine(dt, tm2)+timedelta(days=1)),
                      'info_s':self.info_s,
                        'id': self.id,
                       
                    }
                }
        if [y.id for y in self.warehouse_ids] and (not self.company_id):
            self.warehouse_ids = []
            raise Warning(_('Please select company of those warehouses to get correct view.\nYou should remove all warehouses first from selection field.'))
        print 'fffffffffffffffffffffffffffffffffffffffffffff--------------------------'
        return self.env['report'].get_action(self, 'gakd.valuation_ondate_report_itshore', data=datas)


class StockValuationDate(models.AbstractModel):
    _name = 'report.gakd.valuation_ondate_report_itshore'

   
    @api.multi
    def print_it(self):
        print " it's printed  ------ said -------- report de srock"
    
      
    
    
    def get_point_ventes(self,point_vente_id):
        print 'get_point_ventes ----->>>'
        print point_vente_id
        if not point_vente_id:
            return [ point_vente_id.id for  point_vente_id in self.env['stock.location'].sudo().search([('typedeEmp','=','point')])]
        else:
            return  [point_vente_id]
    def get_location(self,location_id):
        print  'get_location'
        print location_id
        print 'hhhhhhh'
        if not location_id:
            print '[ location_id.id for  location_id in self.en'
            print [ location_id.id for  location_id in self.env['stock.location'].sudo().search([('typedeEmp','=','point')])]
            return [ location_id.id for  location_id in self.env['stock.location'].sudo().search([('typedeEmp','=','point')])]
        else:
            print ' print if locaionn'
            print  [location_id]
            return  [location_id]

    def  get_location_name(self,location_id):
   
        return self.env['stock.location'].sudo().search([('id','=',location_id)]).name
    
    def  get_point_vente_name(self,point_vente_id):
        print 'get_point_vente_name'
        print point_vente_id
       
        return self.env['stock.location'].search([('point_vente_id','=',point_vente_id)]).name

    def get_location_by_point_vente(self,point_vente_id):
        print ' get_location_by_point_vente(s '
        print 'point_vente_id'
        print point_vente_id
        print  self.env['stock.location'].search([('point_vente_id','=',point_vente_id)])
   
        return  self.env['stock.location'].search([('point_vente_id','=',point_vente_id)]).id
    
    def get_transfert(self,product_id,location_id,form_date,to_date):
     
        query = """
                    select  sum((
                            CASE WHEN (spt.code ='internal' OR spt.code is null) AND sm.location_dest_id in ("""+str(location_id)+""") AND sm.location_id not in ("""+str(location_id)+""")  AND sourcel.usage !='inventory' AND destl.usage !='inventory' 
                            THEN (sm.product_qty * pu.factor / pu2.factor)  
                            WHEN (spt.code ='internal' OR spt.code is null) AND sm.location_id in ("""+str(location_id)+""")  AND sm.location_dest_id not in ("""+str(location_id)+""")  AND sourcel.usage !='inventory' AND destl.usage !='inventory' 
                            THEN -(sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0
                            END
                            )) as  qty
                    
                    
                    FROM product_product pp 
                    LEFT JOIN  stock_move sm ON (sm.product_id = pp.id and sm.date >= '"""+str(form_date)+"""' and sm.date <= '""" +str(to_date)+"""'  and sm.state = 'done' and sm.location_id != sm.location_dest_id)
                    LEFT JOIN stock_picking sp ON (sm.picking_id=sp.id)
                     LEFT JOIN stock_picking_type spt ON (spt.id=sp.picking_type_id)
                     LEFT JOIN stock_location sourcel ON (sm.location_id=sourcel.id)
                     LEFT JOIN stock_location destl ON (sm.location_dest_id=destl.id)
                     LEFT JOIN product_uom pu ON (sm.product_uom=pu.id)
                     LEFT JOIN product_uom pu2 ON (sm.product_uom=pu2.id)
                     LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                    where pp.id = """+str(product_id)+""" and sm.picking_id is not null
                  
                 """
        self._cr.execute(query)
        res = self._cr.dictfetchall()
        
        print res[0].get('qty')
       
        resl= res and  res[0].get('qty', 0.0)or 0
        return  float(resl )
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('gakd.valuation_ondate_report_itshore')
        self.begining_qty = 0.0
        self.total_in = 0.0
        self.total_out = 0.0
        self.total_int = 0.0
        self.total_adj = 0.0
        self.total_begin = 0.0
        self.total_end = 0.0
        self.global_subtotal_cost = 0.0
        self.total_inventory = []
        self.value_exist = {}
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
            'time': time,
            'find_warehouses':self.find_warehouses,
            'get_lines':self._get_lines,
            'get_beginning_inventory':self._get_beginning_inventory,
            'produitsParEmplacement':self.produitsParEmplacement,
            'get_warehouses_block':self._get_warehouses_block,
            'get_company':self._get_company,
            'get_product_sales':self.get_product_sales,     
            'get_payment':self.get_payment,   
            'get_transfert':self.get_transfert,
            'get_point_ventes':self.get_point_ventes,
            'get_location_name':self.get_location_name,
            'get_location':self.get_location,
            'get_point_vente_name':self.get_point_vente_name,
            'get_location_by_point_vente':self.get_location_by_point_vente,
            'get_fin':self.get_fin,
            'get_location_block':self.get_location_block
            }
       
        return report_obj.render('gakd.valuation_ondate_report_itshore', docargs)
    
    def get_product_sales(self,product_id,location_id,payment_id,form_date,to_date):
        
        query = """
                    select sum(pl.qty) qty from pos_order pos 
                    join pos_order_line pl ON pos.id = pl.order_id
                    join gakd_carte_consommation gc ON gc.id = pos.transaction_id
                    join pos_session psn ON pos.session_id = psn.id
                    join pos_config pc ON pc.id = psn.config_id
                    where pl.product_id = """+str(product_id)+"""
                    and gc.type_vente = '"""+str(payment_id)+"""'
                    and pc.stock_location_id = """+str(location_id)+"""
                    and pos.create_date between '"""+str(form_date)+"""' and '"""+str(to_date)+"""'
                 """
        self._cr.execute(query)
        res = self._cr.dictfetchall()
        resl= res and  res[0].get('qty', 0.0) or 0 
        return float(resl)
  
    def get_payment(self):
        return [('Vente au comptant','Vente au comptant'),('Vente par JNP PASS','Vente par JNP PASS'),('Vente par TV','Vente par TV'),('Vente par MONO PAY','Vente par MONO PAY'),('Vente par carte tampo','Vente par carte tampo')]
    
    def  get_fin(self,product_id,location_id,form_date,to_date):
        tot=0
        for py in self.get_payment():
            tot+=self.get_product_sales(product_id,location_id,py[0],form_date,to_date)
   
        return tot
    def _get_company(self, company_ids):
        res_company_pool = self.env['res.company']
        if not company_ids:
            company_ids = [x.id for x in res_company_pool.search([])]

        # filter to only have warehouses.
        selected_companies = []
        for company_id in company_ids:
            if self.env['stock.warehouse'].search([('company_id', '=', company_id)]):
                selected_companies.append(company_id)

        return res_company_pool.browse(selected_companies).read(['name', 'currency_id'])
    def find_warehouses(self, company_id):
       
        return [x.id for x in self.env['stock.warehouse'].search([('company_id', '=', company_id)])]
    def _find_locations(self, warehouses):
      
        warehouse_obj = self.env['stock.warehouse']
        location_obj = self.env['stock.location']
        stock_ids = []
        for warehouse in warehouses:
            stock_ids.append(warehouse_obj.sudo().browse(warehouse).view_location_id.id)
        # stock_ids = [x['view_location_id'] and x['view_location_id'][0] for x in warehouse_obj.sudo().read(self.cr, 1, warehouses, ['view_location_id'])]
        return [l.id for l in location_obj.search([('location_id', 'child_of', stock_ids)])]

    def _get_warehouses_block(self, warehouse_ids, company_id):
        warehouse_obj = self.env['stock.warehouse']
        warehouses = 'ALL'
        if warehouse_ids:
            warehouse_rec = warehouse_obj.search([
                                  ('id', 'in', warehouse_ids),
                                  ('company_id', '=', company_id)
                                  ])
            if warehouse_rec:
                warehouses = ",".join([x.name for x in warehouse_rec])
            else:
                warehouses = '-'
        return warehouses

    def _get_lines(self, data, company):
    

        start_date = self.convert_withtimezone(data['form']['start_date'] + ' 08:00:00')
        end_date = self.convert_withtimezone(data['form']['end_date'] + ' 07:59:59')

        warehouse_ids = data['form'] and data['form'].get('warehouse_ids', []) or []
        filter_product_categ_ids = data['form'] and data['form'].get('filter_product_categ_ids') or []
        if not warehouse_ids:
            warehouse_ids = self.find_warehouses(company)

        # find all locations from all warehouse for that company
        location_id = data['form'] and data['form'].get('location_id') or False
        if location_id:
            locations = [location_id]
        else:
            locations = self._find_locations(warehouse_ids)

        
        return True
    def get_location_block(self,l1,l2):
        if  not l1:
            return 'All'
        return   self.get_location_name(l2)
    def convert_withtimezone(self, userdate):
        """ 
            Convert to Time-Zone with compare to UTC
        """
        user_date = datetime.strptime(userdate, DEFAULT_SERVER_DATETIME_FORMAT)
        tz_name = self.env.context.get('tz') or self.env.user.tz
        if tz_name:
            utc = pytz.timezone('UTC')
            context_tz = pytz.timezone(tz_name)
            # not need if you give default datetime into entry ;)
            user_datetime = user_date  # + relativedelta(hours=24.0)
            local_timestamp = context_tz.localize(user_datetime, is_dst=False)
            user_datetime = local_timestamp.astimezone(utc)
            return user_datetime.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        return user_date.strftime(DEFAULT_SERVER_DATETIME_FORMAT)

    def _get_beginning_inventory(self, data, company_id, product_id, current_record=None):
        company_id=1
        # find all warehouses and get data for that product
        warehouse_ids = data['form'] and data['form'].get('warehouse_ids', []) or []
        if not warehouse_ids:
            warehouse_ids = self.find_warehouses(1)
        # find all locations from all warehouse for that company

        location_id = data['form'] and data['form'].get('location_id') or False
        if location_id:
            locations = [location_id]
        else:
            locations = self._find_locations(warehouse_ids)

        from_date = self.convert_withtimezone(data['form']['start_date'] + ' 08:00:00')
        self._cr.execute(''' 
                        SELECT id,coalesce(sum(qty), 0.0) as qty
                        FROM
                            ((
                            SELECT pp.id, pp.default_code,m.date,
                                CASE when pt.uom_id = m.product_uom 
                                THEN u.name 
                                ELSE (select name from product_uom where id = pt.uom_id) 
                                END AS name,

                                CASE when pt.uom_id = m.product_uom  
                                THEN coalesce(sum(-m.product_qty)::decimal, 0.0)
                                ELSE coalesce(sum(-m.product_qty * pu.factor / u.factor )::decimal, 0.0) 
                                END AS qty
                        
                            FROM product_product pp 
                            LEFT JOIN stock_move m ON (m.product_id=pp.id)
                            LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                            LEFT JOIN stock_location l ON (m.location_id=l.id)    
                            LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                            LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                            LEFT JOIN product_uom u ON (m.product_uom=u.id)
                            WHERE m.date <  %s AND (m.location_id in %s) AND (m.location_dest_id not in %s) AND m.state='done' AND pp.active=True AND pp.id = %s
                            GROUP BY  pp.id, pt.uom_id , m.product_uom ,pp.default_code,u.name,m.date
                            ) 
                            UNION ALL
                            (
                            SELECT pp.id, pp.default_code,m.date,
                                CASE when pt.uom_id = m.product_uom 
                                THEN u.name 
                                ELSE (select name from product_uom where id = pt.uom_id) 
                                END AS name,
                        
                                CASE when pt.uom_id = m.product_uom 
                                THEN coalesce(sum(m.product_qty)::decimal, 0.0)
                                ELSE coalesce(sum(m.product_qty * pu.factor / u.factor )::decimal, 0.0) 
                                END  AS qty
                            FROM product_product pp 
                            LEFT JOIN stock_move m ON (m.product_id=pp.id)
                            LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                            LEFT JOIN stock_location l ON (m.location_dest_id=l.id)    
                            LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                            LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                            LEFT JOIN product_uom u ON (m.product_uom=u.id)
                            WHERE m.date <  %s AND (m.location_dest_id in %s) AND (m.location_id not in %s) AND m.state='done' AND pp.active=True AND pp.id = %s
                            GROUP BY  pp.id,pt.uom_id , m.product_uom ,pp.default_code,u.name,m.date
                            ))
                        AS foo
                        GROUP BY id
                    ''', (from_date, tuple(locations), tuple(locations), product_id, from_date, tuple(locations), tuple(locations), product_id))

        res = self._cr.dictfetchall()
        self.begining_qty = res and res[0].get('qty', 0.0) or 0.0
        if current_record:
            current_record.update({'begining_qty': res and res[0].get('qty', 0.0) or 0.0})
        return float(self.begining_qty)
    
    @api.multi
    def produitsParEmplacement(self,emplacement_id):
        print ' produitsParEmplacement '
        print ' emplacement_id'
        print  emplacement_id
        data=[]
        p=[]
        emps = self.env['stock.quant'].search([('location_id','=', int( emplacement_id ) )])
        for emp in emps:
            res =emp.search([('location_id','=', int( emplacement_id )),('product_id','=',emp.product_id.id)])
            if len(res)>1:
                if not emp.product_id.id in p:
                    qty=0
                    for r in res:
                        qty +=r.qty
                    data.append({'produit_name':emp.product_id.name,'produit_id':emp.product_id.id,"produit_qty":float(emp.qty),"prix":int(emp.product_id.lst_price)})
                    p.append(emp.product_id.id)   
            else:
                data.append({'produit_name':emp.product_id.name,'produit_id':emp.product_id.id,"produit_qty":float(emp.qty),"prix":int(emp.product_id.lst_price)})
        
        return data
