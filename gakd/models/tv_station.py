# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo import api, fields, models
from random import randint


class tv_station(models.Model):
    _name = 'gakd.tv_station'
    _rec_name = 'ref'
    
    ref = fields.Char(string='Matricule')
    name = fields.Char(string="Nom")
    state = fields.Selection(string='Etat', selection=[('brouillon','Brouillon'),('valide','Validée')],default='brouillon')
    password = fields.Char(string='Mot de passe')
    client = fields.Many2one('res.partner',string='Client',delegate=True)
    tel = fields.Char(string='Tel')
    ticket_ids = fields.One2many('gakd.tv_station_line','tv_station',string='Tickets')
    carte_ids = fields.One2many('gakd.jnppass_station_line','jnppass_station',string='Cartes')
    momo_ids = fields.One2many('gakd.momo_station_line','momo_station',string='Momo')
    hestory_ids = fields.One2many('gakd.tv_station_hestory','tv_station',string='Ventes')
    account_analytic_id = fields.Many2one('account.analytic.account', string='Compte annalytic')
    moov_ids = fields.One2many('gakd.moov_money','moov_station_line',string='Moov Money')
    
    @api.multi
    def setPassword(self):
        self.ensure_one()
        password = randint(1111, 9999)
        self.password = password
        body = """Merci de noter votre code PIN : """+ str(password)
        Subject = "JNP BENIN"
        mobile = self.telephone
        send(mobile,body)

    @api.model
    def create(self, vals):
        #vals['ref'] = self._default_serie()
        codepin = randint(1111, 9999)
        vals['password'] = codepin
        return super(tv_station, self).create(vals)
class tv_station_line(models.Model):
    _name="gakd.momo_station_line"

    state = fields.Selection([('cancel','Annulé'),('valide','Validé'),('paye','payé')])
    montant = fields.Char(string='Montant')   
    momo_station = fields.Many2one('gakd.tv_station',string='Tv Station')
    payement_id = fields.Many2one('gakd.tv_station_hestory',string='payement')
    
class tv_station_line(models.Model):
    _name="gakd.jnppass_station_line"

    state = fields.Selection([('cancel','Annulé'),('valide','Validé'),('paye','payé')])
    carte = fields.Many2one('gakd.carte',string='Carte',delegate=True)
    montant = fields.Char(string='Montant')
    jnppass_station = fields.Many2one('gakd.tv_station',string='Tv Station')
    payement_id = fields.Many2one('gakd.tv_station_hestory',string='paiement')

class tv_station_line(models.Model):
    _name="gakd.tv_station_line"

    state = fields.Selection([('cancel','Annulé'),('valide','Validé'),('paye','payé')])
    tv = fields.Many2one('gakd.ticket_valeur',string='Ticket',delegate=True)
    tv_station = fields.Many2one('gakd.tv_station',string='Tv Station')
    payement_id = fields.Many2one('gakd.tv_station_hestory',string='paiement')

class StationHestory(models.Model):
    _name = "gakd.tv_station_hestory"

    tv_station = fields.Many2one('gakd.tv_station',string='Tv Station')
    type_payment = fields.Selection([('Espece','Espece'),('cheque','Cheque')],string="Type de paiment")
    montant = fields.Float(string='Montant')
    ticket_ids = fields.One2many('gakd.tv_station_line','payement_id',string='Tickets')
    carte_ids = fields.One2many('gakd.jnppass_station_line','payement_id',string='Cartes')
    momo_ids = fields.One2many('gakd.momo_station_line','payement_id',string='Momo')
    state = fields.Selection([('cancel','Annulé'),('valide','Validé')],default='valide')
    datec=fields.Date(string='Paiement  jusqu\'au')
    
    def annuler(self):
        print 'anuuuuuuuuuuuuuuler'
        for a in self.ticket_ids :
            a.sudo().write({
                    'state':'valide',
                    'payement_id':False,
                })
        for a in self.carte_ids :
            a.sudo().write({ 
                    'state':'valide',
                    'payement_id':False,
                })      
        for a in self.momo_ids :
            a.sudo().write({
                    'state':'valide',
                    'payement_id':False,
                })
        self.sudo().write({
            'state':'cancel' 
        })