# -- coding: utf-8 --
from datetime import date
from odoo import fields, models,api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import dateutil.relativedelta as relativedelta
from random import randint

import time
from pprint import pprint

class Versement(models.Model):
    _name = 'hotel.versement'
    _order="create_date desc"

    def mmontant(self):
        montant=0
        if (self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier')) and not self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') :
            commandes=self.env["pos.order"].search([('create_uid.id','=',self.env.user.id),('verse_validate','=',False)])
            for a in commandes:
                for cmd in a.statement_ids :
                    if cmd.journal_id.id == self.moyen_de_paiement.id and cmd.verse==False  and cmd.verse_validate==False :
                        montant+= cmd.amount 
        elif  self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') and not ( self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier') ) :
            montant=0
            print 'hhhhhhhhhhhhhh'
            comp=self.env["account.payment"].search([('user_val','=',self.env.user.id),('verse','=',False),('verse_validate','=',False),('journal_id','=',self.moyen_de_paiement.id)])
            print  str(comp) +"888888888888888888888888888888888888888888"
            for a in comp:
                montant+=a.amount     
            #,('state','=','paid')
        return montant

    versement_line=fields.One2many(comodel_name='hotel.versement_line', inverse_name='versement_id', string='Lignes de versement')
    
    @api.onchange('moyen_de_paiement')
    def _onchange_moyen_de_paiement(self):
        print 'ooooooooon change'
        print self.env.user.id
        if self.moyen_de_paiement:
            self.montant=self.mmontant()

    montant = fields.Float(string='Montant Total',required=True,default=mmontant)
    state = fields.Selection(string='Etat', selection=[('annule', 'Annulé'),('brouillon', 'Brouillon'), ('valide', 'Validé')],default='brouillon')
    access=fields.Many2one(comodel_name='res.users')
    liste_commande=fields.One2many(comodel_name='pos.order', inverse_name='versement_id', string='Recharges')
    montant_saisie = fields.Float(string='Montant Manquant',compute='_getmmontant',store=True)
    moyen_de_paiement = fields.Many2one(comodel_name='account.journal', string='Moyen de payement')
    vers_par  = fields.Many2one(comodel_name='res.users', string='Par',default=lambda self:self.env.user.id)
    liste_facture = fields.One2many(comodel_name='account.invoice',inverse_name='versement_id',string='')
    liste_payement = fields.One2many(comodel_name='account.payment',inverse_name='versement_id',string='')

    @api.onchange('moyen_de_paiement')
    def _getMoyen(self):
        if (self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier')) and not self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') :   
            res = {}
            ids = []
            #journal_ids
            pos=self.env["pos.config"].search([('company_id','=',self.env.user.company_id.id)])
            print '----------------    1'
            for c in pos:
                for a in c.journal_ids :
                    if a.id not in ids :
                        ids.append(a.id)
            res['domain'] = {'moyen_de_paiement': [('id', 'in', ids)]}
            return res 
        elif  self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') and not ( self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier') ) :
            res = {}
            ids = []
            #journal_ids
            pos=self.env["account.journal"].search([('company_id','=',self.env.user.company_id.id)])
            print '----------------    1'
            for a in pos :
                if a.id not in ids and (a.type=='cash' or a.type=='bank'):
                    ids.append(a.id)
            res['domain'] = {'moyen_de_paiement': [('id', 'in', ids)]}

            return res 


    @api.depends('versement_line','montant')   
    def _getmmontant(self):
        print 'depends _getmmontant '
        somme=0
        for a in self.versement_line:
            somme+=a.montant
        self.montant_saisie= self.montant-somme

    
    @api.constrains('versement_line')
    def versement(self):
        somme =0
        for a in self.versement_line:
            somme +=a.montant
        if somme > self.montant:
            raise ValidationError('Le montant versé ne doit pas etre supérieur au montant calculé')
        if self.montant==0:
            raise ValidationError('Le montant versé  doit  etre supérieur a zéro')
            
    @api.model
    def create(self, vals):
        if (self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier')) and not self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') :
            montant=0
            commandes=self.env["pos.order"].sudo().search([('create_uid','=',self.env.user.id),('verse_validate','=',False)])
            print "cmd creaaaaate"
            print commandes
            for a in commandes:
                for cmd in a.statement_ids :
                    if cmd.journal_id.id == vals['moyen_de_paiement'] and  cmd.verse==False and  cmd.verse_validate==False :
                        print "hhhhhhhhhhhhhhhhhh"
                        montant+= cmd.amount
                        cmd.sudo().write({
                            'verse':True
                        })
            vals['liste_commande']=[(4,c.id) for c in commandes]
            vals['montant']=montant
        elif  self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') and not ( self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier') ) :
            montant=0
            comp=self.env["account.payment"].search([('user_val','=',self.env.user.id),('verse','=',False),('verse_validate','=',False),('journal_id','=',vals['moyen_de_paiement'])])#,('state','=','paid')
            for a in comp:
                montant+=a.amount
                a.sudo().write({
                    'verse':True
                })
            vals['liste_payement']=[(4,c.id) for c in comp]
            vals['montant']=montant
        return super(Versement, self).create(vals)


    @api.one
    def valider_versement(self):
        print 'valider_versement'
        if (self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier')) and not self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') :                                
            print 'valider versement restauuuuuuuuuu'
            for m in self.liste_commande:
                for line in m.statement_ids :
                    if line.journal_id == self.moyen_de_paiement:
                        line.sudo().write({
                            'verse_validate':True,
                            'verse':True
                        })
        elif  self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') and not ( self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier') ) :
            print 'valider versement receeeeeeeeeeeeeeeeeeeeept'
            for m in self.liste_payement:
                m.sudo().write({
                        'verse_validate':True,
                        'verse':True
                    })
        self.sudo().write({
            'state':'valide',
            })

    @api.one
    def annuler(self):
        print 'Anuuuuuuler'
        if (self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier')) and not self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') :                                
            for m in self.liste_commande:
                for line in m.statement_ids :
                    if line.journal_id.id == self.moyen_de_paiement.id:
                        line.sudo().write({
                            'verse':False,
                            'verse_validate':False,
                        })
        elif  self.env.user.has_group('hotel_itshore.group_hotel_receptionniste') and not ( self.env.user.has_group('hotel_itshore.group_hotel_itshore_restaurant') or self.env.user.has_group('hotel_itshore.group_hotel_buandier') ) :
            for m in self.liste_payement:
                m.sudo().write({
                    'verse':False,
                    'verse_validate':False,
                    })
        self.sudo().write({
            'state':'annule',
            'liste_commande':[(5,0,0)],
            'liste_payement':[(5,0,0)]
            })

class VersementLine(models.Model):
    _name = 'hotel.versement_line'

    journal_id = fields.Many2one(comodel_name='account.journal', string='Journal des règlements',required=True)  
    num_versement = fields.Char(string='Numéro de versement',required=True)
    versement_id = fields.Many2one(comodel_name='hotel.versement', string='Versement')  
    montant=fields.Float(string='Montant',required=True)

class Pos_order(models.Model):
    _inherit = 'pos.order'

    @api.one
    @api.depends('versement_id','statement_ids.verse','statement_ids')   
    def _get_cmd(self):
        b=0
        for a in self.statement_ids:
            if a.verse==False:
                b+=1
        if b==0:
            self.verse_validate=True
        else :
            self.verse_validate=False
        print 'compuuuuuuuute boolean'
        print self.verse_validate

    versement_id = fields.Many2one(comodel_name='hotel.versement', string='Versement')
    verse_validate= fields.Boolean(string='',compute='_get_cmd',store=True)


class CmdLine(models.Model):
    _inherit = 'account.bank.statement.line'

    verse = fields.Boolean(string='',default=False)
    verse_validate= fields.Boolean(string='',default=False)

class AccountPayement(models.Model):
    _inherit = 'account.payment'

    user_val = fields.Many2one(comodel_name='res.users', string='')
    
    versement_id = fields.Many2one(comodel_name='hotel.versement', string='Versement')
    verse = fields.Boolean(string='',default=False)
    verse_validate= fields.Boolean(string='',default=False)

    @api.multi
    def post(self):
        super(AccountPayement,self).post()
        a= self.env['account.invoice'].search([('id','in',self.invoice_ids.ids)])
        print 'poooooost'
        a.sudo().write({
            'user_val':self.env.user.id,
        })
        self.sudo().write({
            'user_val':self.env.user.id,
        })

# class Picking(models.Model):
#     _inherit = "stock.picking"

#     @api.model
#     def create(self, vals):
#         super(Picking, self)
#         print ' Create Picking ------ before create hotel '
#         if  not vals.get('name'):
#                vals['name']=self.sudo().env['ir.sequence'].next_by_code('stock.picking')
#         p =super(Picking, self).create(vals)
#         return p 


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    user_val = fields.Many2one(comodel_name='res.users', string='')
    versement_id = fields.Many2one(comodel_name='hotel.versement', string='Versement')
    verse = fields.Boolean(string='',default=False)
    verse_validate= fields.Boolean(string='',default=False)