# -- coding: utf-8 --
from odoo import api, fields, models,_
from odoo.exceptions import ValidationError
from datetime import date, timedelta
import datetime
from dateutil.parser import parse
# import ValidationError

class ListeVersement(models.Model):
    _name = 'gakd.liste'
    _description='liste versement'

    #----- header------
    compte_bancaire=fields.Many2one(comodel_name='account.journal',string='compte bancaire', domain=[('type','=','bank')] )
    # compte_bancaire=fields.Char(string='compte bancaire')
    
    yesterday = date.today() - timedelta(days=1)
    hier=yesterday.strftime('%Y-%m-%d')
    date_du=fields.Date(string='Date du')
    date_au=fields.Date(string='Date au')
    
    #daters=fields.Date(string='Date opperation')
    point_vente = fields.Many2one("gakd.point.vente",string="Point de vente")
    categorie_produit=fields.Selection(
        [('-','null'),('Lubrifiants','Lubrifiants'),('Produits blancs','Produits blancs'),('Gaz et accessoire','Gaz et accessoire'),('Lavage','Lavage')],
        string="Catégorie des produits", 
        default='Lubrifiants'
        )

    state_validation = fields.Selection(
        string='Etat',
        selection=[('annule', 'Annulé'),('valide', 'Validé'),('M.Ref-Mt.Diff', 'Même référence mais momtant différent'),('M.Date-M.PV Mt.Diff', 'Même date Même PV mais montant différent'),('n existe pas','n\'existe pas')],
        default='valide'
        )
   
    #----menu ------
    table=fields.One2many('gakd.versementreleve','liste_versement',string='')

    #ON CHANGE    
    @api.onchange('compte_bancaire')
    def _onchange_field1(self):
        if self.compte_bancaire:            
            if len(self.compte_bancaire)>0:
                nomcompte=self.compte_bancaire[0].name
            else:
                nomcompte=self.compte_bancaire.name

            codepv=None
            pointevente=None
            
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle

            #etat             
            etat=str(self.state_validation)

            if self.point_vente and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()
                
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','ilike',codepv),
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif not self.point_vente and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()

                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif self.point_vente and not self.date_du and not self.date_au:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('point_vente','ilike',codepv),
                    ])
            elif not self.point_vente and not self.date_du and not self.date_au:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ])
            self.table=listeCB
    
    @api.onchange('point_vente')
    def _onchange_field2(self):
        if self.point_vente:
            if len(self.compte_bancaire)>0:
                nomcompte=self.compte_bancaire[0].name
            else:
                nomcompte=self.compte_bancaire.name
            
            codepv=None
            pointevente=None
            
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle

            etat=str(self.state_validation)
            
            if self.compte_bancaire and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()
                
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','ilike',codepv),
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif not self.compte_bancaire and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()

                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','ilike',codepv),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif self.compte_bancaire and not self.date_du and not self.date_au:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('point_vente','ilike',codepv),
                    ])
            elif not self.compte_bancaire and not self.date_du and not self.date_au:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','ilike',codepv),
                    ])
            self.table=listeCB
    
    @api.onchange('categorie_produit')
    def _onchange_field3(self):
        if len(self.compte_bancaire)>0:
            nomcompte=self.compte_bancaire[0].name
        else:
            nomcompte=self.compte_bancaire.name

        codepv=None
        pointevente=None
        
        if len(self.point_vente)>0:
            codepv=self.point_vente[0].libelle
        else:
            codepv=self.point_vente.libelle

        etat=str(self.state_validation)
        #1
        if self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([                
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),
                ('point_vente','ilike',codepv),
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                ])
        #2
        elif not self.compte_bancaire and not self.point_vente and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit), 
                ])
        #3
        elif not self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
             #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                ])
        #4
        elif  not self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
             #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                ])
        #5
        elif self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('point_vente','ilike',codepv),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                ])
        #6
        elif self.point_vente and not self.compte_bancaire and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('point_vente','ilike',codepv),
                ])
        #7
        elif self.point_vente and self.compte_bancaire and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('point_vente','ilike',codepv),
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ])
        #1
        elif not self.point_vente and self.compte_bancaire and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ])
        self.table=listeCB

    @api.onchange('state_validation')
    def _onchange_field4(self):
        if len(self.compte_bancaire)>0:
            nomcompte=self.compte_bancaire[0].name
        else:
            nomcompte=self.compte_bancaire.name

        codepv=None
        pointevente=None
        
        if len(self.point_vente)>0:
            codepv=self.point_vente[0].libelle
        else:
            codepv=self.point_vente.libelle
        
        etat=str(self.state_validation)

        #1
        if self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([                
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),
                ('point_vente','ilike',codepv),
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                ])
        #2
        elif not self.compte_bancaire and not self.point_vente and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit), 
                ])
        #3
        elif not self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
             #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                ])
        #4
        elif  not self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
             #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                ])
        #5
        elif self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('point_vente','ilike',codepv),
                ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                ])
        #6
        elif self.point_vente and not self.compte_bancaire and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('point_vente','ilike',codepv),
                ])
        #7
        elif self.point_vente and self.compte_bancaire and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),                
                ('point_vente','ilike',codepv),
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ])
        #1
        elif not self.point_vente and self.compte_bancaire and not self.date_du and not self.date_au:
            listeCB=self.env['gakd.versementreleve'].search([
                ('statu_validaion','=',etat),
                ('categorie_produit','=',self.categorie_produit),
                ('compte_bancaire','ilike',nomcompte[0:5]),
                ])
        self.table=listeCB
    
    @api.onchange('date_du','date_au')
    def _onchange_field5(self):
        if self.date_du and self.date_au:
            if len(self.compte_bancaire)>0:
                nomcompte=self.compte_bancaire[0].name
            else:
                nomcompte=self.compte_bancaire.name

            codepv=None
            pointevente=None
            
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle          
            
            
            #date du "format yyyy-mm-dd " datFormatDU=datduParse.strftime('%Y-%m-%d')
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            etat=str(self.state_validation)
            if self.point_vente and self.compte_bancaire:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('statu_validaion','=',etat),
                    ('point_vente','ilike',codepv),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif self.point_vente and not self.compte_bancaire:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','ilike',codepv),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif not self.point_vente and self.compte_bancaire:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('compte_bancaire','ilike',nomcompte[0:5]),
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            elif not self.point_vente and not self.compte_bancaire:
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU)
                    ])
            self.table=listeCB