# -*- coding: utf-8 -*-

from odoo import fields, models
import datetime


class AccountBalanceReport(models.TransientModel):
    _inherit = "account.common.account.report"
    _name = 'account.gakd.bilan.actif.report'
    def get_years(self):
        year_list = []
        ye = range(2017, datetime.datetime.now().year + 1)
        for i in ye:
            year_list.append((i, str(i)))
        print year_list
        return year_list


    journal_ids = fields.Many2many('account.journal', 'account_balance_report_journal_rel', 'account_id', 'journal_id', string='Journals', required=True, default=[])
    yearr = fields.Selection(string='Annee', selection=get_years)
    bilan = fields.Selection(string='Bilan', selection=[('py', 'Payszage'),('passif', 'Bilan Passif'), ('actif', 'Bilab Actif')])
    
    def _print_report(self, data):
        data = self.pre_print_report(data)
        data['form'].update({
            'year':self.yearr,
            'bilan':self.bilan
        })
        records = self.env[data['model']].browse(data.get('ids', []))
        print "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
        return self.env['report'].get_action(records, 'gakd.gakd_bilan_actif', data=data)