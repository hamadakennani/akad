{
    'name': 'facture',
    'version': '1.0',
    'summary': 'Module pour la gestion des facturations',
    'description': """ Gestion des facturations """,
    'depends': 
    [
        'base','sale',
    ],
    'sequence': 1,
    'demo': [
    ],
    'data': [ 
    'security/ir.model.access.csv',
    'views/facture_view.xml',
    'views/company_view.xml',
    'views/chargement_view.xml',
    'views/picking.xml',
    'views/banque_view.xml',
    'views/configuration_view.xml',
    'report/facture_report.xml',
    'report/lubr_facture.xml',
    'report/sengen_facture.xml',
    'report/autreproduit_report.xml',
    'report/autreproduitislamic.xml',
    'report/lubrifislamic.xml',
    'wizard/company_view.xml',
    'wizard/lubr_view.xml',
    'wizard/autreproduit.xml',
    'wizard/autreproduitislamic.xml',
    'wizard/Fac_Gas_Sen_Gen_view.xml',
    ],
   'qweb': [

    ],
 

    'installable': True,
    'application': True,
    'auto_install': False,
 
}