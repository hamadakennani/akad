# -*- coding: utf-8 -*-
from odoo import api, fields, models


class MoovMoney(models.Model): 
    
    _name = 'gakd.moov_money'
    _rec_name ='moov_station'
    moov_station = fields.Many2one('gakd.tv_station',string='Tv Station')
    montant = fields.Char(string='Montant')   
    state = fields.Selection([('cancel','Annule'),('Valide','Valide'),('Paye','Paye')] , string="Statut")
    moov_station_line = fields.Many2one("gakd.tv_station",string="Station ligne")
    payement_id = fields.Many2one('gakd.tv_station_hestory',string='paiement')



