# -*- coding: utf-8 -*-
from odoo import api, fields, models

class transaction(models.Model):
    _name = 'gakd.transaction'
    
    nbtransaction=fields.Integer("Nomber de transaction")
    montant=fields.Float("Montant")
    quantite=fields.Integer("Quantite")
    type_vente=fields.Many2one("gakd.type_vente")