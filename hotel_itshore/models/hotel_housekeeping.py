# -- coding: utf-8 --
from odoo import api, fields, models

from datetime import datetime, timedelta
class hotel_housekeeping(models.Model):
    _inherit = 'hotel.housekeeping'

    housekeeping_type = fields.Selection(string='Housekeeping Type', 
    selection=[('clean','Cleaning'), ('maintenance', 'Maintenance'),],required=True)
    end_date = fields.Date(string='Date de fin')
    current_date = fields.Date(string='Date de début')
    #inspector  clean_type-daily  rom_no  end_date inspect_date_time 
    inspector = fields.Many2one(comodel_name='res.users', string='Contrôleur',required=False)
    inspect_date_time = fields.Datetime(string='Date et heure de contrôle',required=False)
    # state = fields.Selection(selection_add=[('Maintenance', 'Maintenance')])
    state = fields.Selection([('inspect', 'Inspect'), ('dirty', 'Dirty'),('maintenance','Maintenance'),
                              ('clean', 'Clean'),
                              ('done', 'Done'),
                              ('cancel', 'Cancelled')], 'State',
                             states={'done': [('readonly', True)]},
                             index=True, required=True, readonly=True,
                             default=lambda *a: 'inspect')
    @api.onchange('state','room_no')
    def _onchange_state(self):

        if str(self.current_date)==str(datetime.now().date()):
           
            self.room_no.get_title()
        self.house_keeping_satat()
    @api.depends('state','room_no')
    def _depends_state(self):
        if str(self.current_date)==str(datetime.now().date()):
          self.room_no.get_title()
        
    @api.multi
    def action_set_to_dirty(self):
        self.state = 'dirty'
        for line in self:
            line.quality = False
            for activity_line in line.activity_lines:
                activity_line.write({'clean': False})
                activity_line.write({'dirty': True})
        self.room_no.get_title()
        return True
    @api.multi
    def room_clean(self):
        self.state = 'clean'
        for line in self:
            line.quality = False
            for activity_line in line.activity_lines:
                activity_line.write({'clean': True})
                activity_line.write({'dirty': False})
        self.room_no.get_title()        
        return True
    @api.multi
    def house_keeping_satat(self):
        
        for House_k in self.env['hotel.housekeeping'].search([('housekeeping_type','=','maintenance')]):
              
                if str(House_k.current_date)==str(datetime.now().date()):
                    House_k.state='maintenance'
                  
                    for activity_line in House_k.activity_lines:
                        activity_line.write({'clean': False})
                        activity_line.write({'dirty': True})
                # elif datetime.now().date()>= datetime.strptime(House_k.end_date, '%Y-%m-%d ') : 
                #     House_k.state='maintenance'
                #     for activity_line in House_k.activity_lines:
                #         activity_line.write({'clean': False})
                #         activity_line.write({'dirty': True})
        # for House_k in self.env['hotel.housekeeping'].search([('housekeeping_type','=','clean')]):
        #         if str(House_k.current_date)==str(datetime.now().date()):
        #             House_k.state='maintenance'
        #             for activity_line in House_k.activity_lines:
        #                 activity_line.write({'clean': False})
        #                 activity_line.write({'dirty': True})
               

        
        
   
    
    
    
    
    
