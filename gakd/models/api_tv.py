# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from collections import OrderedDict
import collections
#from unidecode import unidecode
from random import randint
from keyid import keyid
import time
#import swagger_client
#from swagger_client.rest import ApiException
from pprint import pprint
from unidecode import unidecode
from datetime import datetime

class api_tv(models.Model):

    @api.multi
    def existTicket(self,qrcode):
		tck=self.env['gakd.ticket_valeur'].search(['|',('qrcode','=',qrcode.get("qrcode")),('num_serie','=',qrcode.get("qrcode"))])
		
		if(len(tck) == 0):
			return 0
		else:
			return 1

    @api.multi
    def checkCodePinByQrcode(self,param):
	carte = self.env['gakd.carte'].search([['qrcode', '=', param.get("qrcode")]])
	if carte.code_pin == int(param.get("code_pin")):
		result =  1
	else: result = 0
	print result
	return result

    @api.multi
    def getInfoParTicket(self,qrcode):
		data=[]
		if qrcode.get("type") == "TV":
			tck=self.env['gakd.ticket_valeur'].search(['|',('qrcode','=',qrcode.get("qrcode")),('num_serie','=',qrcode.get("qrcode"))])
			if len(tck) > 0:
				data.append([('id',tck[0].id),('client',tck[0].client.name),('etat',tck[0].etat),("num_serie",tck[0].num_serie),('montant',tck[0].tv_type.montant),('qrcode',tck[0].qrcode)])
		if qrcode.get("type") == "JNP":
			tck=self.env['gakd.carte'].search([('num_serie','=',qrcode.get("qrcode"))])
			if len(tck) > 0:
				data.append([('id',tck[0].id),('client',tck[0].owner_id.name),('etat',''),("num_serie",tck[0].num_serie),('montant',''),('qrcode','')])
		return data

    @api.multi
    def valideTicket(self,qrcode):
		tck=self.env['gakd.ticket_valeur'].search([('id','=',qrcode.get("id"))])
		if len(tck) > 0:
			if tck[0].id:
				tck[0].etat='util'

		return 1

    @api.multi
    def checkCartByQrcode(self,qrcode):

	carte = self.env['gakd.carte'].search([['qrcode', '=', qrcode.get("qrcode")]])
	if not carte.id:
		result = 0
	else: result = 1
	
	return result

    @api.multi
    def getCartByQrcode(self,qrcode):
	data = []
	carte = self.env['gakd.carte'].search([['qrcode', '=', qrcode.get("qrcode")]])

	data = [("id",carte.id),("client",unidecode(carte.owner_id.name)),("serie",carte.num_serie),("qrcode",carte.qrcode),("solde",carte.solde),("state",carte.state)]
	#data = {"id":carte.id,"serie":carte.num_serie,"qrcode":carte.qrcode,"solde":carte.solde}
	print data
        return data



    @api.multi
    def insertTicket(self,param):
		st = self.env['gakd.tv_station'].sudo().search([('id','=',param.get("station_id"))])
		cl = self.env['res.partner'].sudo().search([('id','=',st.client.id)])
		montant = 0
		if param.get("type") == "MOMO":
			st.sudo().write({
				'momo_ids':[(0,0,{
					'montant':float(param.get('montant')),
					'state':'valide'
				})]
			})
			cl.sudo().write({
				'montant_tv': cl.montant_tv + float(param.get('montant'))
			})
			montant = float(param.get('montant'))
		if param.get("type") == "CARTE":
			solde = 0
			st = self.env['gakd.tv_station'].sudo().search([('id','=',param.get("station_id"))])
			cl = self.env['res.partner'].sudo().search([('id','=',st.client.id)])
			carte = self.env['gakd.carte'].sudo().search([('id','=',param.get("carte_id"))])
			if carte.id:
				solde_carte = float(float(carte.solde) - float(param.get('montant')))
				carte.solde = solde_carte
				client = self.env['res.partner'].search([['id', '=', carte.owner_id.id]])
				for carte in client.carte_ids:
					solde += carte.solde
					client.solde_carte = solde
				st.sudo().write({
					'carte_ids':[(0,0,{
						'carte':carte.id,
						'montant':float(param.get('montant')),
						'state':'valide'
					})]
				})
				cl.sudo().write({
					'montant_tv': cl.montant_tv + float(param.get('montant'))
				})
				montant = float(param.get('montant'))
				body = """Merci pour votre passage chez JNP.
				Details transaction :
				Point de vente : """+str(st.name)+"""  
				Montant : """+str(montant)+""" FCFA
				Solde carte : """+str(carte.solde)+""" FCFA"""
				#Montant : ZZZZZZZZ  , 		Date et heure : XXXXXXXXXXXXXXXX
				#Nouveau solde de la carte : """+str(carte.solde)
				mailsto = client.email
				Subject = "JNP BENIN"
				mobile = client.mobile
				try:
					send(mobile,body)
				except:
					print("Something else went wrong")

		if param.get("type") == "TV":
			st = self.env['gakd.tv_station'].sudo().search([('id','=',param.get("station_id"))])
			cl = self.env['res.partner'].sudo().search([('id','=',st.client.id)])
			ticket = self.env['gakd.ticket_valeur'].sudo().search([('id','=',param.get("ticket_id"))])
			
			if ticket.id:
				ticket[0].etat='util'
			st.sudo().write({
				'ticket_ids':[(0,0,{
					'tv':ticket.id,
					'state':'valide'
				})]
			})
			cl.sudo().write({
				'montant_tv': cl.montant_tv + ticket.tv_type.montant
			})
			montant = float(ticket.tv_type.montant)
			
		if param.get("type") == "MOOV":
			st.sudo().write({
				'moov_ids':[(0,0,{
					'montant':float(param.get('montant')),
					'state':'Valide'
				})]
			})
			montant = float(param.get('montant'))
		data = [(0,0 ,{'account_id':20605 ,'partner_id':'','name':'Vente à partir du promoteur','debit': montant,'analytic_account_id':st.account_analytic_id.id}),
			(0,0 ,{'account_id':31554,'partner_id':'','name':'Vente à partir du promoteur','credit':montant,'analytic_account_id':st.account_analytic_id.id})]
		av= self.env['account.move'].create({
		'journal_id':141,
		'date':datetime.now().date(),
		'ref': 'sssss-'+str(datetime.now()),
		'line_ids':data
		})  
		av.post()
		return 1

    @api.multi
    def getListeTicketByStation(self,param):
	
	 
	data = []
	total = 0
	station = self.env['gakd.tv_station'].search([['id', '=', param.get("station_id")]],order='create_date desc')
	print station
	print 'stationstationstationstationstationstationstation'
	if param.get("type") == 'TV':
		for ticket in list(reversed(station.ticket_ids)):
			if ticket.state != 'paye':
				data.append([("id",ticket.tv.id),("numero_serie",ticket.tv.num_serie),("montant",ticket.tv.tv_type.montant),('date',ticket.create_date),('state',ticket.state)])
			
	if param.get("type") == 'JNP':
		for ticket in list(reversed(station.carte_ids)):
			if ticket.state != 'paye':
				data.append([("id",ticket.id),("numero_serie",ticket.carte.num_serie),("montant",ticket.montant),('date',ticket.create_date),('state',ticket.state)])
			
	if param.get("type") == 'MOMO':
		for ticket in list(reversed(station.momo_ids)):
			if ticket.state != 'paye':
				data.append([("id",ticket.id),("numero_serie",''),("montant",ticket.montant),('date',ticket.create_date),('state',ticket.state)])
	if param.get("type") == 'MOOV':
		print 'moooooooooooooooooooov'
		for ticket in list(reversed(station.moov_ids)):
			if ticket.state == u'Valide':
				data.append([("id",ticket.id),("numero_serie",''),("montant",ticket.montant),('date',ticket.create_date),('state',ticket.state)])
				print 'iiiiiiiiiiiiiiin'
	print data
	return data

    @api.multi
    def getMontant(self,param):
	data = []
	total = 0
	station = self.env['gakd.tv_station'].search([['id', '=', param.get("station_id")]])
	if param.get("type") == 'TV':
		for ticket in station.ticket_ids:
			if ticket.state == 'valide':
				total += float(ticket.tv.tv_type.montant)
	if param.get("type") == 'JNP':
		for ticket in station.carte_ids:
			if ticket.state == 'valide':
				total += float(ticket.montant)
	if param.get("type") == 'MOMO':
		for ticket in station.momo_ids:
			if ticket.state == 'valide':
				total += float(ticket.montant)
	return total


    @api.multi
    def checkAgentByMatricule(self,qrcode):
	agent = self.env['gakd.tv_station'].search([['ref', '=', qrcode.get("matricule")],['password', '=', qrcode.get("password")]])
	if not agent.id:
		result = 0
	else: result = 1
	
	return result

    @api.multi
    def getAgentByMatricule(self,qrcode):
	data = []
	station = self.env['gakd.tv_station'].search([['ref', '=', qrcode.get("matricule")]])

	data = [("id",station.id),("matricule",unidecode(station.ref)),("name",unidecode(station.ref))]
	#data = {"id":carte.id,"serie":carte.num_serie,"qrcode":carte.qrcode,"solde":carte.solde}
	return data


    @api.multi
    def annuleTicket(self,param):
	print '888888888888888888888888888888'
	print param.get('type') 
	if param.get('type') == 'TV':
		print 'uuuuuuuuuuuuuuuuuuuuuuu'
		ticket_id = self.env['gakd.ticket_valeur'].sudo().search([('num_serie','=',param.get('num_serie'))]).id
		st_line = self.env['gakd.tv_station_line'].sudo().search([('tv_station','=',int(param.get('station_id'))),('tv','=',ticket_id),('state','=','valide')])
		ticket = self.env['gakd.ticket_valeur'].sudo().search([('id','=',param.get("ticket_id"))])
		
		if len(st_line) > 0 :
			ticket = self.env['gakd.ticket_valeur'].sudo().search([('id','=',ticket_id)])
			print ticket[0]
			ticket[0].etat='nonutil'
			st_line.tv_station.client.sudo().write({
				'montant_tv': st_line.tv_station.client.montant_tv - ticket.tv_type.montant
			})
			st_line.sudo().write({
				'state':'cancel'
			})
	if param.get('type') == 'MOMO':
		print "00000000000000000"
		print param.get('num_serie')
		st_line = self.env['gakd.momo_station_line'].sudo().search([('id','=',param.get('num_serie'))])
		#st_line = self.env['gakd.tv_station_line'].sudo().search([('tv_station','=',int(param.get('station_id'))),('tv','=',ticket_id),('state','=','valide')])
		print st_line.montant
		print st_line.momo_station.client.montant_tv
		if len(st_line) > 0 :
			st_line.momo_station.client.sudo().write({
				'montant_tv': float(st_line.momo_station.client.montant_tv) - float(st_line.montant)
			})
			st_line.sudo().write({
				'state':'cancel'
			}) 
	if param.get('type') == 'JNP':
		print "00000000000000000"
		print param.get('num_serie')
		solde_carte = 0
		solde = 0
		st_line = self.env['gakd.jnppass_station_line'].sudo().search([('id','=',param.get('num_serie'))])
		carte = st_line.carte
		#st_line = self.env['gakd.tv_station_line'].sudo().search([('tv_station','=',int(param.get('station_id'))),('tv','=',ticket_id),('state','=','valide')])
		print st_line
		print st_line.carte.solde
		solde_carte = float(float(carte.solde) + float(st_line.montant))
		carte.solde = solde_carte
		client = self.env['res.partner'].search([['id', '=', carte.owner_id.id]])
		for carte in client.carte_ids:
			solde += carte.solde
			client.solde_carte = solde
		if len(st_line) > 0 :
			st_line.jnppass_station.client.sudo().write({
				'montant_tv': float(st_line.jnppass_station.client.montant_tv) - float(st_line.montant)
			})
			st_line.sudo().write({
				'state':'cancel'
			}) 
	if param.get('type') == 'MOOV': 
		# print param.get('num_serie')
		st_line = self.env['gakd.moov_money'].sudo().search([('id','=',param.get('num_serie'))])
		if len(st_line) > 0 : 
			st_line.sudo().write({
				'state':'cancel'
			}) 
	return 1

    @api.multi
    def getListeMontant(self,param):
        print 'fuuuuuuuuuuuuuuuuunction'
        montant=0
        #id =self._context.get('active_ids',False)[0]
        tv_station=self.env["gakd.tv_station"].search([('id','=',param.get("station_id"))])
        print tv_station.moov_ids
        data=[]
        montant_tv=0
        montant_momo=0
        montant_carte=0
        montant_moov=0
        dt=datetime.strptime(param.get("date"), '%Y-%m-%d')
        tm2=datetime.strptime('08:00:00', '%H:%M:%S').time()
        datee=str(datetime.combine(dt, tm2))

        for a in tv_station.ticket_ids:            
            if a.state=='valide' and a.create_date< datee:
                montant_tv+=a.tv.tv_type.montant
        for a in tv_station.carte_ids:            
            if a.state=='valide'   and a.create_date< datee:
                montant_carte+=float(a.montant)
        for a in tv_station.momo_ids:  
            if a.state=='valide'   and a.create_date< datee:
                montant_momo+=float(a.montant)
        for a in tv_station.moov_ids:  
            if a.state=='Valide'   and a.create_date< datee:
                print 'oooooooooooooooooooooooooooon'
                montant_moov+=float(a.montant)
                print montant_moov
        montant=montant_tv+montant_momo+montant_carte+montant_moov
        data.append([('montant',montant_tv),('type','TV')])
        data.append([('montant',montant_momo),('type','MOMO')])
        data.append([('montant',montant_carte),('type','JNP PASS')])
        data.append([('montant',montant_moov),('type','MOOV')])
        data.append([('montant',montant),('type','Total')])
        # print data
        
        return data


