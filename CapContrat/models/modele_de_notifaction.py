# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint

import time
#import swagger_client
#from swagger_client.rest import ApiException
from pprint import pprint


class Departement(models.Model):
	_name = 'capcontrat.notifaction'
	_rec_name = 'libelle'
	libelle=fields.Char('Libellé', size=128, required=True)
	
	def _get_e(self):
		self.ensure_one()
		if self.conditions_id:
			self.echeance="" 
		elif self.clauses_id:
			self.echeance=" "+self.clauses_id.type_operastion

		else :
			self.echeance=""
	echeance_id=fields.Many2one('capcontrat.contrat.echeance',string=" Medele de notification")
	Subject =fields.Char(string="Sujet")
	body=fields.Html(string="Contenu")