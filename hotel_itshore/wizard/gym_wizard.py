# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import time
from pprint import pprint
from datetime import datetime, timedelta
from dateutil.relativedelta import *

class Gym_wizard(models.TransientModel):

    _name="hotel.wizard.gym"
    _rec_name='id'

    def _getGymAbon_id(self):
        print '_getGymAbon_id_getGymAbon_id_getGymAbon_id_getGymAbon_id'
        return self._context.get('active_ids',False)[0]

    def test(self):
        print 'jujhjh'
        
        return 0
    
    @api.one
    @api.depends('duree','discount','type_discount','discount_fix')
    def _get_montant(self):
        print '_get_montant _get_montant'
        abon= self.env['hotel.fitness.abonnement'].search([('id','=',self._context.get('active_ids',False)[0])])
        coe=0
        if self.duree=='mois':
            coe=1
        if self.duree=='trimestre':
            coe=3
        if self.duree=='semestre':
            coe=6
        if self.duree=='annee':
            coe=12
        price=abon.product_id.lst_price 
        tax=abon.product_id.taxes_id
        cout=price*coe
        tva=0
        for ta in tax:
            if tax.amount_type=='fixed':
                tva=ta.amount
                if ta.price_include:
                    cout-=ta.amount               
            elif ta.amount_type=='percent':
                print ' in percent'
                tva=(cout*ta.amount)*0.01
                print ' oprt done'
                print cout
                if ta.price_include:
                    cout-=tva

        self.prix_ht=cout 
        self.taxes =tva
        self.montant=tva+cout
        
        if self.type_discount=='fixe':
            self.montant=self.montant-self.discount_fix
        if self.type_discount=='pourcentage':
            self.montant=self.montant-self.montant*self.discount*0.01

        return self.montant


    @api.one
    @api.depends('duree')
    def _get_days(self):
        print'_get_days _get_days'
        # last_date =False
        date_from  =datetime.now().date()
        date_to=datetime.now()
        abon= self.env['hotel.fitness.abonnement'].search([('id','=',self._context.get('active_ids',False)[0])])
        
        # self.env['hotel.fitness.abonnement.line'].search([('id','=',12)]).unlink()
        # print 'done'
        for a in abon.abonnement_lines:
            print 'hhhhhhhh'
            print a.date_fin
            print date_to
            print '-----------'
            if  datetime.strptime(str(date_from), '%Y-%m-%d')  <  datetime.strptime(str(a.date_fin), '%Y-%m-%d'):
                date_from=a.date_fin                 
        print '555555555'
        if datetime.strptime(str(date_from), '%Y-%m-%d') < datetime.strptime(str(datetime.now().date()), '%Y-%m-%d'):
            date_from = datetime.now().date()
        # else :
        #     date_from = last_date
            
        if self.duree=='mois':
            date_to=   datetime.strptime(str(date_from), '%Y-%m-%d')  + relativedelta(months=+1) 
        if self.duree=='trimestre':
            date_to=  datetime.strptime(str(date_from), '%Y-%m-%d') + relativedelta(months=+3) 
        if self.duree=='semestre':
            date_to= datetime.strptime(str(date_from), '%Y-%m-%d') + relativedelta(months=+6) 
        if self.duree=='annee':
            date_to= datetime.strptime(str(date_from), '%Y-%m-%d') + relativedelta(years=+1) 
        
        
        self.date_debut=date_from
        self.date_fin=date_to
        
        print datetime.strptime(str(date_from), '%Y-%m-%d')
        final=date_to- datetime.strptime(str(date_from), '%Y-%m-%d')
        print 'final '
        self.nb_jours=final.days

    # montant = fields.Float(string='Montant',compute='_get_montant')

    type_discount = fields.Selection(string='Type de remise', selection=[('pourcentage', 'Pourcentage'), ('fixe', 'Fixe'),])
    discount  = fields.Float(string='Remise (%)')
    discount_fix  = fields.Float(string='Remise Fixe')


    duree = fields.Selection(string='Durée', selection=[('mois', 'Mois'), ('trimestre', 'Trimestre'),('semestre', 'Semestre'),('annee', 'Année')],required=True)
    nb_jours = fields.Integer(string='Nombres des jours',compute='_get_days')    
    date_debut = fields.Datetime(string='Date de début')
    date_fin = fields.Datetime(string='Date de fin')
    gym_abonnement_id = fields.Many2one("hotel.fitness.abonnement",string="Gym Abonnement",default=_getGymAbon_id)
    

    montant = fields.Float(string='Montant TTC',compute='_get_montant')
    prix_ht = fields.Float(string='Prix HT',compute='_get_montant')
    taxes = fields.Float(string='Taxes',compute='_get_montant')

    @api.model
    def create(self, values):
        print 'creaaate'
        print values
        a= super(Gym_wizard, self).create(values)
        p= self.env['hotel.fitness.abonnement'].search([('id','=',self._context.get('active_ids',False)[0])])
        p.write({
            'abonnement_lines':[(0,0,{
            'duree':a.duree,
            'type_discount':a.type_discount,
            'discount_fix':a.discount_fix,
            'discount':a.discount,
            'date_debut':a.date_debut,
            'date_fin':a.date_fin,
            })]
            })
        return a
    