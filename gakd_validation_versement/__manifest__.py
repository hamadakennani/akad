# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'gakd Validation Versements',
    'version': '1.1',
    'summary': 'Groupe AKAD',
    'category': 'Groupe AKAD',
    'author': 'IT SHORE',
    'sequence': 10,
    'description':"",
    'depends': ['base','account','gakd'],
    'installable': True,
    'application': True,
    'auto_install': False,

    'data': [
        'views/veresementLine_views.xml',
        'views/import_view.xml',
        'views/releve_bancaire_views.xml',        
        'views/rapport.xml',
        'security/ir.model.access.csv',
        # 'views/versementLine.xml',
        
        # 'views/rslt.xml',
        # 'views/versement_releveBancaire_view.xml',
        # 'views/versement_view.xml',
        
    ],


    }
