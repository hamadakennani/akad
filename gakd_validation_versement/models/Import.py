# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError
import datetime
import pandas as pd
import xlrd 
import base64
import io
from unidecode import unidecode
import json
import chardet    


class ImportLine(models.Model):
    _name = 'gakd.import_line'

    file = fields.Binary(string='Fichier',required=True)
    file_name = fields.Char("File Name")
    bank = fields.Selection([('BAB','BAB'),('BOA','BOA'),('ECOBANK','ECOBANK'),
            ('ATLANTIQUE','ATLANTIQUE'),('BGFI','BGFI'),('CASHCOLLECTION','CASH COLLECTION'),
            ('ECO','ECO'),('ORA','ORA'),('SGB','SGB')],string="banque",required=True)
    import_id = fields.Many2one('gakd.import',string = "Import ID")

    def ImportData(self) :
        toread = io.BytesIO()
        toread.write(base64.b64decode(self.file))  # `decrypted` string as the argument 
        toread.seek(0)  # reset the pointer
        data = None
        # result = chardet.detect(toread)
        # print 'encoding =============== ' + result['encoding'] 
        if self.file_name.endswith('csv') or self.file_name.endswith('CSV') or self.file_name.endswith('txt'):
            sep = ","
            if self.bank in ['BGFI','ORA','SGB']:
                sep = ";"
            data = pd.read_csv(toread,sep=sep,encoding='latin1')
        else :
            data =  pd.read_excel(toread,0)
        # jsonData = json.loads(data.to_json(orient='records',lines=False))
        # print data 
        jsonData = json.loads(data.to_json(orient='records'))
        # for row in jsonData:
        #     print row
        # return
        if self.bank == 'ORA' :
            data = {}
            isFirstTime = True
            for row in jsonData :
                # print(row.get(u'Date',"")+" = "+row.get(u'Libell\u9824e l\'op\u9ca1tion',"")," = "+row.get(u'Cr\u9929t(XOF)',""))
                currentData = {
                    'date_releve':row.get(u'Date',""),
                    'libelle':row.get(u'Libellé de l\'opération',""),
                    'montant':row.get(u'Crédit(XOF)',""),
                    'bank':'ORA'
                }
                if isFirstTime :
                    data = currentData
                    isFirstTime = False
                elif currentData.get('date_releve','') == ' ' :
                    data['libelle'] = data.get('libelle') + '\n' + currentData.get('libelle')
                else :
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','like',data.get(u'libelle',"")),
                    ('date_releve','=',data.get(u'date_releve',"")),
                    ('montant','=',data.get(u'montant',"") if data.get(u'montant',"") != None else False),
                    ('bank','=',self.bank),
                    ])
                    if len(existData) <= 0 :
                        self.env['gakd.releve_bancaire'].sudo().create(data)
                    data = currentData
            self.env['gakd.releve_bancaire'].sudo().create(data)
        if self.bank == 'ECO' :
            for row in jsonData :
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','=',row.get(u'Description',"")),
                    ('date_releve','=',row.get(u'Transaction Date',"")),
                    ('montant','=',row.get(u'Deposits',"") if row.get(u'Deposits',"") != 0.0 else False  ),
                    ('reference','=',row.get(u'Reference No',"")),
                    ('bank','=',self.bank),
                ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create({
                        'libelle':row.get(u'Description',""),
                        'reference':row.get(u'Reference No',""),
                        'date_releve':row.get(u'Transaction Date',""),
                        'montant':row.get(u'Deposits',""),
                        'bank':'ECO'
                    })
        if self.bank == 'CASHCOLLECTION' : 
            for row in jsonData :
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','=',row.get(u'Note/Message',"")),
                    ('date_releve','=',row.get(u'Date',"")),
                    ('montant','=',row.get(u'Montant',"")),
                    ('bank','=',self.bank),
                ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create({
                        'date_releve':row.get(u'Date'),
                        'montant':row.get(u'Montant'),
                        'libelle':row.get(u'Note/Message'),
                        'bank':self.bank
                    })
        if self.bank == 'BGFI':
            data = {}
            isFirstTime = True
            for row in jsonData :
                # print(row .get(u'Date',"")+" = "+row.get(u'Libell\u9824e l\'op\u9ca1tion',"")," = "+row.get(u'Cr\u9929t(XOF)',""))
                currentData = {
                    'date_releve':row .get(u'Date',""),
                    'libelle':row.get(u'Libellé de l\'opération',""),
                    'montant':row.get(u'Crédit(XOF)',""),
                    'bank':'BGFI'
                }
                if isFirstTime :
                    data = currentData
                    isFirstTime = False
                elif currentData.get('date_releve','') == ' ' :
                    data['libelle'] = data.get('libelle') + ' ' + currentData.get('libelle')
                else :
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','like',data.get(u'libelle',"")),
                        ('date_releve','=',data.get(u'date_releve',"")),
                        ('montant','=',data.get(u'montant',"")),
                        ('bank','=',self.bank),
                    ])
                    if len(existData) <= 0 :
                        self.env['gakd.releve_bancaire'].sudo().create(data)
                        data = currentData
            existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','like',data.get(u'libelle',"")),
                        ('date_releve','=',data.get(u'date_releve',"")),
                        ('montant','=',data.get(u'montant',"")),
                        ('bank','=',self.bank),
                    ])
            if len(existData) <= 0 :
                self.env['gakd.releve_bancaire'].sudo().create(data)
        if self.bank == 'BAB' or self.bank == 'ATLANTIQUE':
            for row in jsonData :
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','=',row.get(u'Libellé',"")),
                    ('date_releve','=',row.get(u'Date de l\'opération',"")),
                    ('montant','=',row.get(u'Montant',"")),
                    ('reference','=',row.get(u'Référence',"")),
                    ('bank','=',self.bank),
                ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create({
                        'libelle':row.get(u'Libellé',""),
                        'reference':row.get(u'Référence',""),
                        'date_releve':row.get(u'Date de l\'opération',""),
                        'montant':row.get(u'Montant',""),
                        'bank':self.bank
                    })
        elif self.bank == 'BOA':
            for row in jsonData :
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','=',row.get(u'Description',"")),
                    ('date_releve','=',row.get(u'Date op.',"")),
                    ('montant','=',row.get(u'Crédit',"")),
                    ('reference','=',row.get(u'Reference',"")),
                    ('bank','=',self.bank),
                ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create({
                        'libelle':row.get(u'Description',""),
                        'reference':row.get(u'Reference',""),
                        'date_releve':row.get(u'Date op.',""),
                        'montant':row.get(u'Crédit',""),
                        'bank':self.bank
                    })
        elif self.bank == 'ECOBANK' :
            for row in jsonData :
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','=',row.get(u'TRANSACTIONREFERENCE',"")),
                    ('date_releve','=',row.get(u'VALUEDATE',"")),
                    ('montant','=',row.get(u'AMOUNT',"")),
                    ('bank','=',self.bank),
                ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create({
                        'libelle':row.get(u'TRANSACTIONREFERENCE',""),
                        'date_releve':row.get(u'VALUEDATE',""),
                        'montant':row.get(u'AMOUNT',""),
                        'bank':self.bank
                    })
        elif self.bank == 'SGB' :
            for row in jsonData :
                
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                    ('libelle','=',row.get(u'LIBELLE',"")),
                    ('date_releve','=',row.get(u'DATE OPÉRATION',"")),
                    ('montant','=',row.get(u'Crédit',"")),
                    ('bank','=',self.bank),
                ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create({
                        'libelle':row.get(u'LIBELLE',""),
                        'date_releve':row.get(u'DATE OPÉRATION',""),
                        'montant':row.get(u'Crédit',""),
                        'bank':self.bank,
                    })

class Import(models.Model):
    _name = 'gakd.import'
    _rec_name = "id"
    import_lines = fields.One2many('gakd.import_line','import_id',string='Import Lines')

    def ImportAll(self) :
        for line in self.import_lines :
            line.ImportData()