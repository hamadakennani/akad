# -*- coding: utf-8 -*-
import time
import odoo
from datetime import datetime
from odoo import SUPERUSER_ID
from odoo import fields, api, models,_
from odoo.tools.translate import _
import glob
import random
import errno

class ir_attachment(models.Model):
    _inherit = 'ir.attachment'


    type=fields.Selection([('url','A partir d\'un url'), ('binary','A partir d\'un emplacement') ,('scanner','A partir d\'un scanner')],string='Nature de document', help="Binary File or URL", required=False, change_default=True)
    write_uid=fields.Many2one('res.users', string='Modifier par', readonly=True)
    write_date=fields.Datetime(string='Date modification', readonly=True)
    type_fichie= fields.Selection([(u'pdf','PDF'),(u'word','WORD'),(u'excel','EXCEL')],string=u"Type de fichier")
    #state=fields.Selection([('bruillon',u'Brouillon'),('ready',u'Activé'),('cancel',u'Annulé'),('archives',u'Archivé')], string='Statut', readonly=True,default='bruillon')
    #flag_historique=fields.Boolean(string='flag_historique', readonly=True)
    #flag_historique_write=fields.Boolean(string='flag_historique_write', readonly=True)
    #extention_autorise=fields.Char(string='extention autorise',readonly=True)
    commantaire=fields.Text(string='Commentaire')
    description=fields.Text(string='Description')
    #id_recu_cdg=fields.Integer(string='id_recu_cdg')
    #flag_recu=fields.Boolean(string='falg_recu')
    #auto_type=fields.Char(string='Emplacement')
    #show_document=fields.Char(string=u"Afficher document")
    #image =fields.Binary(string="Image")
    #company_id=fields.Many2one('res.company', string='Company',default=lambda self: self.env['res.company']._company_default_get('res.users'))
    contrat_id=fields.Many2one('capcontrat.contrat')

    def mymod_get_document(self):
        ged = self.browse([])[0]
        document = ged.datas
        return {'document':document}

    def button_form_ged_scanne(self):
    
        if not ids: return []
        dummy, view_id = self.env['ir.model.data'].get_object_reference('button_form_ged_scanne', 'button_form_incident_solution')
        return {
            'name':(u"Solution"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'ir.attachment',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'active_id':ids[0],
                }
        }
          
    @api.model
    def create(self,vals):
        #
        print "create Methode ----------------------------------------------->"
        # repertoire = self.env['document.directory'].browse(vals.get("parent_id"))
        # if repertoire.name == 'Document annexes':
        #     vals['flag_rep_annexe'] = True
        # if repertoire.name == 'Document divers':
        #     vals['flag_rep_divers'] = True
        #     vals['flag_historique'] = True
        # typee = vals.get("type")
        # flag_recu = vals.get("flag_recu")
        # if typee == 'binary':
        #     vals['auto_type'] = "A partir d'un emplacement"
        # if typee == 'url':
        #     vals['auto_type'] = "A partir d'un url"
        # if typee == 'scanner':
        #     vals['auto_type'] = "A partir d\'un scanner"
        # if flag_recu:
        #     vals['auto_type'] = "A partir du system"
        if not vals.get('description'):
        	vals["description"] = "** Aucune description"
        if not vals.get('commantaire'):
        	vals["commantaire"] = "** Aucun Commantaire"
        return super(ir_attachment, self).create(vals)
    # -- workflow methode

    def mymod_ready(self):
        self.write({'state': 'ready'})
        return True

    def mymod_cancel(self):
        self.write({'state': 'cancel'})
        return True

    def mymod_bruillon(self):
        self.write({'state': 'bruillon'})
        return True

    def mymod_archives(self):
        self.write({'state': 'archives'})
        return True

    # -- workflow methode
    @api.model
    def write(self,vals):
		print "write Methode ----------------------------------------------->"
		# print vals['index_content']
		# old_document = self.browse([])
		# for doc in old_document:
		# 	repertoire = self.env['document.directory'].browse(vals.get("parent_id"))
		# 	if repertoire.name == 'Document annexes':
		# 		print "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
		# 		vals['flag_rep_annexe'] = True
		# 		vals['flag_rep_divers'] = False
		# 	if repertoire.name == 'Document divers':
		# 		vals['flag_rep_divers'] = True
		# 		vals['flag_rep_annexe'] = False
			
		# 	if not vals.get("state") == "cancel":
		# 		vals['flag_historique_write'] = True
		return super(ir_attachment, self).write(vals)
	
    @api.onchange('type_fichier')
    def OnChangeTypeFichier(self):
        vals = {}
        if type_fichier == "word":
            vals['extention_autorise'] = "Les extentions autorisées sont : .docx , .doc et .odt"
        if type_fichier == "excel":
            vals['extention_autorise'] = "L'extention autorisée est : .xlsx "
        if type_fichier == "pdf":
            vals['extention_autorise'] = "L'extention autorisée est : .pdf"
        return {'value':vals}
	# -- Test upload 
    def button_form_show_document_test(self):
		files = glob.glob("/opt/odoo/TEST/*")
		print files
		print "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
		for name in files: # 'file' is a builtin type, 'name' is a less-ambiguous variable name.
			print name
			a = open(name, "rb").read().encode("base64")
			vals = {
			"type":"scanner",
			"name" : name,
			"flag_rep_divers" : True,
			"datas_fname" :name,
			"datas":a,
			}
			self.env['ir.attachment'].create(vals)
		return True

    # -- afficher le document
    def button_form_show_document(self):
		print "okkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"
		ged = self.browse([])[0]
		print "mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm",ged.file_size
		extention_file = ged.datas_fname[ged.datas_fname.index(".")+1:len(ged.datas_fname)]
		if not extention_file == "pdf":
			My_error_Msg = 'vous devez afficher seulement les fichiers pdf'
			raise osv.except_osv(("Warning!"), (My_error_Msg.decode('utf-8')))
		elif extention_file == "pdf":
			if not ids: return []
			dummy, view_id = self.env['ir.model.data'].get_object_reference('ged', 'button_form_notif_show_document')
		return {
		    'name':(u"Document en consultation"),
		    'view_mode': 'form',
		    'view_id': view_id,
		    'view_type': 'form',
		    'res_model': 'ir.attachment',
		    'type': 'ir.actions.act_window',
		    'nodestroy': True,
		    'target': 'new',
		    'domain': '[]',
		    'context': {
		        'active_id':ids[0],
		        }
        	}
