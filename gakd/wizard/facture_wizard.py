# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from dateutil.parser import parse
import datetime
import pandas as pd
import xlrd 
import base64
import io
import re
from unidecode import unidecode
import json
import chardet
import csv
import dateparser
import os.path
import pandas as pd
class Facture_Wizard(models.Model):
    _name = 'gakd.facture_wizard'

    file = fields.Binary(string='Fichier',required=True)


    def importer(self):


        toread = io.BytesIO()
        toread.write(self.file)  # `decrypted` string as the argument 
        toread.seek(0)
        data = pd.read_excel(toread,0)
        jsonData = json.loads(data.to_json(orient='records'))
        for row in jsonData :
            account = self.env['account.invoice'].search([('number','=',row.get(u'ref_facture'))])
            if account:
                account.write({
                    'code_mecef':row.get(u'code_mecef',""),
                    'nim':row.get(u'nim',""),
                    'mecef_compteur':row.get(u'mecef_compteur',""),
                    'mecef_date_heure':row.get(u'mecef_date_heure',""),
                    'qr_code': row.get(u'qr_code',""),
                    'montant_ttc_a_payer':row.get(u'montant_ttc_a_payer',""),
                })


     
