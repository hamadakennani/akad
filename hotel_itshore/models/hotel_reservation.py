# -- coding: utf-8 --
from datetime import date
from odoo import fields, models,api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from unidecode import unidecode


class historique_reserv(models.Model):
    _name='historique.reservation'

    reservation_id= fields.Many2one(comodel_name='hotel.reservation', string='Réservation')
    date_modif = fields.Datetime('D.Modification',default=lambda self: fields.datetime.now())
    user_id=fields.Many2one('res.users','Utilisateur',default=lambda  s : s.env["res.users"].sudo().search([('id','=',s.env.user.id)]).id)
    champs= fields.Char('Champs Modifié')

class hotel_reservation(models.Model):

    _inherit="hotel.reservation"
   


    meal_type = fields.Selection(string='Moyen de paiement',selection=[('Credit Card', 'Credit Card'),('cheque', 'Chèque'), ('especes', 'Espèces'),('versement','Versement')])
    

    contact_re= fields.Many2one(comodel_name='res.partner', string='Contact de réservation')   
    num_of_rooms=fields.Integer(compute='_get_num_of_rooms',string='Nombre des chambres')
    advance_amont=fields.Float(string="Montant de l'avance")
    checkin = fields.Datetime('Expected-Date-Arrival', required=False,
                              readonly=True,)
    checkout = fields.Datetime('Expected-Date-Departure', required=False,
                               readonly=True,
                            )
       
    # meal_type = fields.Selection(string='Type de repas', 
    # selection=[('no_meal', 'No meal'), ('cont_break', 'Continetal Breakfast'),
    # ('cont_break', 'Continetal Breakfast'),('buff_break', 'Buffet Breakfast'),('helf_board', 'Half-board'),
    # ('full_board ', 'Full-board'),])
     
    warehouse_id = fields.Many2one(comodel_name='stock.warehouse' )
    # default=2
    
    deposit_recv_acc = fields.Many2one(comodel_name='account.account', string='Deposit Account')
    total_of_rs_line_sub_total=fields.Float(string='Untaxed Amount ',default=0.0,compute='_get_num_of_rooms')
    total_cost1 = fields.Float(string='Total ',compute='_get_total_cost1')
    untaxed_amt = fields.Float(string='Montant HT ',compute='_get_untaxed_amt')
    total_tax = fields.Float(string=' Taxes ',compute='_get_total_tax')
    deposit_cost2 = fields.Float(string='Advance Payment ',compute='_get_deposit_cost2')
    #anwer code
    via = fields.Selection(string='Via', selection=[('direct', 'Réservation directe'), ('web', 'A travers le web'),('agent', 'A travers un agent / Une agence')])
    agence = fields.Many2one(comodel_name='res.partner', string='Agence')       
    pick_up = fields.Selection(string='Pickup Demandé',default='no', selection=[('yes', 'Oui'), ('no', 'Non'),])
    service_type = fields.Selection(string='Responsablité du Pickup', selection=[('interne', 'Interne'), ('tierce', 'Tierce personne'),])
    chargeable = fields.Boolean(string='Facturable')
    trans_partner_id = fields.Many2one(comodel_name='hr.employee', string='Chauffeur')
    trans_mode_id = fields.Many2one(comodel_name='transport.mode', string='Mode de transport')
    pickup_time= fields.Datetime(string='Heure de ramassage')
    source_id = fields.Many2one(comodel_name='location.master', string='Lieu de ramassage')
    destination_id= fields.Many2one(comodel_name='location.master', string='Déstination')
    
    #Pickup aller 
    aller_num_flight=fields.Char(string='Numéro de Flight ')
    venant_de = fields.Many2one(comodel_name='res.country', string='Venant de')
    date_heure_arrivee= fields.Datetime(string="Date et Heure d'arriveé")
    date_heure_ar_ramassage= fields.Datetime(string="Date et Heure de ramassage")
    #Pickup retour 
    retour_num_flight=fields.Char(string='Numéro de Flight ')
    allant_a = fields.Many2one(comodel_name='res.country', string='Allant à')
    date_heure_retour= fields.Datetime(string="Date et Heure dépert")
    date_heure_re_ramassage= fields.Datetime(string="Date et Heure de ramassage")
    #to add on view
    to_confirmer=fields.Boolean(string='i',default=False,compute='_get_to_confirmer')
    # ----------------------------------------------> anouar 7/12
    qui_paye = fields.Selection(string='Qui va payer ?', selection=[('client', 'Le client'), ('societe', 'La société'),],required=True)
    invite_principale = fields.Char(string='Invité')
    

    id_line_ids = fields.One2many(comodel_name='reservation.detail', inverse_name='reservation_id', string='Détail')
    company_id = fields.Many2one(comodel_name='res.company', string='Company',default=lambda  s : s.env["res.users"].sudo().search([('id','=',s.env.user.id)]).company_id)
    historique_id = fields.One2many(comodel_name='historique.reservation', inverse_name='reservation_id', string='Historique')

    @api.multi
    def send_reservation_maill(self):
        '''
        This function opens a window to compose an email,
        template message loaded by default.
        @param self: object pointer
        '''
        assert len(self._ids) == 1, 'This is for a single id at a time.'
        ir_model_data = self.env['ir.model.data']
        print "ir_model_data"
        print ir_model_data
        try:
            template_id = (ir_model_data.get_object_reference
                           ('hotel_itshore',
                            'mail_template_hotel_itshore_reservation3')[1])
            print ' template ----'
            print template_id
        except ValueError:
            template_id = False
        try:
            compose_form_id = (ir_model_data.get_object_reference
                               ('mail',
                                'email_compose_message_wizard_form')[1])
        except ValueError:
            compose_form_id = False
        ctx = dict()
        ctx.update({
            'default_model': 'hotel.reservation',
            'default_res_id': self._ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
            'force_send': True,
            'mark_so_as_sent': True
        })
        return {
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
            'force_send': True
        }

    def button_duplicate_register(self):
        print ' new _ rev'
        new_rev=self.copy()
        new_rev.state='draft'
        print self.reservation_line
        new_rev_line=self.reservation_line.copy()
        data=[]
        for detail_ids in  self.id_line_ids:
            data.append((0,0,{'partner_name':detail_ids.partner_name.id,'line_id':detail_ids.line_id.id,
            'Purpose':detail_ids.Purpose,'Arriving_From':detail_ids.Arriving_From.id,'Departing_To':detail_ids.Departing_To.id,
            'Business_Partners':detail_ids.Business_Partners.id,'occupation':detail_ids.occupation.id}))
            # new_detail_ids=detail_ids.copy()
            # new_detail_ids.reservation_id=new_rev.id
        
        # print ' new _ rev'
        print new_rev_line
       
       
        
        new_rev.id_line_ids=data
       

        new_rev.reservation_line=[(4,new_rev_line.id)]
     
        return {
                'type': 'ir.actions.act_window',
                'res_model': "hotel.reservation",
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'current',
                'res_id': new_rev.id,
                'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}},
            }
    
    def _get_date(self):
        if self.state=='done':
            data=[]
            print 'geeeet date'
            folio= self.env['hotel.folio'].search([('reservation_id','=',self.id)])
            id= folio.id
            echanges=self.env['currency.exchange'].search([('folio_no','=',id)])            
            # date=""
            for a in echanges:
                data.append(str(datetime.strptime(a.today_date,'%Y-%m-%d %H:%M:%S').date().strftime("%d-%m-%Y")) )
            return data

    def _get_nb_guest(self):
        s=0
        s+=self.adults+self.children
        return s


    def _get_folio_name(self):
        s=" "
        if self.state=='done':
            s+=self.folio_id[0].name
        return s
    @api.constrains('reservation_line', 'adults', 'children','id_line_ids')
    def check_reservation_lines(self):
        ctx = dict(self._context) or {}
        print ' check reservation line '
        print  ctx
        print  len(self.reservation_line)
        print  self.reservation_line
       
        if len(self.reservation_line)>1:
            raise ValidationError("Une réservation concerne qu'une seule chambre")
        elif len(self.reservation_line)<=0 and  not ctx.get('duplicate'):
            raise ValidationError("Veuillez choisir une chambre pour terminer votre réservation 1111")
        
    @api.multi
    def write(self, vals):
        print 'vaaaaaaaaaaaaaaaaaaaaaaaals'
        aaaa = self.env['hotel.reservation'].fields_get()
        # a=  [(k, v['string']) for k, v in aaaa.items()]
        champs=""
        for a,l in aaaa.items():
            name= str(a)
            if name in vals:
                print a
                # print l["string"]
                if  name !='reservation_line' and name !='id_line_ids' and name!='historique_id':
                    champs+="-"+ unidecode(l['string']) +"-"
        if len(champs)>0:
            pp=self.env['historique.reservation'].create({
                'champs':champs,
                'reservation_id':self.id,
            })
        p= super(hotel_reservation, self).write(vals)
        return p
        

    @api.model
    def create(self, vals):
        super(hotel_reservation, self)
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        """
        print self.sudo().env['ir.sequence']
        print 'ooooooooooooooooooo     eeeeeeeeeeeee'
        # print vals
        if not vals:
            vals = {}
        

        
        print 'jjjjjjjjjjjj'
        p= super(hotel_reservation, self).create(vals)
        if p.reservation_no=='New':
             p.write({
                'reservation_no':self.sudo().env['ir.sequence'].next_by_code('hotel.reservation')
            })
       
        # if 1==1:
        #     raise ValidationError("MMMMMMMMMMMMMMMMMMMMM")
        # else :
        #     return p
        return p

    # -------------------------------------> fin 7 / 12

    @api.one
    @api.depends('reservation_line')
    def _get_to_confirmer(self):
        count=0
        for res in self.reservation_line:
            if res.to_confirmer:
              count+=1
        if len(self.reservation_line)==count:
            self.to_confirmer=True
        self.total_guest=self.adults+self.children

    @api.constrains('reservation_line', 'adults', 'children')
    def check_reservation_rooms(self):

        super(hotel_reservation,self)
        ctx = dict(self._context) or {}
        for reservation in self:
            cap = 0
            for rec in reservation.reservation_line:
                if len(rec.reserve) == 0:
                    dt='%Y-%m-%d %H:%M:%S'
                    self.to_confirmer=False
                    room_bool=False
                    ids={}
                    if rec.checkout and rec.checkin:
                        reserv_checkin = datetime.strptime(rec.checkin, dt)
                        reserv_checkout = datetime.strptime(rec.checkout, dt)
                        rooms=self.env['hotel.room'].search([('categ_id','=',rec.categ_id.id)])
                        
                        for room_id in rooms:
                            print 'rooms for '
                            if room_id.room_reservation_line_ids:
                                print 'if eoom id' 
                                for reserv in room_id.room_reservation_line_ids.search([('status', 'in', ('confirm', 'done')),
                                                                ('room_id', '=', room_id.id)]):
                                    print 'reserv in search env'
                                    check_in = datetime.strptime(reserv.check_in, dt)
                                    check_out = datetime.strptime(reserv.check_out, dt)
                                    if check_in <= reserv_checkin <= check_out:
                                        room_bool = True
                                    if check_in <= reserv_checkout <= check_out:
                                        room_bool = True
                                    if reserv_checkin <= check_in and \
                                            reserv_checkout >= check_out:
                                        room_bool = True
                                    print room_bool
                            if not room_bool:
                              print 'room bool'
                              if  room_id.id  not in ids :
                                         ids[room_id.id ]=room_id.id 
                    print ids
                    if len(ids)>0:
                        print 'ids>0'
                        for i in ids :
                            print 'for '
                            print i
                            rec.reserve=i
                  
                
                for room in rec.reserve:
                    cap += room.capacity
                    print 'cap '
                    print cap
            if not ctx.get('duplicate'):
                print 'duplicate'
                print reservation.adults
                print reservation.children
                if (reservation.adults + reservation.children) > cap:
                    raise ValidationError(('Room Capacity Exceeded \n Please \
                                            Select Rooms According to Members \
                                            Accomodation.'))
            if reservation.adults <= 0:
                raise ValidationError(('Adults must be more than 0'))
    # @api.model
    # def create(self, values):
    #     if len(values.get('id_line_ids'))>=1:
    #         return super(hotel_reservation, self).create(values)
    #     else : 
    #         raise ValidationError("Veuillez remplir les détails de la résérvation")
    @api.multi
    def check_overlap(self, date1, date2):
        date2 = datetime.strptime(date2, '%Y-%m-%d')
        date1 = datetime.strptime(date1, '%Y-%m-%d')
        delta = date2 - date1
        return set([date1 + timedelta(days=i) for i in range(delta.days + 1)])
    @api.one
    @api.depends('advance_amont')
    def _get_deposit_cost2(self):
        self.deposit_cost2=self.advance_amont
    @api.one
    @api.depends('total_tax','untaxed_amt')
    def _get_total_cost1(self):
        self.total_cost1=self.total_tax+self.untaxed_amt
    @api.one
    @api.depends('reservation_line')
    def _get_total_tax(self):
        total=0
        for line in self.reservation_line:
            total+=line.tva
        self.total_tax=total
    @api.one
    @api.depends('reservation_line')
    def _get_untaxed_amt(self):
        total=0
        for line in self.reservation_line:
            total+=line.sub_total
        self.untaxed_amt=total
    @api.one
    @api.depends('reservation_line')
    def _get_num_of_rooms(self):
        self.num_of_rooms=len(self.reservation_line)
        for line  in self.reservation_line:
          self.total_of_rs_line_sub_total+=line.sub_total
    def confirmed_reservation(self):
        """
        This method create a new record set for hotel room reservation line
        -------------------------------------------------------------------
        @param self: The object pointer
        @return: new record set for hotel room reservation line.
        """
        super(hotel_reservation,self)
        if len(self.id_line_ids)<1:
            raise ValidationError("Veuillez remplir les détails de la réservation")
        if not self.to_confirmer:
            raise ValidationError(("Veuillez sélectionner des chambres \
                            Pour réservation."))    
        if not self.meal_type : 
            raise ValidationError("Veuillez choisir un moyen de payement")

        reservation_line_obj = self.env['hotel.room.reservation.line']
        vals = {}
        dt='%Y-%m-%d %H:%M:%S'
        da='%Y-%m-%d'
        mytime = "%Y-%m-%d"
        for reservation in self:
            # reservation_line
            
            room_bool = False
            room_house_keeping = False
            for line_id in reservation.reservation_line:
               
                reserv_checkin = datetime.strptime(line_id.checkin, dt)
                reserv_checkout = datetime.strptime(line_id.checkout, dt)
                for hk in  self.env['hotel.housekeeping'].search([('room_no','=',line_id.reserve.id),
                ('state','=', 'maintenance')]):
                   
                   
                    if hk.end_date and hk.current_date:
                        check_out = datetime.strptime(hk.end_date,  da)
                        check_in = datetime.strptime(hk.current_date,  da)
                        if check_in <= reserv_checkin <= check_out:
                            room_house_keeping = True
                        if check_in <= reserv_checkout  <= check_out:
                            room_house_keeping = True
                        if reserv_checkin  <= check_in and \
                                reserv_checkout  >= check_out:
                            room_house_keeping = True
                        r_checkin = datetime.strptime(line_id.checkin,dt).date()
                        r_checkin = r_checkin.strftime(mytime)
                        r_checkout = datetime.strptime(line_id.checkout, dt).date()
                        r_checkout = r_checkout.strftime(mytime)
                        range1 = [r_checkin, r_checkout]
                        range2 = [hk.current_date, hk.end_date]
                        overlap_dates_hk = self.check_overlap(*range1) \
                                    & self.check_overlap(*range2)
                        overlap_dates_hk = [datetime.strftime(dates,
                                                                '%d/%m/%Y') for
                                                dates in overlap_dates_hk]
                        if room_house_keeping:
                                raise ValidationError(('You tried to Confirm '
                                                        'Reservation with room'
                                                        ' those already '
                                                        'State Maintenance '
                                                        'Reservation Period. '
                                                        'Overlap Dates are '
                                                        '%s') % overlap_dates_hk)
                    elif hk.current_date:
                         check_in = datetime.strptime(hk.current_date,  da)
                         if check_in == reserv_checkin ==check_out:
                            room_house_keeping = True
                         if room_house_keeping:
                                raise ValidationError(('You tried to Confirm '
                                                        'Reservation with room'
                                                        ' those already '
                                                        'State Maintenance '
                                                        'Reservation Period. '
                                                        'Overlap Dates are '
                                                        '%s') % overlap_dates_hk)

                  
                
                for room_id in line_id.reserve:
                    if room_id.room_reservation_line_ids:
                        for reserv in room_id.room_reservation_line_ids.\
                                search([('status', 'in', ('confirm', 'done')),
                                        ('room_id', '=', room_id.id)]):
                            check_in = datetime.strptime(reserv.check_in, dt)
                            check_out = datetime.strptime(reserv.check_out, dt)
                            if check_in <= reserv_checkin <= check_out:
                                room_bool = True
                            if check_in <= reserv_checkout <= check_out:
                                room_bool = True
                            if reserv_checkin <= check_in and \
                                    reserv_checkout >= check_out:
                                room_bool = True
                          
                            r_checkin = datetime.strptime(line_id.checkin,
                                                    dt).date()
                            r_checkin = r_checkin.strftime(mytime)
                            r_checkout = datetime.\
                                strptime(line_id.checkout, dt).date()
                            r_checkout = r_checkout.strftime(mytime)
                            check_intm = datetime.strptime(reserv.check_in,
                                                           dt).date()
                            check_outtm = datetime.strptime(reserv.check_out,
                                                            dt).date()
                            check_intm = check_intm.strftime(mytime)
                            check_outtm = check_outtm.strftime(mytime)
                            range1 = [r_checkin, r_checkout]
                            range2 = [check_intm, check_outtm]
                            overlap_dates = self.check_overlap(*range1) \
                                & self.check_overlap(*range2)
                            overlap_dates = [datetime.strftime(dates,
                                                               '%d/%m/%Y') for
                                             dates in overlap_dates]
                            if room_bool:
                                raise ValidationError(('You tried to Confirm '
                                                        'Reservation with room'
                                                        ' those already '
                                                        'reserved in this '
                                                        'Reservation Period. '
                                                        'Overlap Dates are '
                                                        '%s') % overlap_dates)
                            else:
                                self.state = 'confirm'
                                vals = {'room_id': room_id.id,
                                        'check_in': line_id.checkin,
                                        'check_out': line_id.checkout,
                                        'state': 'assigned',
                                        'reservation_id': reservation.id,
                                        }
                                room_id.write({'isroom': False,
                                               'status': 'occupied'})
                        else:
                            self.state = 'confirm'
                            vals = {'room_id': room_id.id,
                                    'check_in': line_id.checkin,
                                    'check_out': line_id.checkout,
                                    'state': 'assigned',
                                    'reservation_id': reservation.id,
                                    }
                            room_id.write({'isroom': False,
                                           'status': 'occupied'})
                    else:
                        self.state = 'confirm'
                        vals = {'room_id': room_id.id,
                                'check_in': line_id.checkin,
                                'check_out': line_id.checkout,
                                'state': 'assigned',
                                'reservation_id': reservation.id,
                                }
                        room_id.write({'isroom': False,
                                       'status': 'occupied'})
                    print reservation_line_obj.search([('reservation_id','=',self.id)])
                    reservation_line_obj.search([('reservation_id','=',self.id)]).sudo().unlink()
                    print 888888888888888200000000000000000000000
                    reservation_line_obj.search([('reservation_id','=',self.id)])
                    reservation_line_obj.create(vals)
                    print reservation_line_obj
                    # raise ValidationError('Please Select Rooms ')
                    line_id.create_hosekeeping_line()
        return True

    @api.multi
    def create_folio(self):
        """
        This method is for create new hotel folio.
        -----------------------------------------
        @param self: The object pointer
        @return: new record set for hotel folio.
        """
        super(hotel_reservation,self)
        if  self.to_confirmer==False:
              raise ValidationError(('Please Select Rooms \
                            For Reservation.'))               
                                

        hotel_folio_obj = self.env['hotel.folio']
        room_obj = self.env['hotel.room']
        for reservation in self:
            folio_lines = []
            checkin_date = reservation['checkin']
            checkout_date = reservation['checkout']
            
            duration_vals = (self.onchange_check_dates
                             (checkin_date=checkin_date,
                              checkout_date=checkout_date, duration=False))
            duration = duration_vals.get('duration') or 0.0
            folio_vals = {
                'date_order': reservation.date_order,
                'warehouse_id': reservation.warehouse_id.id,
                'partner_id': reservation.partner_id.id,
                'pricelist_id': reservation.pricelist_id.id,
                'partner_invoice_id': reservation.partner_invoice_id.id,
                'partner_shipping_id': reservation.partner_shipping_id.id,
                'avance_m':self.advance_amont,
                'duration': duration,
                'reservation_id': reservation.id,
                'service_lines': reservation['folio_id']
            }
            for line in reservation.reservation_line:
                for r in line.reserve:
                    if line.tax.id:
                        folio_lines.append((0, 0, {
                            'checkin_date': line.checkin,
                            'checkout_date':line.checkout,
                            'product_id': r.product_id and r.product_id.id,
                            'name': reservation['reservation_no'],
                            'price_unit': line.prix,
                            'product_uom_qty': duration,
                            'from_resv': True,
                            'discount':line.remise,
                            'tax_id':[(4,line.tax.id)],
                            'is_reserved': True}))
                    else:
                        folio_lines.append((0, 0, {
                        'checkin_date': line.checkin,
                        'checkout_date':line.checkout,
                        'product_id': r.product_id and r.product_id.id,
                        'name': reservation['reservation_no'],
                        'price_unit': line.prix,
                        'product_uom_qty': duration,
                        'from_resv': True,
                        'discount':line.remise,
                       
                        'is_reserved': True}))
                    res_obj = room_obj.browse([r.id])
                    res_obj.write({'status': 'occupied', 'isroom': False})
            folio_vals.update({'room_lines': folio_lines})
            folio = hotel_folio_obj.create(folio_vals)
            if folio:
                for rm_line in folio.room_lines:
                    rm_line.product_id_change()
            self._cr.execute('insert into hotel_folio_reservation_rel'
                             '(order_id, invoice_id) values (%s,%s)',
                             (reservation.id, folio.id))
            self.state = 'done'
        return True
#anwer code

class mode_transport(models.Model):
    _name = 'transport.mode'
    _rec_name='libelle'
    libelle = fields.Char(string='Nom')

class location_master(models.Model):
    _name = 'location.master'
    _rec_name='nom'
    nom = fields.Char(string='Nom')
    code = fields.Char(string='Code')


class ResPartner(models.Model):
    _inherit = 'res.partner'

    type_document = fields.Selection([('CIN','CIN'),('PASS','Passeport')], string='Type de document') #,required=True
    valid_from = fields.Date(string='Valide de') #,required=True
    valid_to = fields.Date(string='Valide a' ) #,required=True)
    gender = fields.Selection(string='Sexe', selection=[('m', 'Masculin'), ('f', 'Féminin'),]) #,required=True
    date_birth = fields.Date(string='Date de naissance') #,required=True

    fidelity_number = fields.Char(string='Fidelity card Number')
    email = fields.Char(string='E-mail Address')
    home_adresse = fields.Char(string='Permanent Home Address ')
    adresse_code = fields.Char(string='Address Code')
    home_phone = fields.Char(string='Home phone number')
    business_phone = fields.Char(string='Business phone number')
    id_card_hotel = fields.Char(string='ID Card') #,required=True
    issuing_auth=fields.Char(string='Autorité de délivrance')
    reservation_id = fields.One2many(comodel_name='hotel.reservation', inverse_name='partner_id', string='Liste des réservations')
    total_guest = fields.Integer(string='Number of guests',compute='_get_to_confirmer')
   

    @api.onchange('company')
    def getCompany(self):
        res = {}
        ids = []
        print 'ooooooooooooon change '
        company=self.env["res.partner"].search([])
        for c in company:           
            if  c.company_type =='company':
                ids.append(c.id) 
        res['domain'] = {'company': [('id', 'in', ids)]}
        return res

    company=fields.Many2one(comodel_name='res.partner', string='Socièté')


class reserv_detail(models.Model):
    _name = 'reservation.detail'

    # id_card = fields.Char(string='ID Card',required=True)
    #client_id = fields.Many2one(comodel_name='document.type', string='Type de document',required=True)
    

    @api.onchange('company','partner_name')
    def getCompany(self):
        res = {}
        ids = []
        print 'ooooooooooooon change '
        company=self.env["res.partner"].search([])
        for c in company:           
            if  c.company_type =='company':
                ids.append(c.id) 
        res['domain'] = {'company': [('id', 'in', ids)]}
        return res


    partner_name = fields.Many2one('res.partner', 'Nom de l\'invité ',
                                      required=True, delegate=True,
                                      ondelete='cascade')

    reservation_id = fields.Many2one(comodel_name='hotel.reservation')
    
    line_id = fields.Many2one(comodel_name='hotel_reservation.line') 
    
    Purpose = fields.Text(string='Purpose of Visit ')
    Arriving_From =fields.Many2one(comodel_name='res.country', string='Arriving From')
    Departing_To =fields.Many2one(comodel_name='res.country', string='Departing To ')
    Business_Partners=fields.Many2one(comodel_name='res.company', string='Business Partners in Ivory Coast')
    
    
    # registration_number = fields.Integer(string='Arnhotel Registration Number')
    occupation = fields.Many2one(comodel_name='hotel.occupation', string='Occupation')
   
    @api.model
    def create(self, vals):
        self.env['historique.reservation'].create({
                'champs':'Création  d\'une nouvelle ligne ID Détail',
                'reservation_id':vals['reservation_id'],
            })
        return super(reserv_detail, self).create(vals)


    
class documet_type(models.Model):
    _name = 'hotel.occupation'
    _rec_name='libelle'

    libelle = fields.Char(string='Occupation')
    




class documet_type(models.Model):
    _name = 'document.type'

    name = fields.Char(string='ID Card')
    id_code=fields.Char(string='ID Code') 
   
