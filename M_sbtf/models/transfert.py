# -- coding: utf-8 --
from odoo import api, fields, models


class transfert(models.Model):
    _name = 'sbtf.transfert'

    # transfert = fields.Many2many('gakd.transfert')
    num = fields.Char("Num",readonly="1")
    type_transfert = fields.Selection(string=' Type de transfert', selection=[('Dépot-Client','Dépot-Client'),('a', 'Dépot-Camion'), ('b', 'Dépot-Camion-station'),('c','Camion-station'),('d', 'Dépot-Dépot')],readonly="1")
    source = fields.Many2one('stock.location', "Source",readonly="1")
    etat = fields.Selection(string='État', selection=[('Bon de commande','Bon de commande'),('test','Bon de commande'),('test2','Pré-Brouillon'),('brouillon','Brouillon'),('partiellementv', 'Partiellement Validé'),('Valider', 'Validé'),('Annuler', 'Annulé')],readonly="1")
    date_creation = fields.Datetime("Créer le",readonly="1")
    pv = fields.Char(string="Point de vente",readonly="1")
    qty  = fields.Float("Quantité")
    produit = fields.Many2one('product.product', "Produit")
    gakd_tran = fields.Many2one('gakd.transfert', "Transfert")
    jnp_picking_id = fields.Many2one('stock.picking', "Picking")
    travel=fields.Many2one('tms.travel')
class Sale_order(models.Model):
    _inherit = 'sale.order'

    def action_confirm(self) :
        for  line in self:
            pv=self.env['res.partner'].search([("id","=",line.partner_id.id)])
            pv=pv.name
            date_creation=line.date_order
            for line1 in line.order_line :
                self.env["sbtf.transfert"].sudo().create({
                    "num":line.name,
                    "type_transfert":"Dépot-Client",
                    # "source":line.location_id.id,
                    "etat":"Bon de commande",
                    "date_creation":date_creation,
                    "pv":pv,
                    "produit":line1.product_id.id,
                    "qty":line1.product_uom_qty,
                })
        return super(Sale_order,self).action_confirm()
    # def envoyer_sbtf(self):
    #     for  line in self:
    #         pv=self.env['res.partner'].search([("id","=",line.partner_id.id)])
    #         pv=pv.name
    #         date_creation=line.date_order
    #         for line1 in line.order_line :
    #             self.env["sbtf.transfert"].sudo().create({
    #                 "num":line.name,
    #                 "type_transfert":"Dépot-Client",
    #                 # "source":line.location_id.id,
    #                 "etat":"Bon de commande",
    #                 "date_creation":date_creation,
    #                 "pv":pv,
    #                 "produit":line1.product_id.id,
    #                 "qty":line1.product_uom_qty,
    #             })
class Transfert(models.Model):
    _inherit = 'gakd.transfert'

    @api.model 
    def create(self,vals):
        pa = super(Transfert, self).create(vals)
        for  line in pa:
            pv=""
            for line1 in line.station_move_lines :
                pv=self.env['gakd.point.vente'].search([("id","=",line1.point_vente_id.id)])
                pv=pv.libelle
                for line2 in line1.move_lines :           
                    self.env["sbtf.transfert"].sudo().create({
                        "num":line.num,
                        "type_transfert":line.transfer_type,
                        "source":line.location_id.id,
                        "etat":line.states,
                        "date_creation":line.create_date,
                        "pv":pv,
                        "produit":line2.product_id.id,
                        "qty":line2.qty,
                        "gakd_tran":line.id
                    })
        return pa
