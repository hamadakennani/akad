{
    'name': 'Dubai Itshore',
    'version': '1.0',
    'summary': 'Groupe AKAD',
    'description': '',
    'category': 'Groupe AKAD',
    'author': 'IT SHORE',
    'website': '',
    'license': '',
    'depends': ['base','purchase','sale','stock_landed_costs'],
    'data': [
        
         'security/res_groups.xml',
        #'security/ir_rule.xml',
        'security/ir.model.access.csv',
       
        
       
          'views/anwer_view.xml',
       
          'views/sale_view.xml',
          'views/purchase_view.xml',
          'views/res_partner_view.xml',
          'views/contrat_view.xml',
          'views/contrat_line_view.xml',
          'views/sale_order_view.xml',
          'views/product_views.xml',
        #'reports/service_line.xml',
      
          'reports/facture_fiscale_lub_repport.xml',
          'reports/facture_fi_pb_repport.xml',
          'reports/facture_fiscale_lub_pro_repport.xml',
          
          'reports/facture_fiscale_autre.xml',
          
         'views/menu.xml',
          #'wizard/report_wizard_view.xml',
        
    ],
    'qweb': [
    ],
    'css': [],
    'js': [],
    'installable': True,
    'auto_install': False,
}
