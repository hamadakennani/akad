# -*- coding: utf-8 -*-

import xlwt
import cStringIO
import base64
import time
import datetime
from datetime import datetime
from datetime import timedelta
from openerp import api, fields, models

class AccountReportBalance(models.TransientModel):
    _inherit = "account.balance.report"

    @api.multi
    def check_report_excel(self):
        res = super(AccountReportBalance, self).check_report()
        data = res['data']
        return self._print_report_excel(data)

    @api.multi
    def _print_report_excel(self, data):
        workbook = xlwt.Workbook()
        title_style_comp = xlwt.easyxf('align: horiz center ; font: name Times New Roman,bold off, italic off, height 450')
        title_style_comp_left = xlwt.easyxf('align: horiz left ; font: name Times New Roman,bold off, italic off, height 450')
        title_style = xlwt.easyxf('align: horiz center ;font: name Times New Roman,bold off, italic off, height 350')
        title_style2 = xlwt.easyxf('font: name Times New Roman, height 200')
        title_style1 = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190; borders: top double, bottom double, left double, right double;')
        title_style1_table_head = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head1 = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200')
        title_style1_consultant = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head_center = xlwt.easyxf('align: horiz center ; font: name Times New Roman,bold on, italic off, height 190; borders: top thick, bottom thick, left thick, right thick;')

        title_style1_table_data = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold on, italic off, height 190')
        title_style1_table_data_sub = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190')
        title_style1_table_data_sub_right = xlwt.easyxf('align: horiz right; font: name Times New Roman,bold off, italic off, height 190')
        balance_report_obj = self.env['report.account.report_trialbalance']
        
        display_account = data['form'].get('display_account')
        accounts = self.env['account.account'].search([])
        account_res = balance_report_obj.with_context(data['form'].get('used_context'))._get_accounts(accounts, display_account)
        sheet_name = 'Partner Ledger'
        sheet = workbook.add_sheet(sheet_name)
        comp_id = self.env.user.company_id
        currency_id = comp_id.currency_id
        sheet.write_merge(0, 1, 0, 6, comp_id.name + ': Trial Balance', title_style_comp_left)
        sheet.write(0, 8, 'Printing Date: '+datetime.now().strftime('%Y-%m-%d'), title_style1_table_head1)
        
        column = sheet.col(0)
        column.width = 256 * 20
        column = sheet.col(4)
        column.width = 256 * 20
        sheet.write(3, 0, 'Display Account :',title_style1_table_head1)
        if self.display_account == 'all':
            sheet.write(4, 0, 'All accounts' ,title_style1_table_data_sub)
        if self.display_account == 'movement':
            sheet.write(4, 0, 'With movements' ,title_style1_table_data_sub)
        if self.display_account == 'not_zero':
            sheet.write(4, 0, 'With balance not equal to zero' ,title_style1_table_data_sub)
        column = sheet.col(1)
        column.width = 256 * 30
        if self.date_from:
            sheet.write(3, 2, 'Date from :', title_style1_table_head1)
            sheet.write(4, 2, self.date_from, title_style1_table_data_sub)
        if self.date_to:
            sheet.write(3, 3, 'Date To :', title_style1_table_head1)
            sheet.write(4, 3, self.date_to, title_style1_table_data_sub)
        
        sheet.write(3, 4, 'Target Moves :',title_style1_table_head1)
        if self.target_move == 'all':
            column = sheet.col(8)
            column.width = 256 * 18
            sheet.write(4, 4, 'All Entries',title_style1_table_data_sub)
        if self.target_move == 'posted':
            column = sheet.col(8)
            column.width = 256 * 18
            sheet.write(4, 4, 'All Posted Entries',title_style1_table_data_sub)
        
        sheet.write(8, 0, 'Code',title_style1_table_head)
        sheet.write(8, 1, 'Account',title_style1_table_head)
        sheet.write(8, 2, 'Debit',title_style1_table_head)
        sheet.write(8, 3, 'Credit',title_style1_table_head)
        sheet.write(8, 4, 'Balance',title_style1_table_head)
        
        #[{'code': u'11200', 'credit': 0.0, 'balance': 13200.0, 'name': u'Trade Debtors', 'debit': 13200.0}, {'code': u'21310', 'credit': 1200.0, 'balance': -1200.0, 'name': u'GST Collected', 'debit': 0.0}, {'code': u'41110', 'credit': 12000.0, 'balance': -12000.0, 'name': u'Sales Product #1', 'debit': 0.0}]
        roww = 9
        for line in account_res:
            sheet.write(roww, 0, line['code'], title_style1_table_data_sub)
            sheet.write(roww, 1, line['name'], title_style1_table_data_sub)
            sheet.write(roww, 2, str(line['debit'])+' '+currency_id.symbol, title_style1_table_data_sub_right)
            sheet.write(roww, 3, str(line['credit'])+' '+currency_id.symbol, title_style1_table_data_sub_right)
            sheet.write(roww, 4, str(line['balance'])+' '+currency_id.symbol, title_style1_table_data_sub_right)
            roww = roww+1

            
        stream = cStringIO.StringIO()
        workbook.save(stream)
        attach_id = self.env['accounting.tb.report.output.wizard'].create({'name':'Trial_Balance.xls', 'xls_output': base64.encodestring(stream.getvalue())})
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'accounting.tb.report.output.wizard',
            'res_id':attach_id.id,
            'type': 'ir.actions.act_window',
            'target':'new'
        }

class AccountingTBReportOutputWizard(models.Model):
    _name = 'accounting.tb.report.output.wizard'
    _description = 'Wizard to store the Excel output'

    xls_output = fields.Binary(string='Excel Output')
    name = fields.Char(string='File Name', help='Save report as .xls format', default='Trial_Balance.xls')

#vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
