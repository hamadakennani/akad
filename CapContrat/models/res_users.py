# -*- coding: utf-8 -*-
from odoo import fields, api, models
from odoo.exceptions import  ValidationError
import datetime
from random import randint


class   Res_users(models.Model):
    _inherit = "res.users"
    _rec_name = 'name'
    privilege_ids=fields.One2many('capcontrat.contrat.privilege','res_users')
    contrat_id=fields.One2many('capcontrat.contrat','manage',string="Contrats")

    # lecture_de_cantrat=fields.One2many('capcontrat.contrat','lecture',string="Lecture")
    # ecriture_de_cantrat=fields.One2many('capcontrat.contrat','ecriture',string="Ecriture")

    # access_en_lecture=fields.Many2many('capcontrat.contrat',relation='user_contrat_rel_lecture2', column2='contarat_id',column1='user_id', string="lecture")
    # access_en_ecriture=fields.Many2many('capcontrat.contrat',relation='user_contrat_rel_ecriture2', column2='contarat_id',column1='user_id', string="ecriture")
    emails=fields.One2many("capcontrat.contrat.echeance","mailto")





    # lecture=fields.Many2one('capcontrat.contrat',string="lecture")
    # ecriture=fields.Many2one('capcontrat.contrat',string="ecriture")


    