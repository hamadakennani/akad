# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
from datetime import datetime
from random import randint
from dateutil import parser
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT


class liste_transaction_by_client(models.Model):
    #_inherit = "res.partner"
    _name="gakd.liste_transaction_by_client"

    client = fields.Many2one("gakd.res_partner",string="Client")
    carte = fields.Many2one("gakd.carte",string="Client")   
    promoteur = fields.Many2one("gakd.tv_station",string="Promoteur")  
    start_date = fields.Datetime(
         string='Date Début',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
    end_date = fields.Datetime(
         string='Date Fin',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )