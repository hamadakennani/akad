# -*- coding: utf-8 -*-
import time
from odoo import api, models, _
from odoo.exceptions import UserError


class ReportTrialBalance(models.AbstractModel):
    _inherit = 'report.account.report_trialbalance'

    @api.model
    def render_html(self, docids, data=None):
        super(ReportTrialBalance,self)
        if not data.get('form') or not self.env.context.get('active_model'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
        display_account = data['form'].get('display_account')
        accounts = docs if self.model == 'account.account' else self.env['account.account'].search([])
        account_res = self.with_context(data['form'].get('used_context'))._get_accounts(accounts, display_account)
        print 'hhhhhhhhiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii'
        print data['form']
        if data['form']['by_group']:
            data_group = {}
            for ac in account_res :
                acc = self.env['account.account'].sudo().search([('code','=',ac['code'])])
                if acc.group_id.name not in data_group.keys():
                    data_group[acc.group_id.name] = {
                        'credit':ac['credit'],
                        'debit':ac['debit'],
                        'balance':ac['balance']
                    }
                else:
                    data_group[acc.group_id.name]['credit'] = float(data_group[acc.group_id.name]['credit']) + float(ac['credit']) 
                    data_group[acc.group_id.name]['debit'] = float(data_group[acc.group_id.name]['debit']) + float(ac['credit'])
                    data_group[acc.group_id.name]['balance'] = float(data_group[acc.group_id.name]['balance']) + float(ac['credit'])
           
            print  data_group
            docargs = {
                'doc_ids': self.ids,
                'doc_model': self.model,
                'data': data['form'],
                'docs': docs,
                'time': time,
                'Groups': data_group,
                }
            'hiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii'
            return self.env['report'].render('gakd.report_trialbalance_group', docargs)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'Accounts': account_res,
        }
        return self.env['report'].render('account.report_trialbalance', docargs)