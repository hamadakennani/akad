# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from datetime import date, datetime, timedelta
import dateutil.relativedelta as relativedelta
from random import randint
from keyid import keyid
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
from  sms_sender import send
import logging

class gakd_type_carte(models.Model):
    _name = 'gakd.type.carte'

    _rec_name = 'libelle'
    _description = ''

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    libelle = fields.Char(string="Libellé")
    front_carte = fields.Binary(string="Background du recto")
    front_carte_name = fields.Char("Background du recto")
    back_carte = fields.Binary(string="Background du verso")
    back_carte_name = fields.Char("Background du verso")
    state = fields.Selection([('activee','Actiée'),('suspendu','Suspendu'),('annule','Annulée')] , string="Statut" , default="activee")





class gakd_carte(models.Model):
    _name = 'gakd.carte'

    _rec_name = 'libelle'
    _description = ''


    @api.multi
    def setToBrouillon(self):
        self.ensure_one()
        self.state="brouillon"

    @api.multi
    def setToActivee(self):
        self.ensure_one()
        self.state="activee"
	self.date_activation=fields.datetime.now()

    @api.multi
    def setToGenere(self):
        self.ensure_one()
        self.state="generee"


    @api.multi
    def setToEnd(self):
        self.ensure_one()
        self.state="terminee"
	self.date_termine=fields.datetime.now()

    @api.multi
    def setToSuspendu(self):
        self.ensure_one()
        self.state="suspendu"
	self.date_suspension=fields.datetime.now()

    @api.multi
    def setToPerdu(self):
        self.ensure_one()
        self.state="perdu"
	self.date_perte=fields.datetime.now()

    @api.multi
    def setToExpire(self):
        self.ensure_one()
        self.state="expiree"
	self.date_expiration=fields.datetime.now()

    @api.multi
    def setToAnnule(self):
        self.ensure_one()
        self.state="annule"
	self.date_annulation=fields.datetime.now()



    def _default_serie(self):
	result = True
	serie_proposition = 0
        while result==True:
		serie_proposition = str(randint(111, 999)) + str(randint(111, 999)) + str(randint(111, 999))
		result = self.search([['num_serie', '=', int(serie_proposition)]]).id
        return serie_proposition

    def _getQrCode(self,string):
	import hashlib
	md5 = hashlib.md5()
	qrcode = ""
	result = True
	serie_proposition = 0
        while result==True:
		md5.update(string+str(serie_proposition))
		qrcode = md5.hexdigest()
		serie_proposition += 1 
		result = self.search([['qrcode', '=', qrcode ]]).id
	return qrcode

    @api.multi
    def setCodePine(self):
        self.ensure_one()
        codepin = randint(1111, 9999)
        self.code_pin = codepin

        body = """Cate """+self.libelle.name+""" votre code PIN est : """+str(codepin)
        Subject = "JNP BENIN"
        mobile = self.libelle.mobile
        send(mobile,body)
        # api_instance = swagger_client.SmsApi()
        # smsrequest = swagger_client.SmsUniqueRequest("eb717305f6545b5ea0d57a7aba06dc54",None, None, body,mobile,Subject,None,None,None, None, None) # SMSRequest | sms request
        # try:
        #     api_response = api_instance.send_sms(smsrequest)
        # except ApiException as e:
        #     print ("Exception when calling SmsApi->send_sms: %s\n" % e)



    #@api.depends('num_serie_hide')
    @api.onchange('libelle')
    def _send_serie(self):
	
	i = -1  # This could have been any integer, positive or negative
	today = fields.datetime.now().date()
#(self.create_date).strftime('%Y-%m-%d %H:%M:%S')


#date.strptime(self.create_date,'%Y-%m-%d %H:%M:%S') #date.today()

	nextyear = fields.datetime.now().replace(year=fields.datetime.now().year + 2)
	#nextyear = fields.date.now().replace(year=fields.date.now().year + 2)

	

    @api.constrains('solde')
    def _check_solde(self):
        if self.solde<0:
            raise ValidationError(_("Le solde de la carte devrait rester superieur ou égal à zéro"))
	#elif self.solde>self.owner_id.solde_compte:
        #    raise ValidationError(_("------------solde < solde owner ----------"))



    @api.multi
    def getCartByQrcode(self):
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	print "======================"
	#carte = self.search([['qrcode', '=', qrcode]])
        
        return 88
    @api.one
    @api.depends('compute_field')
    def _get_user(self):
		self.ensure_one()
		if self.env.user.has_group('gakd.group_gakd_client') or  self.env.user.has_group('gakd.group_gakd_president') or self.env.user.has_group('gakd.group_gakd_chargeur'):
			self.compute_field = True
		else:
			self.compute_field = False


    # ***************************************************************************************
    
    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    owner_id = fields.Many2one("res.partner",string="Client", required=True)
    type_carte_id = fields.Many2one("gakd.type.carte",string="Type de carte", required=True)
    libelle = fields.Many2one("gakd.customer",string="Consommateur", required=True)
    num_serie = fields.Integer(string="Numéro de serie")
    code_pin = fields.Integer(string="code pin")
    qrcode = fields.Char(string="dddd")
    solde = fields.Float(string='Solde actuel')
    montant_affecter = fields.Float(string='Montant à affecter')
    date_activation = fields.Datetime("Date d'activation", readonly=True)
    date_perte = fields.Datetime("Date de perte", readonly=True)
    date_suspension = fields.Datetime("Date de suspension", readonly=True)
    date_expiration = fields.Date("Date d'expiration", readonly=True)
    date_termine = fields.Datetime("Date fin service", readonly=True)
    date_annulation = fields.Datetime("Date d'annulation", readonly=True)
    carte_consommation_ids = fields.One2many("gakd.carte.consommation","carte_id",string="Historiques", readonly=True)
    log_ids = fields.One2many("gakd.log","carte_id",string="Historiques", readonly=True)
    state = fields.Selection([('brouillon','Brouillon'),('generee','Générée'),('suspendu','Suspendu'),('expiree','Expirée'),('perdu','Perdue'),('terminee','Terminée'),('annule','Annulée')],string="Statut",default="generee")
    click_create = fields.Boolean("check click")
    compute_field = fields.Boolean(string="check field", compute='_get_user')


    def SendMail(self,mailsto,Subject,body):
	import smtplib
	from email.MIMEMultipart import MIMEMultipart
	from email.MIMEText import MIMEText
	mail_server = self.env['ir.mail_server'].search([["name","=","localhost"]])

#self.pool.get('ir.mail_server').browse(cr,uid,self.pool.get('ir.mail_server').search(cr, uid, [('name','=','Contencia-SOFT')])[0])
	msg = MIMEMultipart()
	msg.set_charset("utf-8")
	msg['From']    = mail_server.smtp_user
	msg['To']      = mailsto
	msg['Subject'] = Subject
	body = body
	msg.attach(MIMEText(body, 'html'))
	server = smtplib.SMTP(mail_server.smtp_host, 587)
	server.ehlo()
	server.starttls()
	server.ehlo()
	server.login(mail_server.smtp_user,mail_server.smtp_pass)
	text = msg.as_string()
	server.sendmail(mail_server.smtp_host, mailsto.split(','), text)
	return True

    @api.model
    def create(self, vals):
	owner = self.env["res.partner"].search([['id', '=', int(vals.get('owner_id'))]])
	if vals.get("montant_affecter",False) > owner.solde_compte:
	    raise ValidationError(_("------------ Le solde affecté à la carte > au solde du compte client  ----------"))
	if vals.get("montant_affecter",False) and vals.get("montant_affecter",False)>0:
		vals['solde'] = vals.get("montant_affecter",False)
		vals['montant_affecter'] = 0
	elif vals.get("montant_affecter",False)<0:
		raise ValidationError(_("------------solde affecter au carte > 0 ----------"))

	obj = self.env['gakd.customer'].browse(vals['libelle'])
	#print obj.mobile
	obj["partner_id"] = vals['owner_id']

	today = fields.datetime.now().date()
	vals['date_expiration'] = today.replace(year=today.year + 2)
        vals['num_serie'] = self._default_serie()
	codepin = randint(1111, 9999)
        vals['code_pin'] = codepin
	qrcode = self._getQrCode(str(vals.get("num_serie"))+'<:>AKAD<:>'+str(vals.get("date_expiration")))
	if qrcode=="":
		raise ValidationError(_("Merci de contacter le prestataire pour la génération du QRCODE"))
	vals['qrcode'] = qrcode
	vals['click_create'] = True
	carteResult = super(gakd_carte, self).create(vals)
	
	
	body = """Merci de noter le code PIN de la carte : """+ str(codepin)
	Subject = "JNP BENIN"
	mobile = obj.mobile
	send(mobile,body)
        # api_instance = swagger_client.SmsApi()
        # smsrequest = swagger_client.SmsUniqueRequest("eb717305f6545b5ea0d57a7aba06dc54",None, None, body,mobile,Subject,None,None,None, None, None) # SMSRequest | sms request
        # try:
        #     api_response = api_instance.send_sms(smsrequest)
        # except ApiException as e:
        #     print ("Exception when calling SmsApi->send_sms: %s\n" % e)
	
        return carteResult





    @api.multi
    def write(self, vals):
	for carte in self:
		acteurName = self.env.user.name
		logs = []
		if vals.get("acteur_name",False):
			acteurName = vals.get("acteur_name",False)
		crt = self.search([['id', '=', carte.id]])
		if vals.get("libelle",False):
			logs.append(self.log(self.id,acteurName,self.libelle,vals.get("libelle",False),"Libellé"))
			
			
		if vals.get("montant_affecter",False):
			solde = crt.solde
			owner = crt.owner_id
			if vals.get("montant_affecter",False) <0:
			    raise ValidationError(_("------------ Le solde affecté à la carte doit être superieru à 0  ----------"))
			if vals.get("montant_affecter",False) > owner.solde_compte:
			    raise ValidationError(_("------------ Le solde affecté à la carte > au solde du compte client  ----------"))
			if vals.get("montant_affecter",False) > owner.solde_compte_hide:
				raise ValidationError(_("------------ Le solde affecté à la carte > au solde du compte client  ----------"))
			else:
				owner.solde_compte_hide = owner.solde_compte_hide - vals.get("montant_affecter",False)
			
			vals['solde'] = solde + vals.get("montant_affecter",False)
			if vals.get("montant_affecter"):
				vals_field = {
				"client_id":self.owner_id.id,
				"carte_id":self.id,
				"agent_id" : self.env.user.id,
				"old_version" : solde,
				"montant" : vals.get("montant_affecter",False),
				"new_version" : solde + vals.get("montant_affecter",False),
				}
				self.env["gakd.historique_recharge_carte"].create(vals_field)
			vals['montant_affecter'] = 0
			logs.append(self.log(self.id,acteurName,str(self.solde),str(vals.get("solde",False)),"Solde"))
			
			if vals.get('solde',False)<0:
				raise ValidationError(_("Le solde de la carte devrait rester superieur ou égal à zéro"))
		if logs:
			vals['log_ids'] = logs
	
	return super(gakd_carte, self).write(vals)


    def log(self,ObjtId,acteurName,old_version,new_version,champ):
	return (0,0, {
				"carte_id" : ObjtId,
				"acteur_name" : acteurName,
				"old_version" : old_version,
				"new_version" : new_version,
				"champ" : champ,
			})



class gakd_carte_consommation(models.Model):
    _name = 'gakd.carte.consommation'

    _rec_name = 'carte_id'
    _description = ''

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente", required=True)
    agent_id = fields.Many2one("gakd.agent",string="Agent")
    carte_id = fields.Many2one("gakd.carte",string="Carte")
    product_ids = fields.Many2one("product.product",string="Produit", required=True)
    quantite = fields.Float(string='Quantité')
    quantite_remise = fields.Float(string='Quantité remisé')
    montant = fields.Float(string='Montant')
    shift_id = fields.Many2one("gakd.carte.shift",string="Shift")
    valider_par_agent = fields.Boolean(string ="Validé par l'agent")
    valider_par_gerant = fields.Boolean(string ="Validé par le gérant")
    verse = fields.Boolean(string ="verser par le gérant", default=False)
    state = fields.Selection([('valider','Valider'),('annuler','Annuler')] , string="Statut" , default="valider")
    hest_ben_ids = fields.One2many("gakd.historique_ben","hes_id",string="Historiques")
    bonus_conso = fields.Float(string="Bonus consommateur")
    total_bonus = fields.Float(string="Total bonus")
    ticket_id = fields.Many2one("gakd.ticket_valeur",string="Ticket")
    ticket_ids = fields.One2many("gakd.ticket_valeur","transaction_id",string="Ticket")
    type_vente = fields.Selection([('Vente au comptant','Vente au comptant'),('Vente par JNP PASS','Vente par JNP PASS'),('Vente par TV','Vente par TV'),('Vente par MOMO PAY','Vente par MOMO PAY'),('Vente par carte tampo','Vente par carte tampo'),('Vente par MOOV PAY','Vente par MOOV PAY')] , string="Type de vente")
    transaction_lines = fields.One2many('gakd.transaction_line','transaction_id',string="Lines")
    typevente=fields.Many2one("gakd.type_vente")
    nbtransaction=fields.Integer(string="Nombre de transaction")

    @api.model
    def create(self, vals):
        if vals.get("type_vente") == "Vente par JNP PASS":
            catre = self.env["gakd.carte"].search([['id', '=', int(vals["carte_id"])]])
            print catre
            print '-------------------------------'
            print vals
            print '==============================='
            ben_hest = []
            vals["bonus_conso"] = float(float(float(catre.libelle.taux) * float(vals["montant"]))/100)
            #vals["total_bonus"] = float(vals["bonus_conso"])+
            res = super(gakd_carte_consommation, self).create(vals)
            print 'yyyyyyyyyyyyyyyffffffffffffffffyyyyyyyyyyyyyyyy'
            print catre.owner_id
            print catre.owner_id.beneficiaires_ids
            total = 0
            if catre.owner_id.beneficiaires_ids:
                for ben in catre.owner_id.beneficiaires_ids:
                    print ben.name
                    mnt = float(float(float(ben.taux) * float(vals["montant"]))/100)
                    total += mnt
                    print '*********************'
                    print mnt
                    print ben
                    historique_vals ={
                        'hes_id':res.id,
                        'beneficiaire_id':ben.id,
                        'mnt':mnt
                    }
                    print historique_vals
                    cal = self.env["gakd.historique_ben"].create(historique_vals)
                    print cal
                    ben_hest.append(cal)
            transaction = self.env["gakd.carte.consommation"].search([['id', '=', int(res)]])
            transaction.total_bonus = float(float(total) + float(transaction.bonus_conso))
        else:
            res = super(gakd_carte_consommation, self).create(vals)
            #res = True
        # print ben_hest
        # vals["hest_ben_ids"] = (0, 0, ben_hest)
        return res

class gakd_carte_shift(models.Model):
    _name = 'gakd.carte.shift'

    transaction_ids = fields.One2many("gakd.carte.consommation","shift_id",string="Agents")
    libele = fields.Char(string="Libele")
    agent_id = fields.Many2one("gakd.agent",string="Agent")
    pointVente_id = fields.Many2one("gakd.point.vente",string="Point Vente")
    gerant_id = fields.Many2one("gakd.agent",string="Gerant")
    total = fields.Char(string="Total")
    

    @api.model
    def create(self, vals):
        print 'vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaals'
        print vals
        
	return super(gakd_carte_shift, self).create(vals)


class gakd_log(models.Model):
    _name = 'gakd.log'

    _rec_name = 'carte_id'
    _description = ''

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )

    acteur_name = fields.Char(string="Acteur")
    client_id = fields.Many2one("res.partner",string="Client")
    carte_id = fields.Many2one("gakd.carte",string="Carte")
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente")
    agent_id = fields.Many2one("gakd.agent",string="Agent")
    old_version = fields.Char(string="Ancienne valeur")
    new_version = fields.Char(string="Nouvelle valeur")
    champ = fields.Char(string="Type modification")


class gakd_point_vente(models.Model):
    _name = 'gakd.point.vente'

    _rec_name = 'libelle'
    _description = ''

    @api.multi
    def setToActive(self):
        self.ensure_one()
        self.state="activee"
	self.date_activation=fields.datetime.now()

    @api.multi
    def setPayment(self):
        print 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj'
        pos=self.env["pos.config"].search([('company_id','=',1)])
        for a in pos:
            a.write({
                'journal_ids':[(4,181)]
            })
            print 'doooone'
 
    @api.multi
    def setToSuspendu(self):
        self.ensure_one()
        self.state="suspendu"
	self.date_suspension=fields.datetime.now()

    @api.multi
    def setToAnnule(self):
        self.ensure_one()
        self.state="annule"
	self.date_annulation=fields.datetime.now()

    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    libelle = fields.Char(string="Code")
    name = fields.Char(string="Point de vente")
    adress = fields.Text(string="Adresse")
    historique_ids = fields.One2many("gakd.carte.consommation","carte_id",string="Historiques", readonly=True)
    agent_ids = fields.One2many("gakd.agent","point_vente_id",string="Agents")
    state = fields.Selection([('brouillon','Brouillon'),('activee','Activé'),('suspendu','Suspendu'),('annule','Annulé')] , string="Statut" , default="activee")
    type = fields.Selection([('avec','Avec Boutique'),('sans','Sans Boutique') ] , string="Boutique" , default="avec")
    date_activation = fields.Datetime("Date d'activation", readonly=True)
    date_suspension = fields.Datetime("Date de suspension", readonly=True)
    date_annulation = fields.Datetime("Date d'annulation", readonly=True)
    log_ids = fields.One2many("gakd.log","point_vente_id",string="Historiques", readonly=True)
    transaction_ids = fields.One2many("gakd.carte.consommation","point_vente_id",string="Historiques", readonly=True)
    #emplacement_id = fields.Many2one("stock.location",string="Location")
    user_point_vente = fields.Many2one("res.users",string="User")
    account_analytic_id = fields.Many2one('account.analytic.account', string='Compte annalytic')
    journal_id = fields.Many2one(comodel_name='account.journal', string='MOMO PAY',required=True)

    @api.multi
    def write(self, vals):
	for carte in self:
		logs = []
		acteurName = self.env.user.name
		if vals.get("acteur_name",False):
			acteurName = vals.get("acteur_name",False)
		
		if vals.get("libelle",False):
		   logs.append(self.log(self.id,acteurName,self.libelle,vals.get("libelle",False),"Libellé"))
		
		if vals.get("adress",False):
		   logs.append(self.log(self.id,acteurName,self.adress,vals.get("adress",False),"Adresse"))

		if vals.get("type",False):
		   logs.append(self.log(self.id,acteurName,self.type,vals.get("type",False),"Type"))


		if logs:
			vals['log_ids'] = logs
	
	return super(gakd_point_vente, self).write(vals)

    def log(self,ObjtId,acteurName,old_version,new_version,champ):
	return (0,0, {
				"point_vente_id" : ObjtId,
				"acteur_name" : acteurName,
				"old_version" : old_version,
				"new_version" : new_version,
				"champ" : champ,
			})


class gakd_agent(models.Model):
    _name = 'gakd.agent'

    _rec_name = 'name'
    _description = ''


    @api.multi
    def setToActive(self):
        self.ensure_one()
        self.state="activee"
	self.date_activation=fields.datetime.now()

    @api.multi
    def setToSuspendu(self):
        self.ensure_one()
        self.state="suspendu"
	self.date_suspension=fields.datetime.now()

    @api.multi
    def setToAnnule(self):
        self.ensure_one()
        self.state="annule"
	self.date_annulation=fields.datetime.now()

    @api.multi
    def setCodePine(self):
        self.ensure_one()
        password = randint(1111, 9999)
        self.password = password
        body = """Merci de noter votre code PIN : """+ str(password)
        Subject = "JNP BENIN"
        mobile = self.telephone
        send(mobile,body)

        # Liste_transaction = self.env["gakd.carte.consommation"].search([('verse','=',False)])
        # count = self.env["gakd.carte.consommation"].search_count([('verse','=',False)])
        # i=0
        # for transaction in Liste_transaction:
        #     i=i+1
        #     #print str(i)+str('/')+str(count)
        #     logging.info('COOOOOteur')
        #     logging.info(str(i) + str('/')+ str(count))
        #     print transaction.type_vente
        #     if transaction.type_vente == "Vente par TV":
        #         if transaction.ticket_id:
        #             tvs = [(4,int(transaction.ticket_id.id))]
        #             type_vente = [(0,0,{'type_vente':transaction.type_vente,'montant':float(transaction.montant)})]
        #             print type_vente
        #             print tvs
        #             transaction.ticket_ids = tvs
        #             if len(transaction.transaction_lines) == 0:
        #                 transaction.transaction_lines =  type_vente

            # if transaction.type_vente == "Vente par JNP PASS":
            #     print 555555555555555
            #     if len(transaction.transaction_lines) == 0:
            #         type_vente = [(0,0,{'type_vente':transaction.type_vente,'carte_id':transaction.carte_id,'montant':float(transaction.montant)})]
            #         transaction.transaction_lines =   type_vente
            # if transaction.type_vente == "Vente par MONO PAY":
            #     print 555555555555555
            #     if len(transaction.transaction_lines) == 0:
            #         type_vente = [(0,0,{'type_vente':transaction.type_vente,'montant':float(transaction.montant)})]
            #         transaction.transaction_lines =  type_vente

        # trs = self.env['gakd.carte.consommation'].search([('type_vente','=',"Vente par TV")])
        # count = self.env['gakd.carte.consommation'].search_count([('type_vente','=',"Vente par TV")])
        # i=0
        # for tr in trs :
        #     i=i+1
        #     print str(i)+str('/')+str(count)
        #     tr.ticket_ids.sudo().write({
        #         'etat':'util'
        #     })
        #     tr.ticket_id.sudo().write({
        #         'etat':'util'
        #     })

        # Liste_transaction = self.env["gakd.carte.consommation"].search([('state','=','annuler')])
        # count = self.env["gakd.carte.consommation"].search_count([('state','=','annuler')])
        # i=0
        # for transaction in Liste_transaction:
        #     i=i+1
        #     print str(i)+str('/')+str(count)
        #     stock_location_id = self.env['stock.location'].search([('point_vente_id','=', int(transaction.point_vente_id) )])
        #     valsa={
        #         'qty':transaction.quantite,
        #         'company_id':1,
        #         'location_id':9,
        #         'product_id':transaction.product_ids.id,
        #         'in_date':transaction.create_date,


        #     }
        #     sq=self.env['stock.quant'].create(valsa)
        #     vals = {
        #         "product_id":transaction.product_ids.id,
        #         "location_id":9,
        #         "location_dest_id":stock_location_id.id,
        #         "product_uom_qty":transaction.quantite,
        #         "date":transaction.create_date,
        #         "company_id":1,
        #         "name":"prouit annuler",
        #         "product_uom":transaction.product_ids.uom_id.id,
        #         'history_ids':sq.id,
        #         'state':'done',
        #     }
        #     sm=self.env['stock.move'].create(vals)
        #     sq.write({
        #         'history_ids': [(6, 0, [sm.id,5])]
        #     })
        # api_instance = swagger_client.SmsApi()
        # smsrequest = swagger_client.SmsUniqueRequest("eb717305f6545b5ea0d57a7aba06dc54",None, None, body,mobile,Subject,None,None,None, None, None) # SMSRequest | sms request
        # try:
        #     api_response = api_instance.send_sms(smsrequest)
        # except ApiException as e:
        #     print ("Exception when calling SmsApi->send_sms: %s\n" % e)

    _sql_constraints = [('matricule_unique', 'unique(matricule)', 'Agent déja existe')]
    #company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id )
    name = fields.Char(string="Nom")
    fonction = fields.Selection([('Gerant','Gérant'),('Pompiste','Pompiste'),('Boutiquier','Boutiquier'),('Chauffeur','Chauffeur'),('Autre','Autre')] , string="Fonction" , default="Pompiste")
    autre_fonction = fields.Char(string="Autre fonction")
    matricule = fields.Char(string="Matricule",required=True)
    password = fields.Char(string="password")
    telephone = fields.Char(string="Téléphone",required=True)
    mail = fields.Char(string="Email")
    adress = fields.Text(string="Adresse")
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente")
    state = fields.Selection([('activee','Activé'),('suspendu','Suspendu'),('annule','Annulée') ] , string="Statut" , default="activee")
    date_activation = fields.Datetime("Date d'activation", readonly=True)
    date_suspension = fields.Datetime("Date de suspension", readonly=True)
    date_annulation = fields.Datetime("Date d'annulation", readonly=True)
    log_ids = fields.One2many("gakd.log","agent_id",string="Historiques", readonly=True)
    transaction_ids = fields.One2many("gakd.carte.consommation","agent_id",string="Historiques", readonly=True)
    camion_id = fields.Many2one('fleet.vehicle', string='Camion')

    @api.model
    def create(self, vals):
        password = randint(1111, 9999)
        vals['password'] = password
        body = """Bonjour,
        Veuillez noter votre code PIN : """+ str(password)
        Subject = "JNP BENIN"
        mobile = self.telephone
        #send(mobile,body)
        # api_instance = swagger_client.SmsApi()
        # smsrequest = swagger_client.SmsUniqueRequest("eb717305f6545b5ea0d57a7aba06dc54",None, None, body,mobile,Subject,None,None,None, None, None) # SMSRequest | sms request
        # try:
        #     api_response = api_instance.send_sms(smsrequest)
        # except ApiException as e:
        #     print ("Exception when calling SmsApi->send_sms: %s\n" % e)
        
	return super(gakd_agent, self).create(vals)


    @api.multi
    def write(self, vals):
	for carte in self:
		logs = []
		acteurName = self.env.user.name
		if vals.get("acteur_name",False):
			acteurName = vals.get("acteur_name",False)
		
		if vals.get("name",False):
		   logs.append(self.log(self.id,acteurName,self.name,vals.get("name",False),"Nom"))

		if vals.get("fonction",False):
		   logs.append(self.log(self.id,acteurName,self.fonction,vals.get("fonction",False),"Fonction"))

		if vals.get("matricule",False):
		   logs.append(self.log(self.id,acteurName,self.matricule,vals.get("matricule",False),"Matricule"))

		
		if vals.get("mail",False):
		   logs.append(self.log(self.id,acteurName,self.mail,vals.get("mail",False),"Email"))

		
		if vals.get("adress",False):
		   logs.append(self.log(self.id,acteurName,self.adress,vals.get("adress",False),"Adresse"))


		if logs:
			vals['log_ids'] = logs
	
	return super(gakd_agent, self).write(vals)

    def log(self,ObjtId,acteurName,old_version,new_version,champ):
	return (0,0, {
				"agent_id" : ObjtId,
				"acteur_name" : acteurName,
				"old_version" : old_version,
				"new_version" : new_version,
				"champ" : champ,
			})


class ModuleName(models.Model):
    _inherit = 'account.account'

    is_true = fields.Boolean(string='is True')

class TransactionLine(models.Model) :
    _name="gakd.transaction_line"

    typevente=fields.Many2one("gakd.type_vente")
    transaction_id = fields.Many2one('gakd.carte.consommation',string="Transaction",delegate=True)
    type_vente = fields.Char(string="Type de vente",default="Vente par JNP PASS")
    montant = fields.Float(string="Montant")
    carte_id = fields.Many2one("gakd.carte",string="Carte")
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente", required=True,compute="getPointVente")

    @api.multi
    def getPointVente(self):
        for v in self:
            v.point_vente_id = v.transaction_id.point_vente_id.id