# -*- coding: utf-8 -*-

import xlwt
import cStringIO
import base64
import time
import datetime
from datetime import datetime
from datetime import timedelta

from openerp import api, fields, models

class AccountReportPartnerLedger(models.TransientModel):
    _inherit = "account.report.partner.ledger"

    @api.multi
    def check_report_excel(self):
        res = super(AccountReportPartnerLedger, self).check_report()
        data = res['data']
        data['computed'] = {}

        obj_partner = self.env['res.partner']
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        data['computed']['move_state'] = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            data['computed']['move_state'] = ['posted']
        result_selection = data['form'].get('result_selection', 'customer')
        if result_selection == 'supplier':
            data['computed']['ACCOUNT_TYPE'] = ['payable']
        elif result_selection == 'customer':
            data['computed']['ACCOUNT_TYPE'] = ['receivable']
        else:
            data['computed']['ACCOUNT_TYPE'] = ['payable', 'receivable']

        self.env.cr.execute("""
            SELECT a.id
            FROM account_account a
            WHERE a.internal_type IN %s
            AND NOT a.deprecated""", (tuple(data['computed']['ACCOUNT_TYPE']),))
        data['computed']['account_ids'] = [a for (a,) in self.env.cr.fetchall()]
        
        params = [tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".reconciled = false '
        query = """
            SELECT DISTINCT "account_move_line".partner_id
            FROM """ + query_get_data[0] + """, account_account AS account, account_move AS am
            WHERE "account_move_line".partner_id IS NOT NULL
                AND "account_move_line".account_id = account.id
                AND am.id = "account_move_line".move_id
                AND am.state IN %s
                AND "account_move_line".account_id IN %s
                AND NOT account.deprecated
                AND """ + query_get_data[1] + reconcile_clause
        self.env.cr.execute(query, tuple(params))
        partner_ids = [res['partner_id'] for res in self.env.cr.dictfetchall()]
        partners = obj_partner.browse(partner_ids)
        partners = sorted(partners, key=lambda x: (x.ref, x.name))
        datas  = {
            'data': data, 
            'docs': partners,
        }
        return self._print_report_excel(datas)
        
    @api.multi
    def _print_report_excel(self, datas):
        workbook = xlwt.Workbook()
        title_style_comp = xlwt.easyxf('align: horiz center ; font: name Times New Roman,bold off, italic off, height 450')
        title_style_comp_left = xlwt.easyxf('align: horiz left ; font: name Times New Roman,bold off, italic off, height 450')
        title_style = xlwt.easyxf('align: horiz center ;font: name Times New Roman,bold off, italic off, height 350')
        title_style2 = xlwt.easyxf('font: name Times New Roman, height 200')
        title_style1 = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190; borders: top double, bottom double, left double, right double;')
        title_style1_table_head = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head1 = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200')
        title_style1_consultant = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head_center = xlwt.easyxf('align: horiz center ; font: name Times New Roman,bold on, italic off, height 190; borders: top thick, bottom thick, left thick, right thick;')

        title_style1_table_data = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold on, italic off, height 190')
        title_style1_table_data_sub = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190')
        title_style1_table_data_sub_amount = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold off, italic off, height 190')
        title_style1_table_data_sub_balance = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold off, italic off, height 190')
        gl_report_obj = self.env['report.account.report_partnerledger']
        sheet_name = 'Partner Ledger'
        sheet = workbook.add_sheet(sheet_name)
        
        sheet.write_merge(0, 1, 0, 6, 'Partner Ledger', title_style_comp_left)
        sheet.write(0, 8, 'Printing Date: '+datetime.now().strftime('%Y-%m-%d'), title_style1_table_head1)
        
        comp_id = self.env.user.company_id
        currency_id = comp_id.currency_id
        sheet.write(3, 0, 'Company',title_style1_table_head1)
        sheet.write(4, 0, comp_id.name, title_style1_table_data_sub)
        
        if self.date_from:
            sheet.write(3, 2, 'Date From',title_style1_table_head1)
            sheet.write(3, 3, self.date_from, title_style1_table_data_sub)
        if self.date_to:
            sheet.write(4, 2, 'Date to',title_style1_table_head1)
            sheet.write(4, 3, self.date_to, title_style1_table_data_sub)
        
        sheet.write(3, 5, 'Target Moves',title_style1_table_head1)
        if self.target_move == 'all':
            column = sheet.col(8)
            column.width = 256 * 18
            sheet.write(4, 5, 'All Entries',title_style1_table_data_sub)
        if self.target_move == 'posted':
            column = sheet.col(8)
            column.width = 256 * 18
            sheet.write(4, 5, 'All Posted Entries',title_style1_table_data_sub)
        
        sheet.write(8, 0, 'Date',title_style1_table_head)
        sheet.write(8, 1, 'JRNL',title_style1_table_head)
        sheet.write(8, 2, 'Account',title_style1_table_head)
        sheet.write(8, 3, 'Ref',title_style1_table_head)
        sheet.write(8, 4, 'Debit',title_style1_table_head)
        sheet.write(8, 5, 'Credit',title_style1_table_head)
        sheet.write(8, 6, 'Balance',title_style1_table_head)
        if self.amount_currency:
            sheet.write(8, 7, 'Currency',title_style1_table_head)
        
        roww = 9
        for partner in datas['docs']:
            ref = partner.ref if partner.ref else ''
            sheet.write(roww, 0, str(ref) + '-'+ partner.name, title_style1_table_head1)
            sheet.write(roww, 4, str(gl_report_obj._sum_partner(datas['data'], partner, 'debit'))+' '+currency_id.symbol, title_style1_table_data)
            sheet.write(roww, 5, str(gl_report_obj._sum_partner(datas['data'], partner, 'credit'))+' '+currency_id.symbol, title_style1_table_data)
            sheet.write(roww, 6, str(gl_report_obj._sum_partner(datas['data'], partner, 'debit - credit'))+' '+currency_id.symbol, title_style1_table_data)
            
            row_data = roww+1
            for line in gl_report_obj._lines(datas['data'], partner):
                sheet.write(row_data, 0, line['date'], title_style1_table_data_sub)
                column = sheet.col(0)
                column.width = 256 * 25
                #currency =  self.env['res.currency'].browse(line['currency_id']).symbol if line['currency_id'] else ''
                currency = line['currency_id'].symbol if line['currency_id'] else ''
                sheet.write(row_data, 1, line['code'], title_style1_table_data_sub)
                sheet.write(row_data, 2, line['a_code'], title_style1_table_data_sub)
                sheet.write(row_data, 3, line['displayed_name'], title_style1_table_data_sub)
                sheet.write(row_data, 4, str(line['debit'])+' '+currency_id.symbol, title_style1_table_data_sub_amount)
                sheet.write(row_data, 5, str(line['credit'])+' '+currency_id.symbol, title_style1_table_data_sub_amount)
                sheet.write(row_data, 6, str(line['progress'])+' '+currency_id.symbol, title_style1_table_data_sub_amount)
                if self.amount_currency:
                    sheet.write(row_data, 7, str(line['amount_currency'])+' '+currency, title_style1_table_data_sub_amount)
                
                row_data = row_data + 1
            roww = row_data + 3
            
        stream = cStringIO.StringIO()
        workbook.save(stream)
        attach_id = self.env['accounting.pl.report.output.wizard'].create({'name':'Partner_Ledger.xls', 'xls_output': base64.encodestring(stream.getvalue())})
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'accounting.pl.report.output.wizard',
            'res_id':attach_id.id,
            'type': 'ir.actions.act_window',
            'target':'new'
        }

class AccountingPLReportOutputWizard(models.Model):
    _name = 'accounting.pl.report.output.wizard'
    _description = 'Wizard to store the Excel output'

    xls_output = fields.Binary(string='Excel Output')
    name = fields.Char(string='File Name', help='Save report as .xls format', default='Partner_Ledger.xls')

#vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
