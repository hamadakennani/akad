# -*- coding: utf-8 -*-
from odoo import fields, models,api

class chargeur(models.Model):

    _name="gakd.chargeur"
    _rec_name='access'

    montant_plafond = fields.Float(string='Montant plafond')
    rest=fields.Float(string='Montant actuel')
    sous_chargeur = fields.One2many("gakd.sous_chargeur","superv",string="Les caissiers")
    access = fields.Many2one("res.users",string="Trésorier",required=True)
    historique = fields.One2many(comodel_name='gakd.historique', inverse_name='chargeur', string='Historique')
    
    montant_plafond_tv = fields.Float(string='Montant plafond')


    @api.onchange('access')
    def _getCharger(self):
     res = {}
     ids = []
     chargeur=self.env["gakd.chargeur"].search([])
     for c in chargeur:
         ids.append(c.access.id)
     res['domain'] = {'access': [('id', 'not in', ids),('groups_id','=',self.env.ref('gakd.group_gakd_chargeur').id)]}
     print res
     return res

    @api.model
    def create(self, vals):
     vals['rest']=vals['montant_plafond']
     p = super(chargeur, self).create(vals)
    #  chargeur_group = self.env.ref('gakd.group_gakd_chargeur')
    #  chargeur_group.write({'users': [(4, vals['access'])]})    
     return p

    @api.multi
    def write(self,vals):
        print ' whriteeeeeeeeeeeeeeee,'
        print vals
        po=self.env["gakd.sous_chargeur"].search([('superv','=',self.id)])
        cl=super(chargeur,self).write(vals)
        if 'montant_plafond' in vals.keys():
            po.write({
                'montant_super':vals['montant_plafond']
            })
            print 'UPDATED ------------------->>'
        if 'montant_plafond_tv' in vals.keys():
            po.write({
                'montant_super_tv':vals['montant_plafond_tv']
            })
            print 'UPDATED TV ------------------->>'    
        return cl
	
    # @api.multi
    # def write(self,vals):
    #     print ' whriteeeeeeeeeeeeeeee,'
    #     print vals
        
    #     return super(chargeur,self).write(vals)

	#  cl = self.env["res.users"].create({
	# 			'display_name':vals.get('name'),
	# 			'password':1111,
	# 			'name':vals['name'],
	# 			'signup_token':vals.get('email'),
	# 			'email':vals.get('email'),
	# 			'owner_id':False,
	# 			"login":vals.get('email'),
	# 			'active':True,
	# 			'share':False
	# 	})
	#  p.write({

	# 	'access':cl.id
		
	#      })
	#  print cl.id
	#  print 'HHHHHHHHHHHHHHHHHHHHuuu'
	#  print p.id
	




