# -*- coding: utf-8 -*-
###################################################################################
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################
import json
from odoo import models, api


class ResUsers(models.Model):
    _inherit = 'res.users'

    @api.model
    def get_access_right_report_data(self):
        res = {}
        try:
            category_data = {}
            # Add groups
            print self.env['ir.module.category'].search([('id','=',52),('name','!=','Clients')])
            for category in self.env['ir.module.category'].search([('id','=',52),('name','!=','Clients')]):
                print category.id
                for group in self.env['res.groups'].search([('id','!=',54)]):
                    if group.category_id.id == category.id:
                        if category.id in category_data:
                            category_data[category.id].append(group.id)
                        else:
                            category_data[category.id] = [group.id]
            # Add other groups
            for group in self.env['res.groups'].search([('id','!=',54)]):
                if not group.category_id:
                    if False in category_data:
                        category_data[False].append(group.id)
                    else:
                        category_data[False] = [group.id]
            # Add Sequence
            data = []
            for category_id, group_ids in category_data.items():
                if category_id == 52:
                    category = self.env['ir.module.category'].browse(category_id)
                    data.append({'category_id': category,
                                'group_ids': self.env['res.groups'].browse(group_ids).ids,
                                'sequence': category.sequence or 1000})
            data_sorted = sorted(data, key=lambda x: x['sequence'])

            head = []
            groupe_name = ''
            datae  = []
            for each in data_sorted:
                for x in each['group_ids']:
                    if self.env['res.groups'].browse(x).name == 'Achat Niveau 2':
                        datae.append('Achat')
                    else:
                        datae.append(self.env['res.groups'].browse(x).name)
                print datae
                head.append({'category': each['category_id'].name or 'Undefined',
                             'group_count': len(each['group_ids']),
                            'group_names': datae})

            res['head'] = head
            print '00000000000000000000000000'
            print head
            print '00000000000000000000000000'
            res['records'] = []
            i=0
            for user in self.env['res.users'].search([], order='share,name'):
                if not user.has_group('gakd.group_gakd_client') and user.has_group('gakd.group_gakd_manager'):
                    i = i+1
                    print str(i)+str('/')+str(len(self.env['res.users'].search([], order='share,name')))
                    row = [{'user_id': user.id,
                            'name': user.name,
                            'login': user.login,
                            'company': user.company_id.name,
                            'allowed_companies': ','.join([x.name for x in user.company_ids]) or 'None',
                            'image': str(user.image and user.image.decode("utf-8")),
                            'form_view_id': self.env.ref('base.view_users_form').id,
                            'current_uid': self._uid,
                            'row-user-class': 'row-user-internal' if not user.share else 'row-user-external',
                            }, []]
                    j=0
                    for category in data_sorted:
                        j+= 1
                        print '#######################'
                        print str(j)+str('/')+str(len(data_sorted))
                        print '#######################'
                        print category
                        for group in category['group_ids']:
                            group_id = self.env['res.groups'].browse(group)
                            row[1].append(user.id in group_id.users.ids)
                    res['records'].append(row)
        except Exception as e:
            res['exception'] = str(e)
        return json.dumps(res)

