# -*- coding: utf-8 -*-

from odoo import fields, models,api


class AccountBalanceReport(models.TransientModel):
    _inherit = 'account.balance.report'
    _description = 'Trial Balance Report'

    by_group = fields.Boolean(string='Par groupe')
    
    @api.multi
    def check_report(self):
        super(AccountBalanceReport,self)
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_from', 'date_to', 'journal_ids', 'target_move','by_group'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang') or 'en_US')
        print '---------------------------------------------------------------------'
        print '9999999999999999999999999999999999999999'
        return self._print_report(data)