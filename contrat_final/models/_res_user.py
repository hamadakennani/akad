# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint




class capcontrat_users_access(models.Model):
	_name="capcontrat.users.access"
	_rec_name='users_id'
	_order='create_date desc'
	contrat_id = fields.Many2one('capcontrat.projets',string='contrat')
	curuser = fields.Integer("current user",default=lambda self: self.env.user)
	users_id=fields.Many2one('res.users',string='utilisateur',required=True)
	
