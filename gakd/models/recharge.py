# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import  ValidationError
class recharge(models.Model):

    _name="gakd.recharge"
    
    montant_recharge = fields.Float(string='Montant plafond JNP',required=True)
    carte = fields.Many2one("gakd.carte",string="Carte",required=True)
    agent = fields.Many2one("gakd.agent",string="Agent",required=True)
    state = fields.Selection([('Valide','Validé'),('annule','Annulé'),('demande annulation','Demande d\'annulation')],string="Statut",required=True,default='Valide')