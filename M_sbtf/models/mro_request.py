# -- coding: utf-8 --

import time
from odoo import api, fields, models, _
from odoo import netsvc
import odoo.addons.decimal_precision as dp


class mro_request(models.Model):

    _name = 'mro.signalisation'

    name = fields.Char('Reference', size=64)
    state = fields.Selection([('Signalisation','Signalisation'),('Diagnostic','Diagnostic')], 'Status',default='Signalisation')
    vehicle_id=fields.Many2one('fleet.vehicle',"Camion",required=True) 
    garage_id=fields.Many2one('mro.garage',"Garage" ,required=True)
    date_panne=fields.Datetime("Date de la panne")
    lieu=fields.Char("Lieu")
    Chauffeur  = fields.Many2one('hr.employee','Chauffeur',required=True)
    voyage_id=fields.Many2one('tms.travel',"Voyage")
    cause = fields.Char('Cause')
    description = fields.Text('Description de la panne')
    requested_date = fields.Datetime('Date de signalisation', default=time.strftime('%Y-%m-%d %H:%M:%S'))
    execution_date = fields.Datetime('Date de réparation demandée',default=time.strftime('%Y-%m-%d %H:%M:%S'))
    panne = fields.Boolean('Panne')
    
    @api.onchange('vehicle_id')
    def get_info_vehicle(self):
        camion=self.env['fleet.vehicle'].search([('id', '=', self.vehicle_id.id)])
        self.Chauffeur=camion.employee_id.id
        data=[]
        for line in camion.voyage_camion:
            voy=self.env['tms.travel'].search([('id', '=', line.id)])
            data.append(voy.id)        
        return {'domain':{'voyage_id':[('id','in' ,data)]}}

    def diagnostiquer(self):
        order = self.env['mro.diagnostic']
        order_id = False
        for request in self:
            order_id = order.create({
                'date_planned':request.requested_date,
                'date_scheduled':request.requested_date,
                'vehicle_id':request.vehicle_id.id,
                'date_execution':request.requested_date,
                'origin': request.name,
                'state': 'Diagnostic',
                'description': request.cause,
                'problem_description': request.description,
            })
        self.write({'state': 'Diagnostic'})
        return order_id.id
    @api.model
    def create(self, vals):
        if vals.get('name','/')=='/':
            vals['name'] = self.env['ir.sequence'].next_by_code('mro.signalisation') or '/'
        return super(mro_request, self).create(vals)