# -- coding: utf-8 --

import time
from odoo import api, fields, models, _
from odoo import netsvc
import odoo.addons.decimal_precision as dp
from openerp.exceptions import ValidationError


class mro_order(models.Model):

    _name = 'mro.diagnostic'

    def _get_available_parts(self):
        for order in self:
            line_ids = []
            available_line_ids = []
            done_line_ids = []
            if order.procurement_group_id:
                for procurement in order.procurement_group_id.procurement_ids:
                    line_ids += [move.id for move in procurement.move_ids if move.location_dest_id.id == order.garage_id.location_id.id]
                    available_line_ids += [move.id for move in procurement.move_ids if move.location_dest_id.id == order.garage_id.location_id.id and move.state == 'assigned']
                    done_line_ids += [move.id for move in procurement.move_ids if move.location_dest_id.id == order.garage_id.location_id.id and move.state == 'done']
            order.parts_ready_lines = line_ids
            order.parts_move_lines = available_line_ids
            order.parts_moved_lines = done_line_ids

    name = fields.Char('Reference', size=64)
    origin = fields.Char('Source Document')
    state = fields.Selection([('Diagnostic','En diagnostic'),('Pièces en attente','Pièces en attente'),('Prêt pour la réparation','Prêt pour la réparation'),('Fait','Fait')], 'Status', readonly=True, default='Diagnostic')
    task_id = fields.Many2one('mro.tache', 'Tache')
    description = fields.Char('Description')
    date_debut_planifiee = fields.Datetime('Date début planifiée', required=True, default=time.strftime('%Y-%m-%d %H:%M:%S'))
    date_fin_planifiee = fields.Datetime('Date fin planifiée', required=True, default=time.strftime('%Y-%m-%d %H:%M:%S'))
    date_debut_reelle = fields.Datetime('Date début réelle')
    date_fin_reelle = fields.Datetime('Date fin réelle')
    parts_lines = fields.One2many('mro.diagnostic.parts.line', 'maintenance_id')
    parts_ready_lines = fields.One2many('stock.move', compute='_get_available_parts')
    parts_move_lines = fields.One2many('stock.move', compute='_get_available_parts')
    parts_moved_lines = fields.One2many('stock.move', compute='_get_available_parts')
    labor_description = fields.Text('Labor Description',translate=True)
    problem_description = fields.Text('Problem Description')
    company_id = fields.Many2one('res.company','Company',required=True, readonly=True,default=lambda self: self.env['res.company']._company_default_get('mro.order'))
    procurement_group_id = fields.Many2one('procurement.group', 'Procurement group', copy=False)
    wo_id = fields.Many2one('mro.workorder', 'Work Order', ondelete='cascade')
    vehicle_id=fields.Many2one('fleet.vehicle',"Camion",required=True)
    garage_id=fields.Many2one('mro.garage',"Garage",required=True)
    tache_id=fields.Many2many("mro.tache",string="Les taches")
    produit=fields.Many2many("product.product")
    piece  = fields.One2many('mro.piece','maint_id')
    travail_fait = fields.Text()
    

    def valide_diagnostic(self):  
        purchase = self.env['purchase.order']
        test=0
        for line in self.parts_lines :
            produit=self.env['product.product'].search([('id', '=', line.parts_id.id)])
            qty_stock=self.env['stock.quant'].search([('product_id', '=', line.parts_id.id),('location_id', '=', self.garage_id.location_id.id)])
            if qty_stock.qty < line.parts_qty:
                test=1
                qty=line.parts_qty-qty_stock.qty
                self.state='Pièces en attente'
                four=''
                for pro in produit.seller_ids:
                    four=pro.name.id
                    break
                vals = {
                    'partner_id': four,
                    'order_line': [(0, 0, {
                        'name': produit.name,
                        'product_id': line.parts_id.id,
                        'product_qty': qty,
                        'product_uom': produit.uom_id.id,
                        'price_unit': produit.lst_price,
                        'state':'draft',
                        'date_planned': fields.Datetime.now()})]
                    }
                pur_id = purchase.create(vals)
                raise ValidationError("Attention: des pièces nécessaires pour cette opération sont toujours manquantes !")

        if test==0:
            self.state='Prêt pour la réparation'
    def verifier(self):  
        purchase = self.env['purchase.order']
        test=0
        for line in self.parts_lines :
            produit=self.env['product.product'].search([('id', '=', line.parts_id.id)])
            qty_stock=self.env['stock.quant'].search([('product_id', '=', line.parts_id.id),('location_id', '=', self.garage_id.location_id.id)])
            if qty_stock.qty < line.parts_qty:
                test=1
                qty=line.parts_qty-qty_stock.qty
                self.state='Pièces en attente'
                four=''
                for pro in produit.seller_ids:
                    four=pro.name.id
                    break
                vals = {
                    'partner_id': four,
                    'order_line': [(0, 0, {
                        'name': produit.name,
                        'product_id': line.parts_id.id,
                        'product_qty': qty,
                        'product_uom': produit.uom_id.id,
                        'price_unit': produit.lst_price,
                        'state':'draft',
                        'date_planned': fields.Datetime.now()})]
                    }
                pur_id = purchase.create(vals)
                raise ValidationError("Attention: des pièces nécessaires pour cette opération sont toujours manquantes !")
    
    @api.model
    def create(self, vals):
        if vals.get('name','/')=='/':
            vals['name'] = self.env['ir.sequence'].next_by_code('mro.diagnostic') or '/'
        return super(mro_order, self).create(vals)

    def fait(self):
        if date_debut_reelle and date_fin_reelle : 
            self.state='Fait'
        else :
            raise ValidationError("Attention: Veuillez renseigner les dates de début et de fin réelles de l'intervention")
    
   
class mro_tache(models.Model):
    
    _name = 'mro.tache'

    

    name = fields.Char('Description', size=64, required=True, translate=True)
    parts_lines = fields.One2many('mro.tache.parts.line', 'task_id', 'Pièces')
    tools_description = fields.Text('Tools Description',translate=True)
    labor_description = fields.Text('le travail à réaliser',translate=True)
    operations_description = fields.Text('Operations Description',translate=True)
    documentation_description = fields.Text('Documentation Description',translate=True)
    active = fields.Boolean('Active', default=True)

class mro_piece(models.Model):

    _name = 'mro.piece'

    produit=fields.Many2one("product.product",domain=[('categ_id', '=', 'Outils')],required=True)
    qty =fields.Float("Quantité" )
    qty_stock =fields.Float("Quantité en stock",readonly=True )
    desc = fields.Char("Description")
    maint_id=fields.Many2one('mro.order')
    planification_id=fields.Many2one('mro.planification')
   
    @api.onchange('produit')
    def check_qty_produit(self):
        qty_stock=self.env['stock.quant'].search([('product_id', '=', self.produit.id)])
        self.qty_stock=qty_stock.qty
     
class mro_diagnostic_parts_line(models.Model):
    _name = 'mro.diagnostic.parts.line'
    _description = 'Maintenance Planned Parts'

    name = fields.Char('Description', size=64)
    parts_id = fields.Many2one('product.product', 'Parts', required=True)
    parts_qty = fields.Float('Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1.0)
    parts_uom = fields.Many2one('product.uom', 'Unit of Measure', required=True)
    maintenance_id = fields.Many2one('mro.diagnostic', 'Maintenance Order')
    planification_id=fields.Many2one('mro.planification_id')
    request_id = fields.Many2one('mro.signalisation')
    @api.onchange('parts_id')
    def onchange_parts(self):
        self.parts_uom = self.parts_id.uom_id
    def unlink(self):
        self.write({'maintenance_id': False})
        return True

    # @api.model
    # def create(self, values):
    #     ids = self.search([('maintenance_id','=',values['maintenance_id']),('parts_id','=',values['parts_id'])])
    #     if len(ids)>0:
    #         values['parts_qty'] = ids[0].parts_qty + values['parts_qty']
    #         ids[0].write(values)
    #         return ids[0]
    #     ids = self.search([('maintenance_id','=',False)])
    #     if len(ids)>0:
    #         ids[0].write(values)
    #         return ids[0]
    #     return super(mro_diagnostic_parts_line, self).create(values)
class mro_task_parts_line(models.Model):
    _name = 'mro.tache.parts.line'
    _description = 'Maintenance Planned Parts'

    name = fields.Char('Description', size=64)
    parts_id = fields.Many2one('product.product', 'Parts', required=True)
    parts_qty = fields.Float('Quantity', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1.0)
    parts_uom = fields.Many2one('product.uom', 'Unit of Measure', required=True)
    task_id = fields.Many2one('mro.tache', 'Maintenance Task')
    @api.onchange('parts_id')
    def onchange_parts(self):
        self.parts_uom = self.parts_id.uom_id.id

    def unlink(self):
        self.write({'task_id': False})
        return True

    @api.model
    def create(self, values):
        ids = self.search([('task_id','=',values['task_id']),('parts_id','=',values['parts_id'])])
        if len(ids)>0:
            values['parts_qty'] = ids[0].parts_qty + values['parts_qty']
            ids[0].write(values)
            return ids[0]
        ids = self.search([('task_id','=',False)])
        if len(ids)>0:
            ids[0].write(values)
            return ids[0]
        return super(mro_task_parts_line, self).create(values)