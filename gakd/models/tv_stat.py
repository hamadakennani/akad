# -*- coding: utf-8 -*-
from odoo import api, fields, models
from random import randint

class TvStat(models.Model):
    _name = 'gakd.tv_stat'
    point_vent_id = fields.Many2one('gakd.point.vente',string='Point vente')
    agent_id = fields.Many2one('gakd.agent',string='Agent')
    montan_stat = fields.Selection([('gt','Old montant > New montant'),('lt','Old montant < New montant'),('eq','Old montant = New montant')],string='Montant')

    def print_report(self):
        return self.env['report'].get_action(self,'gakd.tv_old')
       
    
    def getData(self):
        print ' hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh'
        data = []
        conditions = ""
        if self.point_vent_id:
            conditions += " and gn.point_vente_id = "+str(self.point_vent_id.id)
        if self.agent_id :
            conditions += " and gn.agent_id = "+str(self.agent_id.id)
        if self.montan_stat :
            if self.montan_stat == 'gt' :
                conditions += " and gn.montant > t.montant"
            elif self.montan_stat == 'lt' :
                conditions += " and gn.montant < t.montant"
            elif self.montan_stat == 'eq' :
                conditions += " and gn.montant > t.montant"
        self._cr.execute("select gt.num_serie,t.montant,gt.client,gn.codebre,gn.montant,gn.point_vente_id,gn.agent_id from gakd_ticket_valeur gt,gakd_ticke_non_connue gn,gakd_tv_type t where gt.num_serie = gn.codebre and gt.tv_type = t.id  " + conditions)
        data_sql = self._cr.fetchall()
        total = 0
        for t in data_sql :
            total += abs(t[1]-t[4])
            data.append({
                'old_tv':t[3],
                'pos': self.env['gakd.point.vente'].sudo().search([('id','=',t[5])]).name ,
                'agent':self.env['gakd.agent'].sudo().search([('id','=',t[6])]).name,
                'client':self.env['res.partner'].sudo().search([('id','=',t[2])]).name,
                'montant':t[4],
                'new_tv':t[0],
                'new_montant':t[1],
                'diff':abs(t[1]-t[4]),
                'total':total
            })
        return data