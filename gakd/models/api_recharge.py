# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from collections import OrderedDict
import collections
#from unidecode import unidecode
from random import randint
from keyid import keyid
import time
#import swagger_client
#from swagger_client.rest import ApiException
from pprint import pprint
from unidecode import unidecode
from datetime import datetime

class api_recharge(models.Model):

##################################################
    @api.multi
    def checkCartByQrcode(self,qrcode):
	print qrcode.get("qrcode") 
	carte = self.env['gakd.carte'].search([['qrcode', '=', qrcode.get("qrcode")]])
	if not carte.id:
		result = 0
	else: result = 1
	
	return result

    @api.multi
    def getCartByQrcode(self,qrcode):
	data = []
	carte = self.env['gakd.carte'].search([['qrcode', '=', qrcode.get("qrcode")]])

	data = [("id",carte.id),("client",unidecode(carte.owner_id.name)),("serie",carte.num_serie),("qrcode",carte.qrcode),("solde",carte.solde),("state",carte.state)]
	#data = {"id":carte.id,"serie":carte.num_serie,"qrcode":carte.qrcode,"solde":carte.solde}
	print data
        return data

    @api.multi
    def chargerCarte(self,qrcode):
	print 5555555555555555
	data = []
	carte = self.env['gakd.carte'].search([['id', '=', qrcode.get("id")]])
	carte.solde += float(qrcode.get("solde"))
	
	#data = [("id",carte.id),("client",unidecode(carte.owner_id.name)),("serie",carte.num_serie),("qrcode",carte.qrcode),("solde",carte.solde),("state",carte.state)]
	#data = {"id":carte.id,"serie":carte.num_serie,"qrcode":carte.qrcode,"solde":carte.solde}
	#print data
        return 1


    @api.multi
    def checkAgentByMatriculee(self,qrcode):
	agent = self.env['gakd.agent'].search([['matricule', '=', qrcode.get("matricule")],['password', '=', qrcode.get("password")]])
	if not agent.id:
		result = 0
	else: result = 1
	
	return result


    @api.multi
    def getAgentByMatriculee(self,qrcode):
	data = []
	agent = self.env['gakd.agent'].search([['matricule', '=', qrcode.get("matricule")]])
	print '###########################'
	print agent.caissier_id.montant_plafond
	print '###########################'
	data = [("id",agent.id),("matricule",unidecode(agent.matricule)),("name",unidecode(agent.name)),('solde',agent.caissier_id.montant_plafond)]
	#data = {"id":carte.id,"serie":carte.num_serie,"qrcode":carte.qrcode,"solde":carte.solde}
	print data
	return data

    @api.multi
    def addRecharge(self,qrcode):
		print 'oooooooooooooooooooooooooooooooooo'
		print qrcode
		agent_caissier = self.env['gakd.agent'].search([['id', '=', qrcode.get('agent')]]).caissier_id
		client = self.env['gakd.carte'].search([['id', '=', qrcode.get('carte')]]).owner_id
		vals = {
            'montant_init':agent_caissier.montant_plafond,
            'montant_fin':agent_caissier.montant_plafond-float(qrcode.get('montant_recharge')),
			'agent':qrcode.get('agent'),
			'carte':qrcode.get('carte'),
			'diff':qrcode.get('montant_recharge'),
			'type_op':'Recharge carte',
			'sous_chargeur':agent_caissier.id,
			'client_id': client.id
		}
		#print vals
		res = self.env['gakd.historique'].create(vals)
		if res:
			agent_caissier.write({
			'montant_plafond':agent_caissier.montant_plafond-float(qrcode.get('montant_recharge'))
			})
		return 1


    @api.multi
    def getListeRecharge(self,param):
		data = []
		agent_caissier = self.env['gakd.agent'].search([['id', '=', param.get('agent_id')]]).caissier_id
		print agent_caissier
		recharges = self.env['gakd.historique'].search([("sous_chargeur","=",int(agent_caissier)),('type_op','=','Recharge carte'),('state',"=",param.get("state"))],order='create_date desc')
		print recharges
		for recharge in recharges:
			print recharge.carte
			if recharge.carte.id:
				data.append([("id",recharge.id),("serie",recharge.carte.num_serie),("carte",recharge.carte.libelle.name),("montant_recharge",recharge.diff),("date",recharge.create_date),("state",recharge.state)])
		
		print data
		return data

    @api.multi
    def getDetailRecharge(self,param):
		recharge = self.env['gakd.historique'].search([('id','=',param.get('id'))])
		data = [("id",recharge.id),("serie",recharge.carte.num_serie),("carte",unidecode(recharge.carte.libelle.name)),("montant_recharge",recharge.diff),("date",recharge.create_date),("state",recharge.state),('verse_validate',recharge.verse_validate)]
		print data
		return data


    @api.multi
    def demandeAnnulation(self,param):
		print '5555555555555555'
		recharge = self.env['gakd.historique'].search([('id','=',param.get('id'))])
		print recharge
		print recharge.carte
		recharge.state = "demande annulation"
		recharge.carte.solde -= float(recharge.diff)
		print '5555555555555555'
		print recharge.sous_chargeur
		print float(recharge.diff)
		print '5555555555555555'
		recharge.sous_chargeur.write({
		'montant_plafond':recharge.sous_chargeur.montant_plafond+float(recharge.diff)
		})
		print '5555555555555555'
		return 1

    @api.multi
    def getListeAllJournal(self,param):
	data = []
	company_id = 1
	if param.get("user"):
		company_id = self.env['gakd.agent'].search([('id','=',int(param.get("user")))]).company_id.id
	journals = self.env['account.journal'].search([("company_id","=",company_id),'|',("type","=","bank"),("type","=","cash")],order="name asc")
	for journal in journals:
		data.append([('id',journal.id),('name',unidecode(journal.name))])
	
	return data

    @api.multi
    def getMontantVersement(self,param):
		agent_caissier = self.env['gakd.agent'].search([['id', '=', param.get('agent')]]).caissier_id
		
		recharges = self.env['gakd.historique'].search([("sous_chargeur","=",int(agent_caissier)),('type_op','=','Recharge carte'),('state',"=",'valide'),('verse_validate','=',False),('tres_validate','=',False)],order='create_date desc')
		print recharges
		total = 0
		for recharge in recharges:
			total += recharge.diff
		return total

    @api.multi
    def InsertVersement(self,param):
	print param
	dataa=[]
	point_vente_id = self.env['gakd.agent'].search([['id', '=', param.get('agent')]]).point_vente_id
	dataa.append((0,0 ,{'journal_id':int( param.get("journal_id") ),'num_versement':param.get("num_versement"),'montant':float( param.get("montant") )}))
	vals = {
		'point_vente_id':point_vente_id.id,
		'versement_line':dataa,
		'montant':float( param.get("montant") ),
		'moyen_de_paiement': 'especes',
		'vers_par': 'caissier_station',
		'montant_saisie':0,
        'type_versement':'Produits blancs',
	}
	print vals
	res = self.env['gakd.versement'].create(vals)
	versement = self.env['gakd.versement'].search([['id', '=', int(res)]])
	versement.montant_saisie = 0
	return 1

    @api.multi
    def getListeVersement(self,param):
	data = []
	point_vente_id = self.env['gakd.agent'].search([['id', '=', param.get('point_vente_id')]]).point_vente_id
	caissier_id = self.env['gakd.agent'].search([['id', '=', param.get('point_vente_id')]]).caissier_id
	print '1111111111111111'
	print point_vente_id
	print caissier_id.access
	print '1111111111111111'
	versements = self.env['gakd.versement'].search([('point_vente_id','=', int( point_vente_id )),('access','=', int( caissier_id.access ))])
	
	for versement  in versements:
		if versement.type_versement:
			for t in versement.versement_line:
				journal_id = t.journal_id.id
				num_versement = t.num_versement
			data.append([('point_vente_id',versement.point_vente_id.id)
		,('journal_id',journal_id),("montant",versement.montant),("type_versement",versement.type_versement),("vers_par",versement.vers_par),("num_versement",num_versement),("date_versement",versement.create_date)])
		else:
			for t in versement.versement_line:
				journal_id = t.journal_id.id
				num_versement = t.num_versement
			data.append([('point_vente_id',versement.point_vente_id.id)
		,('journal_id',journal_id),("montant",versement.montant),("type_versement",'noting'),("vers_par",versement.vers_par),("num_versement",num_versement),("date_versement",versement.create_date)])
		
	print data
	return data



    @api.multi
    def getPlafondCaissier(self,param):
        agent_caissier = self.env['gakd.agent'].search([['id', '=', param.get('agent')]]).caissier_id
	return agent_caissier.montant_plafond
    

#############################################################""""