# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import  ValidationError
class sous_chargeur(models.Model):

    _name="gakd.sous_chargeur"
    _rec_name='access'
   
    montant_plafond = fields.Float(string='Montant plafond JNP',required=True)
    access = fields.Many2one("res.users",string="Caissier",required=True)
    superv = fields.Many2one("gakd.chargeur",string="Trésorier", default=lambda  s : s.env["gakd.chargeur"].search([('access','=',s.env.user.id)]) )
    montant_super=fields.Float(readonly=True,default=lambda  s : s.env["gakd.chargeur"].search([('access','=',s.env.user.id)]).montant_plafond)
    rest=fields.Float(string='Montant actuel')
    historique = fields.One2many(comodel_name='gakd.historique', inverse_name='sous_chargeur', string='Historique')
    
    montant_super_tv=fields.Float(readonly=True,default=lambda  s : s.env["gakd.chargeur"].search([('access','=',s.env.user.id)]).montant_plafond_tv)
    montant_plafond_tv = fields.Float(string='Montant plafond TV',required=True)
    # @api.onchange('montant_plafond')
    # def _getsoldeTresor(self):
    #     print 'ooooooooooooooooooo'
    #     print self.env.user.id
    #     return float(self.env["gakd.chargeur"].search([('access','=',self.env.user.id)]).montant_plafond)

    @api.model
    def create(self, vals):
        flag=0
        po=self.env["gakd.chargeur"].search([('access','=',self.env.user.id)])
        if  vals['montant_plafond'] <= po.montant_plafond :
            if  vals['montant_plafond'] !=0:
                self.env['gakd.historique'].create({
                    'montant_init':po.montant_plafond,
                    'montant_fin':po.montant_plafond-vals['montant_plafond'],
                    'chargeur':po.id,
                    'type_op':'Affectation JNP',
                    'diff':abs(po.montant_plafond-po.montant_plafond-vals['montant_plafond'])                
                })
                vals['montant_super'] = po.montant_plafond-vals['montant_plafond']            
                flag=1
        else :
            raise ValidationError("Le montant indiqué doit etre inférieur ou égale a votre solde")
            
        if  vals['montant_plafond_tv'] <= po.montant_plafond_tv :
            if  vals['montant_plafond_tv']!=0:
                self.env['gakd.historique'].create({
                    'montant_init':po.montant_plafond_tv,
                    'montant_fin':po.montant_plafond_tv-vals['montant_plafond_tv'],
                    'chargeur':po.id,
                    'type_op':'Affectation TV',
                    'diff':abs(po.montant_plafond_tv-po.montant_plafond_tv-vals['montant_plafond_tv'])
                    
                })
                vals['montant_super_tv'] = po.montant_plafond_tv-vals['montant_plafond_tv']
                flag=1
        else :
            raise ValidationError("Le montant indiqué doit etre inférieur ou égale a votre solde")
        if flag==0:
            raise ValidationError("L'un des montants  indiqués doit etre différent de zéro")
        po.write({
                'montant_plafond_tv':po.montant_plafond_tv-vals['montant_plafond_tv'],
                'montant_plafond':po.montant_plafond-vals['montant_plafond']
            })
        return super(sous_chargeur, self).create(vals)

    @api.onchange('access')
    def _getCharger(self):
     res = {}
     ids = []
     chargeur=self.env["gakd.chargeur"].search([])
     print '----------------    1'
     for c in chargeur:
         ids.append(c.access.id)
     chargeur=self.env["gakd.sous_chargeur"].search([])
     print '----------------    2'
     for c in chargeur:
         ids.append(c.access.id)
     res['domain'] = {'access': [('id', 'not in', ids),('groups_id','=',self.env.ref('gakd.group_gakd_sous_chargeur').id)]}
     return res 


    # @api.multi
    # def unlink(self):
        # po=self.env["gakd.chargeur"].search([('superv','=',self.superv.id)])
        # print po.rest
        # print 'jujjukjkjknj'


    # @api.constrains("montant_plafond")
    # def montant_controle(self):
    #     if  float(self.montant_plafond) > float(self.superv.rest ):
    #         raise ValidationError("Le montant indiqué doit etre inférieur ou égale a votre solde")



    # @api.multi
    # def write(self,vals):
    #     print ' HHHHHHHHHHHHHHJAOZle,'
    #     print vals['montant_plafond']
    #     print self.montant_plafond
    #     dif=vals['montant_plafond']-self.montant_plafond
    #     print dif
    #     p=self.env["gakd.chargeur"].search([('access','=',self.env.user.id)])
    #     print p.montant_plafond-dif
    #     print 'iiiiiiiiiiiiiiii'
    #     p.write({
    #         'montant_plafond':p.montant_plafond-dif
    #     })
        
    #     return super(sous_chargeur,self).write(vals)

    # if dif > 0:
        #     p=self.env["gakd.chargeur"].search([('access','=',self.env.user.id)])
        #     p.write({
        #         'montant_plafond':p.montant_plafond-dif
        #     })
        # if dif < 0:
        #     p=self.env["gakd.chargeur"].search([('access','=',self.env.user.id)])
        #     p.write({
        #         'montant_plafond':p.montant_plafond - dif
        #     })



