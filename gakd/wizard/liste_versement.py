# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time

class gakd_liste_versement(models.TransientModel):
    _name = 'gakd.wizard.liste_versement'

    banque_id = fields.Many2one(comodel_name='account.journal', string='Banque')
    type_versement=fields.Selection([('Lubrifiants','Lubrifiants'),('Produits blancs','Produits blancs'),('Gaz et accessoire','Gaz et accessoire'),('Lavage','Lavage')],string="Catégorie des produits")
    
    start_date = fields.Date(
         string='Date Début',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
    end_date = fields.Date(
         string='Date Fin',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )

    @api.multi
    def print_report(self):
        ds=datetime.strptime(self.start_date, '%Y-%m-%d').strftime('%Y-%m-%d')
        de=datetime.strptime(self.end_date, '%Y-%m-%d').strftime('%Y-%m-%d')
        print '##############################'
        print ds
        print '##############################'
        res = []
        dd = []
        i=0
        versement_lines = self.env["gakd.versement_line"].search([('journal_id','=',self.banque_id.id)])
        # move_line = self.env["account.move.line"].search([],order='account_id desc')
        # table = []
        # i=0
        # for lin in move_line:
        query = """select account_id,date(create_date),partner_id,debit,credit,balance,move_id,ref,name 
        from account_move_line aml 
        where date(aml.create_date) between '"""+str(ds)+"""' and '"""+str(de)+"""'
        order by account_id desc"""

        self._cr.execute("""select count(*) 
        from account_move_line aml 
        where date(aml.create_date) between '"""+str(ds)+"""' and '"""+str(de)+"""'
        """)
        result=self._cr.fetchone()

        self._cr.execute(query)
        
    	for lin in self._cr.fetchall():
    		# totalMontant+= float(rec[3])
            partner_name = ""
            move_name =  ""
            if lin[2]:
                partner_name =  self.env["res.partner"].search([('id','=',int(lin[2]))]).name
            else:
                partner_name = ""
            # if lin[6]:
            #     move_name =  self.env["account.move.line"].search([('id','=',int(lin[4]))]).name
            # else:
            #     move_name = ""
            
            i=i+1
            print str(i)+'/'+str(result)
            data = {
                        "account":lin[0],
                        "date":lin[1],
                        "JRNL":'',
                        "PARTNER":partner_name,
                        "REF":lin[7],
                        "Label":lin[8],
                        "DEBIT":lin[3],
                        "CREDIT":lin[4],
                        "BALANCE":lin[5],
                    }
            #data = {"date":'',"JRNL":'',"BALANCE":lin.balance,}
            balance = "jmjjjmljlm"
            if lin[0] not in dd:
                aa = {
                    "name" : "ooooooooooo",
                    "total_debit" : 0,
                    "total_credit" : 0,
                    "total_debit" : 0,
                }

            res.append(data)
        print "0000000000000000000000"
        #print res
        print "0000000000000000000000"
        # for line in versement_lines:
        #     if datetime.strptime(line.create_date, '%Y-%m-%d %H:%M:%S')>= ds and datetime.strptime(line.create_date, '%Y-%m-%d %H:%M:%S')<= de:
                
        #         if line.versement_id.type_versement == self.type_versement:
        #             data = {
        #                 "type_versement":line.versement_id.type_versement,
        #                 "point_vente":line.versement_id.point_vente_id.name,
        #                 "montant":line.montant,
        #                 "banque":line.journal_id.name,
        #                 "nume_versement":line.num_versement
        #             }
        #             #print data
        #             i=i+1
        #             res.append(data)
        total = 0  
        
        # for val in res:
        #     total+= val['montant']

        datas = {
            'form':
            {
                'banque':self.banque_id.name,
                'categorie':self.type_versement,
                'date_debut':self.start_date,
                'date_fin':self.end_date,
                'transactions':res,
                'total':total,
            }
        }
        
        return self.env['report'].get_action(self, 'gakd.liste_versement_report', data=datas)

class liste_versement_report(models.AbstractModel):
    _name = 'report.gakd.liste_versement_report'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('gakd.liste_versement_report')
        
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
            }
       
        return report_obj.render('gakd.liste_versement_report', docargs)