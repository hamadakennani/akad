# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time
from room_disponiblite import is_room_disponible,room_state
from odoo import api, fields, models



class RoomState(models.Model):
    _name = 'hotel.room_state'
    _description = 'New Description'
    date_debut = fields.Date(string='Date')
    lines = fields.One2many(comodel_name='hotel.room_state.line', inverse_name='room_state', string='line')
    

   
    @api.onchange('date_debut')
    @api.multi
    def get_rooms(self):
        if self.date_debut:
            lines=[]
            print " getttt ___ rooms "
        
            for room in self.env['hotel.room'].search([]):
                val= room_state(self,room.id,str(self.date_debut),str(self.date_debut))
                lines.append((0,0,{'room_id':room.name,'etat':val[0],'type_chambre':room.categ_id.id,
                'date_fin_maintenance':val[1]}))
        
            return {'value':{'lines':lines}}

class RoomStateLine(models.Model):
    _name = 'hotel.room_state.line'
    _description = 'New Description'
    
    room_id = fields.Char(string='Chambre')
    etat = fields.Char(string='Statut')
    type_chambre = fields.Many2one(comodel_name='hotel.room.type', string='Type de Chambre')
    date_fin_maintenance = fields.Char(string='Date fin de maintenance')
   
    room_state = fields.Many2one(comodel_name='hotel.room_state', string='')
   
   
   
   