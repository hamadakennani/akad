# -*- coding: utf-8 -*-
# Copyright (c) 2015-Present TidyWay Software Solution. (<https://tidyway.in/>)
import xlwt
import cStringIO
import base64
from . import xls_format
import time

from odoo import models, api, fields, _
from odoo.exceptions import Warning
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
import datetime as dt

from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from ..report.stock_valuation import StockValuationCategory


class StockValuationDateReport(models.TransientModel,
                               StockValuationCategory):
    _name = 'stock.valuation.ondate.repor2'
    def category_wise_valuee(self, start_date, end_date, locations,company,product, filter_product_categ_ids=[]):
        """
        Complete data with category wise
            - In Qty (Inward Quantity to given location)
            - Out Qty(Outward Quantity to given location)
            - Internal Qty(Internal Movements(or null movements) to given location: out/in both : out must be - ,In must be + )
            - Adjustment Qty(Inventory Loss movements to given location: out/in both: out must be - ,In must be + )
        Return:
            [{},{},{}...]
        """
        print company
        print start_date
        DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        start_date=datetime.strptime(start_date, DATETIME_FORMAT)
        start_date=start_date+ timedelta(hours=1,minutes=0)
        start_date=str(start_date)
        end_date=datetime.strptime(end_date, DATETIME_FORMAT)
        end_date=end_date+ timedelta(hours=1,minutes=0)
        end_date=str(end_date)
        print start_date
        print end_date
        print 'ccccccccccccccccccccccccccccccccccccc>>'
        print product
        print 'productproductproductproductproductproductproductproductproductproduct'
        # select sum(product_uom_qty) from stock_move where location_id=85 and create_date<= '24-11-2019 12:16:50' and
        #  create_date>= '22-11-2019 12:16:50' and  product_id=155 and location_dest_id not in
        #   (select id from stock_location where usage in ('customer','supplier','inventory'));
        self._cr.execute('''
                        SELECT pp.id AS product_id,sourcel.id,
                            sum((
                            CASE WHEN   sm.location_id in %s AND sourcel.usage ='internal' AND destl.usage ='customer' 
                            THEN -(sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0 
                            END
                            )) AS product_qty_out,
                            sum((
                            CASE WHEN   sm.location_dest_id in %s AND sourcel.usage ='supplier' AND destl.usage ='internal' 
                            THEN (sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0 
                            END
                            )) AS product_qty_in,
                        
                            sum(( 
                            CASE WHEN   sm.location_dest_id in %s AND sm.location_id not in %s  
                            AND sm.location_id not in  (select id from stock_location where usage in ('customer','supplier','inventory'))
                            THEN (sm.product_qty * pu.factor / pu2.factor)  

                            WHEN   sm.location_id in %s AND sm.location_dest_id not in %s  AND sm.location_dest_id not in  (select id from stock_location where usage in ('customer','supplier','inventory'))
                            THEN -(sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0 
                            END
                            )) AS product_qty_internal, 
                            sum((
                            CASE WHEN sourcel.usage = 'inventory' AND sm.location_dest_id in %s   AND sourcel.usage !='view'
                            THEN  (sm.product_qty * pu.factor / pu2.factor)
                            WHEN destl.usage ='inventory' AND sm.location_id in %s 
                            THEN -(sm.product_qty * pu.factor / pu2.factor)
                            ELSE 0.0 
                            END
                            )) AS product_qty_adjustment 
                        FROM product_product pp 
                        LEFT JOIN  stock_move sm ON (sm.product_id = pp.id and sm.date >= %s and sm.date <= %s and sm.state = 'done' and sm.location_id != sm.location_dest_id)
                        LEFT JOIN stock_picking sp ON (sm.picking_id=sp.id)
                        LEFT JOIN stock_picking_type spt ON (spt.id=sp.picking_type_id)
                        LEFT JOIN stock_location sourcel ON (sm.location_id=sourcel.id)
                        LEFT JOIN stock_location destl ON (sm.location_dest_id=destl.id)
                        LEFT JOIN product_uom pu ON (sm.product_uom=pu.id)
                        LEFT JOIN product_uom pu2 ON (sm.product_uom=pu2.id)
                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                        WHERE   pp.product_tmpl_id= %s  
                        GROUP BY sourcel.id, pp.id order by sourcel.id
                        
                        ''', (tuple(locations), tuple(locations), tuple(locations), tuple(locations), tuple(locations), tuple(locations), tuple(locations), tuple(locations), start_date, end_date,product))
                        # WHERE   pp.product_tmpl_id=156
        all_values=[]
        values = self._cr.dictfetchall()
        print values
        print 'valuesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvalues'
        if len(values) >0:
            for a in values:
                self._cr.execute(''' 
                            SELECT id,coalesce(sum(qty), 0.0) as qty
                            FROM
                                ((
                                SELECT pp.id, pp.default_code,m.date,
                                    CASE when pt.uom_id = m.product_uom 
                                    THEN u.name 
                                    ELSE (select name from product_uom where id = pt.uom_id) 
                                    END AS name,

                                    CASE when pt.uom_id = m.product_uom  
                                    THEN coalesce(sum(-m.product_qty)::decimal, 0.0)
                                    ELSE coalesce(sum(-m.product_qty * pu.factor / u.factor )::decimal, 0.0) 
                                    END AS qty
                            
                                FROM product_product pp 
                                LEFT JOIN stock_move m ON (m.product_id=pp.id)
                                LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                LEFT JOIN stock_location l ON (m.location_id=l.id)    
                                LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                WHERE m.date <  %s AND (m.location_id in %s) AND (m.location_dest_id not in %s) AND m.state='done' AND pp.active=True AND pp.id = %s  
                                GROUP BY  pp.id, pt.uom_id , m.product_uom ,pp.default_code,u.name,m.date
                                ) 
                                UNION ALL
                                (
                                SELECT pp.id, pp.default_code,m.date,
                                    CASE when pt.uom_id = m.product_uom 
                                    THEN u.name 
                                    ELSE (select name from product_uom where id = pt.uom_id) 
                                    END AS name,
                            
                                    CASE when pt.uom_id = m.product_uom 
                                    THEN coalesce(sum(m.product_qty)::decimal, 0.0)
                                    ELSE coalesce(sum(m.product_qty * pu.factor / u.factor )::decimal, 0.0) 
                                    END  AS qty
                                FROM product_product pp 
                                LEFT JOIN stock_move m ON (m.product_id=pp.id)
                                LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                                LEFT JOIN stock_location l ON (m.location_dest_id=l.id)    
                                LEFT JOIN stock_picking p ON (m.picking_id=p.id)
                                LEFT JOIN product_uom pu ON (pt.uom_id=pu.id)
                                LEFT JOIN product_uom u ON (m.product_uom=u.id)
                                WHERE m.date <  %s AND (m.location_dest_id in %s) AND (m.location_id not in %s) AND m.state='done' AND pp.active=True AND pp.id = %s  
                                GROUP BY  pp.id,pt.uom_id , m.product_uom ,pp.default_code,u.name,m.date
                                ))
                            AS foo
                            GROUP BY id
                        ''', (start_date, tuple([a['id']]), tuple([a['id']]), 156, start_date,tuple([a['id']]),tuple([a['id']]), product )) 
                res = self._cr.dictfetchall()
                if len(res)>0:
                    a['test']=  res[0].get('qty', 0.0) or 0.0
                else :
                    a['test']=0

                self._cr.execute('''
                        SELECT pp.id AS product_id,pt.categ_id,
                            sum((
                            CASE WHEN   sm.location_id = %s AND sourcel.usage ='internal' AND destl.usage ='customer' 
                            THEN -(sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0 
                            END
                            )) AS product_qty_out,
                            sum((
                            CASE WHEN   sm.location_dest_id = %s AND sourcel.usage ='supplier' AND destl.usage ='internal' 
                            THEN (sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0 
                            END
                            )) AS product_qty_in,
                        
                            sum((
                            CASE WHEN (spt.code ='internal' OR spt.code is null) AND sm.location_dest_id = %s AND sm.location_id != %s AND sourcel.usage !='inventory' AND destl.usage !='inventory' 
                            THEN (sm.product_qty * pu.factor / pu2.factor)  
                            WHEN (spt.code ='internal' OR spt.code is null) AND sm.location_id = %s AND sm.location_dest_id != %s AND sourcel.usage !='inventory' AND destl.usage !='inventory' AND destl.usage !='customer' 
                            THEN -(sm.product_qty * pu.factor / pu2.factor) 
                            ELSE 0.0 
                            END
                            )) AS product_qty_internal,
                        
                            sum((
                            CASE WHEN sourcel.usage = 'inventory' AND sm.location_dest_id = %s   AND sourcel.usage !='view'
                            THEN  (sm.product_qty * pu.factor / pu2.factor)
                            WHEN destl.usage ='inventory' AND sm.location_id = %s 
                            THEN -(sm.product_qty * pu.factor / pu2.factor)
                            ELSE 0.0 
                            END
                            )) AS product_qty_adjustment
                        
                        FROM product_product pp 
                        LEFT JOIN  stock_move sm ON (sm.product_id = pp.id and sm.date >= %s and sm.date <= %s and sm.state = 'done' and sm.location_id != sm.location_dest_id)
                        LEFT JOIN stock_picking sp ON (sm.picking_id=sp.id)
                        LEFT JOIN stock_picking_type spt ON (spt.id=sp.picking_type_id)
                        LEFT JOIN stock_location sourcel ON (sm.location_id=sourcel.id)
                        LEFT JOIN stock_location destl ON (sm.location_dest_id=destl.id)
                        LEFT JOIN product_uom pu ON (sm.product_uom=pu.id)
                        LEFT JOIN product_uom pu2 ON (sm.product_uom=pu2.id)
                        LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
                        WHERE   pp.product_tmpl_id= %s
                        GROUP BY pt.categ_id, pp.id order by pt.categ_id

                        ''', (tuple([a['id']]), tuple([a['id']]), tuple([a['id']]), tuple([a['id']]), tuple([a['id']]), tuple([a['id']]),tuple([a['id']]), tuple([a['id']]), start_date, end_date,product))
                        
                        
                ress = self._cr.dictfetchall()
                if len(ress)>0: 
                    a['product_qty_in']=  ress[0].get('product_qty_in', 0.0) or 0.0
                    a['product_qty_internal']=  ress[0].get('product_qty_internal', 0.0) or 0.0
                    a['product_qty_adjustment']=  ress[0].get('product_qty_adjustment', 0.0) or 0.0
                    a['product_qty_out']=  ress[0].get('product_qty_out', 0.0) or 0.0
                    all_values.append(a)
            values=all_values 


        for none_to_update in values:
            if not none_to_update.get('product_qty_out'):
                none_to_update.update({'product_qty_out':0.0})
            if not none_to_update.get('product_qty_in'):
                none_to_update.update({'product_qty_in':0.0})

        # print values
        # print 'valuesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvaluesvalues'
        # raise UserError("Invoice must be in draft or Pro-forma state in order to validate it.")
        # filter by categories
        # if filter_product_categ_ids:
        #     values = self._remove_product_cate_ids(values, filter_product_categ_ids)
        # else : 
        #     final_values = []
        #     if company:
        #     # # if data['form'] and data['form'].get('company_id'):
        #         for rm_products in values:
        #             categ=self.env['product.category'].search([('id', '=',rm_products['categ_id'])])
        #             # print  rm_products['categ_id']
        #             # print company[0]
        #             if categ.company_id.id==company[0]:
        #                 final_values.append(rm_products)
                
        #         values=final_values
                
                # if rm_products['categ_id'] not in filter_product_categ_ids:
                #     pass
                # else:
                #     final_values.append(rm_products)
                # print rm_products['categ_id']
                # print rm_products
                # print 5555555555555555555555555555
                # print data['form'].get('company_id', [])
            # final_values.append(1)
            # values=final_values
        return values
    def _get_ending_inventory(self, in_qty, out_qty, internal_qty, adjust_qty,big_corr):
        """
        Process:
            -Inward, outward, internal, adjustment
        Return:
            - total of those qty
        """
        return    in_qty + out_qty + internal_qty + adjust_qty+big_corr
    def _get_cost(self, company_id, product_id, inventory_date):
        """
        Return:
            - inventory cost on  date
            - Working only for average and standard cost
        """
        company_args = []
        if company_id:
            company_args = [('company_id', 'in', [company_id, False])]
        history = self.env['product.price.history'].search(company_args + [
            ('product_id', '=', product_id),
            ('create_date', '<=', inventory_date or fields.Datetime.now())],
           limit=1, order="id desc")
        cost_value = history.cost or 0.0
        if not cost_value:
            move = self.env['stock.move'].search(company_args+[
                ('product_id', '=', product_id),
                ('purchase_line_id', '!=', False),
                ('state', '=', 'done'),
                ('date', '<=', inventory_date and fields.Datetime.now())],
               limit=1, order="id desc")
            cost_value = move.purchase_line_id.price_unit or 0.0
        return cost_value and round(cost_value, 2) or 0.0
    def _get_subtotal_cost(self, cost, ending_inv, current_record):
        subtotal_cost = cost and ending_inv and round((cost * ending_inv), 2) or 0.0
        current_record.update({'subtotal_cost': subtotal_cost})
        return subtotal_cost
    def _get_categ(self, categ,company):
        """
        Find category name with id
        """
        # print company  
        # print 52582582000000000000000000
        a=self.env['stock.location'].browse(categ).read(['name'])[0]['name']
        return a
    @api.multi
    def print_report_excel(self):
        workbook = xlwt.Workbook()
        company = self.env['res.company'].search([("id","=",self.company_id.id)])
        sheet_name = u'Évaluation des stocks par emplacement'
        sheet = workbook.add_sheet(sheet_name)
        sheet.write(0,0,company.name)
        sheet.write(0,1, u'Début')
        sheet.write(0,2, u'Reçus')
        sheet.write(0,3, u'Ventes')
        sheet.write(0,4, u'Transferts')
        sheet.write(0,5, u'Ajustements')
        sheet.write(0,6, u'Fin')
        sheet.write(0,7, u'Coût')
        sheet.write(0,8, u'Valorisation')
        row=1
        data1=[]
        data=[]
        location_obj = self.env['stock.location'].search([])
        for a in location_obj:
            data1.append(a.id)
        for a in self.category_wise_valuee(self.start_date,self.end_date,data1,self.company_id,self.typedeEmp.id,data):
            sheet.write(row,0, self._get_categ(a['id'],self.company_id.id))
            sheet.write(row,1, a['test'])
            sheet.write(row,2, a['product_qty_in'])
            sheet.write(row,3, a['product_qty_out'])
            sheet.write(row,4, a['product_qty_internal'])
            sheet.write(row,5, a['product_qty_adjustment'])
            sheet.write(row,6, self._get_ending_inventory(a['product_qty_in'],a['product_qty_out'],a['product_qty_internal'],a['product_qty_adjustment'],a['test']))
            sheet.write(row,7, self._get_cost(self.company_id.id, self.typedeEmp.id, self.end_date))
            sheet.write(row,8, self._get_subtotal_cost(self._get_cost(self.company_id.id, self.typedeEmp.id, self.end_date), self._get_ending_inventory(a['product_qty_in'],a['product_qty_out'],a['product_qty_internal'],a['product_qty_adjustment'],a['test']), a))
            row+=1
        stream = cStringIO.StringIO()
        workbook.save(stream)
        attach_id = self.env['exam.report.output'].sudo().create({'name':'Évaluation des stocks par emplacement.xls', 'xls_output': base64.encodestring(stream.getvalue())})
        return {
                'context': self.env.context,
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'exam.report.output',
                'res_id':attach_id.id,
                'type': 'ir.actions.act_window',
                'target':'new',
                'name':'Évaluation des stocks par emplacement'
        }
    company_id = fields.Many2one('res.company', string='Company')
    warehouse_ids = fields.Many2many('stock.warehouse', string='warehouse')
    location_id = fields.Many2one('stock.location', string='Location')
    typedeEmp =  fields.Many2one('product.product', string='Produit',required=True)
    start_date = fields.Datetime(
         string='From Date',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DF)))
         )
    end_date = fields.Datetime(
         string='To Date',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DF)))
         )
#     inventory_date = fields.Date(
#          string='To Date',
#          required=True,
#          default=lambda *a: (parser.parse(datetime.now().strftime(DF)))
#          )
    filter_product_ids = fields.Many2many('product.product', string='Products')
    filter_product_categ_ids = fields.Many2many(
        'product.category',
        string='Categories'
        )
    only_summary = fields.Boolean(
      string='Display Only Summary?',
      help="True, it will display only total summary of categories.",
      )

    @api.onchange('company_id')
    def onchange_company_id(self):
        """
        Make warehouse compatible with company
        """
        cat_data = []
        if self.company_id:
            cat_data = [(6,0,[c.id for c in self.env['product.category'].search([('company_id','=',self.company_id.id)])])]
        return {'domain':{'company_id':[('id','in',[c.id for c in self.env.user.company_ids])],'filter_product_categ_ids':[('company_id','in',[c.id for c in self.company_id])]}}
    

        
    @api.onchange('warehouse_ids', 'company_id')
    def onchange_warehouse(self):
        """
        Make warehouse compatible with company
        """
        location_obj = self.env['stock.location']
        location_ids = location_obj.search([('usage', '=', 'internal')])
        total_warehouses = self.warehouse_ids
        if total_warehouses:
            addtional_ids = []
            for warehouse in total_warehouses:
                store_location_id = warehouse.view_location_id.id
                addtional_ids.extend([y.id for y in location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])])
            location_ids = addtional_ids
        elif self.company_id:
            total_warehouses = self.env['stock.warehouse'].search([('company_id', '=', self.company_id.id)])
            addtional_ids = []
            for warehouse in total_warehouses:
                store_location_id = warehouse.view_location_id.id
                addtional_ids.extend([y.id for y in location_obj.search([('location_id', 'child_of', store_location_id), ('usage', '=', 'internal')])])
            location_ids = addtional_ids
        else:
            location_ids = [p.id for p in location_ids]
        return {
                  'domain':
                            {
                             'location_id': [('id', 'in', location_ids)]
                             },
                  'value':
                        {
                            'location_id': False
                        }
                }

    @api.multi
    def print_report(self):
        """
            Print report either by warehouse or product-category
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        print self.start_date
        print type(self.start_date)
        start= dt.datetime.strptime(self.start_date, '%Y-%m-%d %H:%M:%S')+ timedelta(hours=1)
        end= dt.datetime.strptime(self.end_date, '%Y-%m-%d %H:%M:%S')+ timedelta(hours=1)
        print 8222222222222222222222222222220
        datas = {
                 'form':
                    {
                        'company_id': self.company_id and [self.company_id.id] or [],
                        'warehouse_ids': [y.id for y in self.warehouse_ids],
                        'location_id': self.location_id and self.location_id.id or False,
                        'location_name': self.location_id.name and self.location_id.name or False,
                        #'inventory_date': self.inventory_date,
                        'typedeEmp':  self.typedeEmp and self.typedeEmp.id or False,
                        'product_name':self.typedeEmp.name and self.typedeEmp.name or False,
                        'start_date': str(start),
                        'end_date': str(end),
                        'only_summary': self.only_summary,
                        'id': self.id,
                        'filter_product_ids': [p.id for p in self.filter_product_ids],
                        'filter_product_categ_ids': [p.id for p in self.filter_product_categ_ids] 
                    }
                }

        if [y.id for y in self.warehouse_ids] and (not self.company_id):
            self.warehouse_ids = []
            raise Warning(_('Please select company of those warehouses to get correct view.\nYou should remove all warehouses first from selection field.'))
        return self.env['report'].with_context(landscape=True).get_action(self, 'stock_valuation_on_dat2.stock_valuation_ondate_repor2', data=datas)

    def _to_company(self, company_ids):
        company_obj = self.env['res.company']
        warehouse_obj = self.env['stock.warehouse']
        if not company_ids:
            company_ids = [x.id for x in company_obj.search([])]

        # filter to only have warehouses.
        selected_companies = []
        for company_id in company_ids:
            if warehouse_obj.search([('company_id', '=', company_id)]):
                selected_companies.append(company_id)

        return selected_companies

    def xls_get_warehouses(self, warehouses, company_id):
        warehouse_obj = self.env['stock.warehouse']
        if not warehouses:
            return 'ALL'

        warehouse_rec = warehouse_obj.search([
                                              ('id', 'in', warehouses),
                                              ('company_id', '=', company_id),
                                              ])
        return warehouse_rec \
            and ",".join([x.name for x in warehouse_rec]) or '-'

    @api.model
    def _product_detail(self, product_id):
        product = self.env['product.product'].browse(product_id)
        variable_attributes = product.attribute_line_ids.filtered(
                      lambda l: len(l.value_ids) > 1).mapped('attribute_id')
        variant = product.attribute_value_ids._variant_name(
                                                variable_attributes)
        product_name = variant and "%s (%s)" % (
                                    product.name, variant) or product.name
        return product_name, product.barcode, product.default_code

    @api.model
    def _value_existed(
                      self,
                      beginning_inventory,
                      product_qty_in,
                      product_qty_out,
                      product_qty_internal,
                      product_qty_adjustment,
                      ending_inventory
                      ):
        value_existed = False
        if beginning_inventory or product_qty_in or product_qty_out or \
            product_qty_internal or product_qty_adjustment or \
                ending_inventory:
            value_existed = True
        return value_existed

    @api.multi
    def print_xls_report(self):
        """
            Print ledger report
        """
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        workbook = xlwt.Workbook()

        M_header_tstyle = xls_format.font_style(position='center', bold=1, border=1, fontos='black', font_height=400, color='grey')
        header_tstyle_c = xls_format.font_style(position='center', bold=1, border=1, fontos='black', font_height=180, color='grey')
        other_tstyle_c = xls_format.font_style(position='center', fontos='black', font_height=180, color='grey')
        other_tstyle_cr = xls_format.font_style(position='center', fontos='purple_ega', bold=1, font_height=180, color='grey')
        other_tstyle_r = xls_format.font_style(position='right', fontos='purple_ega', bold=1, font_height=180, color='grey')
        other_tstyle_grandc = xls_format.font_style(position='center', fontos='purple_ega', bold=1,border=1, font_height=180, color='grey')
        other_tstyle_grandr = xls_format.font_style(position='right', fontos='purple_ega', bold=1,border=1, font_height=180, color='grey')

        datas = {
                 'form':
                    {
                        'company_id': self.company_id and [self.company_id.id] or [],
                        'warehouse_ids': [y.id for y in self.warehouse_ids],
                        'location_id': self.location_id and self.location_id.id or False,
                        #'inventory_date': self.inventory_date,
                        'start_date': self.start_date,
                        'end_date': self.end_date,
                        'only_summary': self.only_summary,
                        'id': self.id,
                        'filter_product_ids': [p.id for p in self.filter_product_ids],
                        'filter_product_categ_ids': [p.id for p in self.filter_product_categ_ids] 
                    }
                }

        company_ids = self._to_company(
                       self.company_id and [self.company_id.id] or [])

        company_obj = self.env['res.company']
        summary = self.only_summary and 'Summary Report' or 'Detail Report'
        for company in company_ids:
            c_rec = company_obj.sudo().browse(company)
            Hedaer_Text = '%s' % (str(c_rec.name))
            sheet = workbook.add_sheet(Hedaer_Text)
            sheet.set_panes_frozen(True)
            sheet.set_horz_split_pos(9)
            sheet.row(0).height = 256 * 3
            sheet.write_merge(0, 0, 0, 11, Hedaer_Text, M_header_tstyle)

            total_lines = self._get_lines(datas, company)
            warehouses = self.xls_get_warehouses(
                         [y.id for y in self.warehouse_ids], company)
            sheet_start_header = 3
            sheet_start_value = 4
            sheet.write_merge(sheet_start_header, sheet_start_header, 0, 1, 'Date', header_tstyle_c)
            sheet.write_merge(sheet_start_value, sheet_start_value, 0, 1, self.start_date + ' To ' + self.end_date, other_tstyle_cr)
            sheet.write_merge(sheet_start_header, sheet_start_header, 2, 3, 'Company', header_tstyle_c)
            sheet.write_merge(sheet_start_value, sheet_start_value, 2, 3, c_rec.name, other_tstyle_c)
            sheet.write_merge(sheet_start_header, sheet_start_header, 4, 5, 'Warehouse(s)', header_tstyle_c)
            sheet.write_merge(sheet_start_value, sheet_start_value, 4, 5, warehouses, other_tstyle_c)
            sheet.write_merge(sheet_start_header, sheet_start_header, 6, 7, 'Currency', header_tstyle_c)
            sheet.write_merge(sheet_start_value, sheet_start_value, 6, 7, c_rec.currency_id.name, other_tstyle_c)
            sheet.write_merge(sheet_start_header, sheet_start_header, 8, 9, 'Display', header_tstyle_c)
            sheet.write_merge(sheet_start_value, sheet_start_value, 8, 9, summary, other_tstyle_c)

            if self.only_summary:
                header_row_start = 8
                sheet.col(0).width = 256 * 25
                sheet.write(header_row_start, 0, 'Category Name ', header_tstyle_c)
                sheet.col(1).width = 256 * 25
                sheet.write(header_row_start, 1, 'Total Beginning', header_tstyle_c)
                sheet.col(2).width = 256 * 25
                sheet.write(header_row_start, 2, 'Total Received', header_tstyle_c)
                sheet.col(3).width = 256 * 25
                sheet.write(header_row_start, 3, 'Total Sales', header_tstyle_c)
                sheet.col(4).width = 256 * 25
                sheet.write(header_row_start, 4, 'Total Internal', header_tstyle_c)
                sheet.col(5).width = 256 * 25
                sheet.write(header_row_start, 5, 'Total Adjustment', header_tstyle_c)
                sheet.col(6).width = 256 * 25
                sheet.write(header_row_start, 6, 'Total Ending', header_tstyle_c)
                sheet.col(7).width = 256 * 25
                sheet.write(header_row_start, 7, 'Total Values', header_tstyle_c)
                row = 9
                grand_total_qty_in, grand_total_qty_out, grand_total_qty_internal, \
                    grand_total_product_qty_adjustment, \
                    grand_total_beginning_inventory,\
                    grand_total_ending_inventory, grand_total_subtotal_cost = 0.0, \
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0
                for key, values in total_lines.items():
                    total_qty_in, total_qty_out, total_qty_internal, \
                        total_product_qty_adjustment, \
                        total_beginning_inventory,\
                        total_ending_inventory, total_subtotal_cost = 0.0, \
                        0.0, 0.0, 0.0, 0.0, 0.0, 0.0
                    for line in values:
                        product_qty_in = line.get('product_qty_in', 0.0) or 0.0
                        product_qty_out = line.get('product_qty_out', 0.0) or 0.0
                        product_qty_internal = line.get('product_qty_internal', 0.0) or 0.0
                        product_qty_adjustment = line.get('product_qty_adjustment', 0.0) or 0.0
                        beginning_inventory = self._get_beginning_inventory(
                                      datas,
                                      company,
                                      line.get('product_id', '') or '',
                                      line
                                      )
                        unit_cost = self._get_cost(
                          company,
                          line.get('product_id', '') or '',
                          self.end_date,
                          )
                        ending_inventory = self._get_ending_inventory(
                          product_qty_in,
                          product_qty_out,
                          product_qty_internal,
                          product_qty_adjustment,
                          )
                        subtotal_cost = self._get_subtotal_cost(
                          unit_cost,
                          ending_inventory,
                          line
                          )
                        total_beginning_inventory += beginning_inventory
                        total_qty_in += product_qty_in
                        total_qty_out += product_qty_out
                        total_qty_internal += product_qty_internal
                        total_product_qty_adjustment += product_qty_adjustment
                        total_ending_inventory += ending_inventory
                        total_subtotal_cost += subtotal_cost

                    grand_total_beginning_inventory += total_beginning_inventory
                    grand_total_qty_in += total_qty_in
                    grand_total_qty_out += total_qty_out
                    grand_total_qty_internal += total_qty_internal
                    grand_total_product_qty_adjustment += total_product_qty_adjustment
                    grand_total_ending_inventory += total_ending_inventory
                    grand_total_subtotal_cost += total_subtotal_cost
                    sheet.write(
                            row,
                            0,
                            self._get_categ(key),
                            other_tstyle_c)
                    sheet.write(
                            row,
                            1,
                            "%.2f" % total_beginning_inventory,
                            other_tstyle_r)
                    sheet.write(
                            row,
                            2,
                            "%.2f" % total_qty_in,
                            other_tstyle_r)
                    sheet.write(
                            row,
                            3,
                            "%.2f" % total_qty_out,
                            other_tstyle_r)
                    sheet.write(
                            row,
                            4,
                            "%.2f" % total_qty_internal,
                            other_tstyle_r)
                    sheet.write(
                            row,
                            5,
                            "%.2f" % total_product_qty_adjustment,
                            other_tstyle_r)
                    sheet.write(
                            row,
                            6,
                            "%.2f" % total_ending_inventory,
                            other_tstyle_r)
                    sheet.write(
                            row,
                            7,
                            "%.2f" % total_subtotal_cost,
                            other_tstyle_r)
                    row += 1

                sheet.write(
                        row,
                        0,
                        "Grand Total",
                        other_tstyle_grandc)
                sheet.write(
                        row,
                        1,
                        "%.2f" % grand_total_beginning_inventory,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        2,
                        "%.2f" % grand_total_qty_in,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        3,
                        "%.2f" % grand_total_qty_out,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        4,
                        "%.2f" % grand_total_qty_internal,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        5,
                        "%.2f" % grand_total_product_qty_adjustment,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        6,
                        "%.2f" % grand_total_ending_inventory,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        7,
                        "%.2f" % grand_total_subtotal_cost,
                        other_tstyle_grandr)
            else:
                header_row_start = 8
                sheet.write(header_row_start, 0, 'Category Name ', header_tstyle_c)
                sheet.col(0).width = 256 * 20
                sheet.write(header_row_start, 1, 'Product Name ', header_tstyle_c)
                sheet.col(1).width = 256 * 40
                sheet.write(header_row_start, 2, 'Product Barcode ', header_tstyle_c)
                sheet.col(2).width = 256 * 20
                sheet.write(header_row_start, 3, 'Default Code ', header_tstyle_c)
                sheet.col(3).width = 256 * 20
                sheet.write(header_row_start, 4, 'Beginning', header_tstyle_c)
                sheet.col(4).width = 256 * 20
                sheet.write(header_row_start, 5, 'Received', header_tstyle_c)
                sheet.col(5).width = 256 * 20
                sheet.write(header_row_start, 6, 'Sales', header_tstyle_c)
                sheet.col(6).width = 256 * 20
                sheet.write(header_row_start, 7, 'Internal', header_tstyle_c)
                sheet.col(7).width = 256 * 20
                sheet.write(header_row_start, 8, 'Adjustments', header_tstyle_c)
                sheet.col(8).width = 256 * 20
                sheet.write(header_row_start, 9, 'Ending', header_tstyle_c)
                sheet.col(9).width = 256 * 20
                sheet.write(header_row_start, 10, 'Cost', header_tstyle_c)
                sheet.col(10).width = 256 * 20
                sheet.write(header_row_start, 11, 'Total Value', header_tstyle_c)
                sheet.col(11).width = 256 * 20
                row = 9
                grand_total_qty_in, grand_total_qty_out, grand_total_qty_internal, \
                    grand_total_product_qty_adjustment, \
                    grand_total_beginning_inventory,\
                    grand_total_ending_inventory, grand_total_subtotal_cost = 0.0, \
                    0.0, 0.0, 0.0, 0.0, 0.0, 0.0
                for values in total_lines.values():
                    for line in values:
                        product_qty_in = line.get('product_qty_in', 0.0) or 0.0
                        product_qty_out = line.get('product_qty_out', 0.0) or 0.0
                        product_qty_internal = line.get('product_qty_internal', 0.0) or 0.0
                        product_qty_adjustment = line.get('product_qty_adjustment', 0.0) or 0.0
                        beginning_inventory = self._get_beginning_inventory(
                                      datas,
                                      company,
                                      line.get('product_id', '') or '',
                                      line
                                      )
                        unit_cost = self._get_cost(
                          company,
                          line.get('product_id', '') or '',
                          self.end_date,
                          )
                        ending_inventory = self._get_ending_inventory(
                          product_qty_in,
                          product_qty_out,
                          product_qty_internal,
                          product_qty_adjustment,
                          )
                        subtotal_cost = self._get_subtotal_cost(
                          unit_cost,
                          ending_inventory,
                          line
                          )
                        product_name, product_barcode, product_code = \
                            self._product_detail(line.get('product_id', '') or '')
                        grand_total_beginning_inventory += beginning_inventory
                        grand_total_qty_in += product_qty_in
                        grand_total_qty_out += product_qty_out
                        grand_total_qty_internal += product_qty_internal
                        grand_total_product_qty_adjustment += product_qty_adjustment
                        grand_total_ending_inventory += ending_inventory
                        grand_total_subtotal_cost += subtotal_cost
                        if self._value_existed(
                                          beginning_inventory,
                                          product_qty_in,
                                          product_qty_out,
                                          product_qty_internal,
                                          product_qty_adjustment,
                                          ending_inventory
                                          ):
                            sheet.write(
                                    row,
                                    0,
                                    self._get_categ(line.get('categ_id', '') or ''),
                                    other_tstyle_c)
                            sheet.write(
                                    row,
                                    1,
                                    product_name or '',
                                    other_tstyle_c)
                            sheet.write(
                                    row,
                                    2,
                                    product_barcode or '',
                                    other_tstyle_c)
                            sheet.write(
                                    row,
                                    3,
                                    product_code or '',
                                    other_tstyle_c)
                            sheet.write(
                                    row,
                                    4,
                                    "%.2f" % beginning_inventory,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    5,
                                    "%.2f" % product_qty_in,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    6,
                                    "%.2f" % product_qty_out,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    7,
                                    "%.2f" % product_qty_internal,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    8,
                                    "%.2f" % product_qty_adjustment,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    9,
                                    "%.2f" % ending_inventory,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    10,
                                    "%.2f" % unit_cost,
                                    other_tstyle_r)
                            sheet.write(
                                    row,
                                    11,
                                    "%.2f" % subtotal_cost,
                                    other_tstyle_r)
                            row += 1

                sheet.write(
                        row,
                        3,
                        "Grand Total",
                        other_tstyle_grandc)
                sheet.write(
                        row,
                        4,
                        "%.2f" % grand_total_beginning_inventory,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        5,
                        "%.2f" % grand_total_qty_in,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        6,
                        "%.2f" % grand_total_qty_out,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        7,
                        "%.2f" % grand_total_qty_internal,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        8,
                        "%.2f" % grand_total_product_qty_adjustment,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        9,
                        "%.2f" % grand_total_ending_inventory,
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        10,
                        "-",
                        other_tstyle_grandr)
                sheet.write(
                        row,
                        11,
                        "%.2f" % grand_total_subtotal_cost,
                        other_tstyle_grandr)

        stream = cStringIO.StringIO()
        workbook.save(stream)

        export_obj = self.env['stock.valuation.success.box']
        res_id = export_obj.create({
                'file': base64.encodestring(stream.getvalue()),
                'fname': "Stock Valuation Report.xls"
                })
        return {
             'type': 'ir.actions.act_url',
             'url': '/web/binary/download_document?model=stock.valuation.success.box&field=file&id=%s&filename=Stock Valuation Report.xls'%(res_id.id),
             'target': 'new',
             }


class StockValuationSuccessBox(models.TransientModel):
    _name = 'stock.valuation.success.box'

    file = fields.Binary('File', readonly=True)
    fname = fields.Char('Text')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

class print_excel(models.Model):
	_name = 'exam.report.output'

	xls_output = fields.Binary(string='Excel Output')
	name = fields.Char(string='File Name', help='Save report as .xls format', default='Trial_Balance.xls')
