# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError



class Contrat(models.Model):
    _name = 'dubai.contrat'
    _rec_name='reference'
    reference  = fields.Char(string=' Reference ',required=True)
    type_de_contrat= fields.Selection(string='Contract  type', selection=[(u'achat', u'Achat'), (u'vente', u'Vente'),
     (u'echange reçu', u'Échange reçu'), (u'echange cede,', u'Échange cédé'), 
     ('prestation de service', u'Prestation de service'),],required=True)
    date_debut  = fields.Date(string='Start date',required=True)
    date_fin  = fields.Date(string='Finish date ',required=True)
    parties = fields.Selection(string='Les parties au contrat',
     selection=[('vendeur,', 'Vendeur'), ('acheteur', 'Acheteur'),])
    lieu = fields.Many2one(comodel_name='res.country', string='Location')
   
   
    taux = fields.Float(string='Change rate')
    line_ids = fields.One2many(comodel_name='dubai.contrat.line', inverse_name='dubai_contrat', string=" produits")
    term = fields.Many2one(comodel_name='account.payment.term', string='Payment Terms')
    incoterm_id = fields.Many2one(comodel_name='stock.incoterms', string='Incoterm')
    partner = fields.Many2one(comodel_name='res.partner', string='Stockholder',required=True) #,domain=[('supplier','=',True)]
    purchase = fields.One2many(comodel_name='purchase.order', inverse_name='contrat', string='purchase')
    mode_de_transport= fields.Selection(string='Freight mode', selection=[('mod1', 'mod2'), ('mod2', 'mod2'),])
    mode_de_paiement= fields.Selection(string='Payment mode ', selection=[('mod1', 'mod1'), ('mod2', 'mod2'),])
    delai_de_pay= fields.Selection(string='Payment terms', selection=[('mod1', 'mod1'), ('mod2', 'mod3'),])
    unit = fields.Many2one(comodel_name='product.uom', string='UOM')
    currency= fields.Many2one(comodel_name='res.currency', string='Currency',required=True)
    qty = fields.Float(string='Contract qty',compute='get_qty')
    amount = fields.Float(string='Amount',compute='get_amount')
    @api.one
    @api.depends('line_ids')
    def get_qty(self):
        qty=0
        for q in self.line_ids:
            qty+=q.qte_stard
        self.qty=qty
    @api.one
    @api.depends('line_ids')
    def get_amount(self):
        amount=0
        for q in self.line_ids:
            amount+=q.amount
        self.amount=amount
    
    
     
    
class Contratlines(models.Model):
    _name = 'dubai.contrat.line'
    dubai_contrat = fields.Many2one(comodel_name='dubai.contrat', string='')
    product_id = fields.Many2one(comodel_name='product.product', string='Product')
    prix = fields.Many2one(comodel_name='dubai.contrat.line.listdesprix', string='Price')
    qte_stard = fields.Float(string='Contract Qty' )
    qte = fields.Float(string='Remaining qty',readonly=True)
    unit = fields.Many2one(comodel_name='product.uom', string='UOM',required=True)
    currency= fields.Many2one(comodel_name='res.currency', string='Currency')
    amount = fields.Float(string='Amount',compute='get_amount')
    @api.one
    @api.depends('prix')
    def get_amount(self):
        amount=0
        for q in self.prix:
            amount+=q.prix_fin
        self.amount=amount
    

    
    
    @api.model
    def create(self, values):
        # Add code here
        print 55555555555555555555555555555
        values['qte']=values['qte_stard']
        p = super(Contratlines, self).create(values)
        print 444444444444444444444444
        return p
    
    
class ContratListPrix(models.Model):
    _name = 'dubai.contrat.line.listdesprix'
    _rec_name='prix_fin'
    type_de_prix = fields.Selection(string='Price type', selection=[('fixe', 'Fixed price'), ('formule', 'Formula'),],default='fixe')
    prix= fields.Float(string='Price')
    prix_fin = fields.Float(compute='get_prix')
    unit = fields.Many2one(comodel_name='product.uom', string='UOM')
    

    formuls = fields.One2many(comodel_name='dubai.contrat.line.listdesprix_method',
     inverse_name='par', string='Formulas')
    
    @api.one
    @api.depends('prix','type_de_prix')
    def get_prix(self):
        if self.type_de_prix=='fixe':
            self.prix_fin=self.prix
            return self.prix
        tot=0
        for formul in self.formuls:
            tot+=formul.tot
        self.prix_fin=tot
        return 0
class  methodPrix(models.Model):
    _name = 'dubai.contrat.line.listdesprix_method'
    
    par = fields.Many2one(comodel_name='dubai.contrat.line.listdesprix', string='sss')
    obj_name = fields.Many2one(comodel_name='dubai.contrat.line.listdesprix_methodname', string='name')
    
   
    type = fields.Selection(string='Type', selection=[('percentage', 'Percentage'), 
    ('fix', 'Fix'),])
    tot = fields.Float(string='Value')
    
class   methodPrixname(models.Model):
    
    _name = 'dubai.contrat.line.listdesprix_methodname'
    _rec_name='name'

    name = fields.Char(string='name')
