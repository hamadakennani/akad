# -- coding: utf-8 --
from datetime import date
from odoo import fields, models,api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
class Roomtype(models.Model):
    _inherit = 'hotel.room.type'

    name = fields.Char(string='Name')
    variantes = fields.One2many(comodel_name='hotel.room.type.variante', inverse_name='room_type', string='Variantes')
    product_cat = fields.Many2one(comodel_name='product.category', string='')
    
    @api.model
    def create(self, values):
        # Add code here
        p= super(Roomtype, self).create(values)
        print 'prent'
        print p.categ_id.product_cat
        cat= self.env['product.category'].create({
            'name':p.name,
            'parent_id':p.categ_id.product_cat.id,
            'property_valuation':'manual_periodic'
        })
        p.product_cat=cat.id
        print cat
        print 'caaaaaaat'
        return p




class Variante(models.Model):
    _name = 'hotel.room.type.variante'
    _rec_name='title'
    title = fields.Char(string='libelle',compute='_get_title',store=True)
    @api.one
    @api.depends('valeur')
    def _get_title(self):
        
        if self.name and self.valeur:
            self.title = self.name+' / '+str(self.valeur)
    
    
    name = fields.Char(string='Nom',required=True)
    valeur = fields.Integer(string='Valeur',required=True)
    room_type = fields.Many2one(comodel_name='hotel.room.type', string='')






    
    

   