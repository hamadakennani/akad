﻿# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import ValidationError
from itertools import groupby
from datetime import datetime, timedelta


from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp

class hotel_reservation(models.Model):
    _inherit = 'hotel.folio'

    company_id = fields.Many2one(comodel_name='res.company', string='Société')
    warehouse_id = fields.Many2one('stock.warehouse', 'Hotel', required=True)
    food_lines = fields.One2many(comodel_name='folio.foodlinee', inverse_name='reservation_id', string='Restaurant Lines')
    transport_lines = fields.One2many(comodel_name='folio.trasnportline', inverse_name='transport_id', string='Transport Lines')
    laundry_lines = fields.One2many(comodel_name='folio.lundryline', inverse_name='laundry_id', string='Lundry Lines')
    
    # tax_id=  fields.Many2one(comodel_name='account.tax', string='Taxe')
    product_uom_qte = fields.Integer(string='Quantité')
    categ_id = fields.Many2one(comodel_name='hotel.room.type', string='Room Type')
    sous_total = fields.Float(string='Sous total')
   
    fiscal_position_id	 = fields.Many2one(comodel_name='account.fiscal.position', string='Position fiscale	')
    note = fields.Text(string='')
    origin = fields.Char(string='Document d\'origine')

    @api.depends('montant_avance','order_id.avance_m')
    def get_mont(self):
        print 'get montant folio'
        self.montant_avance=self.order_id.avance_m

    montant_avance=fields.Float(string="Montant de l'avance",compute='get_mont')
    
    @api.depends('tax_id','prix','price_unit')
    def get_sold(self):
        return  format(self.amount_total-self.montant_avance, '.3f')
    @api.multi
    def check_overlap(self, date1, date2):
        date2 = datetime.strptime(date2, '%Y-%m-%d %H:%M:%S')
        date1 = datetime.strptime(date1, '%Y-%m-%d %H:%M:%S')
       
        delta = date2 - date1
        print 'Delta'
        print delta
        print delta.days
        return [date1 + timedelta(days=i) for i in range(delta.days)]
    @api.multi
    def get_report_data(self):
        #room_lines service_lines food_lines transport_lines  laundry_lines
        data=[]
        print 'Folio report ----'
        print ' Add ---- doc -'
        print self.checkin_date
       
        # list.sort(key=lambda item:item['date'], reverse=True)
        # to do  Add time  of transport and food lines
        for  rl in self.room_lines:
            for  dat in self.check_overlap(rl.checkin_date,rl.checkout_date):
              data.append({'date': dat.date(), 'price':format(rl.price_unit, '.3f'), 'Title': 'Hébergement'})
        
        for  ser in self.service_lines:

            if ser.product_uom_qty>1 and len(self.check_overlap(ser.ser_checkin_date,ser.ser_checkout_date))<=1:
                str_date=datetime.strptime(ser.ser_checkin_date, '%Y-%m-%d %H:%M:%S').date()
                for  dat in range(int(ser.product_uom_qty)) :
                     data.append({'date': str_date, 'price': format(ser.price_unit), 'Title': 'Service'})
            else:
              for  dat in self.check_overlap(ser.ser_checkin_date,ser.ser_checkout_date):
                data.append({'date': dat.date(), 'price': format(ser.price_unit), 'Title': 'Service'})
        for  tr in self. transport_lines:
            data.append({'date':datetime.strptime(self.checkin_date,'%Y-%m-%d %H:%M:%S').date(), 'price': format(tr.price_unit, '.3f'), 'Title': 'Transport'})
        for  fo in self.food_lines:
            data.append({'date':datetime.strptime(self.checkin_date,'%Y-%m-%d %H:%M:%S').date(), 'price':format(fo.price_unit, '.3f'), 'Title': 'Restaurant'})
        for  la in self.laundry_lines:
            data.append({'date':datetime.strptime(self.checkin_date,'%Y-%m-%d %H:%M:%S').date(), 'price':format(la.price_unit, '.3f'), 'Title': 'laundry'})
       
        print 'data'
        print data
        data.sort(key=lambda item:item['date'])
        print data
        return data
    
    @api.model
    def create(self, values):
        print 'hotel_folio hotel_folio hotel_folio hotel_folio'
        p= super(hotel_reservation, self).create(values)
        print 'ordeeeeeeeer id'
        print p.order_id
        lst=[]
        if not p.name:
            p.write({
                'name':self.sudo().env['ir.sequence'].next_by_code('hotel.folio')
        })
        for a in p.room_lines:
            date_from=  datetime.strptime(a.checkin_date,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(a.checkout_date,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            a.write({
                 'qty_to_invoice':delta.days
            })
        for a in p.service_lines:
            date_from=  datetime.strptime(a.ser_checkin_date,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(a.ser_checkin_date,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            a.write({
                'qty_to_invoice':delta.days
            })
        for a in p.food_lines:
            print 'old id ' + str(a.order_id)
            print 'new id '+str(p.order_id.id)
            lst.append(a.order_id)
            a.write({
                'order_id':p.order_id.id,
                'qty_to_invoice':a.product_uom_qty
            })    
            print 'old id ' + str(a.order_id)
            print 'new id '+str(p.order_id.id)        
        for a in p.transport_lines:
            lst.append(a.order_id)
            a.write({
                    'order_id':p.order_id.id,
                    'qty_to_invoice':a.product_uom_qty
                })
        for a in p.laundry_lines:
            lst.append(a.order_id)
            a.write({
                    'order_id':p.order_id.id,
                    'qty_to_invoice':a.product_uom_qty

                }) 
        print lst
        for a in lst :

            print 'delete orrrder' + str(a.id)
            print a.pricelist_id.company_id
            print self.env.uid
            # print self.env.search()
            # .company_id
            if a.id != p.order_id.id:
                self.env['sale.order'].search([('id','=',a.id)]).sudo().unlink()
                print 'iiiiiiiiiiiiii'
        # for a in p.food_lines:
        #     print a.order_id
        #     print 'new id'
        return p 
    	
    @api.multi
    def write(self, values):
        print 'wriiiiiiiiiiiiiiiiiiiite'
        p= super(hotel_reservation, self).write(values)
        for a in self.room_lines:
            date_from=  datetime.strptime(a.checkin_date,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(a.checkout_date,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            a.write({
                'qty_to_invoice':delta.days
            })
            
        for a in self.service_lines:
            date_from=  datetime.strptime(a.ser_checkin_date,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(a.ser_checkout_date,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            a.write({
                'qty_to_invoice':delta.days
            })
        lst=[]
        for a in self.food_lines:
            lst.append(a.order_id)
            a.write({
                'order_id':self.order_id.id,
                'qty_to_invoice':a.product_uom_qty
            })
        for a in self.transport_lines:
            lst.append(a.order_id)
            a.write({
                    'order_id':self.order_id.id,
                    'qty_to_invoice':a.product_uom_qty
                })
        for a in self.laundry_lines:
            lst.append(a.order_id)
            a.write({
                    'order_id':self.order_id.id,
                    'qty_to_invoice':a.product_uom_qty
                })         
        for a in lst :
            if a.id != self.order_id.id:
                print 'flag'
                if a.id != self.order_id.id:
                    self.env['sale.order'].search([('id','=',a.id)]).unlink()        
        return p




class SaleOrder(models.Model):
    _inherit = 'sale.order.line'
    from_resv = fields.Boolean(string='',default=False)

    @api.model
    def create(self, values):
        if self.env.user.has_group('hotel_itshore.group_hotel_itshore_admin'):
            # print values
            print 'SaleOrder SaleOrder SaleOrder SaleOrder SaleOrder SaleOrder'
            # print 
            print values
            # if values['isline']:
            if 'tax_id' in values:
                if not 'from_resv' in values:
                    if len(values['tax_id'])>0:
                            if len(values['tax_id'][0][2])>1:
                                raise ValidationError("Vous devez choisir un seul taxe")
                else:
                    if not values['from_resv']:
                        if len(values['tax_id'])>0:
                            if len(values['tax_id'][0][2])>1:
                                raise ValidationError("Vous devez choisir un seul taxe")
        
            usr=self.env['res.users'].search([('id','=',self.env.uid)])
            a=self.env['product.pricelist'].search([('company_id','=',usr.company_id.id)])
            if 'order_id' not in values:       
                # values['order_id']=1     
                ord=self.env['sale.order'].create({
                    'project_id':False,
                    'user_id': self.env.uid,
                    #'partner_shipping_id':1,
                    'picking_policy':'direct',
                    'state':'draft',
                    #'team_id':1,
                    'client_order_ref':False,
                    #'partner_invoice_id':1,
                    'pricelist_id':a[0].id,
                    'date_order':datetime.now().date(),
                    'company_id':usr.company_id.id,
                    'partner_id':usr.partner_id.id,
                })
                values['order_id']=ord.id
            print 'ordeeeeeeeer id sale-----------'
            print values['order_id']
        return super(SaleOrder, self).create(values)

    
    @api.depends('product_uom_qty','discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        print 'AAAAAAAAAAAAAAAAA'
        
        for line in self:
            print line.tax_id
            print line
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
            line.update({
                'price_tax': taxes['total_included'] - taxes['total_excluded'],
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })


class SaleOrderr(models.Model):
    _inherit = 'sale.order'

    
        
    avance_m = fields.Float(string='Montant de l\'avance')
    

    @api.depends('order_line.price_total','avance_m')
    def _amount_all(self):
        print 'hhhhhhhhhhhhhhhhh5'
        for order in self:
            amount_untaxed = amount_tax = 0.0
            for line in order.order_line:
                amount_untaxed += line.price_subtotal
                # FORWARDPORT UP TO 10.0
                if order.company_id.tax_calculation_rounding_method == 'round_globally':
                    price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
                    taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=order.partner_shipping_id)
                    amount_tax += sum(t.get('amount', 0.0) for t in taxes.get('taxes', []))
                else:
                    amount_tax += line.price_tax
            order.update({
                'amount_untaxed': order.pricelist_id.currency_id.round(amount_untaxed),
                'amount_tax': order.pricelist_id.currency_id.round(amount_tax),
                'amount_total': amount_untaxed + amount_tax - order.avance_m,
            })


class Folio_line(models.Model):
    _inherit = 'hotel.folio.line'

    # tax_id=fields.Many2one(comodel_name='account.tax', string='Taxe')
    product_uom_qte = fields.Integer(string='Quantité')
    categ_id = fields.Many2one(comodel_name='hotel.room.type', string='Room Type')

    sous_total = fields.Float(string='Sous total')
    name = fields.Text(string='',required=False)
    # room_id=fields.Many2one(comodel_name='hotel.room', string='Room')
    # price_subtotal = fields.Monetary(string='hhh')
    # price_subtotal
    number_of_dayes=fields.Float(string='Nombre de jours',compute='_compute_number_of_dayes')
    @api.one
    @api.depends('number_of_dayes','checkin_date','checkout_date')
    def _compute_number_of_dayes(self):
       
        print 'count days '
        print 'Before  if   of compute dayes onchange --'
        if   self.checkin_date and self.checkout_date:
            print ' in if   of compute dayes onchange --'
            
            date_from=  datetime.strptime(self.checkin_date,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(self.checkout_date,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            # print delta.days
            # if   delta.days<0:
            #    ValidationError(' date')
            self.number_of_dayes =  delta.days
            print 'Get ------- qty from folio ---'
            self.product_uom_qty=self.number_of_dayes
            self.product_uom_qty = float( delta.days)
            print ' qty'
            print self.product_uom_qty
               
      
          
    @api.model
    def create(self, values):
        print 'Folio_   lineFolio_  lineFolio_  lineFolio_line'
        print values
        if   values['checkin_date'] and values['checkout_date']:
            
            date_from=  datetime.strptime(values['checkin_date'],'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(values['checkout_date'],'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            print 'create'
            print delta.days
            
      
           
            values['product_uom_qty']=float(delta.days)
        return super(Folio_line, self).create(values)
    
    @api.onchange('product_id')
    def room_price(self):
        print 'uuuuuuuuuuuuuuuuuuuu'
        self.price_unit = self.product_id.lst_price
        #if len(self.product_id.taxes_id)>0:

           #self.tax_id=self.product_id.taxes_id[0]
 

    @api.onchange('checkin_date','checkout_date')
    def _compute_number_of_dayes(self):
       
        print 'count days '
        if   self.checkin_date and self.checkout_date:
            
            date_from=  datetime.strptime(self.checkin_date,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(self.checkout_date,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            print ' get onchenge days in onchange  folio '
            print delta.days
            print self.product_uom_qty
            self.write({
                'product_uom_qty':float( delta.days)

            })
            self.product_uom_qty = float( delta.days)
            print self.product_uom_qty


    @api.multi
    def write(self, values):
        print ' Write vales Folio lines ---'
        print values 
        p=super(Folio_line, self).write(values)
        print 'p.number_of_dayes'
        print self.number_of_dayes
        print self.product_uom_qty
      
        return p

# class CurrencyExchangeRate(models.Model):

#     _inherit = "currency.exchange"

#     @api.model
#     def create(self, values):
#         print values
#         print '8djkvcjnxkcjv jkcxnv'
#         return super(CurrencyExchangeRate, self).create(values)
    

class foodLine(models.Model):
    _name = 'folio.foodlinee'
    to_hide = fields.Boolean(string='',defaut=True)
    reservation_id=fields.Many2one(comodel_name='hotel.folio', string='Hotel reservation')
    # product_uom_qte = fields.Integer(string='Quantité')
    # product_id = fields.Many2one(comodel_name='product.product', string='Article')
    # name = fields.Char(string='Description')
    # price_unit = fields.Float(string='Prix unitaire	')
    # discount  = fields.Float(string='Remise (%)	')
    # tax_id = fields.Many2one(comodel_name='account.tax', string='Taxes')
    # state= fields.Selection(string='État de la commande	',default='draft', selection=[('draft', 'Devis'), ('sent', 'Devis envoyé'),('sale', 'À facturer'),('progress', 'En progression'),('check_out', 'CheckOut'),('done', 'Fait'),('cancel', 'Annulé')])
    sous_total = fields.Float(string='Sous total')
    # price_subtotal=fields.Float()
    # product_uom = fields.Many2one(comodel_name='product.uom', string='Unité de mesure')
    name = fields.Text(string='',required=False)
    food_line_id = fields.Many2one('sale.order.line', 'Food Line',
                                      required=True, delegate=True,
                                      ondelete='cascade')
    folio_id = fields.Many2one('hotel.folio', 'Folio', ondelete='cascade')
    """@api.onchange('ser_checkout_date','product_id')
    def on_change_product(self):
        print ' cat eg onchange'
        ids=[]
        res={}
        products = self.env['product.product'].search([('isfood','=',True)])
        for  prod in products:          
            ids.append(prod.id)
        res['domain']={'product_id':[('id','in',ids)]}
        return res"""
    @api.onchange('product_id')
    def room_price(self):
        print 'uuuuuuuuuuuuuuuuuuuu'
        self.price_unit = self.product_id.lst_price
        #if len(self.product_id.taxes_id)>0:
        #   self.tax_id=self.product_id.taxes_id[0]

    @api.model
    def create(self, vals, check=True):

        print ' creaaaaaate foodline'
        
        if 'folio_id' in vals:
            folio = self.env['hotel.folio'].browse(vals['folio_id'])
            vals.update({'order_id': folio.order_id.id})
        return super(foodLine, self).create(vals)


    @api.multi
    def unlink(self):
        print 'unliiiiink'
        s_line_obj = self.env['sale.order.line']
        for line in self:
            if line.food_line_id:
                sale_unlink_obj = s_line_obj.browse([line.food_line_id.id])
                sale_unlink_obj.unlink()
        return super(foodLine, self).unlink()
    # @api.model
    # def create(self, values):
    #     p= super(foodLine, self).create(values)
    #     print 'oriiiceeeeeeeeeeeee '
    #     print p.product_id.lst_price
    #     print 'oriiice eeeeeeeeeeeeeeeeeeeeeeeeee foodline'
    #     print p.reservation_id.order_id
    #     a=self.env['sale.order.line'].create({
    #         'product_id':p.product_id.id,
    #         'product_uom':p.product_id.uom_id.id,
    #         'qty_delivered_updateable':True,
    #         'customer_lead':0,
    #         'price_unit':p.product_id.lst_price,
    #         'product_uom_qty':p.product_uom_qty,
    #         'order_id':p.reservation_id.order_id.id,
    #         'state':'draft',
    #         'product_packaging':False,
    #         'discount':p.discount
    #     })
    #     p.food_line_id=a.id
    #     return p
    

class TransportLine(models.Model):
    _name = 'folio.trasnportline'

    transport_id=fields.Many2one(comodel_name='hotel.folio', string='Hotel reservation', ondelete='cascade')
    # name = fields.Char(string='Description')
    # product_id = fields.Many2one(comodel_name='product.product', string='Article')
    # product_uom_qte = fields.Integer(string='Quantité')
    # tax_id = fields.Many2one(comodel_name='account.tax', string='Taxes')
    # discount  = fields.Float(string='Remise (%)	')
    # price_unit = fields.Float(string='Prix unitaire	')
    # sous_total = fields.Float(string='Sous total')
    # state= fields.Selection(string='État de la commande	',default='draft', selection=[('draft', 'Devis'), ('sent', 'Devis envoyé'),('sale', 'À facturer'),('progress', 'En progression'),('check_out', 'CheckOut'),('done', 'Fait'),('cancel', 'Annulé')])
    name = fields.Text(string='',required=False)
    trasnport_line_id = fields.Many2one('sale.order.line', 'Food Line',
                                      required=True, delegate=True,
                                      ondelete='cascade')
    to_hide = fields.Boolean(string='',defaut=True)
    prix = fields.Float(string="Montant de l'avance")
    
    

    @api.depends('tax_id')
    def _get_tax(self):
        return self.tax_id.amount

    @api.depends('tax_id','prix','price_unit')
    def get_sold(self):
        return  format(self.price_unit-self.prix, '.3f')
    @api.onchange('product_id')
    def room_price(self):
        print 'uuuuuuuuuuuuuuuuuuuu'
        self.price_unit = self.product_id.lst_price
        #if len(self.product_id.taxes_id)>0:
        #   self.tax_id=self.product_id.taxes_id[0]

    @api.model
    def create(self, vals, check=True):
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        @return: new record set for hotel service line.
        """
        if 'folio_id' in vals:
            folio = self.env['hotel.folio'].browse(vals['folio_id'])
            vals.update({'order_id': folio.order_id.id})
        return super(TransportLine, self).create(vals)

    @api.multi
    def unlink(self):
        print 'unliiiiink'
        s_line_obj = self.env['sale.order.line']
        for line in self:
            if line.trasnport_line_id:
                sale_unlink_obj = s_line_obj.browse([line.trasnport_line_id.id])
                sale_unlink_obj.unlink()
        return super(TransportLine, self).unlink()

class LaundrytLine(models.Model):
    _name = 'folio.lundryline'

    qte=fields.Integer(string='Quantité')
    qte_view=fields.Integer(string='Quantité')

    laundry_id=fields.Many2one(comodel_name='hotel.folio', string='Hotel reservation',ondelete='cascade')
    trasnport_line_id = fields.Many2one('sale.order.line', 'Food Line',
                                      required=True, delegate=True,
                                      ondelete='cascade')
    to_hide = fields.Boolean(string='',defaut=True)
    name = fields.Text(string='',required=False)
    prix = fields.Float(string="Montant de l'avance")


    @api.depends('tax_id','prix','price_unit','qte')
    def get_sold(self):
        return  format(self.price_unit-self.prix, '.3f')

    # @api.depends('qte')
    # def set_gte(self):
    #     self.product_uom_qte=float(self.qte)
    #     self.qte_view=self.qte
    # @api.onchange('qte')
    # def get_qte(self):
    #     self.product_uom_qte=float(self.qte)
    
    # name = fields.Char(string='Description')
    # product_id = fields.Many2one(comodel_name='product.product', string='Article')
    # product_uom_qte = fields.Integer(string='Quantité')
    # tax_id = fields.Many2one(comodel_name='account.tax', string='Taxes')
    # discount  = fields.Float(string='Remise (%)	')
    # price_unit = fields.Float(string='Prix unitaire	')
    # sous_total = fields.Float(string='Sous total')
    # state= fields.Selection(string='État de la commande	',default='draft', selection=[('draft', 'Devis'), ('sent', 'Devis envoyé'),('sale', 'À facturer'),('progress', 'En progression'),('check_out', 'CheckOut'),('done', 'Fait'),('cancel', 'Annulé')])

    @api.onchange('product_id')
    def room_price(self):
        print 'uuuuuuuuuuuuuuuuuuuu'
        self.price_unit = self.product_id.lst_price
        #if len(self.product_id.taxes_id)>0:
        #   self.tax_id=self.product_id.taxes_id[0]

    @api.model
    def create(self, vals, check=True):
        """
        Overrides orm create method.
        @param self: The object pointer
        @param vals: dictionary of fields value.
        @return: new record set for hotel service line.
        """
        if 'folio_id' in vals:
            folio = self.env['hotel.folio'].browse(vals['folio_id'])
            vals.update({'order_id': folio.order_id.id})
        vals['product_uom_qty']=vals['qte']
        return super(LaundrytLine, self).create(vals)
    
    @api.multi
    def write(self, values):
        
        print 'ijkhjkhkhkjhukij'
        print self.product_uom_qty
        print self.qte
        p= super(LaundrytLine, self).write(values)
        if self.product_uom_qty !=float(self.qte) :
            print 'i write   ---ij'
            print self.product_uom_qty
            print self.qte
            self.product_uom_qty=float(self.qte)
        return p
    
    
    @api.multi
    def unlink(self):
        print 'unliiiiink'
        s_line_obj = self.env['sale.order.line']
        for line in self:
            if line.trasnport_line_id:
                sale_unlink_obj = s_line_obj.browse([line.trasnport_line_id.id])
                sale_unlink_obj.unlink()
        return super(LaundrytLine, self).unlink()