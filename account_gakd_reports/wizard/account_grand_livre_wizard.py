# -*- coding: utf-8 -*-

from odoo import api, fields, models
class AccountGrandLivreReport(models.TransientModel):
    _inherit = "account.report.general.ledger"
    compte_id = fields.Many2one('account.account',string="Compte")

    def pre_print_report(self, data):
        data['form'].update(self.read(['compte_id','display_account'])[0])
        return data