# -- coding: utf-8 --
from odoo import api, models
import math
from datetime import datetime
from datetime import timedelta
from datetime import date
class SituatuionReport(models.AbstractModel):
    _name = 'report.hotel.situation'

    def get_dataa(self):
        print 'weeeeeeeeeeeeeeeeeeeeere IN '
        data = []
        commandes=self.env["pos.order"].search([('company_id','=',self.env.user.company_id.id),('state','=','paid')])
        journals=self.env["account.journal"].search([('company_id','=',self.env.user.company_id.id)])
        print commandes
        today=date.today()
        for j in journals:
            montant_jour = 0
            montant_mois = 0
            journal_id = j
            for cmd in commandes :
                for st in cmd.statement_ids:
                    if j.id == st.journal_id.id :
                        if datetime.strptime(st.date, '%Y-%m-%d').date()<date.today() and datetime.strptime(st.date, '%Y-%m-%d').date() > today.replace(day=1):
                            montant_mois+=st.amount
                        elif datetime.strptime(st.date, '%Y-%m-%d').date() ==date.today():
                            montant_jour +=st.amount
            payements=self.env["account.payment"].search([('company_id','=',self.env.user.company_id.id)])
            for p in payements:
                if p.journal_id.id == j.id :
                    if  datetime.strptime(p.payment_date, '%Y-%m-%d').date()<date.today() and datetime.strptime(p.payment_date, '%Y-%m-%d').date() > today.replace(day=1):
                        montant_mois+=p.amount
                    elif datetime.strptime(p.payment_date, '%Y-%m-%d').date() ==date.today():
                        montant_jour +=p.amount
            if montant_jour != 0 or montant_mois!=0:
                data.append({'montant_jour':montant_jour,'montant_mois':montant_mois,'name':j.name,})
        print data
        return data
    
    def get_data(self):
        data = []
        print 'secooooooooooond'
        montant_hebergement=0
        montant_restau=0
        montant_transport=0
        montant_lundry=0
        montant_service=0

        montant_hebergement_mois=0
        montant_restau_mois=0
        montant_transport_mois=0
        montant_lundry_mois=0
        montant_service_mois=0

        taxe_jour=0
        taxe_mois=0
        
        today=date.today()
        folio=self.env["hotel.folio"].search([('company_id','=',self.env.user.company_id.id)])
        print folio
        for fol in folio :
            print fol
            room_montant=0
            serv_montant=0
            rest_montant=0
            tran_montant=0
            lund_montant=0
            taxe=0
            for room in fol.room_lines:
                room_montant+=room.price_subtotal
                taxe+=taxe+room.price_tax
            for room in fol.service_lines:
                serv_montant+=room.price_subtotal
                taxe+=taxe+room.price_tax
            for room in fol.food_lines:
                rest_montant+=room.price_subtotal
                taxe+=taxe+room.price_tax
            for room in fol.transport_lines:
                tran_montant+=room.price_subtotal
                taxe+=taxe+room.price_tax
            for room in fol.laundry_lines:
                lund_montant+=room.price_subtotal
                taxe+=taxe+room.price_tax
            print fol.invoice_ids
            for  inv in fol.invoice_ids :
                if inv.state=='paid':
                    for payment in inv.payment_ids:
                        print payment.payment_date
                        if datetime.strptime(payment.payment_date, '%Y-%m-%d').date()<date.today() and datetime.strptime(payment.payment_date, '%Y-%m-%d').date() > today.replace(day=1):
                            montant_hebergement_mois+=room_montant
                            montant_restau_mois+=rest_montant
                            montant_transport_mois+=tran_montant
                            montant_lundry_mois+=lund_montant
                            montant_service_mois+=serv_montant
                            taxe_mois+=taxe
                        elif datetime.strptime(payment.payment_date, '%Y-%m-%d').date() ==date.today():
                            montant_hebergement+=room_montant
                            montant_restau+=rest_montant
                            montant_transport+=tran_montant
                            montant_lundry+=lund_montant
                            montant_service+=serv_montant
                            taxe_jour+=taxe
                        break
                break
        data.append({
            'code':'HEBE',
            'libelle':"HEBERGEMENT",
            'mois':montant_hebergement_mois,
            'jour':montant_hebergement,
            'total':montant_hebergement+montant_hebergement_mois,
            })
        data.append({
            'code':'REST',
            'libelle':"RESTAURANT",
            'mois':montant_restau_mois,
            'jour':montant_restau,
            'total':montant_restau+montant_restau_mois,
            })
        data.append({
            'code':'TRANS',
            'libelle':"TRANSPORT",
            'mois':montant_transport_mois,
            'jour':montant_transport,
            'total':montant_transport+montant_transport_mois,
            })
        data.append({
            'code':'LAUN',
            'libelle':"LAUNDRY",
            'mois':montant_lundry_mois,
            'jour':montant_lundry,
            'total':montant_lundry+montant_lundry_mois,
            })
        data.append({
            'code':'SERV',
            'libelle':"SERVICE",
            'mois':montant_service_mois,
            'jour':montant_service,
            'total':montant_service+montant_service_mois,
            })
        data.append({
            'code':' ',
            'libelle':"Total CA réception hors débours",
            'mois':'0',
            'jour':'0',
            'total':'0',
            })
        data.append({
            'code':' ',
            'libelle':"Debours",
            'mois':'0',
            'jour':'0',
            'total':'0',
            })
        data.append({
            'code':' ',
            'libelle':"Taxes",
            'mois':taxe_mois,
            'jour':taxe_jour,
            'total':taxe_mois+taxe_jour,
            })

        total_mois=montant_hebergement_mois+montant_restau_mois+montant_transport_mois+montant_lundry_mois+montant_service_mois
        total_jour=montant_hebergement+montant_restau+montant_transport+montant_lundry+montant_service

        data.append({
            'code':' ',
            'libelle':"Total à encaisser",
            'mois':total_mois,
            'jour':total_jour,
            'total':total_jour+total_mois,
            })

        print 'sttttttttttttttate'
        print data
        return data

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('hotel.situation')
        data = self.get_dataa()
        alll=self.get_data()
        print "reeeeeeeeeeeeeeeeeender html"
        docargs = {
            'doc_ids': docids,
            'doc_model': report.model,
            'docs': self,
            'tvs':data,
            'alll':alll,
        }
        print "get_dataget  _dataget    _dataget    _dataget    _dataget_data"
        return report_obj.render('hotel.situation', docargs)