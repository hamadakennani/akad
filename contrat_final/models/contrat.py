# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint


class capcontrat_projets(models.Model):
 	_name = 'capcontrat.projets'
	_rec_name = 'libelle'
	_order='create_date desc'

	categorie_type = fields.Many2one('capcontrat.categories', string='categorie', required=True)
	template_id=fields.Many2one('capcontrat.fichiers', string='Template',domain = "[('file_id','=',categorie_type)]", required=True)
	libelle =fields.Char(string='Libellé', size=128, required=True)
	signature_id=fields.Many2one('res.partner',string=u"signé avec",required=True)
	version_ids=fields.One2many('capcontrat.versions','contrat_id', required=True)
	state=fields.Selection((('brouillon','Brouillon'),('valide','Validé'),('signe','Signé'),('annule','Annulé')),string='Etat',default='brouillon')
	users_ids=fields.One2many('capcontrat.users.access','contrat_id',required=True,string="Groupe de validation")
	based_on=fields.Binary(string='version basée sur')
	numberofversion=fields.Integer(string='version basée sur')
	
	def SendMail(self,mailsto,Subject,body):
		import smtplib
		from email.MIMEMultipart import MIMEMultipart
		from email.MIMEText import MIMEText
		mail_server = self.env['ir.mail_server'].search([["name","=","localhost"]])
		msg = MIMEMultipart()
		msg.set_charset("utf-8")
		msg['From']    = mail_server.smtp_user
		msg['To']      = mailsto
		msg['Subject'] = Subject
		body = body
		msg.attach(MIMEText(body, 'html'))
		server = smtplib.SMTP(mail_server.smtp_host, 587)
		server.ehlo()
		server.starttls()
		server.ehlo()
		server.login(mail_server.smtp_user,mail_server.smtp_pass)
		text = msg.as_string()
		server.sendmail(mail_server.smtp_host, mailsto.split(','), text)
		return True
	@api.onchange('categorie_id')
	def on_change_file(self):
		v={}
		data = [] 
		
		if categorie_id:
			categorie = self.env['capcontrat.categories'].browse(categorie_id)
			for files in categorie.files:
				data.append(files.id)#id from table in database
			v['template_id'] = data 
			     	
		return {'value': v}
	@api.onchange('templat_id')
	def on_change_template(self):
		v={}
		if templat_id:
			template=self.env['capcontrat.fichiers'].browse(templat_id)
			if base_id:
				base=self.env['capcontrat.projets'].browse(base_id)
				v['version_ids'] = [(0,0,{'file':template.file_upload,'filename':template.filename+"_V0",'state':'valide','write_date':datetime.date.today(),'based_on':base_id})]
			else:
				v['version_ids'] = [(0,0,{'file':template.file_upload,'filename':template.filename+"_V0",'state':'valide','write_date':datetime.date.today()})]
		return {'value': v}

	@api.model
	def create(self,vals):
	    print "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"
	    print vals
	    vals.get('users_ids').append([0, False, {u'curuser': self.env.user.id,u'users_id':self.env.user.id}])
	    print vals['users_ids']
	    res = super(capcontrat_projets, self).create(vals)
	    print res.users_ids
            for user in res.users_ids:
                mailto = user.users_id.email
                Subject = "CapContract"
                body = "une nouvelle Contrat Cree et parteger avec vous<br>Publie par : "+str(self.env.user.name)+" <br> pour Consulter le Contrat <a href='http://localhost:8069/web?debug#id="+str(res.id)+"&view_type=form&model=capcontrat.projets&menu_id=29&action=77'>click ici</a>"
               # self.SendMail(mailto,Subject,body)
            return  res

	def set_annule(self):
		self.write({
			'state':'brouillon'
		})
	def set_signe(self):
		self.write({
			'state':'signe'
		})

	def create_contrat(self):  
		vr_final = self.env['capcontrat.versions'].search([('contrat_id','=',self.id),('state','=','valide')])  
		ir_file = {   'name':vr_final.filename+"_finale",   'datas':vr_final.file,   'display_name':vr_final.filename+"_finale",   'datas_fname':vr_final.filename+"_finale"  }  
		cn=self.env["capcontrat.contrat"].create({   'libelle':self.libelle,   'categorie_type':self.categorie_type.id,   'signature_avec':self.signature_id.id,   'source':True,   'manage':self.env.user.id,   'document_ids':[(0,0,ir_file)]  })  
		return {        'view_mode': 'form',    'res_model': 'capcontrat.contrat',    'target': 'current',    'res_id': cn.id,    'type': 'ir.actions.act_window'      }




	





	
	

	

