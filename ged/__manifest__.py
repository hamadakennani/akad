# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Gestion des documents',
    'version': '1.0',
    'category': 'GED',
    'summary': 'Gestion des documents',
    'description': """
Module pour la gestion électronique des documents
    """,
    'author': 'IT-SHORE',
    'website': 'http://www.it-shore.com',
    'depends': ['base','document'],
    'update_xml': [
	
        # 'views/document.xml',
        #'views/ir_attachment.xml',
        # 'data/document_data.xml',
        'views/ged_recherche.xml',
        #'views/groups.xml',
        'views/document_history.xml',
        # 'security/security.xml',
	    # 'security/ir.model.access.csv',
    ],
   'js': [
	'static/js/jquery.dataTables.min.js',
	'static/js/resource.js',
	'static/js/dataTables.bootstrap.min.js',
	],
   'css': [
	'static/css/style.css',
	'static/css/bootstrap.min.css',
	'static/css/dataTables.bootstrap.min.css',
	],
    'qweb': [
	'static/xml/show_document.xml',
	'static/xml/search_document.xml',
	], 
    'data':[
	'static/data.xml',
	],
    'installable': True,
    'auto_install': False,
    'application': False,
}
