# -- coding: utf-8 --
import time
from datetime import date
from odoo import fields, models,api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import pytz
from unidecode import unidecode

  
class HotelReservationLine(models.Model):

    _inherit = "hotel_reservation.line"
    _description = "Reservation Line"
    

    line_detail = fields.One2many(comodel_name='reservation.detail', inverse_name='line_id', string='Détail')
    num_guests = fields.Integer(string='Number of Guests')

    checkin = fields.Datetime('Date-Prévue-Arrivée',required=True)
    checkout = fields.Datetime('Date-Prévue-Départ',required=True)
    
    line_id = fields.Many2one('hotel.reservation')
   
    categ_id = fields.Many2one('hotel.room.type', 'Type de chambre réservée',required=True)
    categ_facturee = fields.Many2one('hotel.room.type', 'Type de chambre facturée ',required=True)
    type_de_service = fields.Many2one(comodel_name='hotel.room.type.variante', string='Formule')
    number_of_dayes=fields.Float(string='Nombre de jours',compute='_compute_number_of_dayes')
    prix=fields.Float(string='Prix')
    sub_total=fields.Float(string='Sous total',compute='_compute_sub_total')
    remise=fields.Float(string='Remise (%)')
    tax = fields.Many2one(comodel_name='account.tax', string='Tax')
    list_des_demandes_specail = fields.One2many(comodel_name='hotel.demandes', inverse_name='res_line', string='Liste des demandes spéciales')
    show_detail = fields.Boolean(string='',default=False)
    to_confirmer = fields.Boolean(string='',default=False)
    detail = fields.Char(string='',default='hello')
    room_num = fields.Many2one(comodel_name='product.product', string='Numéro de chambre' )
    date_order = fields.Datetime('Date Ordered', readonly=True, required=True,
                                 index=True,
                                 default=(lambda *a: time.strftime('%Y-%m-%d %H:%M:%S')))
    reserve = fields.Many2one(comodel_name='hotel.room', string='Numéro de chambre')
    reserve_view = fields.Many2one(comodel_name='hotel.room', string='Numéro de chambre')
   
    num_of_free_rooms=fields.Integer(compute='_get_free_rooms',string='Numéro de des chambres disponibles')
    tva=fields.Float()

    @api.model    
    def create(self, values):
        # if len(values.get('line_detail'))>=1:
        a= super(HotelReservationLine, self).create(values)
        vals = {}
        print a.room_num.id
        print '77777777777777777777777'
        vals = {'room_id': a.reserve_view.id,
                'check_in': a.checkin,
                'check_out': a.checkout,
                'state': 'assigned',
                'status':'confirm',
                'reservation_id': a.line_id.id,
                }
        print vals
        al=self.env['hotel.room.reservation.line'].sudo().create(vals)
        print 'creaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaate line'
        print al.status
        # raise ValidationError('Please Select Rooms sss')
        # else : 
        #     raise ValidationError("Veuillez remplir les détails de la réservation de chaque ligne")
        return a
    
    @api.multi
    def write(self, vals): 
        aaaa = self.env['hotel_reservation.line'].fields_get()
        champs="Réservation line : "
        for a,l in aaaa.items():
            name= str(a)
            if name in vals: 
                champs+="-"+ unidecode(l['string']) +"-" 
        pp=self.env['historique.reservation'].create({
            'champs':champs,
            'reservation_id':self.line_id.id
        })

        return super(HotelReservationLine, self).write(vals)
    
    # @api.constrains('line_detail')
    # def detail_reservation(self):
    #     print 'detail check'
    #     if len(self.line_detail)<1:
    #         raise ValidationError("Veuillez remplir les détails de la réservation pour chaque ligne")

    @api.onchange('reserve_view')
    def _onchange_reserve_view(self):
        if  self.reserve_view :
             self.to_confirmer=True
             self.reserve=self.reserve_view.id
    
    # @api.onchange('reserve')
    # def _onreserve(self):
       
    #     if self.reserve:
    #          self.to_confirmer=True
    
    
    @api.multi
    def check_overlap(self, date1, date2):
     
        date2 = datetime.strptime(date2, '%Y-%m-%d %H:%M:%S')
        date1 = datetime.strptime(date1 , '%Y-%m-%d %H:%M:%S')
        date1+= timedelta(days=1)
        delta = date2 - date1
        return [date1 + timedelta(days=i) for i in range(delta.days + 1)]
    @api.multi
    def create_hosekeeping_line(self):
       
        for dat in  self.check_overlap(self.checkin,self.checkout):
            
            cluser=self.env['hotel.housekeeping'].create({
               'current_date':dat.date(),
               'housekeeping_type':'clean',
               'clean_type':'daily',
               'room_no':self.reserve.id,
               'state': 'dirty'
           })
        self.reserve.get_title()
           
        
    @api.one
    @api.depends('categ_id','checkin','checkout')
    def _get_free_rooms(self):

        count=0

        dt='%Y-%m-%d %H:%M:%S'
        room_bool=False
        ids={}
        if self.checkout and self.checkin:
            reserv_checkin = datetime.strptime(self.checkin, dt)
            reserv_checkout = datetime.strptime(self.checkout, dt)
            rooms=self.env['hotel.room'].search([('categ_id','=',self.categ_id.id)])
           
            for room_id in rooms:
                
                if room_id.room_reservation_line_ids:
                    for reserv in room_id.room_reservation_line_ids.search([('status', 'in', ('confirm', 'done','draft')),
                                                    ('room_id', '=', room_id.id)]):
                        check_in = datetime.strptime(reserv.check_in, dt)
                        check_out = datetime.strptime(reserv.check_out, dt)
                        if check_in <= reserv_checkin <= check_out:
                            room_bool = True
                        if check_in <= reserv_checkout <= check_out:
                            room_bool = True
                        if reserv_checkin <= check_in and \
                                reserv_checkout >= check_out:
                            room_bool = True
                        if room_bool:
                            if  room_id.id  not in ids :
                                count+=1
                                ids[room_id.id ]=room_id.id 

                        
                       
           
            self.num_of_free_rooms=len(rooms)-count
   

    def _get_free_rooms_for_domain(self):

        ids=[]
        res={}
        rel={}
        room_bool=False
        dt='%Y-%m-%d %H:%M:%S'
        if self.checkout and self.checkin:
            reserv_checkin = datetime.strptime(self.checkin, dt)
            reserv_checkout = datetime.strptime(self.checkout, dt)
            rooms=self.env['hotel.room'].search([('categ_id','=',self.categ_id.id)])
           
            for room_id in rooms:
                room_bool=False
                if room_id.room_reservation_line_ids:
                    for reserv in room_id.room_reservation_line_ids.search([('status', 'in', ('confirm', 'done','draft')),
                                                    ('room_id', '=', room_id.id)]):
                        check_in = datetime.strptime(reserv.check_in, dt)
                        check_out = datetime.strptime(reserv.check_out, dt)
                        if check_in <= reserv_checkin <= check_out:
                            room_bool = True
                            print 'F1'
                        if check_in <= reserv_checkout <= check_out:
                            room_bool = True
                            print 'F2'
                        if reserv_checkin <= check_in and \
                                reserv_checkout >= check_out:
                            room_bool = True
                            print 'F3'
                if not room_bool:
                    if  room_id.id  not in ids :
                       

                        ids.append(room_id.id)
                        rel[room_id.id ]=room_id.id 
                print 'iiiiiiids'
                print ids    
                           
                
        res['domain']={'reserve':[('id','in',ids) ]}
        print res
        return   ids

    @api.onchange('reserve')
    def _onchange_reserve(self):
        if self.reserve :
            
            self.tax=''
            self.tax=self.reserve.product_id.taxes_id.id
            self.room_num=self.reserve.product_id.id
    
    def imprime(self):
        return self.env['report'].get_action(self,'gakd.repport_template_card')
        
    #hotel.room
    @api.constrains('checkin','checkout') 
    def chak_date(self):
        
        if self.checkout and self.checkin:
            date_from=  datetime.strptime(self.checkin,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(self.checkout,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            print delta.days
            if not self.checkin < self.checkout:
               
                raise  ValidationError(' Date-Prévue-Départ doit être supérieur à  Date-Prévue-Départ ')
           
            if self.checkin < self.date_order:
                    raise ValidationError("La date d'arrivée doit être supérieure à \
                                         la date du aujourd'hu")
   
    @api.one
    @api.depends('number_of_dayes','checkin','checkout')
    def _compute_number_of_dayes(self):
       
        print 'count days '
        if   self.checkin and self.checkout:
            
            date_from=  datetime.strptime(self.checkin,'%Y-%m-%d %H:%M:%S').date()
            date_to=  datetime.strptime(self.checkout,'%Y-%m-%d %H:%M:%S').date()
            delta = date_to - date_from
            print delta.days
            if   delta.days<0:
               ValidationError(' date')
      
            self.number_of_dayes =  delta.days
    @api.onchange('type_de_service')
    def _onchange_type_de_service(self):
        if self.type_de_service :
            self.prix=self.type_de_service.valeur

    @api.onchange('categ_id',)
    def on_change_categ_facturee(self):
        if not self.categ_facturee:
            self.categ_facturee=self.categ_id.id
       
        
    @api.onchange('categ_id','type_de_service','categ_facturee')
    def on_change_categ(self):
        self.ensure_one()
        reservation_line_obj = self.env['hotel.room.reservation.line']
        folio_room_line_obj = self.env['folio.room.line']
     
       
        ids=[]
        res={}
        print 'ids of cat'
        print self.categ_id.product_cat
        rooms = self.env['product.product'].search([('categ_id','=',self.categ_id.product_cat.id)])
        if self.categ_id:
            self.show_detail=True

     
        for  room in rooms:
          
                    ids.append(room.id)
        reslt=[]
        if self.categ_id:
          for  variante in self.categ_id.variantes:
              reslt.append(variante.id)
       
      
        res['domain']={'type_de_service':[('id','in',reslt) ],'reserve':[('id','in',self._get_free_rooms_for_domain()) ],
        'reserve_view':[('id','in',self._get_free_rooms_for_domain()) ]}
        # res ['value']={'type_de_service': reslt}
        print res 
        return res
       
       
  
    #made with
    @api.one
    @api.depends('number_of_dayes','prix','tax')
    def _compute_sub_total(self): 
        ids={}
        if self.prix and self.number_of_dayes:
            tva=0
            cout=self.prix*self.number_of_dayes
           
            cout =cout-((self.remise*cout)/100)
          
            for tax in  self.tax: 
                if tax.amount_type=='fixed':
                    tva=tax.amount
                    if tax.price_include:
                        cout-=tax.amount
                   


                elif tax.amount_type=='percent':
                   
                    tva=(cout*tax.amount)*0.01
                    
                    if tax.price_include:
                        cout-=tva
            
            self.tva=tva
            self.sub_total= cout
        else:

         self.sub_total = 0





