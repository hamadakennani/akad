# -*- coding: utf-8 -*-
from odoo import fields, api, models

from odoo.exceptions import ValidationError
from funs import date_dure_cul
import datetime
from datetime import date, datetime, timedelta
import dateutil.relativedelta
from dateutil.relativedelta import relativedelta
import math


class Clauses (models.Model):
    _name = 'capcontrat.contrat.clauses'
    _rec_name = 'libelle'
    @api.one
    @api.depends("fin_theorique","echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence","contrat_id")
    def _get_echeance_text(self):
      print ""
      # self.ensure_one()
      # if self.echeance_type=="Jusqu au":
      #   if self.date_echeance:
      #     self.echeance_text=str(date_dure_cul(self.date_echeance,1,1,1).date())

      # elif   self.echeance_type=="Evenement recurrent":
       
           
      #             if self.si_il_est_date_de_debut_de_recurrence and self.si_il_est_date_de_fin_de_recurrence:

      #               if  self.date_de_debut_de_recurrence and not self.date_de_fin_de_recurrence:
      #                     self.echeance_text= self.echeance_text="Tous les "+self.repeter_type.replace("(","").replace(")","") +" à  partir du "+ self.date_de_debut_de_recurrence
      #                     return  self.echeance_text

      #               elif  not self.date_de_debut_de_recurrence and  self.date_de_fin_de_recurrence:
      #                   if  self.contrat_id.Prise_effet_date_value:


      #                    self.echeance_text= self.echeance_text="Tous les "+self.repeter_type.replace("(","").replace(")","") +" du "+self.contrat_id.Prise_effet_date_value+" au "+self.date_de_fin_de_recurrence 
      #                    return  self.echeance_text
                      

                        
                       
      #               elif  self.date_de_debut_de_recurrence and  self.date_de_fin_de_recurrence:
      #                   self.echeance_text="Tous les "+self.repeter_type.replace("(","").replace(")","") +" du "+ self.date_de_debut_de_recurrence+" au "+self.date_de_fin_de_recurrence 
                       
      #                   return self.echeance_text
                    
      #             if self.contrat_id.fin_theorique:
      #                 self.echeance_text= str(date_dure_cul(self.contrat_id.fin_theorique,1,1,1).date())
      #                 return  self.echeance_text
      # elif   self.echeance_type=="Evenement recurrent":*
        
    @api.one
    @api.depends("echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence")
    def _date_debut(self):
      self.ensure_one()
      if self.si_il_est_date_de_debut_de_recurrence:
          if self.date_de_debut_de_recurrence:
            self.date_debut=self.date_de_debut_de_recurrence
            return self.date_debut
      else :
            self.date_debut=self.contrat_id.Signe_le
            return   self.date_debut
      # self._date_eval()
    @api.one
    @api.depends("echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence")
    def _fin_duree(self):
      self.ensure_one()
      if self.echeance_type=="Jusqu au" and self.date_echeance:
        if self.date_echeance:
          self.fin_theorique= date_dure_cul(self.date_echeance,1,1,1).date()

      elif   self.echeance_type=="Evenement recurrent" or   self.echeance_type=="Jusqu'a la fin du contrat":
       
           
                  if self.si_il_est_date_de_fin_de_recurrence:

                    if   self.date_de_fin_de_recurrence:
                          self.fin_theorique= date_dure_cul(self.date_de_fin_de_recurrence,1,1,1).date()
                          return  self.fin_theorique

                  
                    
                  else:
                      
                       if self.contrat_id.fin_theorique:
                        self.fin_theorique=date_dure_cul(self.contrat_id.fin_theorique,1,1,1).date()
                       return  self.fin_theorique
      elif self.echeance_type=="Pour une duree de" and self.repeter_int and  self.repeter_type and self.contrat_id .Signe_le:
            contrat_duree= date_dure_cul(self.contrat_id .Signe_le,1,self.repeter_int,self.repeter_type).date()   
            self.fin_theorique=contrat_duree
            return self.fin_theorique
      
      if self.contrat_id.fin_theorique: self.fin_theorique=date_dure_cul(self.contrat_id.fin_theorique,1,1,1).date()
      return  self.fin_theorique
      
      
      # self._date_eval()
    echeance_text=fields.Char(string=u"Echéance",compute='_get_echeance_text', store=True)
    libelle=fields.Char(string='Clause', size=128, required=True)
    contrat_id=fields.Many2one('capcontrat.contrat')
    Information_supplementaire=fields.Text(string="Information supplémentaire")
    echeance_type=fields.Selection([("Jusqu'a la fin du contrat","jusqu'à la fin du contrat"),("Pour une duree de","Pour une durée de")
    ,("Jusqu au","Jusqu'au "),("Evenement recurrent","Evénement récurrent")],default="Jusqu au",string="Echéance")
    
    #  if echeance_type value == Pour une durée de
    duree_int=fields.Integer(string="Alert")
    duree_type=fields.Selection([('Jour(s)','Jour(s)'),('Semaine(s)','Semaine(s)'),('Mois','Mois')
    ,('Annee','Année')],default='Jour(s)')
    # end if  
    #  if echeance_type value == Jusqu'au 
    date_echeance=fields.Date()
    #  if echeance_type value == Evenement récurrent
    si_il_est_date_de_debut_de_recurrence=fields.Boolean(string='Début de la récurrence',default=False)
    #if  si_il_est_date_de_debut_de_recurrence= True
    date_de_debut_de_recurrence=fields.Date(string="Date de début ")
    si_il_est_date_de_fin_de_recurrence=fields.Boolean(string=' Fin de la récurrence',default=False)
    #if si_il_est_date_de_fin_de_recurrence = true
    date_de_fin_de_recurrence=fields.Date(string=" Date de fin")
    repeter_int=fields.Integer(string="Répéter tous les")
    repeter_type=fields.Selection([('Jour(s)','Jour(s)'),('Semaine(s)',' Semaine(s)'),('Mois','Mois')
    ,('an(s)','Année')],default='Mois')
    # fin de if 
    alertes_repeter=fields.Selection([(0,0),(1,1),(3,3),(7,7),(15,15),(30,30),(45,45),(90,90)],string="Alertes email",default=1)
    fin_theorique=fields.Date(string=u"Fin théorique",compute=_fin_duree)
    date_debut=fields.Date(compute=_date_debut)
    @api.constrains("echeance_type","date_echeance","si_il_est_date_de_debut_de_recurrence","date_de_debut_de_recurrence","si_il_est_date_de_fin_de_recurrence","date_de_fin_de_recurrence")
    def _chack_echeance(self):
       if self.echeance_type=="a une date fixe":
         if not self.date_echeance:
           raise ValidationError("Conditions financiéres :  la date de échéance est Obligatoir ")
          # datetime.strptime(self.date_de_prise, '%Y-%m-%d')
         elif  self.contrat_id.Signe_le and datetime.strptime(self.date_echeance, '%Y-%m-%d')< datetime.strptime(self.contrat_id.Signe_le, '%Y-%m-%d'):
           raise ValidationError("Conditions financiéres :la date de échéance doit être supérieur à la date du signature")



       elif self.echeance_type=="Evenement recurren":
           if self.si_il_est_date_de_debut_de_recurrence and not self.si_il_est_date_de_debut_de_recurrence:
             if not self.date_de_debut_de_recurrencenot:
               raise ValidationError("La date de Début de la récurrence est Obligatoir")
           if self.si_il_est_date_de_fin_de_recurrence and not self.si_il_est_date_de_fin_de_recurrence :
             if not  self.date_de_fin_de_recurrence :
                raise ValidationError("Conditions financiéres :  la date de Fin de la récurrence est obligatoir")
             elif   datetime.strptime(self.date_de_fin_de_recurrence, '%Y-%m-%d')< datetime.strptime(self.contrat_id.Signe_le, '%Y-%m-%d'):
       
                raise ValidationError("Conditions financiéres : la date de Fin de la récurrence doit être supérieur à la date du signature")
    @api.one            
    @api.constrains('alertes_repeter',"echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence"
    ,"repeter_int","repeter_type")
    def _date_eval(self):
            
            date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
            date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
            difference_date =date_f-date_d
            self.env['capcontrat.contrat.echeance'].search([("clauses_id","=",self.id)]).unlink()
            i=0
            if self.echeance_type=="a une date fixe" or  self.echeance_type=="a une date fixe" or self.echeance_type=="Jusqu'a la fin du contrat" or self.echeance_type=="Pour une duree de":
              echeance =self.env['capcontrat.contrat.echeance'].create(
                      {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(self.fin_theorique,0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
                      'clauses_id':self.id})

            elif self.echeance_type=="Evenement recurrent":
                  if self.repeter_type=='Jour(s)':
                    conunt=difference_date.days
                    date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
                    while(conunt>i):
                        date_d=date_d+relativedelta(days=self.repeter_int)
                        print  " les dates  days  ------------>>>>>>"
                        print date_d
                        if isinstance(self.id,(int,long)):
                    
                          

                          print " hello ---- kate "
                    
                          echeance =self.env['capcontrat.contrat.echeance'].create(
                          {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
                          'clauses_id':self.id})
                          print "  -------- echeance a ete  created "
                          print echeance
                  
                        
                        i=i+1

                    
                  
                
                    
                  elif self.repeter_type=='Semaine(s)':
                      date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
                      date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
                      difference_date =date_f-date_d
                      
                      conunt=difference_date.days/7
                      date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
                      while(conunt>i):
                      
                        date_d=date_d+relativedelta(weeks=self.repeter_int)
                        print  " les dates  weeks   ------------>>>>>>"
                        print date_d
                        if isinstance(self.id,(int,long)):
                
                        
                
                          self.env['capcontrat.contrat.echeance'].create(
                            {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
                            'clauses_id':self.id})
                        i=i+1

                  elif self.repeter_type=='Mois':
                    date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
                    date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
                    difference_date =date_f-date_d
                    conunt=difference_date.days/30
                    date_d=datetime.strptime(str(self.date_debut), '%Y-%m-%d')
                    while(conunt>i):
                      
                        date_d=date_d+relativedelta(months=self.repeter_int)
                        print  " les dates  months   ------------>>>>>>"
                        print date_d
                        if isinstance(self.id,(int,long)):
                
                        
                
                          self.env['capcontrat.contrat.echeance'].create(
                            {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
                            'clauses_id':self.id})
                        i=i+1

                  elif self.repeter_type=='an(s)':
                    date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
                    date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
                    difference_date =date_f-date_d
                    conunt=difference_date.days/365
                    date_d=datetime.strptime(str(self.date_debut), '%Y-%m-%d')
                    while(conunt>i):
                      
                        date_d=date_d+relativedelta(years=self.repeter_int)
                        print  " les dates  years   ------------>>>>>>"
                        print date_d
                        if isinstance(self.id,(int,long)):
                
                          
                          self.env['capcontrat.contrat.echeance'].create(
                            {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
                            'clauses_id':self.id})
                        i=i+1
   
              


          



     