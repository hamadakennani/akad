# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    ThinkOpen Solutions Brasil
#    Copyright (C) Thinkopen Solutions <http://www.tkobr.com>.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging
from datetime import datetime

import werkzeug.contrib.sessions
import werkzeug.datastructures
import werkzeug.exceptions
import werkzeug.local
import werkzeug.routing
import werkzeug.wrappers
import werkzeug.wsgi
from odoo import SUPERUSER_ID
from odoo import api
from odoo import fields, models
from odoo.http import root
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class close_sessions(models.Model):
    _name = 'gakd.close.sessions'
    _description = "Close sessions"

    user_id = fields.Many2one('res.users', 'Utilisateur', ondelete='cascade',
                              required=True)
    flag =fields.Boolean('flag')



    @api.multi
    def close_session(self, logout_type='sk'):
        redirect = False
        liste_sessions = self.env["ir.sessions"].search([])
        print '#######################'
        print liste_sessions
        print '#######################'
        for r in liste_sessions:
            if r.user_id.id == self.user_id.id:
                print '888888888888888888888'
                redirect = True
                session = root.session_store.get(r.session_id)
                
                session.logout(logout_type='to', env=self.env)
                flag = True
        return redirect