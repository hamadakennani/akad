# -*- coding: utf-8 -*-
from odoo import fields, api, models

from odoo.exceptions import ValidationError


class CapContratPrivilege(models.Model):
    _name = 'capcontrat.contrat.privilege'

    
   
    res_users=fields.Many2one('res.users',string='les utilisateurs',required=True)
    contrat_id=fields.Many2one('capcontrat.contrat')
    #lecture écriture
    privileges=fields.Selection([("Lecture","Lecture"),("Ecriture","Écriture"),("aucun droit","Aucun droit ")],string=u"Droit d'accès",required=True)
    lecture=fields.Boolean(string="Lecture")
    ecriture=fields.Boolean(string=u"Écriture")
    first_id = fields.Integer(string='')
    socund_id=fields.Integer()
    
    
   
   
    @api.onchange("res_users","privileges")
    def _set_privilege(self):
      print "*******"
      if self.privileges=="Lecture":
        self.lecture=True
        self.ecriture=False
      elif self.privileges=="Ecriture":
        self.lecture=False
        self.ecriture=True
      else:
        self.lecture=False
        self.ecriture=False


        

    @api.model
    def create(self,vals):

        

      return super(CapContratPrivilege,self).create(vals)
   
   