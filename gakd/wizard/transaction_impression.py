# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import datetime
import math
from datetime import datetime, timedelta
from unidecode import unidecode
import xlwt
from xlsxwriter.workbook import Workbook
from cStringIO import StringIO
import base64
import locale

class Transaction(models.TransientModel):
    _name = 'gakd.wizard.transaction'
    
    def _getClient_id(self):
        return self._context.get('active_ids',False)[0]

    # print_details = fields.Many2one('gakd.carte.consommation',string='print',default=_getClient_id)
    date = fields.Date(string='Date Début',required=True)   
    datef = fields.Date(string='Date Fin',required=True)   
    journal_id = fields.Many2one(comodel_name='account.journal', string='Journal des règlements',required=True)  
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente")
    
    @api.multi
    def print_report(self):
        datas=[]
        if self.point_vente_id:
            a=self.env['gakd.versement'].search([('vers_par','=','gerant'),('point_vente_id','=',self.point_vente_id.id)],order='point_vente_id desc') #('vers_par','=','gerant')
        else :
            a=self.env['gakd.versement'].search([('vers_par','=','gerant')],order='point_vente_id desc')
        mpblanc=0
        mpgaz=0
        mplubr=0
        mplava=0
        mautre=0  
        ds=datetime.strptime(self.date, '%Y-%m-%d')
        de=datetime.strptime(self.datef, '%Y-%m-%d')
        for l in a:
            # ds=datetime.strptime(l.create_date, '%Y-%m-%d %H:%M:%S') 
            # dNow=datetime.strptime(self.date, '%Y-%m-%d') 
            if datetime.strptime(l.create_date, '%Y-%m-%d %H:%M:%S')>= ds and datetime.strptime(l.create_date, '%Y-%m-%d %H:%M:%S')<= de :
                print 'in' 
                for vers in l.versement_line:
                    if vers.journal_id.id==self.journal_id.id :
                        datas.append([('pos',l.point_vente_id.libelle),('date',l.create_date),('montantT',locale.format("%d", float(vers.montant), grouping=True)),('moyen_de_paiment',unidecode(l.moyen_de_paiement)),('banque',vers.journal_id.name),('type_de_produit',l.type_versement)])
                        if l.type_versement=='Produits blancs':
                            mpblanc+=vers.montant
                        elif l.type_versement=='Lubrifiants':
                            mplubr+=vers.montant
                        elif l.type_versement=='Gaz et accessoire':
                            mpgaz+=vers.montant
                        elif l.type_versement=='Lavage':
                            mplava+=vers.montant
                        else :
                            mautre+=vers.montant

        # print 'hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh'  locale.format("%d", float(mautre), grouping=True)
        f = {'data':datas,'mpblanc':locale.format("%d", float(mpblanc), grouping=True),'mplubr':locale.format("%d", float(mplubr), grouping=True),'mpgaz':locale.format("%d", float(mpgaz), grouping=True),'mplava':locale.format("%d", float(mplava), grouping=True),'mautre':locale.format("%d", float(mautre), grouping=True),'banque':self.journal_id.name,'date_debut':self.date,'date_fin':self.datef,'pos':self.point_vente_id.libelle}
        print f
        for z  in f['data'] :
            print 'kkkkkkkkkkkkkkkkkkkkkkline'
            print z
        print f['mpblanc']
        print 'kkkkkkkkkkkkkkkkkkkkkkk,ncklscn,ksd,'
        self.generate_excel_report()
        # return self.env['report'].get_action(self, 'gakd.report_transactions_s', data=f)
        #  {'type': 'ir.actions.report.xml', 'report_name': 'gakd.report_transactionss', 'datas': datas}

    @api.multi
    def generate_excel_report(self):
        filename= 'C:/yahia'
        workbook= xlwt.Workbook(encoding="UTF-8")
        worksheet= workbook.add_sheet('Sheet 1')
        style = xlwt.easyxf('font: bold True, name Arial')
        worksheet.write_merge(0,1,0,3,'your data that you want to show into excelsheet',style)
        fp = StringIO()
        workbook.save(fp)
        record_id = self.env['wizard.excel.report'].create({'excel_file': base64.encodestring(fp.getvalue()),
                                                                                                'file_name': filename},)
        fp.close()
        return {'view_mode': 'form',
                    'res_id': record_id,
                    'res_model': 'wizard.excel.report',
                    'view_type': 'form',
                    'type': 'ir.actions.act_window',
                    # 'context': context,
                  'target': 'new',
       }

class wizard_excel_report(models.Model):
    _name= "wizard.excel.report"
    excel_file = fields.Binary('excel file')
    file_name = fields.Char('Excel File', size=64)
    
class retour_bon_consommation_report(models.AbstractModel):
    _name = 'report.gakd.report_transactions_s'
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('gakd.report_transactions_s')
        
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
            }
        print data
        return report_obj.render('gakd.report_transactions_s', docargs)