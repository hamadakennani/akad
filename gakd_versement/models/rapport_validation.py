# -- coding: utf-8 --
from odoo import api, fields, models,_
from odoo.exceptions import ValidationError
from datetime import date, timedelta
import datetime
from dateutil.parser import parse
import xlwt
from xlsxwriter.workbook import Workbook
from cStringIO import StringIO
import base64

class ListeVersement(models.Model):
    _name = 'gakd.liste'
    _description='liste versement'

    #----- header------
    compte_bancaire=fields.Many2one(comodel_name='account.journal',string='compte bancaire', domain=[('type','=','bank')] )
    # compte_bancaire=fields.Char(string='compte bancaire')
    
    yesterday = date.today()
    to_Day=yesterday.strftime('%d/%m/%Y')
    toDy = fields.Date(string='Date aujourd\'hui', default=date.today())
    date_du=fields.Date(string='Date du')
    date_au=fields.Date(string='Date au')
    
    #daters=fields.Date(string='Date opperation')
    point_vente = fields.Many2one("gakd.point.vente",string="Point de vente")
    categorie_produit=fields.Selection(
        [('-', 'null'), ('Lubrifiants', 'Lubrifiants'), ('Produits blancs', 'Produits blancs'),('Gaz et accessoire', 'Gaz et accessoire'), ('Lavage', 'Lavage')],
        string="Catégorie des produits", 
        default='Lubrifiants'
        )

    state_validation = fields.Selection(
        string='Etat',
        selection=[
        ('valideAuto', 'Validé'),
		('M.Ref-Mt.Diff','Même référence mais momtant différent'),
		('M.Date-M.PV Mt.Diff','Même date Même PV mais montant différent'),
		('V.n_existe_pas_dans_R',"versement n'existe pas dans les relevés"),
		('R.n_existe_pas_dans_V',"relevé n'existe pas dans les versements")],
        default='valideAuto'
        )

# ('valideAvecEcart','Validé avec les Ecarts'),
# ('valideSansEcart','Validé sans les Ecarts'),

    #----menu ------
    table=fields.One2many('gakd.versementreleve','liste_versement',string='')

    #ON CHANGE    
    @api.onchange('compte_bancaire')
    def _onchange_field1(self):
        if self.compte_bancaire:            
            if len(self.compte_bancaire)>0:
                nomcompte=self.compte_bancaire[0].name
            else:
                nomcompte=self.compte_bancaire.name

            codepv=None
            pointevente=None
            
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle
            
            # ett1='valideAvecEcart'
            # ett2='valideSansEcart'
            banque ='CASH COLLECTION'
            #etat             
            etat=str(self.state_validation)
            #1
            if self.point_vente and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()
                
                # if (etat==ett1):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif (etat==ett2):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion', '=', 'valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat != 'R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte==banque:
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('point_vente','=',codepv),
                            ('compte_bancaire','ilike',nomcompte),
                            ('date_versement_releve','>=',getdateDU),
                            ('date_versement_releve','<=',getdateAU)
                            ])
                    else:
                        listeCB = self.env['gakd.versementreleve'].search([
                            ('statu_validaion', '=', etat),
                            ('categorie_produit', '=', self.categorie_produit),
                            ('point_vente', '=', codepv),
                            ('compte_bancaire', 'ilike', nomcompte),
                            ('date_versement_releve', '>=', getdateDU),
                            ('date_versement_releve', '<=', getdateAU)
                        ])
            elif not self.point_vente and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()

                # if (etat==ett1):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif (etat==ett2):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion', '=', 'valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),                
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('compte_bancaire','ilike',nomcompte),
                            ('date_versement_releve','>=',getdateDU),
                            ('date_versement_releve','<=',getdateAU),
                            ])
                    else:
                        listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                        
            elif self.point_vente and (not self.date_du or not self.date_au):
                # if (etat==ett1):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('point_vente','=',codepv),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif (etat==ett2):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('point_vente','=',codepv),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),                
                        ('compte_bancaire','ilike',nomcompte),
                        ('point_vente','=',codepv),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('compte_bancaire','ilike',nomcompte),
                            ('point_vente','=',codepv),
                            ])
                    else:
                        listeCB = self.env['gakd.versementreleve'].search([
                            ('statu_validaion', '=', etat),
                            ('categorie_produit', '=', self.categorie_produit),
                            ('compte_bancaire', 'ilike', nomcompte),
                            ('point_vente', '=', codepv),
                        ])
            elif not self.point_vente and (not self.date_du or not self.date_au):
                # if (etat==ett1):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif (etat==ett2):
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),                
                        ('compte_bancaire','ilike',nomcompte),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('compte_bancaire','ilike',nomcompte),
                            ])
                    else:
                            listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('categorie_produit','=',self.categorie_produit),
                            ('compte_bancaire','ilike',nomcompte),
                            ])
            self.table=listeCB
    
    @api.onchange('point_vente')
    def _onchange_field2(self):
        if self.point_vente:
            if len(self.compte_bancaire)>0:
                nomcompte=self.compte_bancaire[0].name
            else:
                nomcompte=self.compte_bancaire.name
            
            codepv=None
            pointevente=None
            
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle

            etat=str(self.state_validation)
            # ett1='valideAvecEcart'
            # ett2='valideSansEcart'
            banque = 'CASH COLLECTION'

            #1
            if self.compte_bancaire and self.date_du and self.date_au:
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #     ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('point_vente','=',codepv),
                            ('compte_bancaire', 'ilike', nomcompte),
                            ('date_versement_releve','>=',getdateDU),
                            ('date_versement_releve','<=',getdateAU),
                            ])
                    else:
                        listeCB = self.env['gakd.versementreleve'].search([
                            ('statu_validaion', '=', etat),
                            ('categorie_produit', '=', self.categorie_produit),
                            ('point_vente', '=', codepv),
                            ('compte_bancaire', 'ilike', nomcompte),
                            ('date_versement_releve', '>=', getdateDU),
                            ('date_versement_releve', '<=', getdateAU),
                        ])
            elif not self.compte_bancaire and (self.date_du and self.date_au):
                #date du "format yyyy-mm-dd " 
                datdu=self.date_du            
                datduParse=parse(str(datdu))
                getdateDU=datduParse.date()
            
                #date au "format yyyy-mm-dd "
                datau=self.date_au
                datauParse=parse(str(datau))
                getdateAU=datauParse.date()
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
            elif self.compte_bancaire and (not self.date_du or not self.date_au):
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=',etat),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('point_vente','=',codepv),
                #         ('montant_manquant','!=',0)
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),                
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('point_vente','=',codepv),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),                
                        ('compte_bancaire','ilike',nomcompte),
                        ('point_vente','=',codepv),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('compte_bancaire','ilike',nomcompte),
                            ('point_vente','=',codepv),
                            ])
                    else:
                            listeCB=self.env['gakd.versementreleve'].search([
                            ('statu_validaion','=',etat),
                            ('categorie_produit','=',self.categorie_produit),
                            ('compte_bancaire','ilike',nomcompte),
                            ('point_vente','=',codepv),
                            ])
            elif not self.compte_bancaire and (not self.date_du or not self.date_au):
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion', '=', 'valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('montant_manquant','!=',0)
                #         ])                
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('montant_manquant','=',0)
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ])

            self.table=listeCB
    
    @api.onchange('categorie_produit')
    def _onchange_field3(self):
        if len(self.compte_bancaire)>0:
            nomcompte=self.compte_bancaire[0].name
        else:
            nomcompte=self.compte_bancaire.name

        codepv=None
        pointevente=None
        
        if len(self.point_vente)>0:
            codepv=self.point_vente[0].libelle
        else:
            codepv=self.point_vente.libelle

        etat=str(self.state_validation)
        # ett1='valideAvecEcart'
        # ett2='valideSansEcart'
        banque = 'CASH COLLECTION'

        #1
        if self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()

            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([                
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),
            #         ('point_vente','=',codepv),
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','!=',0)
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([                
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),
            #         ('point_vente','=',codepv),
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','=',0)
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([                
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','=',codepv),
                    ('compte_bancaire','ilike',nomcompte),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU)
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([                
                        ('statu_validaion','=',etat),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
                else:
                    listeCB=self.env['gakd.versementreleve'].search([                
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
        #2
        elif not self.compte_bancaire and not self.point_vente and (not self.date_du or not self.date_au):
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit), 
            #         ('montant_manquant','!=',0)
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit), 
            #         ('montant_manquant','=',0)
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit), 
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ])
        #3
        elif not self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','!=',0)
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','=',0)
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('compte_bancaire','ilike',nomcompte),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                else:
                    listeCB = self.env['gakd.versementreleve'].search([
                        ('statu_validaion', '=', etat),
                        ('categorie_produit', '=', self.categorie_produit),
                        ('compte_bancaire', 'ilike', nomcompte),
                        ('date_versement_releve', '>=', getdateDU),
                        ('date_versement_releve', '<=', getdateAU),
                    ])
        #4
        elif  not self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','!=',0)
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','!=',0)
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                    ])
        #5
        elif self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('point_vente','=',codepv),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','!=',0)
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion', '=', 'valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('point_vente','=',codepv),
            #         ('date_versement_releve','>=',getdateDU),
            #         ('date_versement_releve','<=',getdateAU),
            #         ('montant_manquant','=',0)
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('point_vente','=',codepv),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','=',codepv),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                ])          
        #6
        elif self.point_vente and not self.compte_bancaire and (not self.date_du or not self.date_au):            
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('point_vente','=',codepv),
            #         ('montant_manquant','!=',0),
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion', '=', 'valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('point_vente','=',codepv),
            #         ('montant_manquant','=',0),
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('point_vente','=',codepv),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','=',codepv),
                    ])
            
        #7
        elif self.point_vente and self.compte_bancaire and (not self.date_du or not self.date_au):
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('point_vente','=',codepv),
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('montant_manquant','!=',0),
            #         ])
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),                
            #         ('point_vente','=',codepv),
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('montant_manquant','=',0),
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('point_vente','=',codepv),
                    ('compte_bancaire','ilike',nomcompte),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ])
                else:
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit), 
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ])
        #8
        elif not self.point_vente and self.compte_bancaire and (not self.date_du or not self.date_au):
            # if etat==ett1:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('montant_manquant','!=',0),
            #         ]) 
            # elif etat==ett2:
            #     listeCB=self.env['gakd.versementreleve'].search([
            #         ('statu_validaion','=','valideAuto'),
            #         ('categorie_produit','=',self.categorie_produit),
            #         ('compte_bancaire','ilike',nomcompte),
            #         ('montant_manquant','=',0),
            #         ])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('compte_bancaire','ilike',nomcompte),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('compte_bancaire','ilike',nomcompte),
                        ])
                else:
                    listeCB = self.env['gakd.versementreleve'].search([
                        ('statu_validaion', '=', etat),
                        ('categorie_produit', '=', self.categorie_produit),
                        ('compte_bancaire', 'ilike', nomcompte),
                    ])
        self.table=listeCB

    @api.onchange('state_validation')
    def _onchange_field4(self):
        if len(self.compte_bancaire)>0:
            nomcompte=self.compte_bancaire[0].name
        else:
            nomcompte=self.compte_bancaire.name

        codepv=None
        pointevente=None
        
        if len(self.point_vente)>0:
            codepv=self.point_vente[0].libelle
        else:
            codepv=self.point_vente.libelle

        # ett1='valideAvecEcart'
        # ett2='valideSansEcart' 
        etat=str(self.state_validation)
        banque = 'CASH COLLECTION'
        #1
        if self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            # if (etat==ett1):
			# 	listeCB=self.env['gakd.versementreleve'].search([                
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('point_vente','=',codepv),
			# 		('compte_bancaire','ilike',nomcompte),
			# 		('date_versement_releve','>=',getdateDU),
			# 		('date_versement_releve','<=',getdateAU),
			# 		('montant_manquant','!=',0)
			# 		])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([                
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('point_vente','=',codepv),
			# 		('compte_bancaire','ilike',nomcompte),
			# 		('date_versement_releve','>=',getdateDU),
			# 		('date_versement_releve','<=',getdateAU),
			# 		('montant_manquant','=',0)
			# 		])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([                
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','=',codepv),
                    ('compte_bancaire','ilike',nomcompte),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU)
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([                
                        ('statu_validaion','=',etat),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
                else:
                    listeCB=self.env['gakd.versementreleve'].search([                
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU)
                        ])
        #2
        elif not self.compte_bancaire and not self.point_vente and (not self.date_du or not self.date_au):
            # if etat==ett1:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('montant_manquant','!=',0)
			# 		])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('montant_manquant','=',0)
			# 		])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','ilike',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','ilike',etat),
                    ('categorie_produit','=',self.categorie_produit), 
                    ])
        #3
        elif not self.point_vente and self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            # if etat==ett1:
			# 	listeCB=self.env['gakd.versementreleve'].search([
            #     ('statu_validaion','=','valideAuto'),
            #     ('categorie_produit','=',self.categorie_produit),                
            #     ('compte_bancaire','ilike',nomcompte),
            #     ('date_versement_releve','>=',getdateDU),
			# 	('date_versement_releve','<=',getdateAU),
			# 	('montant_manquant','!=',0)
            #     ])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
            #     ('statu_validaion','=','valideAuto'),
            #     ('categorie_produit','=',self.categorie_produit),                
            #     ('compte_bancaire','ilike',nomcompte),
            #     ('date_versement_releve','>=',getdateDU),
			# 	('date_versement_releve','<=',getdateAU),
			# 	('montant_manquant','=',0)
            #     ])
            # else:
            if etat!='R.n_existe_pas_dans_V':		
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('compte_bancaire','ilike',nomcompte),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                else:
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('compte_bancaire','ilike',nomcompte),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
        #4
        elif  not self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            # if etat==ett1:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('date_versement_releve','>=',getdateDU),
			# 		('date_versement_releve','<=',getdateAU),
			# 		('montant_manquant','!=',0)
			# 		])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('date_versement_releve','>=',getdateDU),
			# 		('date_versement_releve','<=',getdateAU),
			# 		('montant_manquant','=',0)
			# 		])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                    ])
        #5
        elif self.point_vente and not self.compte_bancaire and self.date_du and self.date_au:
            #date du "format yyyy-mm-dd " 
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            # if etat==ett1:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),                
			# 		('point_vente','=',codepv),
			# 		('date_versement_releve','>=',getdateDU),
			# 		('date_versement_releve','<=',getdateAU),
			# 		('montant_manquant','!=',0)
			# 		])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),                
			# 		('point_vente','=',codepv),
			# 		('date_versement_releve','>=',getdateDU),
			# 		('date_versement_releve','<=',getdateAU),
			# 		('montant_manquant','=',0)
			# 		])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('point_vente','=',codepv),
                    ('date_versement_releve','>=',getdateDU),('date_versement_releve','<=',getdateAU),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','=',codepv),
                    ('date_versement_releve','>=',getdateDU),
                    ('date_versement_releve','<=',getdateAU),
                    ])
        #6
        elif self.point_vente and not self.compte_bancaire and (not self.date_du or not self.date_au):
            # if etat==ett1:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),                
			# 		('point_vente','=',codepv),
			# 		('montant_manquant','!=',0)
			# 		])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),                
			# 		('point_vente','=',codepv),
			# 		('montant_manquant','=',0)
			# 		])
            # else:
            if etat!='R.n_existe_pas_dans_V':                   
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('point_vente','=',codepv),
                    ])
            elif etat=='R.n_existe_pas_dans_V':                   
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),            
                    ('point_vente','=',codepv),
                    ])
        #7
        elif self.point_vente and self.compte_bancaire and (not self.date_du or not self.date_au):
            # if etat==ett1:
    		#     listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),                
			# 		('point_vente','=',codepv),
			# 		('compte_bancaire','ilike',nomcompte),
			# 		('montant_manquant','!=',0)
			# 		])
            # elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('point_vente','=',codepv),
			# 		('compte_bancaire','ilike',nomcompte),
			# 		('montant_manquant','=',0)
			# 		])
            # else:
            if etat!='R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),                
                    ('point_vente','=',codepv),
                    ('compte_bancaire','ilike',nomcompte),
                    ])
            elif etat=='R.n_existe_pas_dans_V':
                if nomcompte == banque :
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ])
                else:
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('compte_bancaire','ilike',nomcompte),
                        ])
        #8
        elif not self.point_vente and self.compte_bancaire and (not self.date_du or not self.date_au):
			# if etat==ett1:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('compte_bancaire','ilike',nomcompte),
			# 		('montant_manquant','=',0)
			# 		])
			# elif etat==ett2:
			# 	listeCB=self.env['gakd.versementreleve'].search([
			# 		('statu_validaion','=','valideAuto'),
			# 		('categorie_produit','=',self.categorie_produit),
			# 		('compte_bancaire','ilike',nomcompte),
			# 		('montant_manquant','!=',0),
			# 		])
			# else:
            if etat != 'R.n_existe_pas_dans_V':
                listeCB = self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('categorie_produit','=',self.categorie_produit),
                    ('compte_bancaire','ilike',nomcompte),
                    ])
            elif etat == 'R.n_existe_pas_dans_V':
                listeCB=self.env['gakd.versementreleve'].search([
                    ('statu_validaion','=',etat),
                    ('compte_bancaire','ilike',nomcompte),
                    ])
        self.table=listeCB
    
    @api.onchange('date_du','date_au')
    def _onchange_field5(self):
        if self.date_du and self.date_au:
            if len(self.compte_bancaire)>0:
                nomcompte=self.compte_bancaire[0].name
            else:
                nomcompte=self.compte_bancaire.name

            codepv=None
            pointevente=None
            
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle          
            
            
            #date du "format yyyy-mm-dd " datFormatDU=datduParse.strftime('%Y-%m-%d')
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
        
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            
            etat=str(self.state_validation)
            # ett1='valideAvecEcart'
            # ett2='valideSansEcart'
            banque = 'CASH COLLECTION'
            #1
            if self.point_vente and self.compte_bancaire:
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('statu_validaion','=','valideAuto'),
                #         ('point_vente','=',codepv),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0),
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('statu_validaion','=','valideAuto'),
                #         ('point_vente','=',codepv),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0),
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('compte_bancaire','ilike',nomcompte),
                        ('statu_validaion','=',etat),
                        ('point_vente','=',codepv),
                        ('categorie_produit','=',self.categorie_produit),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('compte_bancaire','ilike',nomcompte),
                            ('statu_validaion','=',etat),
                            ('point_vente','=',codepv),
                            ('date_versement_releve','>=',getdateDU),
                            ('date_versement_releve','<=',getdateAU),
                            ])
                    else:
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('compte_bancaire','ilike',nomcompte),
                            ('statu_validaion','=',etat),
                            ('categorie_produit','=',self.categorie_produit),
                            ('point_vente','=',codepv),
                            ('date_versement_releve','>=',getdateDU),
                            ('date_versement_releve','<=',getdateAU),
                            ])
                
            elif self.point_vente and not self.compte_bancaire:                
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0),
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('point_vente','=',codepv),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0),
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('point_vente','=',codepv),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
            elif not self.point_vente and self.compte_bancaire:
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0),
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('compte_bancaire','ilike',nomcompte),
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0),
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('compte_bancaire','ilike',nomcompte),
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    if nomcompte == banque :
                        listeCB=self.env['gakd.versementreleve'].search([
                            ('compte_bancaire','ilike',nomcompte),
                            ('statu_validaion','=',etat), 
                            ('date_versement_releve','>=',getdateDU),
                            ('date_versement_releve','<=',getdateAU),
                            ])
                    else:
                        listeCB = self.env['gakd.versementreleve'].search([
                            ('compte_bancaire', 'ilike', nomcompte),
                            ('statu_validaion', '=', etat),
                            ('categorie_produit', '=', self.categorie_produit),
                            ('date_versement_releve', '>=', getdateDU),
                            ('date_versement_releve', '<=', getdateAU),
                        ])
            elif not self.point_vente and not self.compte_bancaire:
                # if etat==ett1:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','!=',0),
                #         ])
                # elif etat==ett2:
                #     listeCB=self.env['gakd.versementreleve'].search([
                #         ('statu_validaion','=','valideAuto'),
                #         ('categorie_produit','=',self.categorie_produit),
                #         ('date_versement_releve','>=',getdateDU),
                #         ('date_versement_releve','<=',getdateAU),
                #         ('montant_manquant','=',0),
                #         ])
                # else:
                if etat!='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
                elif etat=='R.n_existe_pas_dans_V':
                    listeCB=self.env['gakd.versementreleve'].search([
                        ('statu_validaion','=',etat),
                        ('categorie_produit','=',self.categorie_produit),
                        ('date_versement_releve','>=',getdateDU),
                        ('date_versement_releve','<=',getdateAU),
                        ])
            self.table=listeCB


    def imprimer1(self):
        return self.env['report'].get_action(self, 'gakd_versement.imprimer_repport_validation')
    
    def remplir(self):
        chaine = "select * from gakd_versementreleve where 1=1"
        if self.state_validation:
            chaine += " and statu_validaion= '"+str(self.state_validation)+"'"
        if self.categorie_produit:
            chaine += " and categorie_produit='"+str(self.categorie_produit)+"'"
        if self.compte_bancaire:
            chaine += " and compte_bancaire='"+str(self.compte_bancaire)+"'"
        if self.date_du and self.date_au and self.date_au>self.date_du:
            #date du "format yyyy-mm-dd " datFormatDU=datduParse.strftime('%Y-%m-%d')
            datdu=self.date_du            
            datduParse=parse(str(datdu))
            getdateDU=datduParse.date()
            #date au "format yyyy-mm-dd "
            datau=self.date_au
            datauParse=parse(str(datau))
            getdateAU=datauParse.date()
            chaine += " and date_versement_releve BETWEEN '"+str(getdateDU)+"' and '"+str(getdateAU)+"'"
        if self.point_vente:
            codepv=None
            if len(self.point_vente)>0:
                codepv=self.point_vente[0].libelle
            else:
                codepv=self.point_vente.libelle
            chaine+=" and point_vente='"+ str(codepv)+"'"
        
        print(00000000000000000000000000000000)
        print (chaine)
        self._cr.execute(chaine)
        data=[]
        for row in self._cr.fetchall():
            data.append({
                'compte_bancaire':row[11],'point_vente':row[16],
                'categorie_produit':row[17],'date_versement_releve':row[9],'num_versement':row[4],
                'nom_gerant':row[10],'montant_versement':int(row[14]),'montant_releve':int(row[8]),
                'montant_manquant':int(row[6]),'num_releve':row[7],'lebelle_Releve':row[2],'statu_validaion':row[3]
            })
        return data

