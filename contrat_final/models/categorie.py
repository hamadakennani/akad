# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint



class capcontrat_categories(models.Model):
	_name = 'capcontrat.categories'
	_rec_name = 'nom' 
	_order='create_date desc'

	nom=fields.Char(string='categorie', size=128, required=True)
	files=fields.One2many('capcontrat.fichiers', 'file_id', string='files')
	filename=fields.Char(string='file')
	description=fields.Text(string='Description')

	@api.model
	def create(self,vals):
		# data_order = 
		# [{u'to_invoice': False, 
		# u'data': {u'user_id': 1, u'name': u'Commande 00004-005-0001595', u'partner_id': False, u'amount_paid': 3000, u'pos_session_id': 2, 
		# 		u'lines': [[0, 0, {u'product_id': 2, u'price_unit': 3000, u'qty': 1, u'pack_lot_ids': [], u'discount': 0, u'id':2, 
		# 				u'tax_ids': [[6, False, []]]}]], 
		# u'statement_ids': [[0, 0, {u'journal_id': 1, u'amount': 3000, u'name': u'2018-08-31 09:00:52', u'account_id': 1, u'statement_id': 1}]], u'creation_date': u'2018-08-31 09:00:52', u'amount_tax': 0, u'fiscal_position_id': False, u'uid': u'00004-005-0001595', u'amount_return': 0, u'sequence_number': 5, u'amount_total': 3000}, u'id': u'00004-005-0001595'}]
		# print data_order
		# order = self.env["pos.order"].create_from_ui(data_order)
		# print order.__dict__
		return super(capcontrat_categories, self).create(vals)




