# -*- coding: utf-8 -*-
from odoo import api, models,fields
from num2words import num2words
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from openerp.exceptions import ValidationError
class senegalGeneveWizard(models.TransientModel):
    _name = "senegalgeneve.wizard"
    _description = "Senegal Geneve Wizard"
    
    def _getinvoice_id(self):
        return self._context.get('active_ids',False)[0]  
    company=fields.Many2one('liste.societe')
    invoice_id=fields.Many2one('account.invoice',default=_getinvoice_id)
    @api.multi
    def print_report(self):
        datas={}
        datas={
            'form':{
                'company':self.company.id,
                'invoice_id':self.invoice_id.id,
                'devis':self.id,
            }            
        }
      
        return self.env['report'].get_action(self,'facture.sengen_facture', data=datas)
class retour_bon_consommation_report(models.AbstractModel):
    _name = 'report.facture.sengen_facture'
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('facture.sengen_facture')
        invoice=self.env['account.invoice'].search([('id','=',data['form']['invoice_id'])])
        sale_id=0
        for linne in invoice.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        saleorder=self.env['sale.order'].search([('id','=',sale_id)])   
        company=self.env['liste.societe'].search([('id','=',data['form']['company'])])
        invoice=self.env['account.invoice'].search([('id','=',data['form']['invoice_id'])])
        deviss=self.env['senegalgeneve.wizard'].search([('id','=',data['form']['devis'])])
        stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
        m3=self.env["product.uom"].search([('name','=','M3 à 15°')]).id  
        # on chereche sur l'unite de Mesure qui contient M3 a 15 et on retour leur quantite
        listepro1=[]
        m3q=0
        for a in stock.pack_operation_product_ids: 
            if a.product_uom_id.id==m3:        
                m3q=a.qty_done        
            if a.um1.id==m3:           
                m3q=a.qty1
            if a.um2.id==m3:
                m3q=a.qty2
        currency=self.env['res.currency'].search([('name','=','EUR')])        
        currencyrate=self.env['res.currency.rate'].search([('currency_id','=',currency[0].id)])
        currency2=self.env['res.currency'].search([('name','=','XOF')])        
        currencyrate2=self.env['res.currency.rate'].search([('currency_id','=',currency2[0].id)])
        date1=stock.min_date
        date1_obj = datetime.strptime(date1, "%Y-%m-%d %H:%M:%S")
        d1=date1_obj.strftime("%Y-%m-%d") 
        mini=0
        nouveau =0  
        test =0
        cas1=0
        cas2=0
        mini1=0
        nouveau1 =0  
        test1 =0
        cas3=0
        cas4=0
        date3=datetime.now()
        date5=datetime.now()
        prixaed=0
        prixxof=0
        if saleorder.pricelist_id.currency_id.name=="XOF" : 
            listepro1.append({'product':a.product_id.name,'qte':m3q,'prix':invoice.invoice_line_ids.price_unit,'montantfcfa':m3q*invoice.invoice_line_ids.price_unit}) 
        if saleorder.pricelist_id.currency_id.name=="EUR" : 
            for euro in currencyrate :
                date2=euro.name
                date2_obj = datetime.strptime(date2, "%Y-%m-%d %H:%M:%S")
                d2=date2_obj.strftime("%Y-%m-%d") 
                rd = relativedelta(datetime.strptime(d1,"%Y-%m-%d"),datetime.strptime(d2,"%Y-%m-%d"))
                if rd.days < 0:
                    if cas2==0:
                        cas1+=1
                        if cas1==len(currencyrate):
                            raise ValidationError("Veuillez parametrer dans le système le taux de conversion des devises")   
                elif  rd.days == 0 :
                    prixaed=invoice.invoice_line_ids.price_unit/euro.rate             
                    cas2=1
                    break   
                else :
                    if cas2==0:
                        if test == 0 :
                            mini=rd.days
                            test=1
                        nouveau=rd.days
                        if nouveau <= mini:
                            mini=rd.days
                            date3=date2
                            prixaed=invoice.invoice_line_ids.price_unit/euro.rate
            for xof in currencyrate2:
                date4=xof.name
                date3_obj = datetime.strptime(date4, "%Y-%m-%d %H:%M:%S")
                d3=date3_obj.strftime("%Y-%m-%d") 
                rd2 = relativedelta(datetime.strptime(d1,"%Y-%m-%d"),datetime.strptime(d3,"%Y-%m-%d")) 
                if rd2.days < 0:
                    if cas4==0:
                        cas3+=1
                        if cas3==len(currencyrate2):
                            raise ValidationError("Veuillez parametrer dans le système le taux de conversion des devises")   
                elif  rd2.days == 0 :
                    prixxof=prixaed*xof.rate             
                    cas4=1
                    break   
                else :
                    if cas4==0:
                        if test1 == 0 :
                            mini1=rd2.days
                            test1=1
                        nouveau1=rd2.days
                        if nouveau1 <= mini1:
                            mini1=rd2.days
                            date5=date4
                            prixxof=prixaed*xof.rate
                            
                listepro1.append({'product':a.product_id.name,'qte':m3q,'prix':prixxof,'montantfcfa':m3q*prixxof,'montanteuro':invoice.invoice_line_ids.price_unit*m3q}) 
                           
        if invoice.date_due :
            date_eche = datetime.strptime(invoice.date_due, "%Y-%m-%d")
            date_eche=date_eche.strftime("%d-%m-%Y") 
        if stock.min_date :
            date1_obj = datetime.strptime(stock.min_date, "%Y-%m-%d %H:%M:%S")
            datebl=date1_obj.strftime("%d-%m-%Y ") 
        test0=0
        listeadresse=[]
        for client in saleorder.partner_id :
            listeadresse.append({'adresse1':client.street,'adresse2':client.street2,'city':client.city,'zip':client.zip,'country':client.country_id.name}) 
        for s in saleorder.order_line:
            test1="Sénégal"
            test2=test1.decode('utf-8')
            if s.product_id.name=="Gasoil a 15" and (test2 in saleorder.stock.name or "DAKAR" in saleorder.stock.name):
                test0=1
        if test0==1 :
            if saleorder.pricelist_id.currency_id.name=="EUR" and company.name=="OCEAN AKAD INTERNATIONAL FZCO DUBAI SUCCURSALE DE GENEVE":
                banque=self.env['liste.banque'].search([('filt','=','egpbs')])
            if saleorder.pricelist_id.currency_id.name=="XOF" and company.name=="OCEAN AKAD INTERNATIONAL FZCO":
                banque=self.env['liste.banque'].search([('filt','=','ffpbs')])
            datas={
                'form':{
                    'company_logo':company.header,
                    'company_fotter':company.fotter,
                    'company_name':company.name,
                    'banquee':banque.banquee,
                    'adressebanquee':banque.adressebanquee,
                    'nomcomptee':banque.nomcomptee,
                    'adressecomptee':banque.adressecomptee,
                    'numcomptee':banque.numcomptee,
                    'swiftee':banque.swiftcodee,
                    'ibanee':banque.ibane,
                    'banquef':banque.banquef,
                    'nomcomptef':banque.nomcomptef,
                    'numcomptef':banque.numcomptef,
                    'swiftef':banque.swiftcodef,
                    'ibanef':banque.ibanf,
                    'facture_id':invoice.id,
                    'client':invoice.partner_id.name,
                    'adresse':listeadresse,
                    'tele':invoice.partner_id.phone,
                    'email':invoice.partner_id.email,
                    'produit':listepro1,
                    'prix_commande':invoice.amount_total,
                    'date_echeance':date_eche,
                    'depot':saleorder.stock.name,
                    'datebl':datebl
                }            
            }
        else :
            raise ValidationError("Attention : vous n'avez pas choisi la bonne facture dans le menu")   
    
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': datas,             
            }
        return report_obj.render('facture.sengen_facture',docargs)

