# -- coding: utf-8 --
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError

class res_partner(models.Model):
    _inherit = "res.partner"

    ht_type_client = fields.Selection(string='Type client ' ,selection=
    [('ec','Entreprises et collectivités'), ('av', 'Agences de voyage')#[30671,30640]
    ,('ei', 'Exposants et intermédiaires'),('ci', 'Clients intermédiaires de tourisme'),#[30672,30640] [30673,30640]
    ('p', 'Particuliers'),('cg', 'Client-Group'),#[30674,30640] [30677,30644]
    ('ccp', 'Client État et Collectivités publiques'),('coi' ,'Clients,organismes internationaux'),])#[30678,30640][30679,30640]

    
    ht_type_fournisseurs=fields.Selection(string='Type fournisseurs ' ,selection=
    [('Fournisseurs de marchandises', 'Fournisseurs de marchandises'), ('Fournisseurs de biens non vendables', 'Fournisseurs de biens non vendables')
    ,('Fournisseurs / Prestataires de services', 'Fournisseurs / Prestataires de services'),('Fournisseurs Groupe', 'Fournisseurs Groupe'),
    ('Fournisseurs sous-traitants', 'Fournisseurs sous-traitants'),])
    type_supplier = fields.Boolean(default=False,compute='_get_supplier')
    @api.one
    @api.depends('supplier')
    def _get_supplier(self):
        if self.supplier:
            self.type_supplier=True
        else:
            self.type_supplier=False

    @api.onchange('ht_type_client')
    def _onchange_ht_type_client(self):
        print '2222222222222222222222'
        if self.ht_type_client:
            if self.ht_type_client == 'ec': 
                self.property_account_receivable_id=30670
                self.property_account_payable_id=30640
            if self.ht_type_client == 'av': 
                self.property_account_receivable_id=30671
                self.property_account_payable_id=30640
            if self.ht_type_client == 'ei': 
                self.property_account_receivable_id=30672
                self.property_account_payable_id=30640
            if self.ht_type_client == 'ci': 
                self.property_account_receivable_id=30673
                self.property_account_payable_id=30640
            if self.ht_type_client == 'p': 
                self.property_account_receivable_id=30674
                self.property_account_payable_id=30640
            if self.ht_type_client == 'cg': 
                self.property_account_receivable_id=30677
                self.property_account_payable_id=30640
            if self.ht_type_client == 'ccp': 
                self.property_account_receivable_id=30678
                self.property_account_payable_id=30640
            if self.ht_type_client == 'coi': 
                self.property_account_receivable_id=30679
                self.property_account_payable_id=30640

    @api.onchange('ht_type_fournisseurs')
    def _onchange_ht_type_fournisseurs(self):
        print '888888888888885555555555555555558888888888888888'
        data={'Fournisseurs de marchandises':[30641,30669], 'Fournisseurs de biens non vendables':[30642,30669], 'Fournisseurs / Prestataires de services':[30643,30669],'Fournisseurs Groupe':[30644,30669],'Fournisseurs sous-traitants':[30645,30669]}



        if self.ht_type_fournisseurs:
            print ' if onchange '
            print data[self.ht_type_fournisseurs][1]
            print data[self.ht_type_fournisseurs][0]
            self.property_account_receivable_id=data[self.ht_type_fournisseurs][1]
            self.property_account_payable_id=data[self.ht_type_fournisseurs][0]