# -*- coding: utf-8 -*-
from odoo import api, models,fields
from datetime import datetime, date, timedelta
from openerp.exceptions import ValidationError
class quantite_stock(models.TransientModel):
    _name = "quantite.stock"

    
    def _get_maint_id(self):
        return self._context.get('active_ids',False)[0]  
    def _get_quantite(self):
        maint=self.env['mro.order'].search([('id','=',self._get_maint_id())])
        for line in maint.piece:
            if line.qty>line.qty_stock:
                return line.qty
    def _get_quantite_demande(self):
        maint=self.env['mro.order'].search([('id','=',self._get_maint_id())])
        for line in maint.piece:
            if line.qty>line.qty_stock:
                return line.qty_stock-line.qty          

    maint_id=fields.Many2one('mro.order',default=_get_maint_id)
    qty=fields.Float('Quantité',default=_get_quantite)
    qty_demande=fields.Float('Quantité demandée',default=_get_quantite_demande)
    @api.multi
    def redirect(self):
        return {
        'name':  'Transaction',
        'view_type': 'form',
        'view_mode': 'form',
        'res_model':'quantite.stock', 
        'res_id': self.id,
        'type': 'ir.actions.act_window',
        'target': 'new',
        'nodestroy': True
        }