# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import  ValidationError
from lxml import etree
import datetime
class historique(models.Model):
    _name = 'gakd.historique'
    _order = 'create_date desc'
    
    # @api.one
    # def _calc(self):
    #     print 'CAAAAAAAAAAAALc'
    #     print self.montant_init-self.montant_fin
    #     return  self.montant_init-self.montant_fin
 
    montant_init = fields.Float(string='Montant initiale')
    montant_fin =  fields.Float(string='Montant finale')
    chargeur =     fields.Many2one(comodel_name='gakd.chargeur')
    type_op = fields.Char(string='Type d\'opération')
    sous_chargeur = fields.Many2one(comodel_name='gakd.sous_chargeur')
    diff = fields.Float(string='Montant affecté')
    verse_validate= fields.Boolean(default=False)
    tres_validate=fields.Boolean(default=False)
    versement_id=fields.Many2one(comodel_name='gakd.versement')
    client_id = fields.Many2one(comodel_name='res.partner')
    moyen_de_paiement =  fields.Char(string='Moyen de paiement')
    detail_id=fields.Many2one('gakd.historique_ben_detail')
    carte = fields.Many2one(comodel_name='gakd.carte')
    # flag = fields.Boolean(string='',default=False)

    state = fields.Selection(string='Etat', selection=[('valide', 'Validé'), ('annule', 'Annulé')],default='valide')

    account_move = fields.Many2one(comodel_name='account.move')
    tv_print = fields.Many2one(comodel_name='gakd.tv_print')

    @api.model
    def create(self, values):
        print 'CREAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATE HISTORIQUE'
        return super(historique, self).create(values)
    #     else :
    #         raise ValidationError("Vous n'avez pas le droit d'effectuer cette opération")


    def fields_view_get(self,view_id=None, view_type='form',toolbar=False, submenu=False):
        res = super(historique, self).fields_view_get( view_id=view_id, view_type=view_type,toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        if view_type == 'form' and [0==1]:
            for node_form in doc.xpath("//form"):
                node_form.set("create", 'false')
        res['arch'] = etree.tostring(doc)
        return res
    
    # @api.model
    # def default_get(self, fields):
	# 	res = super(historique, self).default_get(fields)     
	# 	if  self.env.user.has_group('base.group_system'):
	# 		return res
	# 	else :
	# 		raise ValidationError("Vous n'avez pas le droit d'effectuer cette opération")

    @api.multi
    def annuler_recharge(self):
        print 'annuler recharge compte client sssssssssssss'
        print self.type_op
        print self.account_move
        if not  self.verse_validate or not self.tres_validate :
            print 'iiiiiiiiiiiiiiiiiiin'
            print self.client_id.solde_compte_hide
            print self.diff
            self.client_id.solde_compte_hide-=self.diff
            self.sudo().write({
                'state':'annule',
                'verse_validate':True,
                'tres_validate':True,
                # 'account_move':3372916
            }) 
            print self.sous_chargeur
            total = self.sous_chargeur.montant_plafond+self.diff
            print total
            print 'total66666666666666666'
            self.sous_chargeur.sudo().write({
                'montant_plafond':total,
            })
            a=self.account_move.sudo().reverse_moves()
            print a
            if self.type_op == 'T.V':
                self.env['gakd.ticket_valeur'].search([('tv_print','=',self.tv_print.id)]).unlink()
             
            
class historique_ben_detail(models.Model):
    _name="gakd.historique_ben_detail"
    _rec_name='type_de_recharge'

    tva_init=fields.Float(string='')
    montant_bonus=fields.Float(string='')
    prix_ht=fields.Float(string='')
    prix_init=fields.Float(string='')
    qte = fields.Float(string='')
    montant_hor_taxes = fields.Float(string='')
    tva = fields.Float(string='')
    montant_ttc = fields.Float(string='')
    montant_a_recharge_remise = fields.Float(string='')
    type_de_recharge = fields.Char(string='Produit')
    historique_id = fields.Many2one(comodel_name='detail_id', string='')
    
        
