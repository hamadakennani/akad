odoo.define('report_user_access_right.report', function (require) {"use strict";
    var core = require('web.core');
    var Widget = require('web.Widget');
    var QWeb = core.qweb;
    var Model = require('web.Model');

    var AccessRightReportView = Widget.extend({
        start: function() {
            var self = this;
            var model = new Model('res.users');
            model.call('get_access_right_report_data', [], ).then(function (result) {
                this.data = JSON.parse(result)
                self.$el.append(QWeb.render('AccessRightReportViewTemplate', {widget: this}))
            });
        },

    });
    core.action_registry.add('report_user_access_right.report', AccessRightReportView);
});

