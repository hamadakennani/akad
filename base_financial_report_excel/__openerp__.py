# -*- coding: utf-8 -*-

# Part of Probuse Consulting Service Pvt Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': 'Base Financial Excel Report',
    'version': '1.0',
    'price': 1.0,
    'currency': 'EUR',
    'license': 'Other proprietary',
    'live_test_url': 'https://youtu.be/6TF1Vb-_1c8',
    'category': 'Accounting',
    'summary': 'Excel report for Financial Accounting like Trial Balance,General Ledger,Partner Ledger,Balance Sheet,Profit-Loss.',
    'description': """
    Excel report for Financial Accounting like 
    **Trial Balance,
    **General Ledger,
    **Partner Ledger,
    **Balance Sheet,Profit-Loss
    
     You can install xlwt library in following links 
     https://pypi.python.org/pypi/xlwt
     
     Tags:
Account general ledger
Account general ledger Excel
Account Partner Ledger
Account Partner Ledger Excel
Balance Report
Odoo9 Accounting Report in excel
Profit and Loss Report in Excel
excel reports
accounting reports
account finance report
GL report
general ledger report in excel
report in excel
odoo community report
odoo community accounting reports
community accounting reports
financial reports community
excel reports
accounting excel reports
odoo reports
    """,
    'author': 'Probuse Consulting Service Pvt. Ltd.',
    'website': 'www.probuse.com',
    'depends': [
        'account_general_ledger_excel',
        'account_balance_excel',
        'account_partner_ledger_excel',
        'account_financial_report_excel',
    ],
    'data': [
             ],
    'installable': True,
    'application': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
