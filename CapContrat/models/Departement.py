# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint

import time
#import swagger_client
#from swagger_client.rest import ApiException
from pprint import pprint


class Departement(models.Model):
    _name = 'capcontrat.departement'
    _rec_name = 'libelle'
    libelle=fields.Char('Libellé', size=128, required=True)
    contrat_id=fields.One2many("capcontrat.contrat","direction_Departement",invisible=True)