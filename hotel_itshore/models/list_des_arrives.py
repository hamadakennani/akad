# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time
from room_disponiblite import is_room_disponible,room_state
from odoo import api, fields, models

from odoo import api, fields, models


class LiteDesArrives(models.Model):
    _name = 'hotel.list_des_arrive'
    date_fin = fields.Date(string='Date Fin')
    date_debut = fields.Date(string='Date Début')
    lines = fields.One2many(comodel_name='hotel.list_des_arrive.line', inverse_name='arrive_id', string='line')
    

    @api.multi
    @api.onchange('date_debut','date_fin')
    def get_rooms(self):
        if self.date_debut and self.date_fin:
            lines=[]
            print " getttt ___ rooms "
            # for line in self.env['hotel_reservation.line'].search([('checkin','=',datetime.strptime(self.date_debut, '%Y-%m-%d'),
            #         ('checkout','<=',datetime.strptime(self.date_fin, '%Y-%m-%d')])
            for day in self.check_overlap(self.date_debut,self.date_fin):
                print 'len'*20
                # print len(self.check_overlap(self.date_debut,self.date_fin))
                print "self.env['hotel_reservation.line'].search([('checkin','=',str(datetime.strptime(self.date_debut, '%Y-%m-%d')))]):"
                # print self.env['hotel_reservation.line'].search([])
                for line in self.env['hotel_reservation.line'].sudo().search([('line_id.company_id','=',self.env.user.company_id.id)]):
                    # print line.checkout
                    # print 'liiiiiiiiiiiiiiiiiiiiiiiiiiiine'
                    # print ' line '
                    # print datetime.strptime(line.checkin, '%Y-%m-%d %H:%M:%S').date()
                    print "date dute"
                    # print datetime.strptime(self.date_debut, '%Y-%m-%d').date()
                    if line.checkin:
                        if line.line_id.state =='draft' and datetime.strptime(line.checkin, '%Y-%m-%d %H:%M:%S').date()==day.date():
                            print line.number_of_dayes
                            lines.append((0,0,{'res_partner':line.line_id.partner_id.id,'resrv':line.line_id.id ,'date_fin_maintenance':day.date(),'checkout':datetime.strptime(line.checkout, '%Y-%m-%d %H:%M:%S').date(),'number_of_dayes':int(line.number_of_dayes)}))
            print " linessss  --------------"
            # print  lines
            return {'value':{'lines':lines}}
    @api.multi
    def check_overlap(self,date_debut,date_fin):
        
        date2 = datetime.strptime(date_fin, '%Y-%m-%d')
        date1 = datetime.strptime(date_debut, '%Y-%m-%d')
        delta = date2 - date1
       
        dates=[date1 + timedelta(days=i) for i in range(delta.days + 1)]
       
        return dates

class LiteDesArrivesLine(models.Model):
    _name = 'hotel.list_des_arrive.line'
    _description = 'New Description'
    date_fin_maintenance = fields.Char(string='Date')
    checkout = fields.Char(string='Date de départ')
    number_of_dayes = fields.Char(string='Nombre de nuités')
    res_partner= fields.Many2one(comodel_name='res.partner', string='Invité')
    resrv = fields.Many2one(comodel_name='hotel.reservation', string='Réservation')
    arrive_id = fields.Many2one(comodel_name='hotel.hotel.list_des_arrive', string='')

    @api.multi
    def goToReservation(self):
        print 'hello '
        print ' helllll '
        return {
        'view_type': 'form',
        'view_mode': 'form',
        'res_model': 'hotel.reservation',
        'target': 'current',
        'res_id': self.resrv.id,
        'type': 'ir.actions.act_window'
         }
   









#    
# 
# 
# 
# 
# 

class Personne_present(models.Model):
    _name = 'hotel.personne_present'
    date_fin = fields.Date(string='Date Fin')
    date_debut = fields.Date(string='Date Début')
    lines = fields.One2many(comodel_name='hotel.personne_present.line', inverse_name='arrive_id', string='line')
    

    @api.multi
    @api.onchange('date_debut','date_fin')
    def get_rooms(self):
        if self.date_debut and self.date_fin:
            lines=[]
            print " getttt _ rooms "
            for day in self.check_overlap(self.date_debut,self.date_fin):
                for line in self.env['hotel_reservation.line'].sudo().search([('line_id.company_id','=',self.env.user.company_id.id)]):
                    if line.checkin: 
                        if   datetime.strptime(line.checkin, '%Y-%m-%d %H:%M:%S').date()==day.date() and ( line.line_id.state =='done' or line.line_id.state =='confirm') :
                            print line.number_of_dayes
                            val= room_state(self,line.reserve_view.id,str(self.date_debut),str(self.date_debut))
                            lines.append((0,0,{'res_partner':line.line_id.partner_id.id,'resrv':line.line_id.id ,'date_fin_maintenance':day.date(),'checkout':datetime.strptime(line.checkout, '%Y-%m-%d %H:%M:%S').date(),'number_of_dayes':int(line.number_of_dayes),'nb_chambre':line.reserve_view.name,'type_chambre':line.categ_facturee.name,'statut':val[0],'tarif':line.sub_total,'nb_jour':line.number_of_dayes}))
            return {'value':{'lines':lines}}
    @api.multi
    def check_overlap(self,date_debut,date_fin):
        
        date2 = datetime.strptime(date_fin, '%Y-%m-%d')
        date1 = datetime.strptime(date_debut, '%Y-%m-%d')
        delta = date2 - date1
       
        dates=[date1 + timedelta(days=i) for i in range(delta.days + 1)]
       
        return dates
        
class Personne_presentLine(models.Model):
    _name = 'hotel.personne_present.line'
    _description = 'New Description'
    date_fin_maintenance = fields.Char(string='Date IN')
    checkout = fields.Char(string='Date OUT')
    number_of_dayes = fields.Char(string='Nombre de nuités')
    res_partner= fields.Many2one(comodel_name='res.partner', string='Nom client')
    resrv = fields.Many2one(comodel_name='hotel.reservation', string='Réservation')
    arrive_id = fields.Many2one(comodel_name='hotel.personne_present', string='')
    nb_chambre = fields.Char(string='N° de chambre')
    type_chambre = fields.Char(string='Type de chambre')
    tarif = fields.Float(string='Tarif')
    nb_jour= fields.Integer(string='Nombre des nuitées')



class Personne_presentt(models.Model):
    _name = 'hotel.personne_presentt'
    date_fin = fields.Date(string='Date Fin')
    date_debut = fields.Date(string='Date Début')
    lines = fields.One2many(comodel_name='hotel.personne_presentt.line', inverse_name='arrive_id', string='line')
    

    @api.multi
    @api.onchange('date_debut','date_fin')
    def get_rooms(self):
        if self.date_debut and self.date_fin:
            lines=[]
            print " getttt ___ rooms "
            # for line in self.env['hotel_reservation.line'].search([('checkin','=',datetime.strptime(self.date_debut, '%Y-%m-%d'),
            #         ('checkout','<=',datetime.strptime(self.date_fin, '%Y-%m-%d')])
            for day in self.check_overlap(self.date_debut,self.date_fin):
                print 'len'*20
                # print len(self.check_overlap(self.date_debut,self.date_fin))
                print "self.env['hotel_reservation.line'].search([('checkin','=',str(datetime.strptime(self.date_debut, '%Y-%m-%d')))]):"
                # print self.env['hotel_reservation.line'].search([])
                for line in self.env['hotel_reservation.line'].sudo().search([('line_id.company_id','=',self.env.user.company_id.id)]):
                    # print line.checkout
                    # print 'liiiiiiiiiiiiiiiiiiiiiiiiiiiine'
                    # print ' line '
                    # print datetime.strptime(line.checkin, '%Y-%m-%d %H:%M:%S').date() 
                    print "date dute" 
                    # print datetime.strptime(self.date_debut, '%Y-%m-%d').date()
                    if line.checkin: 
                        if   datetime.strptime(line.checkin, '%Y-%m-%d %H:%M:%S').date()==day.date() and line.line_id.state !='cancel':
                            print line.number_of_dayes
                            val= room_state(self,line.reserve_view.id,str(self.date_debut),str(self.date_debut))
                            lines.append((0,0,{'res_partner':line.line_id.partner_id.id,'resrv':line.line_id.id ,'date_fin_maintenance':day.date(),'checkout':datetime.strptime(line.checkout, '%Y-%m-%d %H:%M:%S').date(),'number_of_dayes':int(line.number_of_dayes),'nb_chambre':line.reserve_view.name,'type_chambre':line.categ_facturee.name,'statut':val[0],'tarif':line.sub_total,'nb_jour':line.number_of_dayes}))
            print " linessss  --------------"
            # print  lines
            return {'value':{'lines':lines}}
    @api.multi
    def check_overlap(self,date_debut,date_fin):
        
        date2 = datetime.strptime(date_fin, '%Y-%m-%d')
        date1 = datetime.strptime(date_debut, '%Y-%m-%d')
        delta = date2 - date1
       
        dates=[date1 + timedelta(days=i) for i in range(delta.days + 1)]
       
        return dates
        
class Personne_presenttLine(models.Model):
    _name = 'hotel.personne_presentt.line'
    _description = 'New Description'
    date_fin_maintenance = fields.Char(string='Date IN')
    checkout = fields.Char(string='Date OUT')
    number_of_dayes = fields.Char(string='Nombre de nuités')
    res_partner= fields.Many2one(comodel_name='res.partner', string='Nom client')
    resrv = fields.Many2one(comodel_name='hotel.reservation', string='Réservation')
    arrive_id = fields.Many2one(comodel_name='hotel.personne_presentt', string='')
    nb_chambre = fields.Char(string='N° de chambre')
    type_chambre = fields.Char(string='Type de chambre')
    statut = fields.Char(string='Statut de la chambre')
    
    tarif = fields.Float(string='Tarif')
    nb_jour= fields.Integer(string='Nombre des nuitées')