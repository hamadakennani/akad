# -*- coding: utf-8 -*-

# Part of Probuse Consulting Service Pvt Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': 'Financial (P&L and B&S) Excel Report',
    'version': '1.1',
    'price': 80.0,
    'live_test_url': 'https://youtu.be/jsJdohcAQCc',
    'currency': 'EUR',
    'license': 'Other proprietary',
    'category': 'Accounting',
    'summary': 'Excel report for Profit & Loss / Balance Sheet...',
    'description': """        
     You can install xlwt library in following links 
     https://pypi.python.org/pypi/xlwt
     
Account Financial Reports
Account Financial Excel
excel reports
Profit and Loss Report Excel
accounting reports
account finance report
Financial report
account financial report in excel
report in excel
odoo community report
odoo community accounting reports
community accounting reports
financial reports community
profit excel
loss excel
profit and loss excel
profit loss excel
account_financial_report_excel
financial report excel
excel report
odoo excel report
balance sheet excel
balance sheet report excel
""",
    'author': 'Probuse Consulting Service Pvt. Ltd.',
    'website': 'www.probuse.com',
    'depends': ['account'],
    'data': [
            'security/ir.model.access.csv',
            'wizard/account_financial_report_view.xml',
             ],
    'installable': True,
    'application': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
