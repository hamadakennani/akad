# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError

from random import randint
from funs import date_dure_cul
from datetime import date, datetime
from datetime import datetime, timedelta



from odoo.exceptions import ValidationError
import time
#import swagger_client
#from swagger_client.rest import ApiException
from pprint import pprint


class Contrat(models.Model):
    _name = 'capcontrat.contrat'
    _rec_name = 'libelle'
    _order='create_date desc'

    @api.one
    @api.depends("Signe_le","Prise_effet","contrat_duree_type","reconduction","date_de_prise","date_de_Jusqu_duree","int_duree","int_duree_type",
    'int_reconduction','int_reconduction_type')
    def _fin_duree(self):
        self.ensure_one()
       
        contrat_duree=""
        if self.contrat_duree_type=="Jusqu au" and self.date_de_Jusqu_duree:
           
                contrat_duree= date_dure_cul(self.date_de_Jusqu_duree,1,1,1).date()
        elif self.contrat_duree_type=="Pour une duree de":
                contrat_duree= date_dure_cul(self.Signe_le,1,self.int_duree,self.int_duree_type).date()   
        self.fin_theorique=contrat_duree
        self.fin_theorique_value =contrat_duree
        for  condition in self.conditions_ids:
            print " changer co ndition s"
            condition._get_echeance_text()
            condition._fin_duree()
            condition._date_debut()
            condition.montant_operations()
        for c in self.clauses_ids:
            c._get_echeance_text()
            
            c._fin_duree()
            c._date_debut()
            # c._date_eval()
        return  contrat_duree
        
    
    @api.depends("Indiquer_un_autre_montant","conditions_ids")
    def _get_montant(self):
        if self.Indiquer_un_autre_montant:
         self.montant_global_float=self.montant_float
        elif not self.Indiquer_un_autre_montant :
            montant=0
            for conditions in self.conditions_ids:
                montant=montant+conditions.montant
            self.montant_global_float=montant

    @api.depends("Prise_effet","date_de_prise")
    def _get_Prise_effet_date_value(self):
       
        if self.Prise_effet=="a la date  de signateur":
           self.Prise_effet_date_value=datetime.strptime(self.Signe_le, '%Y-%m-%d')  
           return self.Prise_effet_date_value
       
        elif self.Prise_effet=="a une date du":
             self.Prise_effet_date_value=datetime.strptime(str(self.date_de_prise), '%Y-%m-%d')  
             return self.Prise_effet_date_value



    
    @api.depends("Signe_le","Prise_effet","contrat_duree_type","reconduction","date_de_prise","date_de_Jusqu_duree","int_duree","int_duree_type",
    'int_reconduction','int_reconduction_type')
    def _get_textunit(self):

        Signe_le=""
        #Signé le
        
        if self.Signe_le :
            Signe_le=u"Signé le "+str(datetime.strptime(self.Signe_le, '%Y-%m-%d').date()) 
        # Prise_effet
        Prise_effet=""
        if  self.Signe_le:
            Prise_effet=u",prise d'effet le "+str(datetime.strptime(self.Signe_le, '%Y-%m-%d').date()) 
        
        elif self.Prise_effet=="a une date du" and self.date_de_prise  :
             Prise_effet=u",prise d'effet le " +self.date_de_prise

        #contrat_duree
     
        contrat_duree=""
        if self.contrat_duree_type=="Jusqu au" and self.date_de_Jusqu_duree:
           
                contrat_duree=u" pour une durée Determinée  Jusqu'au "+self.date_de_Jusqu_duree
        elif self.contrat_duree_type=="Pour une duree de":
                contrat_duree=u" pour une durée Determinée initiale de " +str(self.int_duree)+" "+self.int_duree_type
        

        reconduction=""
        if self.reconduction=="Non prevue":
             reconduction=u" Aucune reconduction prévue par le contrat "
        elif self.reconduction=="Tacite":

            # if self.reconduction_duree_type=="Pour une duree de":
                reconduction=u"Renouvelable tacitement pour une durée "+str(self.int_reconduction)+" " +self.int_reconduction_type
          
             
        elif self.reconduction==u"Avec accord":
             reconduction=u"reconduction avec accord par période "+str(self.int_reconduction)+" " +self.int_reconduction_type+u"  Préavis "+str(self.preavis)+" " +self.preavis_type


        for  condition in self.conditions_ids:
            print " changer co ndition s"
            condition._get_echeance_text()
            condition._fin_duree()
            condition._date_debut()
            condition.montant_operations()
            # condition._date_eval()
        for c in self.clauses_ids:
            c._get_echeance_text()
            
            c._fin_duree()
            c._date_debut()
            # c._date_eval()

       

        self.textunit=Signe_le +" "+Prise_effet+" "+contrat_duree+" "+reconduction
        # print " modiff de contrata "
        # print  isinstance(self.id,(int,long))

        # if self.manage and self.fin_theorique_value and  self.alertes_email and isinstance(self.id,(int,long)):
           
        #     echeances=self.env['capcontrat.contrat.echeance'].search([("contrat_id","=",self.id)]).unlink()
        #     print " echeance"
        #     print echeances
            # for echeance in echeances:
            #         echeance.unlink()
            # self.env['capcontrat.contrat.echeance'].create(
            # {"mailto":self.manage.id,'send_date':date_dure_cul(self.fin_theorique_value,0,self.alertes_email),"contrat_id":self.id})

       
      

  
    @api.constrains("Signe_le","date_de_prise","date_de_Jusqu_duree","Prise_effet","contrat_duree_type","int_duree")
    def les_dates_chake(self):
        if not self.source :
            if not self.Signe_le:
                raise ValidationError(" La date du signateure est Obligatoir ") 

            if self.Prise_effet=="a une date du":
                if not self.date_de_prise :
                    raise ValidationError("Cycle de vie : La date de Prise d'effet  est obligatoire ") 
                elif datetime.strptime(self.date_de_prise, '%Y-%m-%d')<datetime.strptime(self.Signe_le, '%Y-%m-%d'):
                    raise ValidationError("Cycle de vie :  La date de prise d'effet doit être supérieure à    la date du signature") 
            if self.contrat_duree_type=="Pour une duree de":
                if not self.int_duree:
                    raise ValidationError(" La durée est obligatoire ") 
                elif self.int_duree<=0:
                    raise ValidationError(" La durée doit être supérieur à zéro") 

            elif self.contrat_duree_type=="Jusqu au":
                if not self.date_de_Jusqu_duree :
                    raise ValidationError("Cycle de vie :  Veuillez renseigner le champ durée") 
                elif datetime.strptime(self.date_de_Jusqu_duree, '%Y-%m-%d')<datetime.strptime(self.Signe_le, '%Y-%m-%d'):
                    raise ValidationError(" Cycle de vie :  La date de fin doit être supérieure à la date de signature") 
                elif datetime.strptime(self.date_de_Jusqu_duree, '%Y-%m-%d')<datetime.strptime(self.Prise_effet_date_value, '%Y-%m-%d'):
                    raise ValidationError(" Cycle de vie :  La date de fin doit être supérieure à la date de prise d'effet") 
    
   
    @api.depends()
    def _get_current_user(self):
      
        if self.env.user.id==self.manage.id or self.env.user.id==1:
         self.privilege_hide=False
        else:
            self.privilege_hide=True
        return   self.env.user.id
        

    @api.constrains("preavis","int_reconduction","reconduction")
    def _chack_preavis(self):
      if not self.reconduction=="Non prevue":
        # if self.reconduction_duree_type=="Pour une duree de":
           if  self.int_reconduction<=0:
                 raise ValidationError("Reconduction à durée  La durée doit être supérieur à zéro") 
      if   self.reconduction=="Tacite" and self.preavis<=0:
               raise ValidationError(" La preavis doit être supérieur à zéro") 
               
    # @api.onchange("libelle")
    # def _fill_privilege(self):
    #     if not  self.privilege_ids:
    #       privilege=[]
    #       for user in self.env['res.users'].search([]):
    #         if user.id!=1 and user.id!=self.env.user.id:
    #          values={}
    #          values["res_users"]=user.id
    #          privilege.append((0,0,values))
    #       return privilege

    @api.onchange("signature_avec")
    def _privilege(self):
        if self.signature_avec:
            print  " invisible   -------------------"
            if  self.signature_avec.is_company==True or  self.signature_avec.company_type=="company":
                self.interlocuteurs_hide=False
            else:
                self.interlocuteurs_hide=True
    @api.onchange("devise","taxe")
    def _devise_et_taxe(self):
        if self.devise and self.taxe:
            for conditions in self.conditions_ids:
                conditions.write({'montant_monnaie':self.devise,'montant_taxe':self.taxe})
    
          
            




     
   
    libelle=fields.Char(string=u'Libellé', size=128, required=True)
    historique=fields.One2many("capcontrat.historique","contrat")
    # 'information generalle'
  
    categorie_type =fields.Many2one('capcontrat.categories',string="Catégorie", required=True)
    signature_avec=fields.Many2one('res.partner',string='Signé avec', required=True)
    direction_Departement=fields.Many2one('capcontrat.departement',string='Direction/Département')
    lieu_physique =fields.Char(string='Lieu de stockage physique ')
    # en cas de type  Assurance
    '''  Cycle de vie   '''
    #Cycle de vie 
   
  
    Signe_le=fields.Date(string=u"Signé le ",default=datetime.today())
    
    privilege_hide=fields.Boolean(compute=_get_current_user,store=False)
    

   
    
    Prise_effet_date_value=fields.Date(compute=_get_Prise_effet_date_value,store=True,string="Prise d'effet")
    Prise_effet =fields.Selection([
        ('a la date  de signateur','à la date  de signature'),('a une date du','à une date du ')],string="Prise d'effet",
        default='a la date  de signateur')
    # depands on  Prise_effet value = à une date du
    date_de_prise=fields.Date()

    # contrat_duree=fields.Selection([('Indeterminee','Indéterminée'),('Determinee','Déterminée')],default='Indeterminee',string=u"Contrat à  durée")
    # depands on contrat_duree value = Déterminée
    contrat_duree_type=fields.Selection([('Pour une duree de','Pour une durée de '),("Jusqu au","Jusqu'au")],default="Jusqu au",string="Informations sur la duree")
    # depands on  contrat_duree_type value = Jusqu'au
    date_de_Jusqu_duree=fields.Date()
    
    int_duree=fields.Integer()
    int_duree_type=fields.Selection([('Jour(s)','Jour(s)'),('Semaine(s)',' Semaine(s)'),('Mois','Mois')
    ,('an(s)','Année')],default='Mois')
    reconduction=fields.Selection([('Non prevue','Non prévue'),('Tacite','Tacite'),('Avec accord','Avec accord')],default="Non prevue")
    int_reconduction=fields.Integer(string='Recondiction')
    int_reconduction_type=fields.Selection([('Jour(s)','Jour(s)'),('Semaine(s)',' Semaine(s)'),('Mois','Mois')
    ,('an(s)','Année')],default='Mois')
    # @api.depends("int_reconduction_type",'int_reconduction','reconduction',"preavis",'preavis_type')
    # def  _Text_reconduction(self):
    #      print " hello "
    #      reconduction=""
    #      if  self.reconduction=="Non prevue":
    #          reconduction=""
    #      elif self.reconduction=="Tacite":
    #          reconduction=""
    #      elif reconduction=="Avec accord":
    #          reconduction=""
    #      return reconduction

    # Text_reconduction=fields.Text(compute=_Text_reconduction)
    
    # depands on   reconduction values = Tacite 
    preavis=fields.Integer(string=u"Préavis")  
    preavis_type=fields.Selection([('Jour(s)','Jour(s)'),('Semaine(s)',' Semaine(s)'),('Mois','Mois')
    ,('an(s)','Année')],default='Mois')
    alertes_email=fields.Selection([(0,0),(1,1),(3,3),(7,7),(15,15),(30,30),(45,45),(90,90)],string="Alertes email",default=1)

    commentaire=fields.Text(string="Commentaires")
    # filds for view tree 
    fin_theorique=fields.Date(string=u"Fin théorique",store=True,compute=_fin_duree)
    

    fin_theorique_value=fields.Char(string=u"Fin théorique")
 
    # ----------------------------------------------------------------
    '''  Conditions financiéres ,clauses  et document  res users'''
    conditions_ids=fields.One2many('capcontrat.contrat.conditionsfinancieres','contrat_id',string='Conditions financiéres')
    clauses_ids=fields.One2many('capcontrat.contrat.clauses','contrat_id',string='Clauses principales')
    echeance_ids=fields.One2many('capcontrat.contrat.echeance','contrat_id',string='Les Échéances')
    document_ids=fields.One2many('ir.attachment','contrat_id',string='Rattachement')
    interlocuteurs_hide=fields.Boolean()
    interlocuteurs_id=fields.Many2one("res.partner.interlocuteur",string="Interlocuteur")
    # echeance_obj=fields.Many2one("capcontrat.contrat.echeance","capcontrat.contrat.echeance")
   
        
    privilege_ids=fields.One2many('capcontrat.contrat.privilege','contrat_id',string=u'Privilège',copy=True)
    #privilege_ids=fields.One2many('capcontrat.contrat.privilege','contrat_id',string=u'Privilège',copy=True,default=_fill_privilege)
   
   
   
   
    
      
    #manger
    manage=fields.Many2one('res.users',default=lambda self: self.env.user and self.env.user.id or False,string="Manger",store=True)
    versionDep =fields.Many2one('capcontrat.projets',string="Version de base")
    textunit=fields.Text(string="Initialement",compute='_get_textunit')


    ''' Montant global 	'''
    Indiquer_un_autre_montant =fields.Boolean(string="Indiquer un autre montant",default=False)

    montant_global_float=fields.Float(compute='_get_montant', store=True,string="Montant global")
    def _get_montant_str(self):
        self.montant_global_str=str(self.montant_global_float)+" "+self.devise+" "+self.taxe

        return  str(self.montant_global_float)+" "+self.devise+" "+self.taxe
    montant_global_str=fields.Char(string="Montant global",compute=_get_montant_str)
    montant_float=fields.Float()
    source=fields.Boolean(string='source',default=False)
    devise=fields.Selection([("DH","DH"),("USD","USD"),("EUR","EUR")],default="DH")
    taxe=fields.Selection([("HT","HT"),("TTC","TTC")],default="TTC")
    alert_all_users=fields.Boolean(string="Notifier tous les concernés séléctionners en cas d'échéance",default=False)
    @api.onchange("alert_all_users")
    def _alert_all_users(self):
        if self.alert_all_users==True:
          if self.manage and self.fin_theorique_value and  self.alertes_email and isinstance(self.id,(int,long)):
            for p in self.privilege_ids :
             
              self.env['capcontrat.contrat.echeance'].create(
              {"mailto":p.res_users.id,'send_date':date_dure_cul(self.fin_theorique_value,0,self.alertes_email),"contrat_id":self.id})


        else :
            if self.manage and self.fin_theorique_value and  self.alertes_email and isinstance(self.id,(int,long)):
           
              echances=self.env['capcontrat.contrat.echeance'].search([("contrat_id","=",self.id),('mailto','!=',self.manage.id)]).unlink()
            




   
    # self.env['capcontrat.contrat.echeance']
    
    @api.multi
    def write(self,vals):
        print 'eeeeeeeeeeeeeeee'
        print vals 
        data = []
        for key,v in vals.iteritems():
            print key
            print self[key]
            print v
            if key not in ['conditions_ids','clauses_ids','document_ids','privilege_ids']:
                data.append((0,0,{'libelle':key,'ancien':self[key],'nouveau':v}))
        print data
        if len(data)!=0:
            o = self.env["capcontrat.historique"].create({
            'contrat':self.id,
            'champs':data
            })
            print 'aAA'
        return super(Contrat,self).write(vals)

    @api.model
    def create(self,vals):
      print "    ------------------>>>>>>> "
      print vals
      print " hhhhhhhhhhh o oooo hhhh oooo ffff "
      res= super(Contrat,self).create(vals)
      print self.fin_theorique
      print res
      if 'source' not in vals.keys() :
          self.env['capcontrat.contrat.echeance'].create(
          {"mailto":vals['manage'],'send_date':date_dure_cul(vals['fin_theorique_value'],0,vals['alertes_email']),"contrat_id":res.id})
      return res  

    @api.constrains("Signe_le","Prise_effet","contrat_duree_type","reconduction","date_de_prise","date_de_Jusqu_duree","int_duree","int_duree_type",
    'int_reconduction','int_reconduction_type')
    def modif_eheance(self):
        if not self.source :
            print " modiff de contrata "
            print  isinstance(self.id,(int,long))

            if self.manage and self.fin_theorique_value and  self.alertes_email and isinstance(self.id,(int,long)):
            
                echances=self.env['capcontrat.contrat.echeance'].search([("contrat_id","=",self.id)]).unlink()
                self.env['capcontrat.contrat.echeance'].create(
                {"mailto":self.manage.id,'send_date':date_dure_cul(self.fin_theorique_value,0,self.alertes_email),"contrat_id":self.id})
    
    @api.constrains("Signe_le","Prise_effet","contrat_duree_type","reconduction","date_de_prise","date_de_Jusqu_duree","int_duree","int_duree_type",
    'int_reconduction','int_reconduction_type')
    def modif_eheance(self):
        if not self.source :
            print " modiff de contrata "
            print  isinstance(self.id,(int,long))

            if self.manage and self.fin_theorique_value and  self.alertes_email and isinstance(self.id,(int,long)):
            
                echances=self.env['capcontrat.contrat.echeance'].search([("contrat_id","=",self.id)]).unlink()
                self.env['capcontrat.contrat.echeance'].create(
                {"mailto":self.manage.id,'send_date':date_dure_cul(self.fin_theorique_value,0,self.alertes_email),"contrat_id":self.id})
       
   
   
	
