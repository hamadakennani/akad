# -*- coding: utf-8 -*-
from odoo import api, models,fields
from num2words import num2words
from openerp.exceptions import ValidationError
from datetime import datetime, date, timedelta
class companyWizard(models.TransientModel):
    _name = "company.wizard"
    _description = "company Wizard"
    
    def _getinvoice_id(self):
        return self._context.get('active_ids',False)[0]  
    company=fields.Many2one('liste.societe')
    invoice_id=fields.Many2one('account.invoice',default=_getinvoice_id)
    devis=fields.Selection(string="Devis", selection=[('euro', 'EURO'),('ead', 'AED')])
    @api.multi
    def print_report(self):
        datas={}
        datas={
            'form':{
                'company':self.company.id,
                'invoice_id':self.invoice_id.id,
                'devis':self.id,
            }            
        }
        return self.env['report'].get_action(self,'facture.facture_report', data=datas)
class retour_bon_consommation_report(models.AbstractModel):
    _name = 'report.facture.facture_report'
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('facture.facture_report')
        invoice=self.env['account.invoice'].search([('id','=',data['form']['invoice_id'])])
        sale_id=0
        for linne in invoice.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        saleorder=self.env['sale.order'].search([('id','=',sale_id)])   
        company=self.env['liste.societe'].search([('id','=',data['form']['company'])])
        invoice=self.env['account.invoice'].search([('id','=',data['form']['invoice_id'])])
        deviss=self.env['company.wizard'].search([('id','=',data['form']['devis'])])
        stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
        # pour charger la liste d'euro et d'AED
        Deveuro=[]
        for a in invoice.facture:
            Deveuro.append({a.name:a.euro})       
        Devead=[]
        for a in invoice.facture:
            Devead.append({a.name:a.ead})   
        #pour passer les valeur au rapport  
        if stock.min_date :
            date1_obj = datetime.strptime(stock.min_date, "%Y-%m-%d %H:%M:%S")
            datebl=date1_obj.strftime("%d-%m-%Y") 
        date2_obj = datetime.strptime(invoice.date_due,"%Y-%m-%d")
        date_echeance=date1_obj.strftime("%d-%m-%Y ")
        cf=self.env['configuration.facture'].search([])
        bool1=False
        bool2=False
        for p in saleorder.order_line:
            for a in cf :
                for ab in a.listeproduit :
                    if(p.product_id.name==ab.name):
                        bool1=True 
                for ac in a.listedepot :
                    test1=ac.name
                    test2=test1.encode('utf-8')
                    test3=saleorder.stock.name
                    test4=test3.encode('utf-8')                  
                    if(test4==test2):
                        bool2=True
        listeadresse=[]
        for client in saleorder.partner_id :
            listeadresse.append({'adresse1':client.street,'adresse2':client.street2,'city':client.city,'zip':client.zip,'country':client.country_id.name}) 

        if(bool1==True and bool2==True and len(invoice.facture)):
            if company.name=="OCEAN AKAD INTERNATIONAL FZCO DUBAI SUCCURSALE DE GENEVE":
                banque=self.env['liste.banque'].search([('filt','=','gpbcb')])
            if company.name=="OCEAN AKAD INTERNATIONAL FZCO":
                banque=self.env['liste.banque'].search([('filt','=','fpbcb')])
            print Deveuro[6]['CIF']
            print Devead[6]['CIF']
            print 66666666666666666666666666666666666666
            
            datas={
                'form':{
                    'company_logo':company.header,
                    'company_fotter':company.fotter,
                    'company_name':company.name, 
                    'banquee':banque.banquee,
                    'adressebanquee':banque.adressebanquee,
                    'nomcomptee':banque.nomcomptee,
                    'adressecomptee':banque.adressecomptee,
                    'numcomptee':banque.numcomptee,
                    'swiftee':banque.swiftcodee,
                    'ibanee':banque.ibane,
                    'facture_id':invoice.id,
                    'client':invoice.partner_id.name,
                    'adresse':listeadresse,
                    'produit':invoice.invoice_line_ids.product_id.name,
                    'prix':invoice.invoice_line_ids.price_unit,
                    'quantite':invoice.invoice_line_ids.quantity,
                    'uom':invoice.invoice_line_ids.uom_id.name,
                    'date_echeance':date_echeance,
                    'facture':Deveuro,
                    'factures':Devead,
                    'depot':saleorder.stock.name,
                    'devis':deviss.devis,
                    'lettreeuro':num2words(  Deveuro[6]['CIF'],to='currency',lang='fr'),
                    'lettreead':num2words(  Devead[6]['CIF'],to='currency',lang='fr'),
                    'datebl':datebl
                }            
            }
        else :
            raise ValidationError("Attention : vous n'avez pas choisi la bonne facture dans le menu")   
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': datas,             
            }
        return report_obj.render('facture.facture_report',docargs)
