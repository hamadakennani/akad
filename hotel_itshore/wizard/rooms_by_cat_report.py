# -*- coding: utf-8 -*-
import time
from odoo import api, models, _
from odoo.exceptions import UserError
from datetime import datetime, timedelta

class ReportRoomsByCat(models.AbstractModel):
    _name = 'report.hotel_itshore.report_rooms_by_cat'
    def _get_free_rooms(self,categ_id,start,end):
        count=0
        dt='%Y-%m-%d %H:%M:%S'
        room_bool=False
        ids={}
        if end and start:
            reserv_checkin = datetime.strptime(start, dt)
            reserv_checkout = datetime.strptime(end, dt)
            rooms=self.env['hotel.room'].search([('categ_id','=',categ_id.id)])
            for room_id in rooms:
                if room_id.room_reservation_line_ids:
                    for reserv in room_id.room_reservation_line_ids.search([('status', 'in', ('confirm', 'done')),
                                                    ('room_id', '=', room_id.id)]):
                        check_in = datetime.strptime(reserv.check_in, dt)
                        check_out = datetime.strptime(reserv.check_out, dt)
                        if check_in <= reserv_checkin <= check_out:
                            room_bool = True
                        if check_in <= reserv_checkout <= check_out:
                            room_bool = True
                        if reserv_checkin <= check_in and \
                                reserv_checkout >= check_out:
                            room_bool = True
                        if room_bool:
                            if  room_id.id  not in ids :
                                count+=1
                                ids[room_id.id ]=room_id.id 
            return len(rooms)-count
    def get_free_rooms_by_all_cat(self,start,end):
        print 'dddddd'
        data = {}
        categorys = self.env['hotel.room.type'].search([])
        for c in categorys :
            data[c.id] = [c.name,self._get_free_rooms(c,start,end)]
        print data
        return data
    @api.model
    def render_html(self, docids, data=None):
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
        start = data['form']['date_debut'] 
        end = data['form']['date_fin']
        cats = self.get_free_rooms_by_all_cat(start,end)
        print '--------------------------------------'
        print cats
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'cats': cats,
        }
        return self.env['report'].render('hotel_itshore.report_rooms_by_cat', docargs)