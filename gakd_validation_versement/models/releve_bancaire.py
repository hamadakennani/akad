# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from dateutil.parser import parse
import datetime
import string
import re

class Testreleves(models.Model):
    _name = 'gakd.releve_bancaire' 
    _rec_name='reference'

    reference=fields.Char(string='Référence')
    ref = fields.Char(string="Réf")
    date_operation=fields.Date(string='Date de l\'opération')
    date_releve=fields.Char(string='Date releve')
    montant=fields.Char(string='Montant' ,default='0.0')
    state = fields.Selection(string='Etat',
     selection=[('annule', 'Annulé'),('brouillon', 'Brouillon'), ('valide', 'Validé')],
     default='brouillon')
    etat_creation = fields.Selection(string='Etat de creation', selection=[('non', 'Non'),('oui', 'Oui')], default='non')
    libelle=fields.Char(string='Libellé')
    id_versement= fields.Integer()
    # bank = fields.Many2one(comodel_name='account.journal', string='Journal des règlements',required=True)
    bank = fields.Selection([('BAB','BAB'),('BOA','BOA'),('ECOBANK','ECOBANK'),
            ('ATLANTIQUE','ATLANTIQUE'),('BGFI','BGFI'),('CASHCOLLECTION','CASH COLLECTION'),
            ('ECO','ECO'),('ORA','ORA'),('SGB','SGB')],string="banque",required=True)
    day_of_week = fields.Char(string="Jour de semaine")
    
    #--------- converter la date fr au ang
    def convertDateReleveBancaire(self,dtr,banke): 
        if dtr :
            b1=['BGFI','ORA','BOA','SGB']
            date=None
            print dtr
            if banke in b1:
                print banke                
                td=dtr.split('/')                
                if td[0].isdigit():
                    
                    if banke=='BOA':
                        td[2]='20'+td[2]
                    date=datetime.date(day=int(td[0]),month=int(td[1]),year=int(td[2]))
            elif banke=='CASHCOLLECTION':
                dtr=dtr.replace('/','-')
                dtr=dtr.replace(' ','-')
                td=dtr.split('-')
                date=datetime.date(day=int(td[2]),month=int(td[1]),year=int(td[0]))
            elif banke in ['ECO','BAB','ATLANTIQUE']:
                td=parse(dtr)
                date=datetime.datetime.strptime(str(td), '%Y-%m-%d %H:%M:%S')
                date=date.date()
            return date
        return None
        
    
    @api.model
    def create(self, vals):
        
        print '============================================='
        dateChange=self.convertDateReleveBancaire(vals.get('date_releve',False),vals.get('bank',False))        
        vals['date_operation']=dateChange
        if dateChange:
            # "Lundi Mardi Mercredi Jeudi Vendredi Samedi Dimanche"
            week_days = ("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche")
            
            vals['day_of_week'] = week_days[dateChange.weekday()]
        if  'ECO'!= vals.get('bank',False) and 'ECOBANK'!= vals.get('bank',False):
            if 'reference' in vals.keys() and vals.get('reference',False) :
                splitText = re.findall(r'\d+',str(vals.get('reference',"")))
                if len(splitText) > 0:
                    vals['ref'] = splitText[len(splitText) - 1]
        else :
            if 'libelle' in vals.keys() :
                if vals.get('libelle',"") :
                    c=u'°'
                    libel=vals.get('libelle',"").replace(c,'')
                    splitText = re.findall(r'\d+',libel.replace(' ',''))
                    if len(splitText) > 0:
                        vals['ref'] = splitText[0]
        return super(Testreleves,self).create(vals)