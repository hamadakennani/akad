{
    'name': 'contrat',
    'version': '1.0',
    'summary': '',
    'description': '',
    'category':'Contrat',
    'author': 'IT SHORE',
    'website': '',
    'license': '',
    'depends': ['web','base','auth_signup','odoo_web_login'],
    'data': [
        'views/contrat_view.xml',
        'views/categorie_view.xml',
        'views/version_view.xml',
        
        'views/res_view.xml',
        'views/liste_view.xml',
        'views/users_access_view.xml',
        'views/tempe_views.xml',
        'views/menu_view.xml',
        'security/res_groupe.xml',
        'security/ir.model.access.csv'
    ],
    'installable': True,
    'auto_install': False,
}
