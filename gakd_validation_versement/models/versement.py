# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from unidecode import unidecode
from dateutil.parser import parse
import pytz
import unicodedata
import string
import codecs
import sys
import base64
import re
import datetime
from datetime import datetime as dt


class journal(models.Model):
    _inherit = 'account.journal'
class Versement(models.Model) :
    _inherit = 'gakd.versement'
    day_of_week = fields.Char(string="Jour de semaine",compute='get_day_of_week')
    # @api.model
    # def create(self,vals) :
    #     week_days= ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche']
    #     versement = super(Versement,self).create(vals)
    #     day_of_week = week_days[versement.create_date.weekday()]
    #     versement.write({
    #         'day_of_week':day_of_week
    #     })
    #     return versement
    @api.multi
    def get_day_of_week(self) :
        print ("======================= hiiiii from get_day_of_week ========================")
        week_days= ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche']
        for record in self :
            day_of_week = week_days[record.create_date.weekday()]
            print (day_of_week)
            record.update({
                'day_of_week':day_of_week
            })
        return True
            
    
class VersementLine(models.Model):
    _inherit = 'gakd.versement_line'
    _rec_name='num_versement'
    
    state = fields.Selection(string='Etat',
     selection=[('annule', 'Annulé'),('brouillon', 'Brouillon'), ('valide', 'Validé')],
     default='brouillon')
    etat_creation = fields.Selection(string='Etat creation', selection=[('non', 'Non'),('oui', 'Oui')], default='non') 
    versement_line_id = fields.Many2one('gakd.valider.versement')
    
    
class VersementValider(models.Model):
    _name = 'gakd.valider.versement'
    _description = 'New Description'
    _order="create_date desc"

    versement_line=fields.One2many(comodel_name='gakd.versement_line', inverse_name='versement_line_id', string='Lignes de versement')
    etat_creation = fields.Selection(string='Etat creation', selection=[('non', 'Non'),('oui', 'Oui')], default='non') 


    datD=datetime.datetime.now()-datetime.timedelta(1)
    datF=datetime.datetime.now()
    date_debut= fields.Date(string='Date debut',   default=datD.strftime('%Y-%m-%d %H:%M:%S'), )
    date_fin=fields.Date(string='Date fin',   default=datF.strftime('%Y-%m-%d %H:%M:%S'), )      
    
    @api.multi
    def remplireListeVersement(self,dateD,dateF):
        liste_versements1=self.env['gakd.versement_line'].search([('create_date','>=',dateD),('create_date','<=',dateF)])
        return liste_versements1
    
    @api.onchange('date_debut')
    def _onchange_datedebut(self):
        if self.date_debut<=self.date_fin:
            datf1=self.date_debut    
            datFormatV1=parse(datf1)
            datf1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')

            datf2=self.date_fin    
            datFormatV2=parse(datf2)
            datf2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
            
            data = self.remplireListeVersement(datf1,datf2)
            # print data
            return {'value':{'versement_line':[(6,0,data.ids)]}}

    @api.onchange('date_fin')
    def _onchange_date_fin(self):
        if self.date_debut<=self.date_fin:
            datf1=self.date_debut    
            datFormatV1=parse(datf1)
            datf1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')

            datf2=self.date_fin    
            datFormatV2=parse(datf2)
            datf2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
            
            data = self.remplireListeVersement(datf1,datf2)
            return {'value':{'versement_line':[(6,0,data.ids)]}}

    @api.multi
    def searchInLibelle(self,codePvt,libl):
        bol=False
        if codePvt:
            l=0
            libl=self.replaceCaractere(libl)[0].upper()
            codePvt=self.replaceCaractere(codePvt)[0].upper()
            J0=' '
            if codePvt[0:2]== 'JO':
                J0=codePvt.replace("O",'0')
            if codePvt in libl or J0.upper() in libl:
                code = codePvt if codePvt in libl else J0.upper()
                j=libl.index(code)
                k=len(code)
                try:
                   if not libl[j+k].isdigit():
                    bol=True
                except:
                    bol=False
                
        return bol

    @api.multi
    def valideVersement(self,dateD,dateF):
        liste_versements=self.env['gakd.versement'].search([('state','!=','valide'),('create_date','>=',dateD),('create_date','<=',dateF),('vers_par','=','gerant')])       
        for v in liste_versements:
            listVersementLine=v.versement_line
            for vl in listVersementLine:
                if vl.state=='valide':
                    v.write({
                        'state': 'valide'
                    })
        
    @api.multi
    def returnBrouillon(self):
        print('test test test test test test test test test test test test test test ')
         
        liste_releve=self.env['gakd.releve_bancaire'].search([])
        liste_versementsLine=self.env['gakd.versement_line'].search([])
        print 'number of liste versement'+str(len(liste_versementsLine))  
        for vl in liste_versementsLine:
            print 111111111111111
            vl.write({
                'state': 'brouillon',
                'etat_creation': 'non',
            })
        for rb in liste_releve:
            print 22222222222222
            rb.write({
                'state': 'brouillon',
                'id_versement':0,
                'etat_creation': 'non',
            })
    
    #--- suprimer les espaces et les caractère spéciaux
    @api.one
    def replaceCaractere(self,libele):
        alphabet=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        'à','â','ç','è','é','ê','î','ô','ù','û',
        '0','1','2','3','4','5','6','7','8','9',]
        if not libele:
            libele='null'
        libele=libele.replace(" ","")
        for char in libele:
            if char.lower() not in alphabet:
                libele=libele.replace(char,"")
        return libele
                 
    #   creer les versement line valider
    @api.multi
    def create_versementValide(self,dateD,dateF):
        print('MMM  M MMMM MM11111111111111111111111111111111111111111111111111111111111111111111111111111111111')
        mn=0
        liste_versementV=self.env['gakd.versement_line'].search([('state','=','valide'),('etat_creation','=','non'),('create_date','>=',dateD),('create_date','<=',dateF)])
        for lvc1 in liste_versementV:
            mn+=1
            print(mn,'================================')
            listeIdRB=[]
            som=0.00
            #get journal
            if len(lvc1.journal_id)>0:
                journal=lvc1.journal_id[0]
            else:
                journal=lvc1.journal_id
            #------get pointe vente,versement
            if len(lvc1.versement_id)>0:
                versement=lvc1.versement_id[0]           
                if len(lvc1.versement_id[0].point_vente_id)>0:
                    pointeventes=lvc1.versement_id[0].point_vente_id[0]
                else:
                    pointeventes=lvc1.versement_id[0].point_vente_id
            else:
                versement=lvc1.versement_id
                if len(lvc1.versement_id.point_vente_id)>0:                    
                    pointeventes=lvc1.versement_id.point_vente_id[0]
                else:
                    pointeventes=lvc1.versement_id[0].point_vente_id
            if not pointeventes:
                libelle_PV='no'
            else:
                libelle_PV=pointeventes.libelle

            #date versement "format yyyy-mm-dd "
            datV=lvc1.create_date
            datFormatV=parse(datV)
            datV_date=datFormatV.strftime('%Y-%m-%d')

            liste_RBv1=self.env['gakd.releve_bancaire'].search([('state','=','valide'),('etat_creation','=','non'),('id_versement','=',lvc1.id),('date_operation','>=',dateD),('date_operation','<=',dateF)])
            for lrbc1 in liste_RBv1:
                listeIdRB.append(lrbc1.id)
                som+=float(lrbc1.montant.replace(" ",""))           
                
            if len(listeIdRB)>1:               
                lvc1.write({
                    'etat_creation': 'oui'
                })
                
                for lrbc2 in liste_RBv1:
                    mr1=lrbc2.montant.replace(" ","") 
                    lrbc2.write({
                        'etat_creation': 'oui'
                    })
                    result=self.env['gakd.liste'].sudo().create({
                        'compte_bancaire':journal.id,
                        'point_vente':pointeventes.id,
                        'categorie_produit':versement.type_versement,
                        'statu_validaion':lvc1.state,
                        })
                    resultvr=self.env['gakd.versementreleve'].sudo().create({
                        'compte_bancaire':journal.name,
                        'point_vente':str(libelle_PV),
                        'categorie_produit':versement.type_versement,
                        'date_versement_releve':datV_date,
                        'num_versement':'lvc1.num_versement',
                        'montant_versement':lvc1.montant,
                        'montant_releve':float(mr1),
                        'montant_manquant':0,
                        'num_releve':lrbc2.ref,
                        'lebelle_Releve':lrbc2.libelle,
                        'statu_validaion':lvc1.state,
                        'liste_versement':result.id,
                        })
                resultsom=self.env['gakd.versementreleve'].sudo().create({
                        'compte_bancaire':"somme",
                        'montant_versement':lvc1.montant,
                        'montant_releve':som,
                        'montant_manquant':0,
                        'lebelle_Releve':"some du produi "+versement.type_versement+" "+lvc1.num_versement,
                        'statu_validaion':lvc1.state,
                        })
            
            elif len(listeIdRB)==1 :
                lvc1.write({
                    'etat_creation': 'oui'
                })                
                rbc1=self.env['gakd.releve_bancaire'].search([('id','=',listeIdRB[0])])
                rbc1.write({
                    'etat_creation': 'oui'
                })
                mr2=rbc1.montant.replace(" ","")                
                result=self.env['gakd.liste'].sudo().create({
                        'compte_bancaire':journal.id,
                        'point_vente':pointeventes.id,
                        'daters':datV_date,
                        'categorie_produit':versement.type_versement,
                        'statu_validaion':lvc1.state,
                    })
                resultvr1=self.env['gakd.versementreleve'].sudo().create({
                    'compte_bancaire':journal.name,
                    'point_vente':str(libelle_PV),
                    'categorie_produit':versement.type_versement,
                    'date_versement_releve':datV_date,
                    'num_versement':lvc1.num_versement,
                    'montant_versement':lvc1.montant,
                    'montant_releve':float(mr2),
                    'montant_manquant':0,
                    'um_releve':rbc1.ref,
                    'lebelle_Releve':rbc1.libelle,
                    'statu_validaion':lvc1.state,
                    'liste_versement':result.id,
                    })

    #creer les versement non valider
    @api.multi
    def create_versementNonValide(self,dateD,dateF):
        #------ liste veresement
        print('222222222222222222222222222222222222222222222222222222222222222222')
        m=0
        listes_versement=self.env['gakd.versement_line'].search([('state','=','brouillon'),('etat_creation','=','non'),('create_date','>=',dateD),('create_date','<=',dateF)])
        print('nombre line versement line anuller=  ',len(listes_versement))
        for lvc2 in listes_versement:
            #get journal
            if len(lvc2.journal_id)>0:
                journal=lvc2.journal_id[0]
            else:
                journal=lvc2.journal_id
            #------get pointe vente,versement
            if len(lvc2.versement_id)>0:
                versement=lvc2.versement_id[0]
                if len(lvc2.versement_id[0].point_vente_id)>0:
                    pointeventes=lvc2.versement_id[0].point_vente_id[0]
                else:
                    pointeventes=lvc2.versement_id[0].point_vente_id
            else:
                versement=lvc2.versement_id
                if len(lvc2.versement_id.point_vente_id)>0:                    
                    pointeventes=lvc2.versement_id.point_vente_id[0]
                else:
                    pointeventes=lvc2.versement_id[0].point_vente_id

            #date versement "format yyyy-mm-dd "
            datV=lvc2.create_date
            datFormatV=parse(datV)
            datV_date=datFormatV.strftime('%Y-%m-%d')
            m+=1
            print(m,'===================================')
            print(datV_date)

            #----get categorier produit
            categorierProduitV=versement.type_versement
            

            #deviser le num des versement 'num1/num2/num3' to ['num1','num2','num3']
            c=u'°'
            numver=u''.join(lvc2.num_versement.replace(c,'')).encode('utf-16')
            listNumVersement = re.findall(r'\d+',str(numver))
            listeRef=[]
            for n in listNumVersement:
                if len(n)>3:
                    listeRef.append(n)
            
            #point venete
            if not pointeventes:
                libelle_PV='no'
            else:
                libelle_PV=pointeventes.libelle
            #=-------- test si le nume de versement se forme "num1/num2/num3/..."
            i=0
            listID1=[]
            somMantantRB_rf=0.00
            montantVr1=lvc2.montant

            nom_journal=journal.name
            if '034928180000' in journal.name.replace(" ", ""):
                nom_journal='ATLANTIQUE'
            #pour un num versement sous la formt "num1/num2/num3....."
            #test par reference 1
            listes_RB1=self.env['gakd.releve_bancaire'].search([('state','=','brouillon'),
                                                                ('ref','in',listNumVersement),
                                                                ('etat_creation','=','non'),
                                                                ('date_operation','>=',dateD),
                                                                ('date_operation','<=',dateF)])            
            for lrbc3 in listes_RB1:
                if lrbc3.bank in nom_journal.replace(" ", "").upper():
                    i+=1
                    listID1.append(lrbc3.id)
                    montantVr1-=50
                    mantantRB=lrbc3.montant.replace(' ','')
                    somMantantRB_rf+=float(mantantRB)
                    print('vvvvvvv111111111111111')

            #creation par reference
            listes_RB2=self.env['gakd.releve_bancaire'].search([('id','in',listID1)])
            ecart=float(montantVr1)-float(somMantantRB_rf)
            for lrbc2 in listes_RB2:
                print('BB11111111111111111111111111111111111111111111111')
                lrbc2.write({
                    'etat_creation': 'oui'
                    })
                lvc2.write({
                    'etat_creation': 'oui'
                    })
                mr1=lrbc2.montant.replace(" ","") 
                result=self.env['gakd.liste'].sudo().create({
                    'compte_bancaire':journal.id,
                    'point_vente':pointeventes.id,
                    'categorie_produit':versement.type_versement,
                    'statu_validaion':lvc2.state,
                    })    
                resultvr=self.env['gakd.versementreleve'].sudo().create({
                    'compte_bancaire':nom_journal,
                    'point_vente':libelle_PV,
                    'categorie_produit':versement.type_versement,
                    'date_versement_releve':datV_date,
                    'num_versement':lvc2.num_versement,
                    'montant_versement':lvc2.montant,
                    'montant_releve':float(mr1),
                    'montant_manquant':ecart,
                    'num_releve':lrbc2.ref,
                    'lebelle_Releve':lrbc2.libelle,
                    'statu_validaion':'M.Ref-Mt.Diff',
                    'liste_versement':result.id,
                    })
            if i>1:
                resultsom=self.env['gakd.versementreleve'].sudo().create({
                    'compte_bancaire':"somme",
                    'montant_versement':lvc2.montant,
                    'montant_releve':float(somMantantRB_rf),
                    'montant_manquant':ecart,
                    'lebelle_Releve':'M.Ref-Mt.Diff',
                    'statu_validaion':'M.Ref-Mt.Diff',
                    # 'liste_versement':result.id,
                })
            #fin test par reference

            #test par date,pointe vente ,categorie
            #-----test si categorier= produit blants
            listes_RB3=self.env['gakd.releve_bancaire'].search([
                ('state','=','brouillon'),('etat_creation','=','non'),
                ('date_operation','>=',dateD),('date_operation','<=',dateF)
                ])
            # Lubrifiants','Lubrifiants'),('Produits blancs','Produits blancs'),
            # ('Gaz et accessoire','Gaz et accessoire'),('Lavage','Lavage')]
            listcategorie=[]
            if categorierProduitV=='Produits blancs':
                category=versement.categorys                
                if category.upper()=='ESSENCE':
                    listcategorie=['ESSENCE','ESSENC']
                elif category.upper()=='GASOIL':
                    listcategorie=['GASOIL','GAZOIL']
            elif categorierProduitV=='Lubrifiants':
                listcategorie=['Lubrif','Lub']
            elif categorierProduitV=='Gaz et accessoire':
                listcategorie=['Gaz','Gaz']
            elif categorierProduitV=='Lavage':
                listcategorie=['Lavage','Lavage']

            listID2=[]
            somMontantRB_dt=0.00
            montantVr2=lvc2.montant                   
            for lrbc4 in listes_RB3:
                #------get et convertet  date de releve bancaire au yyyy-mm-dd
                if lrbc4.date_operation:
                    datR=lrbc4.date_operation
                    datR2=parse(str(datR))
                    datR_date=datR2.strftime('%Y-%m-%d')                                         
                    
                    #---- suprime les espaces dans montant de releve bancaire
                    if not lrbc4.montant:
                        mantantR=0.0
                    else:
                        mantantR=lrbc4.montant.replace(" ","")
                    #---delete special characters dans les libelle
                    leb=self.replaceCaractere(lrbc4.libelle)[0].upper()
                    banck2=lrbc4.bank.replace(" ", "")
                    
                    if datR_date==datV_date and (banck2.upper() in nom_journal.replace(" ", "").upper()):
                        bol=False
                        if 'Gaz' in listcategorie:
                            bo=True
                        if bol==False:
                            if ((listcategorie[0].upper() in leb) or (listcategorie[1].upper() in leb)):
                                if self.searchInLibelle(libelle_PV,lrbc4.libelle):
                                    listID2.append(lrbc4.id)
                                    montantVr2-=50
                                    somMontantRB_dt+=float(mantantR)
                        else:
                            gzl=['GASOIL','GAZOIL']
                            if ((gzl[0].upper() not in leb) or (gzl[1].upper() not in leb)):
                                if self.searchInLibelle(libelle_PV,lrbc4.libelle):
                                    listID2.append(lrbc4.id)
                                    montantVr2-=50
                                    somMontantRB_dt+=float(mantantR)
                                                           
            #creationm par date 1 
            l=0                              
            ecart=float(montantVr2)-float(somMontantRB_dt)
            listes_RB4=self.env['gakd.releve_bancaire'].search([('id','in',listID2)])
            for lrbc5 in listes_RB4:
                    l+=1
                    lvc2.write({
                        'etat_creation': 'oui'
                    })
                    lrbc5.write({
                        'etat_creation': 'oui'
                    })
                    if not lrbc5.montant:
                        mr2=0
                    else:
                        mr2=lrbc5.montant.replace(" ","")
                        
                    result=self.env['gakd.liste'].sudo().create({
                        'compte_bancaire':journal.id,
                        'point_vente':pointeventes.id,
                        'categorie_produit':versement.type_versement,
                        'statu_validaion':lvc2.state,
                        })    
                    resultvr=self.env['gakd.versementreleve'].sudo().create({
                        'compte_bancaire':nom_journal,
                        'point_vente':libelle_PV,
                        'categorie_produit':versement.type_versement+' '+str(listcategorie[0]),
                        'date_versement_releve':datV_date,
                        'num_versement':lvc2.num_versement,
                        'montant_versement':lvc2.montant,
                        'montant_releve':float(mr2),
                        'montant_manquant':ecart,
                        'num_releve':lrbc5.ref,
                        'lebelle_Releve':lrbc5.libelle,
                        'statu_validaion':'M.Date-M.PV Mt.Diff',
                        'liste_versement':result.id,
                        })
                    print('BB2222222222222222222222222222222222222222222222')
            if l>1:
                resultsom=self.env['gakd.versementreleve'].sudo().create({
                    'compte_bancaire':"somme",
                    'montant_versement':lvc2.montant,
                    'montant_releve':float(somMontantRB_dt),
                    'montant_manquant':ecart,
                    'lebelle_Releve':'M.Date-M.PV Mt.Diff',
                    'statu_validaion':'M.Date-M.PV Mt.Diff',
                    # 'liste_versement':result.id,
                    })
        
    # cree les versement et releve n'existe pas
    @api.multi
    def create_versementNotExixte(self,dateD,dateF):
        #create versement
        print('3333333333333333333333333333333333333333333333333333333333')
        n1=0
        n2=0
        # listeVersemets=self.env['gakd.versement_line'].search([('state','=','brouillon'),('etat_creation','=','non'),('create_date','>=',dateD),('create_date','<=',dateF)])
        # print('nombre line versement line anuller=  ',len(listeVersemets))
        # # create versement n existe pas
        # for lvc3 in listeVersemets:
        #     print()
        #     #get journal
        #     if len(lvc3.journal_id)>0:
        #         journal=lvc3.journal_id[0]
        #     else:
        #         journal=lvc3.journal_id
        #     #------get pointe vente,versement
        #     if len(lvc3.versement_id)>0:
        #         versement=lvc3.versement_id[0]           
        #         if len(lvc3.versement_id[0].point_vente_id)>0:
        #             pointeventes=lvc3.versement_id[0].point_vente_id[0]
        #         else:
        #             pointeventes=lvc3.versement_id[0].point_vente_id
        #     else:
        #         versement=lvc3.versement_id
        #         if len(lvc3.versement_id.point_vente_id)>0:                    
        #             pointeventes=lvc3.versement_id.point_vente_id[0]
        #         else:
        #             pointeventes=lvc3.versement_id[0].point_vente_id

        #     #date versement "format yyyy-mm-dd "
        #     datV=lvc3.create_date
        #     datFormatV=parse(datV)
        #     datV_date=datFormatV.strftime('%Y-%m-%d')
        #     #code pointe vente
        #     codepv=pointeventes.libelle          
            
        #     if not codepv :
        #         codepv='-'
        #     n1+=1
        #     print(n1,'--==--==---==--===---===---===--===-==--==')
        #     print datV_date
        #     # create veresement n existe pas            
        #     result=self.env['gakd.liste'].sudo().create({
        #         'compte_bancaire':journal.id,
        #         'point_vente':pointeventes.id,
        #         'categorie_produit':versement.type_versement,
        #         'statu_validaion':lvc3.state,
        #     })
        #     resultvr=self.env['gakd.versementreleve'].sudo().create({
        #         'compte_bancaire':journal.name,
        #         'point_vente':codepv,
        #         'categorie_produit':versement.type_versement,
        #         'date_versement_releve':lvc3.create_date,
        #         'num_versement':lvc3.num_versement,
        #         'montant_versement':lvc3.montant,
        #         'montant_releve':0,
        #         'montant_manquant':0,
        #         'num_releve':"-",
        #         'lebelle_Releve':"n'\existe pas dans releve banciare",
        #         'statu_validaion':'n existe pas',
        #         'liste_versement':result.id,
        #         })
        #     lvc3.write({
        #         'etat_creation': 'oui'
        #     })

        #create releve bancaire
        liste_PV=self.env['gakd.point.vente'].search([])
        liste_CB=self.env['account.journal'].search([('type','=','bank')])
        listeRB=self.env['gakd.releve_bancaire'].search([('state','=','brouillon'),('etat_creation','=','non'),('date_operation','>=',dateD),('date_operation','<=',dateF)])
        for lrbc7 in listeRB:
            codepv='-'
            idpv=0
            for pv in liste_PV:
                if self.searchInLibelle(pv.libelle,lrbc7.libelle):
                    codepv=pv.libelle
                    idpv=pv.id

            nom_journal='-'
            idcb=0
            nj='00000000'
            for cb in liste_CB:
                nj=cb.name
                if '034928180000' in nj.replace(" ", ""):
                    nj='ATLANTIQUE'
                if lrbc7.bank.upper() in nj.replace(" ", "").upper():
                    nom_journal=cb.name
                    idcb-cb.id

            #------get et convertet  date de releve bancaire au yyyy-mm-dd
            datR=lrbc7.date_operation
            datR2=parse(str(datR))
            datR_date=datR2.strftime('%Y-%m-%d')
            
            n2+=1
            print(n2,'--==--==---==--===---===---===--===-==--==')
            print (datR_date)
            if not lrbc7.montant:
                mr1=0
            else:
                mr1=lrbc7.montant.replace(" ","") 
            result=self.env['gakd.liste'].sudo().create({
                'compte_bancaire':idcb,
                'point_vente':idpv,
                'categorie_produit':"-",
                'statu_validaion':lrbc7.state,
            })    
            resultvr=self.env['gakd.versementreleve'].sudo().create({
                'compte_bancaire':nom_journal,
                'point_vente':codepv,
                'categorie_produit':"-",
                'date_versement_releve':datR_date,
                'num_versement':"-",
                'montant_versement':0,
                'montant_releve':float(mr1),
                'montant_manquant':0,
                'num_releve':lrbc7.ref,
                'lebelle_Releve':lrbc7.libelle,
                'statu_validaion':'n existe pas',
                'liste_versement':result.id,
            })
            lrbc7.write({
            'etat_creation': 'oui'
            })
    
    # #validation caissaier
    # def validationVersement_caissier(self):
        
    #valider les versemment line des week-end
    @api.one  
    def weekEndsValidator(self):
        date1=self.date_debut    
        datFormatV1=parse(date1)
        date1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')
        date2=self.date_fin    
        datFormatV2=parse(date2)
        date2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
        data = []
        liste_versements=self.env['gakd.versement_line'].search([('state','!=','valide'),('etat_creation','=','non'),('create_date','>=',date1),('create_date','<=',date2)])
        weekend_versements = [v for v in liste_versements if v.day_of_week in ['Samedi','Dimanche']]
        for versement in weekend_versements : 
            relve_date = None
            addedDays = 0
            if versement.day_of_week == 'Samedi' :
                addedDays = 2
            else :
                addedDays = 1
            relve_date = dt.strptime(versement.create_date, "%Y-%m-%d %H:%M:%S") + datetime.timedelta(days=addedDays)
            relve = self.env['gakd.releve_bancaire'].search([('date_operation','=',relve_date),('montant','>=',(versement.montant-100)),('montant','<=',versement.montant),('libelle','ilike','%'+versement.versement_id.point_vente_id.libelle+'%'),('state','!=','valide')])
            if len(relve) > 0 :  
                for r in relve : 
                    if str(versement.journal_id.name).replace(" ","").find(r.bank) >= 0  :
                        versement.write({
                            'state':'valide'
                        })
                        r.write({
                            'state':'valide'
                        })
                        print ("=====================================")
                        print (r)
                        print versement
                        print "====================================="
                        data.append((relve,versement))
                        break
        print len(data)
        
        return True
    
    #validerles versements
    @api.one
    def validerVersementline(self):
        # ------ liste veresement
        self.ensure_one()
        print('ok================================')
        k=0
        date1=self.date_debut    
        datFormatV1=parse(date1)
        date1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')
        
        date2=self.date_fin    
        datFormatV2=parse(date2)
        date2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')

        liste_RB3=self.env['gakd.releve_bancaire'].search([
                ('state','!=','valide'),
                ('etat_creation','=','non'),
                ('date_operation','>=',date1),
                ('date_operation','<=',date2)
                ])
        liste_versementsLine=self.env['gakd.versement_line'].search([
                            ('state','!=','valide'),
                            ('etat_creation','=','non'),
                            ('create_date','>=',date1),
                            ('create_date','<=',date2)
                            ])
        print('nombre des line versement',len(liste_versementsLine))        
        for lv1 in liste_versementsLine:
            #get journal
            if len(lv1.journal_id)>0:
                journal=lv1.journal_id[0]
            else:
                journal=lv1.journal_id
            #------get code ,pointe vente
            if len(lv1.versement_id)>0:
                versement=lv1.versement_id[0]           
                if len(lv1.versement_id[0].point_vente_id)>0:
                    codepv=lv1.versement_id[0].point_vente_id[0].libelle
                else:
                    codepv=lv1.versement_id[0].point_vente_id.libelle
            else:
                versement=lv1.versement_id
                if len(lv1.versement_id.point_vente_id)>0:
                    codepv=lv1.versement_id.point_vente_id[0].libelle
                else:
                    codepv=lv1.versement_id.point_vente_id.libelle

            #date versement "format yyyy-mm-dd "
            datV=lv1.create_date
            datFormatV=parse(datV)
            datV_date=datFormatV.strftime('%Y-%m-%d')
            k+=1
            print(k,'=======_-_--_-__--__--__--__--__--__--__',lv1.id)
            print(datV_date)            
            
            #deviser le num des versement 'num1/num2/num3' to ['num1','num2','num3']
            c=u'°'
            numver=u''.join(lv1.num_versement.replace(c,'')).encode('utf-8')
            listNumVersement = re.findall(r'\d+',str(numver))
            listeRef=[]
            for n in listNumVersement:
                if len(n)>3:
                    listeRef.append(n)
            
            #bank
            nom_journal=journal.name.replace(" ", "")
            if '034928180000' in nom_journal:
                nom_journal='ATLANTIQUE'
            #test par reference
            i=0
            sommontantRB=0.00
            listIDRef1=[]
            montantVrRef=lv1.montant
            liste_RB1=self.env['gakd.releve_bancaire'].search([
                ('state','!=','valide'),('ref','in',listeRef),
                ('etat_creation','=','non'),
                ('date_operation','>=',date1),('date_operation','<=',date2)
            ])
            for lrb1 in liste_RB1:
                if lrb1.bank.upper() in nom_journal.upper():                                           
                    i+=1
                    if not lrb1.montant:
                        montantRB=0.0
                    else:
                        montantRB=lrb1.montant.replace(" ","")
                    sommontantRB+=float(montantRB)
                    listIDRef1.append(lrb1.id)
                    montantVrRef-=50
                    print('11111111111111111111111111111111111111111111111')
            
            #----resulta de test par reference
            if float(montantVrRef)==float(sommontantRB):
                for lrb2 in liste_RB1:  
                    if lrb2.id in listIDRef1:     
                        lv1.write({
                            'state': 'valide'
                        })                    
                        lrb2.write({
                            'state': 'valide',
                            'id_versement': lv1.id,
                        })
                            
                        if versement.moyen_de_paiement == 'versement':
                            account_id_d = 19857
                            account_id_c = 20607            
                            data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant-50}),
                            (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                            av= self.env['account.move'].create({
                                'journal_id':journal.id,
                                'date':datetime.datetime.now().date(),
                                'ref': 'sssss-'+str(datetime.datetime.now()),
                                'line_ids':data
                            })  
                            av.post()          
                        if versement.moyen_de_paiement == 'cheque':
                            account_id_d = 19857
                            account_id_c = 19975           
                            data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant}),
                            (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                            av= self.env['account.move'].create({
                            'journal_id':journal.id,
                            'date':datetime.datetime.now().date(),
                            'ref': 'sssss-'+str(datetime.datetime.now()),                
                            'line_ids':data
                            })  
                            av.post()          
                        if versement.moyen_de_paiement == 'especes':
                            account_id_d = 19857
                            account_id_c = 20607           
                            data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant-50}),
                            (0,0 ,{'account_id':20143,'partner_id':'','name':'Frais de timbre','debit':50}),
                            (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                            av= self.env['account.move'].create({
                            'journal_id':journal.id,
                            'date':datetime.datetime.now().date(),
                            'ref': 'sssss-'+str(datetime.datetime.now()),                
                            'line_ids':data
                            })  
                            av.post()
                        print('111111111111111111112222222222222222222222222222222222222222')

            #test 2               
            #-----test si categorier= produit blants            
            #----get categorier produit
            categorierProduitV=versement.type_versement
            listcategorie=[]
            
            if categorierProduitV=='Produits blancs':
                category=versement.categorys                
                if category.upper()=='ESSENCE':
                    listcategorie=['ESSENCE','ESSENC']
                elif category.upper()=='GASOIL':
                    listcategorie=['GASOIL','GAZOIL']
            elif categorierProduitV=='Lubrifiants':
                listcategorie=['Lubrif','Lub']
            elif categorierProduitV=='Gaz et accessoire':
                listcategorie=['Gaz','Gaz']
            elif categorierProduitV=='Lavage':
                listcategorie=['Lavage','Lavage']
            #boucle de test par date 
            listID2=[]
            somMontantRB_dt=0.00
            montantVr2=lv1.montant                   
            for lrb5 in liste_RB3:
                #------get et convertet  date de releve bancaire au yyyy-mm-dd
                if lrb5.date_operation:
                    datR=lrb5.date_operation
                    datR2=parse(str(datR))
                    datR_date=datR2.strftime('%Y-%m-%d')                                         
                    
                    #---- suprime les espaces dans montant de releve bancaire
                    if not lrb5.montant:
                        mantantR=0.0
                    else:
                        mantantR=lrb5.montant.replace(" ","")
                    #---delete special characters dans les libelle
                    leb=self.replaceCaractere(lrb5.libelle)[0].upper()
                    banck2=lrb5.bank.replace(" ", "")
                    
                    if datR_date==datV_date and (banck2.upper() in nom_journal.replace(" ", "").upper()):
                        bol=False
                        if 'Gaz' in listcategorie:
                            bo=True
                        if bol==False:
                            if ((listcategorie[0].upper() in leb) or (listcategorie[1].upper() in leb)):
                                if self.searchInLibelle(codepv,lrb5.libelle):
                                    listID2.append(lrb5.id)
                                    montantVr2-=50
                                    somMontantRB_dt+=float(mantantR)
                        else:
                            gzl=['GASOIL','GAZOIL']
                            if ((gzl[0].upper() not in leb) or (gzl[1].upper() not in leb)):
                                if self.searchInLibelle(codepv,lrb5.libelle):
                                    listID2.append(lrb5.id)
                                    montantVr2-=50
                                    somMontantRB_dt+=float(mantantR)
            
            if montantVr2==somMontantRB_dt:
                lv1.write({
                    'state': 'valide'
                    })
                lrb5.write({
                    'state': 'valide',
                    'id_versement': lv1.id,
                })
                if versement.moyen_de_paiement == 'versement':
                    account_id_d = 19857
                    account_id_c = 20607            
                    data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant-50}),
                    (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                    av= self.env['account.move'].create({
                        'journal_id':journal.id,
                        'date':datetime.datetime.now().date(),
                        'ref': 'sssss-'+str(datetime.datetime.now()),
                        'line_ids':data
                    })  
                    av.post()          
                if versement.moyen_de_paiement == 'cheque':
                    account_id_d = 19857
                    account_id_c = 19975           
                    data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant}),
                    (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                    av= self.env['account.move'].create({
                    'journal_id':journal.id,
                    'date':datetime.datetime.now().date(),
                    'ref': 'sssss-'+str(datetime.datetime.now()),                
                    'line_ids':data
                    })  
                    av.post()          
                if versement.moyen_de_paiement == 'especes':
                    account_id_d = 19857
                    account_id_c = 20607           
                    data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant-50}),
                    (0,0 ,{'account_id':20143,'partner_id':'','name':'Frais de timbre','debit':50}),
                    (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                    av= self.env['account.move'].create({
                    'journal_id':journal.id,
                    'date':datetime.datetime.now().date(),
                    'ref': 'sssss-'+str(datetime.datetime.now()),                
                    'line_ids':data
                    })  
                    av.post()
                print('333333333333333333333333333333333333333333')       
            
        self.weekEndsValidator()
        self.valideVersement(date1,date2)
        self.create_versementValide(date1,date2)
        self.create_versementNonValide(date1,date2)
        self.create_versementNotExixte(date1,date2)
        data = self.remplireListeVersement(date1,date2)
        self.versement_line = [(6,0,data.ids)]