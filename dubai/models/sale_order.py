# -- coding: utf-8 --
from odoo import api, fields, models
from datetime import date
from odoo import fields, models,api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from num2words import num2words
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools import float_utils

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'


    @api.onchange('product_uom_qty', 'product_uom', 'route_id')
    def _onchange_product_id_check_availability(self):
        if not self.product_id or not self.product_uom_qty or not self.product_uom:
            self.product_packaging = False
            return {}
        if self.product_id.type == 'product':
            precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
            product_qty = self.product_uom._compute_quantity(self.product_uom_qty, self.product_id.uom_id)
            if float_compare(self.product_id.virtual_available, product_qty, precision_digits=precision) == -1:
                is_available = self._check_routing()
                # if not is_available:
                #     warning_mess = {
                #         'title': _('Not enough inventory!'),
                #         'message' : _('You plan to sell %s %s but you only have %s %s available!\nThe stock on hand is %s %s.') % \
                #             (self.product_uom_qty, self.product_uom.name, self.product_id.virtual_available, self.product_id.uom_id.name, self.product_id.qty_available, self.product_id.uom_id.name)
                #     }
                #     return {'warning': warning_mess}
        return {}

    @api.onchange('product_uom', 'product_uom_qty')
    def product_uom_change(self):
        super(SaleOrderLine,self)
        if not self.order_id.contrat.id:
            if not self.product_uom or not self.product_id:
                self.price_unit = 0.0
                return
            if self.order_id.pricelist_id and self.order_id.partner_id:
                product = self.product_id.with_context(
                    lang=self.order_id.partner_id.lang,
                    partner=self.order_id.partner_id.id,
                    quantity=self.product_uom_qty,
                    date=self.order_id.date_order,
                    pricelist=self.order_id.pricelist_id.id,
                    uom=self.product_uom.id,
                    fiscal_position=self.env.context.get('fiscal_position')
                )
                self.price_unit = self.env['account.tax']._fix_tax_included_price_company(self._get_display_price(product), product.taxes_id, self.tax_id, self.company_id)
        else :
            if self.order_id.contrat.id:
                for line in  self.order_id.contrat.line_ids:
                    if self.product_id.id==line.product_id.id:
                        # a.sudo().write({
                        #     'price_unit':line.prix.prix_fin,
                        # })
                        self.price_unit=line.prix.prix_fin


class SaleOrder(models.Model):
    _inherit = 'sale.order'
   
    contrat = fields.Many2one(comodel_name='dubai.contrat', string='Contrat',domain=[('type_de_contrat','=','vente')])#,('partner','=','partner_id')
    livraison = fields.Char(string="Livraison")

    stock = fields.Many2one(comodel_name='stock.location', string='Emplacement',required=True)

    @api.multi
    @api.onchange('partner_id')
    def onchange_partner_id(self):
        """
        Update the following fields when the partner is changed:
        - Pricelist
        - Payment term
        - Invoice address
        - Delivery address
        """
        if not self.partner_id:
            self.update({
                'partner_invoice_id': False,
                'partner_shipping_id': False,
                'payment_term_id': False,
                'fiscal_position_id': False,
            })
            return

        addr = self.partner_id.address_get(['delivery', 'invoice'])
        values = {
            'pricelist_id': self.partner_id.property_product_pricelist and self.partner_id.property_product_pricelist.id or False,
            'payment_term_id': self.partner_id.property_payment_term_id and self.partner_id.property_payment_term_id.id or False,
            'partner_invoice_id': addr['invoice'],
            'partner_shipping_id': addr['delivery'],
        }
        if self.env.user.company_id.sale_note:
            values['note'] = self.with_context(lang=self.partner_id.lang).env.user.company_id.sale_note

        if self.partner_id.user_id:
            values['user_id'] = self.partner_id.user_id.id
        if self.partner_id.team_id:
            values['team_id'] = self.partner_id.team_id.id
        self.update(values)
        if self.contrat.id :
            pl=self.env["product.pricelist"].search([('currency_id','=',self.contrat.currency.id),('for_contrat','=',True)])
            self.pricelist_id= pl[0].id

    @api.onchange('contrat')
    def _onchange_contrat(self):
        value=''
        data=[]
        if self.contrat.id :
            for line in  self.contrat.line_ids:
                data.append((0,0,{'product_id':line.product_id.id,'product_uom_qty': line.qte,'product_qty': line.qte,'price_unit':line.prix.prix_fin,'currency_id':self.contrat.currency.id,
                'product_uom':line.unit.id,'customer_lead':1,'name':str(line.product_id.name),'date_planned':str(datetime.now().date())}))
            # self.order_line=''
            # self.order_line=[data]
            pl=self.env["product.pricelist"].search([('currency_id','=',self.contrat.currency.id),('for_contrat','=',True)])
            self.pricelist_id= pl[0].id
            self.partner_id=self.contrat.partner
            return {'value':{'order_line':data}}

    
    @api.multi
    def action_confirm(self):   
        for line_contrat in  self.contrat.line_ids:
            Find=False
            for line_order in self.order_line:
                if line_contrat.product_id.id==line_order.product_id.id:
                    Find=True
                    
                    if line_contrat.qte<line_order.product_uom_qty:
                        raise ValidationError(u'Qte de produit '+line_order.product_id.name+u' est supérieure a la qté disponible dans le contrat')
                    else :
                        line_contrat.qte-=line_order.product_uom_qty
            if not Find:
                raise ValidationError(u"Le produit "+line_order.product_id.name+u" n\'existe pas dans le contrat ")
        
        super(SaleOrder,self).action_confirm()
        if self.stock:
            source =self.stock.id 
            for tr in self.picking_ids:
                tr.write({
                    'location_id':self.stock.id
                })
                moves = tr.mapped('move_lines').filtered(lambda move: move.state not in ('cancel', 'done'))
                moves.action_assign()
        for a in self.order_line: 
            for il in self.picking_ids:
                for j in il.pack_operation_product_ids:
                    if a.product_id.id==j.product_id.id:
                        j.sudo().write({
                            # 'product_uom_qty': a.,
                            'product_uom_id': a.product_uom.id,
                            # 'um1':tn,
                            # 'qty1': tnq,
                            # 'um2':m3a ,
                            # 'qty2':m3aq ,
                        })
        # raise ValidationError(u"Le produit  n\'existe pas dans le contrat ")
        return True
    @api.model
    def create(self, values):
        # Add code here
        return super(SaleOrder, self).create(values)
    def get_data(self):
        data=[]

        for a in self.order_line:
            data.append({'product_id':a.product_id.name, 'tva':a.price_tax,'pu':a.price_unit, 'qte':a.product_uom_qty, 'subtotal':a.price_subtotal})
            # data.append({'product_id':a.product_id.name, 'tva':a.price_tax,'pu':a.price_unit, 'qte':a.product_uom_qty, 'subtotal':a.price_subtotal})
            # data.append({'product_id':a.product_id.name, 'tva':a.price_tax,'pu':a.price_unit, 'qte':a.product_uom_qty, 'subtotal':a.price_subtotal})
        return data
    def get_num(self):
        return num2words(self.amount_total, lang='fr')
class product_price(models.Model):
    _inherit = 'product.pricelist'

    for_contrat=fields.Boolean(string='For Contrat',default= False)

class InventoryLine(models.Model):
    _inherit = "stock.inventory" 

    @api.multi
    def action_start(self):
        for inventory in self:
            
            vals = {'state': 'confirm' }
            if (inventory.filter != 'partial') and not inventory.line_ids:
                vals.update({'line_ids': [(0, 0, line_values) for line_values in inventory._get_inventory_lines_values()]})
            inventory.write(vals)
        return True
    prepare_inventory = action_start

class InventoryLine(models.Model):
    _inherit = "stock.inventory.line" 


    def _generate_moves(self):
        moves = self.env['stock.move']
        Quant = self.env['stock.quant']
        for line in self:
            line._fixup_negative_quants()

            if float_utils.float_compare(line.theoretical_qty, line.product_qty, precision_rounding=line.product_id.uom_id.rounding) == 0:
                continue
            diff = line.theoretical_qty - line.product_qty
            if diff < 0:  # found more than expected
                vals = line._get_move_values(abs(diff), line.product_id.property_stock_inventory.id, line.location_id.id)
            else:
                vals = line._get_move_values(abs(diff), line.location_id.id, line.product_id.property_stock_inventory.id)
            move = moves.create(vals)
            
            if diff > 0:
                domain = [('qty', '>', 0.0), ('package_id', '=', line.package_id.id), ('lot_id', '=', line.prod_lot_id.id), ('location_id', '=', line.location_id.id)]
                preferred_domain_list = [[('reservation_id', '=', False)], [('reservation_id.inventory_id', '!=', line.inventory_id.id)]]
                quants = Quant.quants_get_preferred_domain(move.product_qty, move, domain=domain, preferred_domain_list=preferred_domain_list)
                Quant.quants_reserve(quants, move)
                if self.inventory_id.date:
                    move.sudo().write({
                        'date':self.inventory_id.date
                    })
                    if quants:
                        for quant in move.quant_ids:
                            quant.sudo.write({
                                'in_date':self.inventory_id.date
                            })
            elif line.package_id:
                move.action_done()
                move.quant_ids.write({'package_id': line.package_id.id})
                quants = Quant.search([('qty', '<', 0.0), ('product_id', '=', move.product_id.id),
                                       ('location_id', '=', move.location_dest_id.id), ('package_id', '!=', False)], limit=1)
                if quants:
                    for quant in move.quant_ids:
                        if quant.location_id.id == move.location_dest_id.id:  #To avoid we take a quant that was reconcile already
                            quant._quant_reconcile_negative(move)
                if self.inventory_id.date:
                    move.sudo().write({
                        'date':self.inventory_id.date
                    })
                    if quants:
                        for quant in move.quant_ids:
                            quant.sudo.write({
                                'in_date':self.inventory_id.date
                            })

        # raise ValidationError('Qte de produit ')
        return moves
