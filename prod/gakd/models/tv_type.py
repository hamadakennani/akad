from odoo import api, fields, models


class TvType(models.Model):
    _name = 'gakd.tv_type'
    _rec_name="rec"

    type_name = fields.Char(string='Nom')
    montant = fields.Integer(string='Montant')
    rec = fields.Char(string='rec')
    mysql_id = fields.Integer(string='mysql id')

    @api.model
    def create(self, vals):
        vals['rec'] = vals['type_name'] + '-'+str(vals['montant']) + 'CFA'
        return super(TvType, self).create(vals)
    
