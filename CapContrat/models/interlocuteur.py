# -*- coding: utf-8 -*-
from odoo import fields, api, models
from odoo.exceptions import  ValidationError
import datetime



class   Interlocuteur(models.Model):
    _name = "res.partner.interlocuteur"
    _rec_name = 'name'
    
    def getContryByDefault(self): 
	    return self.env['res.country'].search([['code', '=', 'BJ']]).id





  

  
    name = fields.Char(size=18,string="Nom",required=True)  
    prenom = fields.Char(size=18,string=u"Prénom",required=True)  
    Fonction=fields.Text(string="Fonction ")
    
    phone = fields.Char(string="Téléphone/Mobile")
    email = fields.Char(string="Email") 
   
    
    
    contrat_id=fields.One2many('capcontrat.contrat','interlocuteurs_id',string='Contrat')
    parent_id=fields.Many2one('res.partner',string='Société')
    
   
    # @api.constrains()
    # def _check_participant(self):
    
      
