from odoo import api, fields, models


class serviceline(models.Model):
    _inherit = 'hotel.service.line'
    to_hide = fields.Boolean(string='',defaut=True)
    prix = fields.Float(string="Montant de l'avance")
    name = fields.Text(string='')
    
    

    @api.depends('tax_id')
    def _get_tax(self):
        return self.tax_id.amount

    @api.depends('tax_id','prix','price_unit')
    def get_sold(self):
        return  format(self.price_unit-self.prix, '.3f')

    @api.onchange('product_id')
    def room_price(self):
        self.price_unit = self.product_id.lst_price
        #if len(self.product_id.taxes_id)>0:
        #   self.tax_id=self.product_id.taxes_id[0]

    
