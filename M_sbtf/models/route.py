# -- coding: utf-8 --
from odoo import api, fields, models

from openerp.exceptions import ValidationError


class voyage(models.Model):
    _inherit = 'tms.route'

    
    autre_frais = fields.One2many('autre.frais','voyage',"Autre frais")
    frais=fields.Selection([('Frais 1','Frais 1'),('Frais 2','Frais 2'),('Frais 3','Frais 3'),('Frais 4','Frais 4'),('Frais 5','Frais 5')],string="Frais de route")
    