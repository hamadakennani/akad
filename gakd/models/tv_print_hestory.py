from odoo import api, fields, models


class TvPrintHestory(models.Model):
    _name = 'gakd.tv_print_hestory'
    _description = 'New Description'

    tv_ids = fields.One2many('gakd.ticket_valeur','print_hestory_id',string='T.V')
    print_id = fields.Many2one('gakd.tv_print',string='print')
    nb_tv = fields.Integer(string='Nombre des tickets')
    tv_type = fields.Many2one('gakd.tv_type',string="Type")
    montant = fields.Float(string="Montant")

    def print_tv(self):
        return self.env['report'].get_action(self,'gakd.repport_tv_template')