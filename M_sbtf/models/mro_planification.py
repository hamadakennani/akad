# -- coding: utf-8 --
from openerp.exceptions import ValidationError
import time
from odoo import api, fields, models, _
from odoo import netsvc
import odoo.addons.decimal_precision as dp


class mro_planification(models.Model):

    _name = 'mro.planification'

    # def _get_available_parts(self):
    #     for order in self:
    #         line_ids = []
    #         available_line_ids = []
    #         done_line_ids = []
    #         if order.procurement_group_id:
    #             for procurement in order.procurement_group_id.procurement_ids:
    #                 line_ids += [move.id for move in procurement.move_ids if move.location_dest_id.id == order.garage_id.location_id.id]
    #                 available_line_ids += [move.id for move in procurement.move_ids if move.location_dest_id.id == order.garage_id.location_id.id and move.state == 'assigned']
    #                 done_line_ids += [move.id for move in procurement.move_ids if move.location_dest_id.id == order.garage_id.location_id.id and move.state == 'done']
    #         order.parts_ready_lines = line_ids
    #         order.parts_move_lines = available_line_ids
    #         order.parts_moved_lines = done_line_ids

    name = fields.Char('Reference', size=64)
    state = fields.Selection([(u'Planifiée','Planifiée'),('Pièces en attente','Pièces en attente'),('Prêt pour la maintenance','Prêt pour la maintenance'),('Fait','Fait')], 'Status', readonly=True, default=u'Planifiée')
    description = fields.Char('Description',required=True)
    date_debut_planifiee = fields.Datetime('Date début planifiée', required=True, default=time.strftime('%Y-%m-%d %H:%M:%S'))
    date_fin_planifiee = fields.Datetime('Date fin planifiée', required=True, default=time.strftime('%Y-%m-%d %H:%M:%S'))
    date_debut_reelle = fields.Datetime('Date début réelle')
    date_fin_reelle = fields.Datetime('Date fin réelle')
    parts_lines = fields.One2many('mro.diagnostic.parts.line', 'planification_id')
    # parts_ready_lines = fields.One2many('stock.move')
    # parts_move_lines = fields.One2many('stock.move')
    # parts_moved_lines = fields.One2many('stock.move')
    labor_description = fields.Text('Labor Description',translate=True)
    company_id = fields.Many2one('res.company','Company',required=True, readonly=True,default=lambda self: self.env['res.company']._company_default_get('mro.order'))
    vehicle_id=fields.Many2one('fleet.vehicle',"Camion",required=True)
    garage_id=fields.Many2one('mro.garage',"Garage",required=True)
    tache_id=fields.Many2many("mro.tache",string="Les taches")
    produit=fields.Many2many("product.product")
    piece  = fields.One2many('mro.piece','planification_id')
    travail_fait = fields.Text()
    

    def demmarer_maintenance(self):  
        purchase = self.env['purchase.order']
        test=0
        for line in self.parts_lines :
            produit=self.env['product.product'].search([('id', '=', line.parts_id.id)])
            qty_stock=self.env['stock.quant'].search([('product_id', '=', line.parts_id.id),('location_id', '=', self.garage_id.location_id.id)])
            if qty_stock.qty < line.parts_qty:
                test=1
                qty=line.parts_qty-qty_stock.qty
                self.state='Pièces en attente'
                four=''
                for pro in produit.seller_ids:
                    four=pro.name.id
                    break
                vals = {
                    'partner_id': four,
                    'order_line': [(0, 0, {
                        'name': produit.name,
                        'product_id': line.parts_id.id,
                        'product_qty': qty,
                        'product_uom': produit.uom_id.id,
                        'price_unit': produit.lst_price,
                        'state':'draft',
                        'date_planned': fields.Datetime.now()})]
                    }
                pur_id = purchase.create(vals)
                raise ValidationError("Attention: des pièces nécessaires pour cette opération sont toujours manquantes !")

        if test==0:
            self.state='Prêt pour la maintenance'
    def verifier(self):  
        purchase = self.env['purchase.order']
        test=0
        for line in self.parts_lines :
            produit=self.env['product.product'].search([('id', '=', line.parts_id.id)])
            qty_stock=self.env['stock.quant'].search([('product_id', '=', line.parts_id.id),('location_id', '=', self.garage_id.location_id.id)])
            if qty_stock.qty < line.parts_qty:
                test=1
                qty=line.parts_qty-qty_stock.qty
                self.state='Pièces en attente'
                four=''
                for pro in produit.seller_ids:
                    four=pro.name.id
                    break
                vals = {
                    'partner_id': four,
                    'order_line': [(0, 0, {
                        'name': produit.name,
                        'product_id': line.parts_id.id,
                        'product_qty': qty,
                        'product_uom': produit.uom_id.id,
                        'price_unit': produit.lst_price,
                        'state':'draft',
                        'date_planned': fields.Datetime.now()})]
                    }
                pur_id = purchase.create(vals)
                raise ValidationError("Attention: des pièces nécessaires pour cette opération sont toujours manquantes !")
    @api.model
    def create(self, vals):
        if vals.get('name','/')=='/':
            vals['name'] = self.env['ir.sequence'].next_by_code('mro.planification') or '/'
        return super(mro_planification, self).create(vals)

    def fait(self):
        if self.date_debut_reelle and self.date_fin_reelle : 
            self.state='Fait'
        else :
            raise ValidationError("Attention: Veuillez renseigner les dates de début et de fin réelles de l'intervention")


    

   
