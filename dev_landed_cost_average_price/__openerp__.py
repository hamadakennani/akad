# -*- coding: utf-8 -*-
##############################################################################
#
#    DevIntelle Solution(Odoo Expert)
#    Copyright (C) 2015 Devintelle Soluation (<http://devintellecs.com/>)
#
##############################################################################
{
    'name': 'Odoo Landed cost on Average Costing',
    'version': '1.0',
    'sequence': 1,
    'category': 'Generic Modules/Warehouse',
    'summary': 'odoo Apps will calculate average costing on landed Cost',
    'description': """
        odoo module will calculate average cost on landed costing
        
        
        odoo landed cost 
        odoo Average costing on landed cost 
        odoo landed cost with Average costing  
    """,
    'depends': ['stock_landed_costs'],
    'data': [
    ],
    'demo': [],
    'test': [],
    'css': [],
    'qweb': [],
    'js': [],
    'images': ['images/main_screenshot.png'],
    'installable': True,
    'application': True,
    'auto_install': False,
    
    # author and support Details =============#
    'author': 'DevIntelle Consulting Service Pvt.Ltd',
    'website': 'http://www.devintellecs.com',    
    'maintainer': 'DevIntelle Consulting Service Pvt.Ltd', 
    'support': 'devintelle@gmail.com',
    'price':69.0,
    'currency':'EUR',
    'live_test_url':'https://youtu.be/duk4b_2M2rk',
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
