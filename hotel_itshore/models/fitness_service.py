# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from dateutil.relativedelta import *
from random import randint

class Fitness_abonnement(models.Model):
    _name = 'hotel.fitness.abonnement'

    def _getQrCode(self,string):
	import hashlib
	md5 = hashlib.md5()
	qrcode = ""
	result = True
	serie_proposition = 0
        while result==True:
		md5.update(string+str(serie_proposition))
		qrcode = md5.hexdigest()
		serie_proposition += 1 
		result = self.search([['qrcode', '=', qrcode ]]).id
	return qrcode

    @api.one
    @api.depends('product_id','client','duree','abonnement_lines')
    def _get_days(self):
        print 'hhhhh'
        if not self.create_date :
            self.date_debut=datetime.now()
        else :
            self.date_debut=self.create_date
        # date_from=  datetime.now()
        date_to=datetime.now()
        print 'fuuuuuunction'
        print datetime.strptime(str(datetime.now()), '%Y-%m-%d %H:%M:%S.%f')
        if self.duree=='mois':
            date_to=   datetime.strptime(str(self.date_debut), '%Y-%m-%d %H:%M:%S')  + relativedelta(months=+1) 
        if self.duree=='trimestre':
            date_to=  datetime.strptime(str(self.date_debut), '%Y-%m-%d %H:%M:%S') + relativedelta(months=+3) 
        if self.duree=='semestre':
            date_to= datetime.strptime(str(self.date_debut), '%Y-%m-%d %H:%M:%S') + relativedelta(months=+6) 
        if self.duree=='annee':
            date_to= datetime.strptime(str(self.date_debut), '%Y-%m-%d %H:%M:%S') + relativedelta(years=+1) 
        # final=date_to.date()- datetime.strptime(str(datetime.now()), '%Y-%m-%d %H:%M:%S.%f').date()

        nb_days=0
        ddate_now=datetime.strptime(str(datetime.now().date()), '%Y-%m-%d')

        for a in self.abonnement_lines :
            nb_days+=a.nb_jours
        
        print nb_days
        self.nb_jours=nb_days
        self.date_fin=date_to
        print self.date_fin
        print self.date_debut
        return 0

    @api.one
    @api.depends('product_id','client','duree','discount','abonnement_lines','type_discount','discount_fix')
    def _get_montant(self):
        coe=0
        if self.duree=='mois':
            coe=1
        if self.duree=='trimestre':
            coe=3
        if self.duree=='semestre':
            coe=6
        if self.duree=='annee':
            coe=12
        price=self.product_id.lst_price 
        tax=self.product_id.taxes_id
        print tax 
        print 't(aaaaax'
        cout=price*coe
        tva=0
        for ta in tax:
            if ta.amount_type=='fixed':
                tva=ta.amount
                if ta.price_include:
                    cout-=ta.amount               
            elif ta.amount_type=='percent':
                print ' in percent'
                tva=(cout*ta.amount)*0.01
                print ' oprt done'
                print cout
                if ta.price_include:
                    cout-=tva

        self.prix_ht=cout 
        self.taxes =tva
        self.montant=tva+cout
        if self.type_discount=='fixe':
            self.montant=self.montant-self.discount_fix
        if self.type_discount=='pourcentage':
            self.montant=self.montant-self.montant*self.discount*0.01
        return self.montant

    product_id = fields.Many2one(comodel_name='product.product', string='Article',required=True)
    etat = fields.Selection(string='État',default='paye',selection=[('paye', 'Payée'), ('non paye', 'Non payé'),('blocked', 'Bloquée')])
    client = fields.Many2one(comodel_name='res.partner', string='Client',required=True)    
    duree = fields.Selection(string='Durée', selection=[('mois', 'Mois'), ('trimestre', 'Trimestre'),('semestre', 'Semestre'),('annee', 'Année')],required=True)
    nb_jours = fields.Integer(string='Nombres des jours',compute='_get_days')
    montant = fields.Float(string='Montant TTC',compute='_get_montant')
    type_discount = fields.Selection(string='Type de remise', selection=[('pourcentage', 'Pourcentage'), ('fixe', 'Fixe'),])
    discount  = fields.Float(string='Remise (%)')
    discount_fix  = fields.Float(string='Remise Fixe')
    date_debut = fields.Datetime(string='Date de début')
    date_fin = fields.Datetime(string='Date de fin')
    abonnement_lines = fields.One2many(comodel_name='hotel.fitness.abonnement.line', inverse_name='fitness_id', string=' Lignes d\'abonnement')
    montant = fields.Float(string='Montant TTC',compute='_get_montant')
    prix_ht = fields.Float(string='Prix HT',compute='_get_montant')
    taxes = fields.Float(string='Taxes',compute='_get_montant')
    qrcode = fields.Char(string="QR")
    historique = fields.One2many(comodel_name='hotel.fitness.historique', inverse_name='abonnement_id', string='Historique')


    @api.onchange('client')
    def _getClient(self):
     res = {}
     ids = []
     clients=self.env["hotel.fitness.abonnement"].search([])
     print '----------------    2'
     for c in clients:
         ids.append(c.client.id)
     res['domain'] = {'client': [('id', 'not in', ids),('create_uid','!=',1)]}
     return res 

    @api.model
    def create(self, values):
        print 'CREAAAAAAATE HOTEL.Fitness_abonnement'
        print values
        qrcode = self._getQrCode('<:>HOTEL<:>'+str(values.get("date_fin")))
        if qrcode=="":
            raise ValidationError("Merci de contacter le prestataire pour la génération du QRCODE")
        values['qrcode'] = qrcode
        p=super(Fitness_abonnement, self).create(values)
        p.write({
            'abonnement_lines':[(0,0,{
            'duree':p.duree,
            'prix_ht':p.prix_ht,
            'taxes':p.taxes,
            'discount':p.discount,
            'fitness_id':p.id,
            'date_debut':p.date_debut,
            'date_fin':p.date_fin,
            })]        
        })
        print  'QQqqqqqqqqR'
        print p.qrcode
        
        return p


class Fitness_abonnement_line(models.Model):
    _name = 'hotel.fitness.abonnement.line'
    def abonnement_line(self):
        return self.env['report'].get_action(self,'hotel_itshore.abonnement_line_report')
   
    @api.one
    @api.depends('duree','discount','date_debut','date_fin')
    def _get_days(self):
        print 'fuuuuuunction  2'
        print self.date_debut
        print self.date_fin
        ddate_now=datetime.strptime(str(datetime.now().date()), '%Y-%m-%d')
        ddate_debut=datetime.strptime(str(self.date_debut), '%Y-%m-%d')
        ddate_fin=datetime.strptime(str(self.date_fin), '%Y-%m-%d')
        
        if ddate_debut >=ddate_now :
            self.etat='oui'
        else :
            self.etat='non'

        if ddate_fin<ddate_now:
            self.nb_jours=0
        else :
            if  ddate_debut>=ddate_now:
                self.nb_jours=(ddate_fin-ddate_debut).days
            else : 
                self.nb_jours=(ddate_now-ddate_debut).days

    @api.one
    @api.depends('duree','discount','date_debut','date_fin')
    def _get_montant(self):
        coe=0
        if self.duree=='mois':
            coe=1
        if self.duree=='trimestre':
            coe=3
        if self.duree=='semestre':
            coe=6
        if self.duree=='annee':
            coe=12
        price=self.fitness_id.product_id.lst_price 
        tax=self.fitness_id.product_id.taxes_id
        cout=price*coe
        tva=0
        for ta in tax:
            if tax.amount_type=='fixed':
                tva=ta.amount
                if ta.price_include:
                    cout-=ta.amount               
            elif ta.amount_type=='percent':
                print ' in percent'
                tva=(cout*ta.amount)*0.01
                print ' oprt done'
                print cout
                if ta.price_include:
                    cout-=tva
        self.prix_ht=cout 
        self.taxes =tva
        self.montant=tva+cout
        if self.type_discount=='fixe':
            self.montant=self.montant-self.discount_fix
        if self.type_discount=='pourcentage':
            self.montant=self.montant-self.montant*self.discount*0.01
        return 0

    @api.multi
    def unlink(self):
        print 'unliiiiink'
        if  self.etat=='oui':
            p= super(Fitness_abonnement_line, self).unlink()
        else :
            raise ValidationError("Vous ne pouvez pas supprimer un abonnement déjà commencé ou bien terminé")
        return p

    date_debut = fields.Date(string='Date de début')
    date_fin = fields.Date(string='Date de fin')
    duree= fields.Selection(string='Durée', selection=[('mois', 'Mois'), ('trimestre', 'Trimestre'),('semestre', 'Semestre'),('annee', 'Année')],required=True)
    montant = fields.Float(string='Montant',compute='_get_montant')
    type_discount = fields.Selection(string='Type de remise', selection=[('pourcentage', 'Pourcentage'), ('fixe', 'Fixe'),])
    discount  = fields.Float(string='Remise (%)')
    discount_fix  = fields.Float(string='Remise Fixe')
    fitness_id = fields.Many2one(comodel_name='hotel.fitness.abonnement', string='')
    nb_jours = fields.Integer(string='Nombres des jours',compute='_get_days')
    prix_ht = fields.Float(string='Prix HT',compute='_get_montant')
    taxes = fields.Float(string='Taxes',compute='_get_montant')
    etat = fields.Selection(string='Etat', selection=[('oui', 'oui'), ('non', 'non'),],compute='_get_days')

    @api.model
    def create(self, values):
        print 'CREAAAAAAATE HOTEL.Fitness_abonnement_line'
        print values
        return super(Fitness_abonnement_line, self).create(values)

class Fitness_historique(models.Model):
    _name = 'hotel.fitness.historique'

    time = fields.Datetime(string="Date d'entrée")
    abonnement_id = fields.Many2one(comodel_name='hotel.fitness.abonnement', string='')
    libelle = fields.Char(string="Libelle")
    
