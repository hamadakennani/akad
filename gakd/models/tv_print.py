# -*- coding: utf-8 -*-
from odoo import api, fields, models
from random import randint
from pprint import pprint
import datetime
import math
import mysql.connector
from num2words import num2words
from odoo.exceptions import  ValidationError
import threading

def synchronized(func):
    func._lock_ = threading.Lock()
    def synced_func(*args, **kws):
        with func._lock_:
            return func(*args, **kws)
    return synced_func

class TvPrint(models.Model):
    _name = 'gakd.tv_print'
    _description = 'New Description'
    _rec_name = 'client'
    _order = 'create_date desc'

    client = fields.Many2one('res.partner',string='Client')
    details = fields.One2many('gakd.tv_print_details','tv_print',string="Détails")
    state = fields.Selection([('brouillon','Brouillon'),('valide','Validée')],string="Statut",default="brouillon")
    print_hestory_ids = fields.One2many('gakd.tv_print_hestory','print_id',string='Impression')
    ref = fields.Char(string='Référence')
    total_tv = fields.Integer(string='Nombre des tickets',compute='_getTotal')
    montant = fields.Float(string='Montant',compute='_getTotal')
    product= fields.Selection(string='Produit',required=True,
    selection=[(155, 'ESSENCE'), (156, 'GASOIL'),(164,'PETROLE')])
    moyen_de_paiement = fields.Selection(string='Moyen de paiement',required=True,selection=[('cheque', 'Chèque'), ('especes', 'Espèces'),('versement','Versement'),('virement','Virement')])
    journal_id = fields.Many2one(comodel_name='account.journal', string='Journal des règlements',required=True)
    hest_id = fields.Many2one('gakd.historique_ben_detail',string='Détail')
    remise_ids = fields.One2many(comodel_name='gakd.tv_remise', inverse_name='tv_print', string='Remise')
    quitance_tva = fields.Selection([('OUI','OUI'),('NON','NON')],string="Quitance TVA ramené?",default="NON",required=True)
    remise_type = fields.Selection([('Espece','Espece'),('Ticket','Ticket')],string="Remise sous forme de",default="Ticket",required=True)
    facture_number = fields.Integer(string="Nombre de Facture")
    ville = fields.Selection([('Parakou','Parakou'),('Cotonou','Cotonou'),('BOHICON','BOHICON')],string="Ville",required=True)


    def getDoubledData(self):
        prints_tv = self.env['gakd.tv_print'].sudo().search([])
        data = {}
        tv = {}
        for tv_p in prints_tv:
            if len(tv_p.remise_ids) > 1 :
                if tv_p.create_uid in data.keys():
                    print  data[tv_p.create_uid]
                    data[tv_p.create_uid]['data'].append(tv_p)
                    data[tv_p.create_uid]['total'] += tv_p.montant
                else:
                    d = []

                    d.append(tv_p)
                    
                    data[tv_p.create_uid] =da = {'data':d,'total':tv_p.montant}
        
        return data

        
    def getTvaAmount(self):
        tva_l=0
        if self.product==155:
            tva_l= self.env['account.tax'].search([('name','=','TVA - ESSENCE')])
        elif  self.product==156:
                tva_l= self.env['account.tax'].search([('name','=','TVA - GASOIL')])
        elif  self.product==164:
            tva_l= self.env['account.tax'].search([('name','=','TVA-PETROLE')])
        return tva_l.amount
    def getTva(self):
        return int(self.getQty()*self.getTvaAmount())

    def getMontantHorsTax(self):
        prix_init = self.getProduct().lst_price
        return int(math.ceil(self.montant/prix_init*(prix_init-self.getTvaAmount())))
    def getMontantTtc(self):
        return self.montant

    def getProduct(self):
        return  self.env['product.product'].sudo().search([('id','=',self.product)])
    def getQty(self):
        product_price = self.getProduct().lst_price
        qty = self.montant * pow(product_price,-1)
        return qty

    @synchronized
    def setValide(self):
        if self.state == 'valide':
            return
        caissier=self.env["gakd.sous_chargeur"].search([('access','=',self.env.user.id)])
        print 'mooontant'
        print self.montant
        print 'monnn'
        print caissier.montant_plafond_tv
        if self.montant<=caissier.montant_plafond_tv:
            m_init =self.montant
            
            # self.solde_sup=caissier.montant_plafond_tv-self.montant
            print 'mooooontant'
        else :
            raise ValidationError("Le montant indiqué doit être inférieur ou égale à votre solde : " +str(caissier.montant_plafond_tv))
        print '----------------------'
        print self.client.client_type
        for a in self.details:
            i = 1
            while i <= a.nbr_tv:
                print 'llllllll'
                self.env['gakd.ticket_valeur'].sudo().create({
                    'tv_type':a.tv_type.id,
                    'client':a.tv_print.client.id

                })
                i+=1
            a.reste = a.nbr_tv
        montant_bonus = 0
        montant_a_pa = 0
        TVA=0
        montant_a_recharge_remise=0
        montant_hor_taxes=0
        qte=0
        
        m_init =self.montant
        m_init =self.montant        
        montant_bonus = 0.0
        montant_a_pa = 0
        TVA=0
        montant_a_recharge_remise=0
        montant_hor_taxes=0
        qte=0
        if self.client.client_type == "client_acc":
            if self.montant >= 100000:
                montant_bonus = self.montant *( self.client.valuer_a_accorde*pow(100,-1))
                montant_bonus = int(montant_bonus/1000) * 1000
        montant_a_pa = self.montant - montant_bonus
        prix_init=self.env['product.product'].search([('id','=',self.product)]).lst_price
        qte=round(self.montant/prix_init, 2)
        print 'ggggggggggggggggggggggggggggggggggggggggSSSSSSSSS'
        print montant_bonus
        self.env['gakd.tv_remise'].sudo().create({
            'montant':montant_bonus,
            'tv_print':self.id
        })
        tva_l=0
        if self.product==155:
            tva_l= self.env['account.tax'].search([('name','=','TVA - ESSENCE')])
        elif  self.product==156:
                tva_l= self.env['account.tax'].search([('name','=','TVA - GASOIL')])
        elif  self.product==164:
            tva_l= self.env['account.tax'].search([('name','=','TVA-PETROLE')])
        montant_hor_taxes=int(math.ceil(self.montant/prix_init*(prix_init-tva_l.amount)))
        
        TVA=int(qte*tva_l.amount)

        montant_a_recharge_remise=montant_hor_taxes+TVA+montant_bonus
        if self.remise_type == 'Espece':
            print montant_hor_taxes
            montant_a_recharge_remise = montant_a_recharge_remise-montant_bonus
            montant_hor_taxes = montant_hor_taxes - montant_bonus
            print '55555555555555555555'
            print montant_hor_taxes
        print 11111111111111111111111
        print montant_hor_taxes
        if self.quitance_tva == "OUI":
            montant_hor_taxes = float(format((montant_hor_taxes+TVA*float(0.6)), '.4f'))
            TVA = float(format((TVA*float(0.4)), '.4f'))
        detail_id =self.env['gakd.historique_ben_detail'].sudo().create({
            'prix_init':self.env['product.product'].search([('id','=',self.product)]).lst_price,
            'qte':qte,
            'montant_hor_taxes':montant_hor_taxes,
            'tva_init':tva_l.amount,
            'tva':TVA,
            'prix_ht':round(self.env['product.product'].search([('id','=',self.product)]).lst_price-tva_l.amount, 3),
            'montant_a_recharge_remise':montant_a_recharge_remise,
            'montant_bonus':montant_bonus,
            'montant_ttc':self.montant,
            'type_de_recharge':self.env['product.product'].search([('id','=',self.product)]).name
            
        })
        if self.quitance_tva == "OUI":
            montant_verse = abs(montant_hor_taxes)
        else:
            montant_verse = self.montant
        hist=self.env['gakd.historique'].create({
                'montant_init':caissier.montant_plafond_tv,
                'montant_fin':caissier.montant_plafond_tv-m_init,
                'sous_chargeur':caissier.id,
                'type_op':'T.V',
                'diff':montant_verse,
                'client_id':self.client.id,
                'moyen_de_paiement':self.moyen_de_paiement
                })
        caissier.write({
                'montant_plafond_tv':caissier.montant_plafond_tv-self.montant
            })
        self.hest_id = detail_id.id
        if self.moyen_de_paiement == 'versement' or self.moyen_de_paiement == 'virement':
            acount_id_escompte = 20228
            acount_id_montant = 19857
            acount_id_tva = 19691
            if self.client.client_type == "client_acc":
                if self.montant >= 100000:
                    data =[(0,0 ,{'account_id':acount_id_escompte ,'partner_id':self.client.id,'name':'ESCOMPTES ACCORDÉS','debit':montant_bonus}),
                    (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':self.montant}),
                    (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':self.montant+montant_bonus})]
                else:
                    data =[(0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':self.montant}),
                    (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':self.montant})]
            else:
                data =[(0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA','debit':TVA}),
                (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
        if self.moyen_de_paiement == 'cheque':
            acount_id_escompte = 20228
            acount_id_tva = 19691
            acount_id_montant =19975
            if self.client.client_type == "client_acc":
                if self.quitance_tva == "OUI":
                    if self.montant >= 100000:
                        data =[(0,0 ,{'account_id':acount_id_escompte ,'partner_id':self.client.id,'name':'ESCOMPTES ACCORDÉS','debit':montant_bonus}),
                        (0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA retenu à la source','debit':TVA}),
                        (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                    else:
                        data =[(0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA retenu à la source','debit':TVA}),
                        (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                else:
                    if self.montant >= 100000:
                        data =[(0,0 ,{'account_id':acount_id_escompte ,'partner_id':self.client.id,'name':'ESCOMPTES ACCORDÉS','debit':montant_bonus}),
                        (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes+TVA}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                    else:
                        data =[(0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes+TVA}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
            else:
                if self.quitance_tva == "OUI":
                    data =[(0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA','debit':TVA}),
                    (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                    (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                else:
                    data =[(0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes+TVA}),
                    (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
        if self.moyen_de_paiement == 'especes':
            acount_id_escompte = 20228
            acount_id_tva = 19691
            acount_id_montant =20608
            if self.client.client_type == "client_acc":
                if self.quitance_tva == "OUI":
                    if float(self.montant) >= float(100000):
                        data =[(0,0 ,{'account_id':acount_id_escompte ,'partner_id':self.client.id,'name':'ESCOMPTES ACCORDÉS','debit':montant_bonus}),
                        (0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA retenu à la source','debit':TVA}),
                        (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                    else:
                        data =[(0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA retenu à la source','debit':TVA}),
                        (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                else:
                    if float(self.montant) >= float(100000):
                        data =[(0,0 ,{'account_id':acount_id_escompte ,'partner_id':self.client.id,'name':'ESCOMPTES ACCORDÉS','debit':montant_bonus}),
                        (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes+TVA}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                    else:
                        data =[(0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes+TVA}),
                        (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
            else:
                if self.quitance_tva == "OUI":
                    data =[(0,0 ,{'account_id':acount_id_tva ,'partner_id':self.client.id,'name':'TVA','debit':TVA}),
                    (0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes}),
                    (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
                else:
                    data =[(0,0 ,{'account_id':acount_id_montant ,'partner_id':self.client.id,'name':'montant hor taxes','debit':montant_hor_taxes+TVA}),
                    (0,0 ,{'account_id':20605,'partner_id':self.client.id,'name':'Montant a recharge','credit':montant_a_recharge_remise})]
        print '8888888888888888888888888888888888888'
        print data
        av= self.env['account.move'].create({
        
        'journal_id':self.journal_id.id,
        'date':datetime.datetime.now().date(),
        'ref': self.client.name+'-'+str(datetime.datetime.now()),
        
        'line_ids':data        
        })    
        av.post()
        self.state = 'valide'
        hist.sudo().write({
                    'account_move':av.id,
                })        
        return True 
    def _getTotal(self):
        for pr in self:
            nb_tv = 0
            mnt = 0
            for d in pr.details:
                nb_tv += d.nbr_tv
                mnt+=d.montant
            pr.update({
                'total_tv':nb_tv,
                'montant':mnt
            })
    def getMontantWords(self):
        print self.montant
        mnt =  self.montant
        text = num2words(mnt, lang='fr')
        print text
        return text.upper()

    def getMontantRemis(self):
        mnt = self.remise_ids.montant
        text = num2words(mnt, lang='fr')
        print text
        return text.upper()
        
    def getMontantTv(self):
        print self.montant
        mnt = self.montant
        text = num2words(mnt, lang='fr')
        print text
        return text.upper()
    
    @api.model
    def create(self, values):
        values['ref'] = str(randint(1111,9999))+str(randint(1111,9999))+str(randint(1111,9999))
        p=super(TvPrint, self).create(values)
        p.client.is_tv = True
        fact = self.env['gakd.facture_number'].sudo().search([])
        if len(fact) == 0 :
            fact = self.env['gakd.facture_number'].sudo().create({})
        
        p.facture_number = fact.num
        fact.num = fact.num +1
        return p
    def connnectToMySql(self):
        print 'hihhhhhhhhhhhhh'
        cnx = mysql.connector.connect(user='root', password='root',
                                    host='localhost',
                                    database='tv_base')
        cursor = cnx.cursor()
        query = ("SELECT * FROM users")
        cursor.execute(query)
        print cursor
        row = cursor.fetchone()
 
        while row is not None:
            print(row)
            row = cursor.fetchone()
        cnx.close()
    def createTypes(self):
        cnx = mysql.connector.connect(user='root', password='Menara@@',
                                    host='localhost',
                                    database='tv_base')
        cursor = cnx.cursor()
        query = ("SELECT * FROM type_coupure")
        cursor.execute(query)
        print cursor
        row = cursor.fetchone()
 
        while row is not None:
            print(row)
            print row[0]
            print row[1]
            print row[2]
            self.env['gakd.tv_type'].sudo().create({
                'type_name':'Coupure',
                'montant':row[1],
                'mysql_id':row[0]
            })
            row = cursor.fetchone()
        cnx.close()
    def createClients(self):
        cnx = mysql.connector.connect(user='root', password='Menara@@',
                                    host='localhost',
                                    database='tv_base')
        cursor = cnx.cursor()
        query = ("SELECT * FROM client")
        cursor.execute(query)
        print cursor
        row = cursor.fetchone()
 
        while row is not None:
            self.env['res.partner'].sudo().create({
                'is_tv':True,
                'name':row[1],
                'street':row[2],
                'mobile':row[3],
                'mysql_id':row[0],
                'property_account_receivable_id':19003,
                'property_account_payable_id':19003
            })
            row = cursor.fetchone()          
        cnx.close()
    def createPrints(self):
        cnx = mysql.connector.connect(user='root', password='Menara@@',
                                    host='localhost',
                                    database='tv_base')
        cursor = cnx.cursor()
        query = ("SELECT * FROM  `code_genere` ORDER BY  `date_gen` DESC LIMIT 100 OFFSET 1900")
        cursor.execute(query)
        print cursor
        row = cursor.fetchone()
 
        while row is not None:
            print(row)
            print row[0]
            print row[1]
            print row[2]
            client = self.env['res.partner'].sudo().search([('mysql_id','=',row[1])])
            tv_t = self.env['gakd.tv_type'].sudo().search([('mysql_id','=',row[5])])
            tv = []
            i = int(row[2])
            while i <= row[3]:
                t = self.env['gakd.ticket_valeur'].sudo().create({
                    'tv_type':tv_t.id,
                    'client':client.id,
                    'qrcode':str(i)
                })
                tv.append(t.id)
                i+=1

            
            row = cursor.fetchone()          
        cnx.close()


class TvPrintDetails(models.Model):
    _name = 'gakd.tv_print_details'

    tv_type = fields.Many2one('gakd.tv_type',string='Type')
    nbr_tv = fields.Integer(string="Nombre de T.V")
    montant = fields.Float(string='Montant à payer',readonly=True)
    imprime = fields.Integer(string='Imprimés',default=0)
    reste = fields.Integer(string='Restants')
    tv_print = fields.Many2one('gakd.tv_print',string='print')
    print_nb = fields.Integer(string='nbr print')


    @api.onchange('tv_type','nbr_tv')
    def _onchange_field_name(self):
        if self.tv_type and self.nbr_tv :
            self.montant = self.tv_type.montant * self.nbr_tv
    @api.one
    def sayhi(self):
        print 'hiiiiiiiiiiiiiiiiiii'

    @api.model
    def create(self, vals):
        mnt = self.env['gakd.tv_type'].search([('id','=',vals.get('tv_type',False))]).montant
        vals["montant"] = vals['nbr_tv'] * mnt
        if 'reste' not in vals:
            vals['reste']= vals['nbr_tv']
        tv_montant = self.env['gakd.tv_type'].search([('id','=',vals.get('tv_type',False))]).montant * vals.get('nbr_tv')
        vals['montant']=tv_montant
        return super(TvPrintDetails, self).create(vals)
    def print_ticket(self):
        return range(self.print_nb)
class Remise(models.Model):
    _name='gakd.tv_remise'

    tv_print = fields.Many2one(comodel_name='gakd.tv_print', string='Impression')
    montant = fields.Integer(string='Montant de la remise')
    montant_rest = fields.Integer(string="Restants")
    tv_ids = fields.One2many(comodel_name='gakd.ticket_valeur', inverse_name='remise', string='Tickets remise')
    montant_print = fields.Float(compute='getMontant',string="Montant des tickets")
    def getRepportData(self):
        data = {}
        for tv in self.tv_ids:
            if tv.tv_type.rec in data.keys():
                data[tv.tv_type.rec] = data[tv.tv_type.rec]+1
            else:
                data[tv.tv_type.rec] = 1
        return data

    @api.multi
    def getMontant(self):
        for r in self:
            r.sudo().update({
                'montant_print':r.montant-r.montant_rest
            })

    @api.model
    def create(self, vals):
        vals['montant_rest']=vals.get('montant',0)
        return super(Remise, self).create(vals)

    def print_tv(self):
        return self.env['report'].get_action(self,'gakd.repport_tv_template_remise')
    
    

class ticke_non_connue(models.Model):
    _name = 'gakd.ticke_non_connue'
    _order = 'create_date desc'
    date_dt =fields.Char(string='Date consommation')
    montant = fields.Integer(string='Montant')
    codebre = fields.Char(string="Code barre")
    etat = fields.Selection([('util','Utilisé'),('nonutil','Non utilisé')],default='nonutil')
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente", required=True)
    agent_id = fields.Many2one("gakd.agent",string="Agent")