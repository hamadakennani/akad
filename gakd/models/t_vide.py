# -*- coding: utf-8 -*-
from odoo import api, fields, models

class Vide(models.Model):
    _name = 't.vide'

    montant = fields.Float(string='Montant')
    quantite = fields.Float(string='Quantité')
    nbtransaction=fields.Integer(string="Nombre de transaction")
    typevente=fields.Many2one("gakd.type_vente", string="Type de vente")