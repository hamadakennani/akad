# -*- coding: utf-8 -*-

import xlwt
import cStringIO
import base64
import time
import datetime
from datetime import datetime
from datetime import timedelta

from openerp import api, fields, models

class AccountReportGeneralLedger(models.TransientModel):
    _inherit = "account.report.general.ledger"

    @api.multi
    def check_report_excel(self):
        res = super(AccountReportGeneralLedger, self).check_report()
        if self._context.get('active_model') == 'account.account':
           accounts = self.env['account.account'].search([('id', 'in', self._context.get('active_ids'))])
        else:
           accounts = self.env['account.account'].search([])
        #accounts = self.env['account.account'].search([])
        init_balance = res['data']['form']['initial_balance']
        sortby = res['data']['form']['sortby']
        display_account = res['data']['form']['display_account']
        gl_report_obj = self.env['report.account.report_generalledger']
        data = gl_report_obj.with_context(res['data']['form'].get('used_context',{}))._get_account_move_entry(accounts, init_balance, sortby, display_account)
        return self._print_report_excel(data)

    @api.multi
    def _print_report_excel(self, data):
        workbook = xlwt.Workbook()
        title_style_comp = xlwt.easyxf('align: horiz center ; font: name Times New Roman,bold off, italic off, height 450')
        title_style_comp_left = xlwt.easyxf('align: horiz left ; font: name Times New Roman,bold off, italic off, height 450')
        title_style = xlwt.easyxf('align: horiz center ;font: name Times New Roman,bold off, italic off, height 350')
        title_style2 = xlwt.easyxf('font: name Times New Roman, height 200')
        title_style1 = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190; borders: top double, bottom double, left double, right double;')
        title_style1_table_head = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head1 = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200')
        title_style1_consultant = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head_center = xlwt.easyxf('align: horiz center ; font: name Times New Roman,bold on, italic off, height 190; borders: top thick, bottom thick, left thick, right thick;')

        title_style1_table_data = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold on, italic off, height 190')
        title_style1_table_data_sub = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190')
        title_style1_table_data_sub_balance = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold off, italic off, height 190')
        
        sheet_name = 'General Ledger'
        sheet = workbook.add_sheet(sheet_name)
        
        sheet.write_merge(0, 1, 0, 6, self.env.user.company_id.name + ': General Ledger', title_style_comp_left)
        sheet.write(0, 8, 'Printing Date: '+datetime.now().strftime('%Y-%m-%d'), title_style1_table_head1)
        
        sheet.write(3, 0, 'Journals:', title_style1_table_head1)
        journals_code = ''
        for journal in self.journal_ids:
            journals_code += journal.code+', '
        sheet.write(4, 0, journals_code, title_style1_table_data_sub)
        comp_id = self.env.user.company_id
        currency_id = comp_id.currency_id
            
        sheet.write(3, 8, 'Target Moves',title_style1_table_head1)
        if self.target_move == 'all':
            column = sheet.col(8)
            column.width = 256 * 18
            sheet.write(4, 8, 'All Entries',title_style1_table_data_sub)
        if self.target_move == 'posted':
            column = sheet.col(8)
            column.width = 256 * 10
            sheet.write(4, 8, 'All Posted Entries',title_style1_table_data_sub)
        
        sheet.write(5, 0, 'Sorted By',title_style1_table_head1)
        if self.sortby == 'sort_date':
            column = sheet.col(0)
            column.width = 256 * 18
            sheet.write(6, 0, 'Date',title_style1_table_data_sub)
        if self.sortby == 'sort_journal_partner':
            sheet.write(6, 0, 'Journal and Partner',title_style1_table_data_sub)
        
        sheet.write(3, 5, 'Display Account',title_style1_table_head1)
        if self.display_account == 'all':
            sheet.write(4, 5, 'All accounts' ,title_style1_table_data_sub)
        if self.display_account == 'movement':
            sheet.write(4, 5, 'With movements' ,title_style1_table_data_sub)
        if self.display_account == 'not_zero':
            sheet.write(4, 5, 'With balance not equal to zero' ,title_style1_table_data_sub)
            
        if self.date_from:
            sheet.write(5, 5, 'Date from :',title_style1_table_head1)
            sheet.write(5, 6, self.date_from ,title_style1_table_data_sub)
        if self.date_to:
            sheet.write(6, 5, 'Date to :',title_style1_table_head1)
            sheet.write(6, 6, self.date_to ,title_style1_table_data_sub)
        column = sheet.col(0)
        column.width = 256 * 50
        sheet.write(8, 0, 'Date',title_style1_table_head)
        sheet.write(8, 1, 'JRNL',title_style1_table_head)
        sheet.write(8, 2, 'Partner',title_style1_table_head)
        sheet.write(8, 3, 'Ref',title_style1_table_head)
        sheet.write(8, 4, 'Move',title_style1_table_head)
        sheet.write(8, 5, 'Entry Label',title_style1_table_head)
        column = sheet.col(5)
        column.width = 256 * 25
        sheet.write(8, 6, 'Debit',title_style1_table_head)
        sheet.write(8, 7, 'Credit',title_style1_table_head)
        sheet.write(8, 8, 'Balance',title_style1_table_head)
        if self.user_has_groups('base.group_multi_currency'):
            sheet.write(8, 9, 'Currency',title_style1_table_head)
        
        row_date = 9
        for account in data:
            sheet.write(row_date, 0, account['code']+' '+account['name'], title_style1_table_head1)
            sheet.write(row_date, 6, str(account['debit'])+' '+currency_id.symbol, title_style1_table_data)
            sheet.write(row_date, 7, str(account['credit'])+' '+currency_id.symbol, title_style1_table_data)
            sheet.write(row_date, 8, str(account['balance'])+' '+currency_id.symbol, title_style1_table_data)
            row_date += 1
            
            for acc_data in account['move_lines']:
                sheet.write(row_date, 0, acc_data['ldate'] ,title_style1_table_data_sub)
                sheet.write(row_date, 1, acc_data['lcode'] ,title_style1_table_data_sub)
                sheet.write(row_date, 2, acc_data['partner_name'] ,title_style1_table_data_sub)
                sheet.write(row_date, 3, acc_data['lref'] ,title_style1_table_data_sub)
                sheet.write(row_date, 4, acc_data['move_name'] ,title_style1_table_data_sub)
                sheet.write(row_date, 5, acc_data['lname'] , title_style1_table_data_sub)
                sheet.write(row_date, 6, str(acc_data['debit'])+' '+currency_id.symbol , title_style1_table_data_sub_balance)
                sheet.write(row_date, 7, str(acc_data['credit'])+' '+currency_id.symbol , title_style1_table_data_sub_balance)
                sheet.write(row_date, 8, str(acc_data['balance'])+' '+currency_id.symbol, title_style1_table_data_sub_balance)
                currency =  self.env['res.currency'].browse(acc_data['currency_id']).symbol if acc_data['currency_id'] else ''
                if currency and self.user_has_groups('base.group_multi_currency'):
                    sheet.write(row_date, 9, str(acc_data['amount_currency'])+' '+currency , title_style1_table_data_sub)
                row_date += 1
        
        stream = cStringIO.StringIO()
        workbook.save(stream)
        attach_id = self.env['accounting.gl.report.output.wizard'].create({'name':'General_Ledger.xls', 'xls_output': base64.encodestring(stream.getvalue())})
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'accounting.gl.report.output.wizard',
            'res_id':attach_id.id,
            'type': 'ir.actions.act_window',
            'target':'new'
        }

class AccountingGLReportOutputWizard(models.Model):
    _name = 'accounting.gl.report.output.wizard'
    _description = 'Wizard to store the Excel output'

    xls_output = fields.Binary(string='Excel Output')
    name = fields.Char(string='File Name', help='Save report as .xls format', default='General_Ledger.xls')

#vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
