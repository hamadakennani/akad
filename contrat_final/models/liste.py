# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint

class capcontrat_fichiers(models.Model):

	_name='capcontrat.fichiers'
	_rec_name='filename'
	_order='create_date desc'

	file_id=fields.Many2one('capcontrat.categories',string='categorie')
	file_upload=fields.Binary(string='files',requiered=True)
	filename=fields.Char(string='file_name')

