from odoo import api, fields, models


class HotelRoom(models.Model):
    _inherit = 'hotel.room'

    _rec_name='title'
   
    title = fields.Char(string='libelle',default=lambda *a: 'inspect',compute='get_title')
    @api.one
    @api.depends('name')
    def get_title(self):
       
        print self.env['hotel.housekeeping'].search([('room_no','=',self.id),('state','=', 'dirty')])

        if  len(self.env['hotel.housekeeping'].search([('room_no','=',self.id),('state','=', 'maintenance')]))>0:   
              self.title = self.name+' /  Maintenance'
        elif len(self.env['hotel.housekeeping'].search([('room_no','=',self.id),('state','=', 'dirty')]))>0:
            self.title = self.name+' / Sale '
           
       
        else:
            
             self.title=self.name
       