# SubaccountAddRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**keyid** | **str** |  | 
**sub_account_edit** | **str** |  | 
**sub_account_login** | **str** |  | 
**sub_account_password** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


