from odoo import api, fields, models


class ReportCheckin(models.AbstractModel):
    _inherit = 'report.hotel_reservation.report_checkin_qweb'

    # def _get_room_type(self, date_start, date_end):
    #     # super(ReportTestCheckin,self)
    #     print 'jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj'
    #     room_dom = [('checkin', '>=', date_start),
    #                 ('checkout', '<=', date_end)]
    #     reservation_lines = self.env['hotel_reservation.line'].search(room_dom)
    #     res_ids = [n.line_id for n in reservation_lines ]
    #     # tids = reservation_obj.search(room_dom)
    #     res = self.env['hotel.reservation'].browse(res_ids)
    #     return res

    # def _get_room_nos(self, date_start, date_end):
    #     # super(ReportTestCheckin,self)
    #     print 'fffffffffffffffffffffffffffffffff'
    #     print 'fffffffffffffffffffffffffffffffff'
    #     room_dom = [('checkin', '>=', date_start),
    #                 ('checkout', '<=', date_end)]
    #     print 'fffffffffffffffffffffffffffffffff'
    #     reservation_lines = self.env['hotel_reservation.line'].search(room_dom)
    #     print 'fffffffffffffffffffffffffffffffff'
    #     res_ids = [n.line_id for n in reservation_lines ]
    #     print 'fffffffffffffffffffffffffffffffff'
    #     tids = self.env['hotel.reservation'].search([('id','in',res_ids)])
    #     # res = reservation_obj.browse(tids)
    #     print 'fffffffffffffffffffffffffffffffff'
    #     # print res
    #     return tids

    def get_checkin(self, date_start, date_end):
        super(ReportCheckin,self)
        print '1'
        room_dom = [('checkin', '>=', date_start),
                    ('checkout', '<=', date_end)]
        print '2'
        reservation_lines = self.env['hotel_reservation.line'].search(room_dom)
        print '3'
        res_ids = [n.line_id.id for n in reservation_lines ]
        print '4'
        print res_ids
        res = self.sudo().env['hotel.reservation'].search([('id','in',res_ids)])
        print res
        print '5'
        return res
class ReportCheckout(models.AbstractModel):
    _inherit = "report.hotel_reservation.report_checkout_qweb"
    def get_checkout(self, date_start, date_end):
        super(ReportCheckout,self)
        room_dom = [('checkin', '>=', date_start),
                    ('checkout', '<=', date_end)]
        reservation_lines = self.env['hotel_reservation.line'].search(room_dom)
        res_ids = [n.line_id.id for n in reservation_lines ]
        res = self.sudo().env['hotel.reservation'].search([('id','in',res_ids)])
        return res
class ReportMaxroom(models.AbstractModel):
    _inherit = "report.hotel_reservation.report_maxroom_qweb"
    def _get_room_used_detail(self, date_start, date_end):
        super(ReportMaxroom,self)
        room_used_details = []
        hotel_room_obj = self.env['hotel.room']
        room_ids = hotel_room_obj.search([])
        for room in hotel_room_obj.browse(room_ids.ids):
            counter = 0
            details = {}
            print '-----'
            print room
            if room.room_reservation_line_ids:
                print 'ppppp'
                for room_resv_line in room.room_reservation_line_ids:
                    if(room_resv_line.reservation_id.reservation_line[0].checkin >= date_start and
                       room_resv_line.reservation_id    .reservation_line[0].checkout <= date_end):
                        counter += 1
            if counter >= 1:
                details.update({'name': room.name or '',
                                'no_of_times_used': counter})
                room_used_details.append(details)
        return room_used_details

class ReportTestRoomres(models.AbstractModel):
    _inherit = "report.hotel_reservation.report_roomres_qweb"
    def get_data(self, date_start, date_end):
        super(ReportTestRoomres,self)
        room_dom = [('checkin', '>=', date_start),
                    ('checkout', '<=', date_end)]
        reservation_lines = self.env['hotel_reservation.line'].search(room_dom)
        res_ids = [n.line_id.id for n in reservation_lines ]
        res = self.sudo().env['hotel.reservation'].search([('id','in',res_ids)])
        return res