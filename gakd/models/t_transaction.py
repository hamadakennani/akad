# -*- coding: utf-8 -*-
from odoo import api, fields, models


class Detail(models.Model):
    _name = 't.detail'

    montant = fields.Float(string='Montant')
    quantite = fields.Float(string='Quantité')
    point_vente_id = fields.Many2one("gakd.point.vente",string="Point de vente")
    agent_id = fields.Many2one("gakd.agent",string="Agent")
    product_ids = fields.Many2one("product.product",string="Produit" )
    date_creation = fields.Datetime(string="Date Transaction")
    transaction_id = fields.Many2one("gakd.carte.consommation",string="Transaction")
    typevente=fields.Many2one("gakd.type_vente", string="Type de vente")

    @api.multi
    def redirect(self):
        return {
        'name':  'Transaction',
        'view_type': 'form',
        'view_mode': 'form',
        'res_model':'gakd.carte.consommation', 
        'res_id': self.transaction_id.id,
        'type': 'ir.actions.act_window',
        'target': 'new',
        'nodestroy': True
        }