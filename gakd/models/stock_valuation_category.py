# -- coding: utf-8 --
from odoo import api, fields, models
import pytz
import time
class StockValuationCategory(models.AbstractModel):
    _inherit = 'report.stock_valuation_on_date.stock_valuation_ondate_report'
  

#     @api.model
#     def render_html(self, docids, data=None):
#         report_obj = self.env['report']
#         report = report_obj._get_report_from_name('gakd.stock_valuation_ondate_report2')
#         self.begining_qty = 0.0
#         self.total_in = 0.0
#         self.total_out = 0.0
#         self.total_int = 0.0
#         self.total_adj = 0.0
#         self.total_begin = 0.0
#         self.total_end = 0.0
#         self.global_subtotal_cost = 0.0
#         self.total_inventory = []
#         self.value_exist = {}
#         docargs = {
#             'doc_ids': self._ids,
#             'doc_model': report.model,
#             'docs': self,
#             'data': data,
#             'time': time,
#             'get_warehouse_name': self.sudo().get_warehouse_name,
#             'get_warehouses_block': self.sudo()._get_warehouses_block,
#             'get_company': self.sudo()._get_company,
#             'get_product_name': self.sudo()._product_name,
#             'get_categ': self._get_categ,
#             'get_lines': self._get_lines,
#             'get_beginning_inventory': self._get_beginning_inventory,
#             'get_ending_inventory': self._get_ending_inventory,
#             'get_value_exist': self._get_value_exist,
#             'total_in': self._total_in,
#             'total_out': self._total_out,
#             'total_int': self._total_int,
#             'total_adj': self._total_adj,
#             'total_begin': self._total_begin,
#             'total_vals': self._total_vals,
#             'total_end': self._total_end,
#             'get_cost': self._get_cost,
#             'get_subtotal_cost': self._get_subtotal_cost,
#             'categ_subtotal_cost': self._categ_subtotal_cost
#             }
#         return report_obj.render('gakd.stock_valuation_ondate_report2', docargs)

# def category_wise_value(self, start_date, end_date, locations, filter_product_categ_ids=[]):
#         super(StockValuationCategory,self)
#         """
#         Complete data with category wise
#             - In Qty (Inward Quantity to given location)
#             - Out Qty(Outward Quantity to given location)
#             - Internal Qty(Internal Movements(or null movements) to given location: out/in both : out must be - ,In must be + )
#             - Adjustment Qty(Inventory Loss movements to given location: out/in both: out must be - ,In must be + )
#         Return:
#             [{},{},{}...]
#         """

#         """ product_qty_out : Sales ,product_qty_in: Recived, product_qty_internal:  Internal   """

#         self._cr.execute('''
#                         SELECT pp.id AS product_id,pt.categ_id,
#                             sum((
#                             CASE WHEN  (destl.name LIKE 'Customers' or destl.name LIKE 'Clients') 
#                             THEN -(sm.product_qty * pu.factor / pu2.factor)  
#                             WHEN  (destl.name LIKE 'Customers' or destl.name LIKE 'Clients') 
#                             THEN -(sm.product_qty * pu.factor / pu2.factor) 
#                             ELSE 0.0 
#                             END
#                             )) AS product_qty_out,
#                             sum((
#                             CASE WHEN sourcel.name LIKE 'Vendors' or sourcel.name LIKE 'Fournisseurs' 
#                             THEN (sm.product_qty * pu.factor / pu2.factor) 
#                             ELSE 0.0 
#                             END
#                             )) AS product_qty_in,
                        
#                             sum((
#                             CASE WHEN destl.name  NOT LIKE 'Customers' AND  destl.name  NOT LIKE 'Clients' 
#                             THEN (sm.product_qty * pu.factor / pu2.factor)  
#                             WHEN  destl.name  NOT LIKE 'Customers' AND  destl.name  NOT LIKE 'Clients' 
                        
#                             THEN -(sm.product_qty * pu.factor / pu2.factor) 
#                             ELSE 0.0 
#                             END
#                             )) AS product_qty_internal,
                        
#                             sum((
#                             CASE WHEN sourcel.usage = 'inventory' AND sm.location_dest_id in %s  
#                             THEN  (sm.product_qty * pu.factor / pu2.factor)
#                             WHEN destl.usage ='inventory' AND sm.location_id in %s 
#                             THEN -(sm.product_qty * pu.factor / pu2.factor)
#                             ELSE 0.0 
#                             END
#                             )) AS product_qty_adjustment
                        
#                         FROM product_product pp 
#                         LEFT JOIN  stock_move sm ON (sm.product_id = pp.id and sm.date >= %s and sm.date <= %s and sm.state = 'done' and sm.location_id != sm.location_dest_id)
#                         LEFT JOIN stock_picking sp ON (sm.picking_id=sp.id)
#                         LEFT JOIN stock_picking_type spt ON (spt.id=sp.picking_type_id)
#                         LEFT JOIN stock_location sourcel ON (sm.location_id=sourcel.id)
#                         LEFT JOIN stock_location destl ON (sm.location_dest_id=destl.id)
#                         LEFT JOIN product_uom pu ON (sm.product_uom=pu.id)
#                         LEFT JOIN product_uom pu2 ON (sm.product_uom=pu2.id)
#                         LEFT JOIN product_template pt ON (pp.product_tmpl_id=pt.id)
#                         GROUP BY pt.categ_id, pp.id order by pt.categ_id

#                         ''', (tuple(locations), tuple(locations), start_date, end_date))

#         values = self._cr.dictfetchall()

#         for none_to_update in values:
#             if not none_to_update.get('product_qty_out'):
#                 none_to_update.update({'product_qty_out':0.0})
#             if not none_to_update.get('product_qty_in'):
#                 none_to_update.update({'product_qty_in':0.0})

#         # filter by categories
#         if filter_product_categ_ids:
#             values = self._remove_product_cate_ids(values, filter_product_categ_ids)
#         return values



class StockValuationDateReport(models.TransientModel,
                               StockValuationCategory):
    _inherit = 'stock.valuation.ondate.report'

    @api.onchange('company_id')
    def onchange_company(self):
        print 'ttttttttttttt-------------------------------'
        cat_data = []
        if self.company_id:
            cat_data = [(6,0,[c.id for c in self.env['product.category'].search([('company_id','=',self.company_id.id)])])]
        return {'domain':{'company_id':[('id','in',[c.id for c in self.env.user.company_ids])],'filter_product_categ_ids':[('company_id','in',[c.id for c in self.company_id])]}}
    