# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
import odoo
from collections import Counter
from datetime import datetime
from odoo import SUPERUSER_ID
from odoo import fields, api, models,_
from odoo.tools.translate import _



class ged_recherche(models.Model):
    _name = 'ged.recherche'

    user_id=fields.Integer(string='user_uid',default=lambda self: self.env.user.id)
    search_document=fields.Integer(string='search_document')
    def mymod_search_document(self,param_search):
	list_search = [('create_uid','!=',1)]
	list_result = []
	liste_id_document = []
	liste_mots_not_search = []
	flag_check_plusieur_mots = 0
	print "rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr"
	for param in param_search:
		if param["name"] == "dossier_num" and param["value"] != "":
			affaire_ids1 = self.env['contentieux.affaire'].search([('ref_precontentieux','ilike',param["value"])])
			affaire_ids2 = self.env['contentieux.affaire'].search([('ref_contentieux','ilike',param["value"])])
			affaire_ids = affaire_ids1 + affaire_ids2
			list_search.append(('affaire_id','in',affaire_ids))
		if param["name"] == "name" and not param["value"] == "":
			list_search.append(('datas_fname','ilike',param["value"]))
		if param["name"] == "txt_state" and not param["value"] == "":
			list_search.append(('state','=',param["value"]))
		if param["name"] == "textarea_description" and not param["value"] == "":
			list_search.append(('description','ilike',param["value"]))
		if param["name"] == "create_date_du" and not param["value"] == "":
			list_search.append(('create_date','>=',param["value"]))
		if param["name"] == "create_date_au" and not param["value"] == "":
			list_search.append(('create_date','<=',param["value"]))
		if param["name"] == "parent_id" and not param["value"] == "":
			list_search.append(('parent_id','=',int(param["value"])))
		if param["name"] == "textarea_document" and not param["value"] == "":
			list_search.append(('index_data','ilike',param["value"]))
		if param["name"] == "textarea_document_plusieur_mot" and not param["value"] == "":
			flag_check_plusieur_mots = 1
			list_plusieur_mot = []
			list_plusieur_mot = param["value"].split(" ")
			list_search_value = []
			for item in list_plusieur_mot:
				list_search_value = list_search[:]
				if item != "":
					list_search_value.append(('index_data','ilike',item))
					document_ids = self.env['ir.attachment'].search(list_search_value)
					count_search = self.env['ir.attachment'].search_count(list_search_value)
					liste_mots_not_search.append(count_search)
					for item in document_ids:
						liste_id_document.append(item)
	if flag_check_plusieur_mots == 0:
		document_ids = self.env['ir.attachment'].search(list_search)
		list_result = self.mymod_getlist_document_byID(document_ids)
	else:
		list_result = self.mymod_getlist_document_byID(list(set(liste_id_document)))
        return list_result,liste_mots_not_search

    def mymod_getlist_document_byID(self,document_ids):
	list_result = []
	for item_ids in document_ids:
		document = self.env['ir.attachment'].browse(item_ids)
		state = ""
		if document.state == "ready":
			state = "Activé"
		elif document.state == "cancel":
			state = "Annulé"
		date_create = datetime.strptime(document.create_date, '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
		if document.description == False:
			description = ""
		else:
			description = document.description
		if document.affaire_id.reference:
			reference = document.affaire_id.reference
			#print 'uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu'
			#print reference
			#print 'uuuuuuuuuuuuuuuuuuuuuuuuuuuuuu'
		val_result = {
			"id":document.id,
			"affaire_id":reference,
			"name_file":document.datas_fname,
			"name_repertoire":document.parent_id.name,
			"commentaire":document.commantaire,
			"state":state,
			"create_date":date_create,
			"description":description,	
		}
		list_result.append(val_result)
	return list_result
    def mymod_getAll_document(self):
	print 'hiuiuuigiugiuguiguigiuguiguig'
	list_result = []
	usersIds = [self.env.user.id]
	if self.env['res.users'].has_group('ged.group_notaire_ged_manager'):
		usersIds = self.env['res.users'].search([(1, '=',1)])
	document_ids = self.env['ir.attachment'].search([])
	print document_ids
	for item_ids in document_ids:
		document = self.env['ir.attachment'].browse(item_ids)
		state = ""
		#if document.state == "ready":
		#	state = "Activé"
		#elif document.state == "cancel":
		#	state = "Annulé"
		#date_create = datetime.strptime(document.create_date, '%Y-%m-%d %H:%M:%S').strftime('%d/%m/%Y')
		#if document.description == False:
		#	description = ""
		#else:
		#	description = document.description
		reference = "N/A"
		#if document.affaire_id.reference:
		#	reference = document.affaire_id.reference
		val_result = {
			"id":document.id.id,
			"affaire_id":"jpojpojpojop",
			"name_file":"jkhohjpojpjpojpo",
			"name_repertoire":"jjhojojpojp",
			"commentaire":"commantaie",
			"state":state,
			"create_date":"",
			"description":"ojojojojo",	
		}
		print val_result
		list_result.append(val_result)
	print list_result
	return list_result
    # def mymod_get_directory(self):
	# list_result = []
	# directory_ids = self.env['document.directory'].search([(1,'=',1)])
	# for item_ids in directory_ids:
	# 	directory = self.env['document.directory'].browse(item_ids)
	# 	val_result = {
	# 		"id" : directory.id,
	# 		"name" : directory.name,
	# 	}
	# 	list_result.append(val_result)
    #     return list_result

	
