# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import time
from pprint import pprint
from datetime import datetime, timedelta
from dateutil.relativedelta import *

class RoomByCat(models.TransientModel):
    _name="report_room_by_cat"
    date_debut = fields.Datetime(string='Date de début')
    date_fin = fields.Datetime(string='Date de fin')
    def print_report(self):
        return True
    def _build_contexts(self, data):
        result = {}
        result['journal_ids'] = 'journal_ids' in data['form'] and data['form']['journal_ids'] or False
        result['state'] = 'target_move' in data['form'] and data['form']['target_move'] or ''
        result['date_from'] = data['form']['date_from'] or False
        result['date_to'] = data['form']['date_to'] or False
        result['strict_range'] = True if result['date_from'] else False
        return result
    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['date_debut', 'date_fin'])[0]
        
        records = self.env[data['model']].browse(data.get('ids', []))
        return self.env['report'].with_context(landscape=True).get_action(records,'hotel_itshore.report_rooms_by_cat', data=data)
