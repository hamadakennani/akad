from odoo import api, fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    contrat_ids = fields.One2many(comodel_name='dubai.contrat', inverse_name='partner', string='Contrat')
    
   
