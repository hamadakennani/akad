# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import ValidationError

class ProductProduct(models.Model):
    _inherit = 'product.product'

    categories_consomable = fields.Selection([('Lubrifiants','Lubrifiants'),('Produits blancs','Produits blancs'),('Gaz','Gaz'),('Accessoire','Accessoire')],string="Catégorie produit")
    #type_accessoire = fields.Selection([('avec_logo','AVEC LOGO'),('sans_logo','SANS LOGO'),('Autre','Autre')],string="Type d'accessoire")
    categories_service = fields.Selection([('Lavage','Lavage'),('Autre','Autre')],string="Catégorie service")
    autre_service = fields.Char(string="Autre service")
    categories_lavage = fields.Selection([('brosse','Lavage automatique à brosse'),('eau','Lavage traditionnelle à eau')],string="Type de lavage")
    type_product_hotel = fields.Selection(string='Type de product hotel',
     selection=[('food', 'Food'), ('transport', 'Transport'), ('lundry', 'Lundry'),('fitness','fitness')])


     
    def changeCategory(self,catName,catId):
        products = self.env['product.product'].sudo().search([('categories_consomable','=',catName)])
        products.sudo().write({
            'categ_id':catId
        })

    def change(self):
        self.changeCategory('Lubrifiants',6)


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    categories_consomable = fields.Selection([('Lubrifiants','Lubrifiants'),('Produits blancs','Produits blancs'),('Gaz','Gaz'),('Accessoire','Accessoire')],string="Catégorie produit")

class ModuleName(models.Model):
    _inherit = 'account.account'

    is_true = fields.Boolean(string='is True')



class ProductProduct(models.Model):
    _inherit = 'stock.inventory'



    date = fields.Datetime(
        'Inventory Date',
        readonly=False, required=True,
        default=fields.Datetime.now,
        help="The date that will be used for the stock level check of the products and the validation of the stock move related to this inventory.")
    