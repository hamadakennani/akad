# -- coding: utf-8 --

import time
from odoo import api, fields, models, _
from odoo import netsvc
import odoo.addons.decimal_precision as dp


class mro_garage(models.Model):

    _name = 'mro.garage'
    _rec_name="libelle"

    num_garage=fields.Integer(u"N° garage")
    type_garage=fields.Selection([('Type 1','Type 1'),('Type 2','Type 2'),('Type 3','Type 3'),('Type 4','Type 4'),('Type 5','Type 5')],string="Type de Garage")
    libelle=fields.Char("Libelle")
    active=fields.Boolean("Active ?")
    mecanicien = fields.Many2many('hr.employee', string='Mecanicien')
    location_id=fields.Many2one('stock.location',string="Emplacement")