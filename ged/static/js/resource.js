 openerp.ged = function (openerp)
{  
 //Afficher document  
openerp.web.form.widgets.add('document_show', 'openerp.ged.Mywidget_show');
    openerp.ged.Mywidget_show = openerp.web.form.FieldChar.extend(
        {
        template : "document_show",
        init: function (view, code) {
            this._super(view, code);
     
        },

		start: function() {
		var ds = new openerp.web.DataSet(this, 'ir.attachment', {});
		ids = [parseInt($.bbq.getState().id)];
		ds.call('mymod_get_document', [ids]).done(function(r) {
			$('#form_notif_document').attr('src',"data:application/pdf;base64,"+r["document"]);
		})  
		  
		
		},
		  
	});

 //recherche document
openerp.web.form.widgets.add('document_search', 'openerp.ged.Mywidget_search');
    openerp.ged.Mywidget_search = openerp.web.form.FieldChar.extend(
        {
        template : "document_search",
        init: function (view, code) {
            this._super(view, code);
		$(".o_control_panel").remove();
     
        },

		start: function() {
		var url      = window.location.href;  
		var arr = url.split("/");
		var result = arr[0] + "//" + arr[2]
		var host = result
			//this.serializeArray();
		var ds = new openerp.web.DataSet(this, 'ged.recherche', {});
		ids = [parseInt($.bbq.getState().id)];
		var a = Array();
		
		// ds.call('mymod_get_directory', []).done(function(r) {
		// 	//$('#form_search_document').attr('src',"http://41.143.254.148:8808/ged/"+r["uid"]+"er_7568491_ti_"+r["hash"]);
		// 	var option = "<option></option>";
		// 	$.each(r,function(i,v){
		// 		option += "<option value='"+v.id+"'>"+v.name+"</option>";
		// 		//alert(v.name)
		// 	})
		// 	$("#txt_repertoire").html(option);
		// }) 

		 
		//$(".oe_view_title").css("opacity","0");
		//$(".oe_view_manager_buttons").css("opacity","0");
	var content_html = '<div class="search_critere"><form method="POST" id="form_critere"><div  style="border:1px solid #ddd;padding: 10px;border-radius: 0px;width:97%;margin:0px auto;position:relative;margin-top:20px"  class="form_search" ><table style="width:100%;margin:0px auto;margin-top: 12px!important;" ><tr><!-- txt name --><td><input type="checkbox" id="check_name" name="check_name" style="margin-right:5px;"/></td><td class="td_label"><label for="check_name"  >Nom du fichier :</label></td><td><input type="text" id="txt_name" name="name"  /></td><!-- txt create date <td><input type="checkbox" id="check_create_date" name="check_create_date" /></td><td class="td_label"><label style="padding-left:5px;" for="check_create_date"   >Date de création : </label></td><td><label>Du :</label><input type="text" id="txt_create_date_du" name="create_date_du" style="width:38%;margin-right:10px"/><label>Au :</label><input type="text" id="txt_create_date_au" name="create_date_au" style="width:38%" /></td>--><td><input type="checkbox" id="check_dossier_num" name="check_dossier_num" style="margin-right:5px;"/></td><td class="td_label"><label for="check_dossier_num"  >Nom du contrat :</label></td><td><input type="text" id="txt_dossier_num" name="dossier_num"  /></td><!-- txt répertoire --><!-- txt type_file --><!--<td><input type="checkbox" id="check_type_file" name="check_type_file" /></td><td class="td_label"><label for="check_type_file"  >Type du fichier : </label></td><td><select id="txt_type_file" name="txt_type_file" ><option value=""></option><option value="pdf" >PDF</option><option value="word">WORD</option><option value="excel">EXCEL</option></select></td> --></tr><tr><!-- txt type --><!-- <td><input type="checkbox" id="check_type" name="check_type" /></td> --><!-- <td class="td_label" ><label for="check_type" style="padding-left:5px;" >Type : </label></td><td><select id="txt_type" name="txt_type" ><option value=""></option><option value="A partir d\'un url" >A partir d\'un url</option><option value="A partir d\'un emplacement">A partir d\'un emplacement</option><option value="A partir du system">A partir du system</option></select></td> --><!-- <td><input type="checkbox" id="check_create_date" name="check_create_date" /></td><td class="td_label"><label style="padding-left:5px;" for="check_create_date"   >Date de création : </label></td><td><input type="text" id="txt_create_date" name="create_date" /></td><td><input type="checkbox" id="check_create_date" name="check_create_date" /></td><td class="td_label"><label style="padding-left:5px;" for="check_create_date"   >Date de création : </label></td><td><input type="text" id="txt_create_date" name="create_date" /></td> --></tr><tr><!-- txt description --><td><input type="checkbox" id="check_description" name="check_description" /></td><td class="td_label"><label for="check_description"  >Description : </label></td><td><textarea name="textarea_description" rows="5" cols="40" id="txt_description" name="description"></textarea></td><!-- txt commentaire --><!-- <td><input type="checkbox" id="check_commentaire" name="check_commentaire" /></td><td class="td_label"><label for="check_commentaire" style="padding-left:5px;"  >Commentaire : </label></td><td><textarea name="textarea_commentaire" rows="3" cols="40" id="txt_commentaire" name="commentaire"></textarea></td> --><!-- txt document --><td class="td_label" colspan="2"><table><tr><td colspan="2"><label>Recherche dans document</label></td></tr><tr><td><input type="checkbox" id="check_document" name="check_document" /></td><td><label for="check_document" style="margin-left: 7px;" >Phrase exacte : </label></td></tr><tr><td><input type="checkbox" id="check_plusieur_mot" name="check_plusieur_mot" /></td><td><label for="check_plusieur_mot" style="margin-left: 7px;" >les mots suivants : </label></td></tr></table></td><td ><div class="div_content_document" ><div class="content_critere"><input type="text"  class="txtsearch"></div></div><textarea class="tous_les_mot" rows="5" cols="45"  name="textarea_document" id="txt_document" ></textarea><textarea  rows="5" cols="45"  name="textarea_document_plusieur_mot" id="textarea_document_plusieur_mot" style="display:none;"></textarea></td><td></td><td></td><td></td></tr></table><table style="width:100%"><!-- txt between date --><!--<tr><td><input type="checkbox" id="check_between_date" name="check_between_date" /></td><td><label for="check_between_date" >Date début : </label></td><td><input type="text" id="txt_date_1" name="start_date" /></td><td></td><td><label>Date fin : </label></td><td><input type="text" id="txt_date_2" name="end_date" /></td></tr>--></table></div></form></div><div class="content_table_liste_ir_attachment" style="width:97%;margin:0px auto"><table id="liste_ir_attachment" class="table table-striped table-bordered" cellspacing="0" ><thead><tr><th style="display:none">ID</th><th >Nom du contrat</th><th >Nom de la pièce jointe</th><th>Type de fichier</th><th>Date de création</th><th>Description</th></tr></thead><tbody></tbody></table></div>';
	
	
	$("body").on("click","#test",function(){
		var body = "";	
		$("#content_ged_search").html(content_html);
		body += "<tr><td>agrément</td><td>agrement.pdf</td><td>PDF</td><td>01/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de support</td><td>support.jpg</td><td>Image</td><td>01/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de travail</td><td>travail.pdf</td><td>PDF</td><td>03/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de maintenance</td><td>maintenance.pdf</td><td>PDF</td><td>02/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de test</td><td></td>test.xls<td>EXCEL</td><td>04/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de support</td><td><contrat.word/td><td>WORD</td><td>01/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de travail</td><td>travail.pdf</td><td>PDF</td><td>02/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de test</td><td>test.xls</td><td>EXCEL</td><td>014/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de travail</td><td>travail.word</td><td>WORD</td><td>05/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de travail</td><td></td>travail.pdf<td>PDF</td><td>03/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de travail</td><td></td>travail.xls<td>EXCEL</td><td>04/09/2018</td><td>** Aucune description</td></tr>";
		body += "<tr><td>Contrat de travail</td><td>travail.word</td><td>WORD</td><td>01/09/2018</td><td>** Aucune description</td></tr>";
		$("#content_ged_search").find("#liste_ir_attachment").find("tbody").html(body);
		//$('#liste_ir_attachment').DataTable();
			
		return false;
	});
	ds.call('mymod_getAll_document', [$("#form_critere").serializeArray()]).done(function(r) {
			//$('#form_search_document').attr('src',"http://41.143.254.148:8808/ged/"+r["uid"]+"er_7568491_ti_"+r["hash"]);
			result = r;
			$('#liste_ir_attachment').dataTable().fnDestroy();
				
			     $('#liste_ir_attachment').dataTable({
				  "data": result,
				   "columns": [
				   	    { "data": "id" ,className:"class_user_id"},
					    { "data": "affaire_id" },
					    { "data": "name_file" },
					    { "data":  "create_date" },
					    { "data": "description" }
				  ],
				  "paging": true,
				  "lengthChange": false,
				  "searching": false,
				  "ordering": true,
				  "info": true,
				  "autoWidth": false,
				  "iDisplayLength": 5,
				  "pagingType": "full_numbers",
				  "language": {
				  "info":"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
				    "paginate": {
				      "next": ">",
				      "previous": "<",
				      "first": "<<",
				      "last": ">>"
				    },

    				  "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
				}
				});	
		}) 
	// alert("vvvvvvvvvvvvvvvvv"+$("#content_ged_search").html());
	//document.getElementById('content_ged_search').innerHTML = content_html;
	$("#txt_dossier_num").prop( "disabled", true );
	$("#txt_name").prop( "disabled", true );
	$("#txt_create_date_du,#txt_create_date_au").prop( "disabled", true );
	$("#txt_description").prop( "disabled", true );
	$("#txt_commentaire").prop( "disabled", true );
	$("#txt_repertoire").prop( "disabled", true );
	$("#txt_partenaire").prop( "disabled", true );
	$("#txt_type_file").prop( "disabled", true );
	$("#txt_state").prop( "disabled", true );
	$("#txt_date_1").prop( "disabled", true );
	$("#txt_date_2").prop( "disabled", true );
	$("#txt_document").prop( "disabled", true );
	$("#txt_type").prop( "disabled", true );
	$("#txt_create_date_du,#txt_create_date_au").datepicker({
	changeMonth: true,
	changeYear: true,
	closeText: 'Fermer',
	prevText: 'Précédent',
	nextText: 'Suivant',
	currentText: 'Aujourd\'hui',
	monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
	monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
	dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
	dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
	dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
	weekHeader: 'Sem.',
	dateFormat: 'dd/mm/yy',
	beforeShow: customRange,
    //dateFormat: "dd M yy",
    firstDay: 1, 
    changeFirstDay: false,
	 onSelect: function(dateText, inst){
	 	getData();
	    }
    });
    function customRange(input) { 
    var min = new Date(2008, 11 - 1, 1), //Set this to your absolute minimum date
        dateMin = min,
        dateMax = null,
        dayRange = 6; // Set this to the range of days you want to restrict to

    if (input.id === "txt_create_date_du") {
    	$("#txt_create_date_au").prop( "disabled", false ).focus();
        if ($("#txt_create_date_au").datepicker("getDate") != null) {
            dateMax = $("#txt_create_date_au").datepicker("getDate");
            //dateMin = $("#txt_create_date_au").datepicker("getDate");
            //dateMin.setDate(dateMin.getDate() - dayRange);
            if (dateMin < min) {
                dateMin = min;
            }
        }
        else {
            dateMax = new Date; //Set this to your absolute maximum date
        }                      
    }
    else if (input.id === "txt_create_date_au") {
       // dateMax = new Date; //Set this to your absolute maximum date
        if ($("#txt_create_date_du").datepicker("getDate") != null) {
            dateMin = $("#txt_create_date_du").datepicker("getDate");
            //var rangeMax = new Date(dateMin.getFullYear(), dateMin.getMonth(),dateMin.getDate() + dayRange);
           // alert(rangeMax)
            // if(rangeMax < dateMax) {
            //     dateMax = rangeMax; 
            // }
        }
    }
    return {
        minDate: dateMin, 
        maxDate: dateMax
    };     
}

/////////////////////////////////////////////////////////////
//$(".txtsearch").height($(".div_content_document").height() - $(".content_critere").height());
$(".div_content_document").click(function(){
	$(".txtsearch").focus();
})
	var width_content = $(".content").width();
 // $(".txtsearch").css("width",width_content-25);
  $(".content_critere").on("click",".remove_critere",function(){
    var critere = 0;
    $(".critere").each(function(){
      critere += $(this).width();
    })
    var width_input = width_content - critere;
    //alert($(".critere").width())
    //alert(width_input)
    //alert($(this).parent().width())
    $(".txtsearch").css("width",$(".txtsearch").width() + $(this).parent().width()+25);
    $(this).parent().remove();
    $(".txtsearch").focus();
    remplir_textearea();
  })
  $(".content_critere").on("keyup",".txtsearch",function(e){
  	  
      var width_input_old = $(this).width();
      if(e.keyCode == 32){

        if($(this).val().match(/[a-zA-Z0-9]/)){
          var content = $(this).val();
          var span = '<span class="tag label label-primary critere"><label class="rr">'+$.trim(content)+'</label>';
              span += '<span class="glyphicon glyphicon-remove remove_critere" style="left: 5px"></span>';
              span += '</span>';
          $(".content_critere").append(span);
          $(this).remove();
          $(".content_critere").append('<input type="text"  class="txtsearch">');
          $(".txtsearch").focus();
          $(this).val("");
          
          var width_input = width_content - $(".content_critere").width();
          var width_critere = $(".content_critere .critere:first-child").width();
          $(this).css("width",$(this).width() - width_critere-14); 
          remplir_textearea();
          
        }

      }
      if(width_input_old < 20 ){
          $(this).css({"display":"block","width":448 - $(".content_critere .critere:first-child").width()-14});
          //$(".content_critere").css("display","block");
      }
      if(e.keyCode == 8){
          if($(this).val() == ""){
            $(".content_critere .critere").each(function(){
                if($(this).index() == $(".content_critere .critere").length-1){
                    $(this).remove();
                }
            });
            remplir_textearea();
          }
          //$(".txtsearch").height($(".div_content_document").height() - $(".content_critere").height());
      }

  	})

	function remplir_textearea(){
		var cont = "";
	        $(".critere").each(function(){

	          cont += $(this).find("label.rr").text()+ " ";
	        })
	        //alert(cont)
          $("#textarea_document_plusieur_mot").val(cont);
          getData();
	}

	$( ".content_table_liste_ir_attachment" ).on("click","#liste_ir_attachment tbody tr", function() {
		if($(this).find("td:eq(0)").attr("class") != "dataTables_empty"){
		  	var id = $(this).find("td:eq(0)").text();
		 	var lien = host+'/web?#id='+id+'&view_type=form&model=ir.attachment';
			
		 	window.open(lien,'_blank');
		}
	});
	function getData(){
		var ds = new openerp.web.DataSet(this, 'ged.recherche', {});
		ids = [parseInt($.bbq.getState().id)];
		var a = Array();
		ds.call('mymod_search_document', [$("#form_critere").serializeArray()]).done(function(r) {
			//$('#form_search_document').attr('src',"http://41.143.254.148:8808/ged/"+r["uid"]+"er_7568491_ti_"+r["hash"]);
			result = r;
			$.each( result[1], function( key, val ) {
			    if(val == 0){
			    	//alert(v)
			    	$(".content_critere .critere:eq("+key+")").find("label").css("cssText","text-decoration:line-through;color:black!important");
			    }
			 });
			$('#liste_ir_attachment').dataTable().fnDestroy();
				
			     $('#liste_ir_attachment').dataTable({
				  "data": result[0],
				   "columns": [
				   	    { "data": "id" ,className:"class_user_id"},
					    { "data": "affaire_id" },
					    { "data": "name_file" },
					    { "data":  "create_date" },
					    { "data": "description" }
				  ],
				  "paging": true,
				  "lengthChange": false,
				  "searching": false,
				  "ordering": true,
				  "info": true,
				  "autoWidth": false,
				  "iDisplayLength": 5,
				  "pagingType": "full_numbers",
				  "language": {
				  "info":"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
				    "paginate": {
				      "next": ">",
				      "previous": "<",
				      "first": "<<",
				      "last": ">>"
				    },

    				  "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
				}
				});	
			     if(result[0].length == 0){
			     	$("#liste_ir_attachment_info").text("Affichage de l'élement 0 à 0 sur 0 éléments")
			     }
			     $("#liste_ir_attachment").find("tr td.class_user_id").css("display","none");
		})  
	}
////////////////////////////////////////
// test checkbox
$("input").keyup(function(){
		if($(this).attr("class") != "txtsearch"){
			getData();
		}
			
	})
	$("textarea").keyup(function(){
		getData();
	})
	$("select").change(function(){
		getData();
	})
$('#check_document').change(function () {
	if($('input[name=check_document]').is(':checked')){
		$(".div_content_document").css({"display":"none","pointer-events":"none","background-color":"#EBEBE4"});
		$(".tous_les_mot").css("display","block");
		$('#check_plusieur_mot').prop('checked', false);
		$("#txt_document").prop( "disabled", false );
		$(".tous_les_mot").focus();
		$(".content_critere").find(".critere").each(function(){
			$(this).remove();
		})
	} else {
		$("#txt_document").prop( "disabled", true );
		$(".div_content_document").css("display","none");
		$(".tous_les_mot").css("display","block");
		$("#txt_document").val("");
		$("#textarea_document_plusieur_mot").val("");
	}	

});
$('#check_plusieur_mot').change(function () {
	//test();
	$(".div_content_document").css({"width":$("#txt_document").width()+20,"height":$("#txt_document").height()+12});
	if($('input[name=check_plusieur_mot]').is(':checked')){
		$(".div_content_document").css({"display":"block","pointer-events":"auto","background-color":"white"});
		$(".tous_les_mot").css("display","none");
		$('#check_document').prop('checked', false);
		$("#txt_document").prop( "disabled", false );
		$(".txtsearch").focus();
	} else {
		$(".div_content_document").css({"width":$("#txt_document").width()+20,"height":$("#txt_document").height()+12});
		$(".div_content_document").css({"display":"none","pointer-events":"none","background-color":"#EBEBE4"});
		$(".tous_les_mot").css("display","block");
		$("#txt_document").prop( "disabled", true );
		$("#txt_document").val("");
		$("#textarea_document_plusieur_mot").val("");
		$(".content_critere").find(".critere").each(function(){
			$(this).remove();
		})

	}	

});

$("input[type='checkbox']").change(function(){
	if($(this).attr("id") != "check_create_date"){
		if($(this).is(":checked")){
			$(this).parent().next().next().find("input[type='text']").prop( "disabled", false );
			$(this).parent().next().next().find("input[type='text']").focus();
			$(this).parent().next().next().find("textarea").prop( "disabled", false );
			$(this).parent().next().next().find("textarea").focus();
			$(this).parent().next().next().find("select").prop( "disabled", false );
			$(this).parent().next().next().find("select").focus();
			getData();
		}else{
			$(this).parent().next().next().find("select").find("option:eq(0)").attr("selected","selected");
			$(this).parent().next().next().find("input[type='text']").val("").prop( "disabled", true );
			$(this).parent().next().next().find("textarea").val("").prop( "disabled", true );
			$(this).parent().next().next().find("select").prop( "disabled", true );
			getData();
		}
	}else{
		if($(this).is(":checked")){
			$("#txt_create_date_du").prop( "disabled", false ).focus();
		}else{
			$("#txt_create_date_du,#txt_create_date_au").val("");
			$("#txt_create_date_du,#txt_create_date_au").prop( "disabled", true );
			getData();
		}
	}
	
})
$('#liste_ir_attachment').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": false,
  "ordering": true,
  "info": true,
  "autoWidth": false,
  "iDisplayLength": 5,
  "pagingType": "full_numbers",
  "language": {
    "paginate": {
      "next": ">",
      "previous": "<",
      "first": "<<",
      "last": ">>"
    },
    "sinfo": "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
}
});
	
		//$(".oe_view_manager_header").remove();
		

		
		},
		serializeArray :function(){
		//var data = $("#form_critere").serializeArray():
		}
	});


}




