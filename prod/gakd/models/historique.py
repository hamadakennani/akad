# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import  ValidationError
from lxml import etree

class historique(models.Model):
    _name = 'gakd.historique'
    
    # @api.one
    # def _calc(self):
    #     print 'CAAAAAAAAAAAALc'
    #     print self.montant_init-self.montant_fin
    #     return  self.montant_init-self.montant_fin

    montant_init = fields.Float(string='Montant initiale')
    montant_fin =  fields.Float(string='Montant finale')
    chargeur =     fields.Many2one(comodel_name='gakd.chargeur')
    type_op = fields.Char(string='Type d\'opération')
    sous_chargeur = fields.Many2one(comodel_name='gakd.sous_chargeur')
    diff = fields.Float(string='Montant affecté')
    verse_validate= fields.Boolean(default=False)
    tres_validate=fields.Boolean(default=False)
    versement_id=fields.Many2one(comodel_name='gakd.versement')
    client_id = fields.Many2one(comodel_name='res.partner')
    moyen_de_paiement =  fields.Char(string='Moyen de paiement')
    detail_id=fields.Many2one('gakd.historique_ben_detail')
    # flag = fields.Boolean(string='',default=False)
    
    # @api.model
    # def create(self, values):
    #     if values['flag']==True:
    #         return super(historique, self).create(values)
    #     else :
    #         raise ValidationError("Vous n'avez pas le droit d'effectuer cette opération")


    def fields_view_get(self,view_id=None, view_type='form',toolbar=False, submenu=False):
        res = super(historique, self).fields_view_get( view_id=view_id, view_type=view_type,toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        if view_type == 'form' and [0==1]:
            for node_form in doc.xpath("//form"):
                node_form.set("create", 'false')
        res['arch'] = etree.tostring(doc)
        return res
        
    # @api.model
    # def default_get(self, fields):
	# 	res = super(historique, self).default_get(fields)     
	# 	if  self.env.user.has_group('base.group_system'):
	# 		return res
	# 	else :
	# 		raise ValidationError("Vous n'avez pas le droit d'effectuer cette opération")


class historique_ben_detail(models.Model):
    _name="gakd.historique_ben_detail"
    _rec_name='type_de_recharge'
    tva_init=fields.Float(string='')
    montant_bonus=fields.Float(string='')
    prix_ht=fields.Float(string='')
    prix_init=fields.Float(string='')
    qte = fields.Float(string='')
    montant_hor_taxes = fields.Float(string='')
    tva = fields.Float(string='')
    montant_ttc = fields.Float(string='')
    montant_a_recharge_remise = fields.Float(string='')
    type_de_recharge = fields.Char(string='Produit')
    historique_id = fields.Many2one(comodel_name='detail_id', string='')
    
        
