# -*- coding: utf-8 -*-

# Part of Probuse Consulting Service Pvt Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': 'Account Partner Ledger Excel Report',
    'version': '1.1',
    'price': 80.0,
    'currency': 'EUR',
    'live_test_url': 'https://youtu.be/zUZmFGm8VRE',
    'license': 'Other proprietary',
    'category': 'Accounting',
    'summary': 'Excel report for Partner ledger print',
    'description': """        
     You can install xlwt library in following links 
     https://pypi.python.org/pypi/xlwt
     
Tags:
Account partner ledger
Account partner ledger Excel
excel reports
accounting reports
account finance report
PL report
partner ledger report in excel
report in excel
odoo community report
odoo community accounting reports
community accounting reports
financial reports community

""",
    'author': 'Probuse Consulting Service Pvt. Ltd.',
    'website': 'www.probuse.com',
    'depends': ['account'],
    'data': [
            'security/ir.model.access.csv',
            'wizard/account_financial_report_view.xml',
             ],
    'installable': True,
    'application': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
