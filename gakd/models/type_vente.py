# -*- coding: utf-8 -*-
from odoo import api, fields, models
from openerp.exceptions import ValidationError

class TypeVente(models.Model):  
    _name = 'gakd.type_vente'
    pointvente = fields.Many2one("gakd.point.vente",string="Point de vente")
    agent = fields.Many2one("gakd.agent",string="Agent")
    product = fields.Many2one("product.product",string="Produit",domain=[('type','!=','service')])
    categ_product = fields.Many2one("product.category",string="Catégorie du produit")
    type_vente = fields.Selection([('Vente au comptant','Vente au comptant'),('Vente par JNP PASS','Vente par JNP PASS'),('Vente par TV','Vente par TV'),('Vente par MOMO PAY','Vente par MOMO PAY'),('Vente par carte tampo','Vente par carte tampo'),('Vente par MOOV PAY','Vente par MOOV PAY')] , string="Type de vente",default="Vente par carte tampo")
    datedebut = fields.Datetime(string="Du" )
    datefin = fields.Datetime(string="Au" )
    trandetail =fields.One2many('t.detail',"typevente" )
    tranvide =fields.One2many('t.vide',"typevente" )
    nombreclique=fields.Integer(default=0)
    def imprimer1(self):
        if self.datedebut!=False and self.datefin!=False :
            return self.env['report'].get_action(self,'gakd.imprimer_transaction_repport')
        else :
            raise ValidationError("Il faut saisir la date de debut et la date de fin ")

    def remplir1(self) :
        data1=[]
        if self.type_vente!=False :
            chaine1="select sum(montant),sum(gakd_carte_consommation.quantite),count(id) from gakd_carte_consommation where type_vente='"+str(self.type_vente)+"'"
            chaine2="select sum(gakd_carte_consommation.montant),sum(gakd_carte_consommation.quantite),count(l.transaction_id) from gakd_carte_consommation, gakd_transaction_line l  where l.transaction_id=gakd_carte_consommation.id and l.type_vente='"+str(self.type_vente)+"' and l.transaction_id not in (select distinct id from gakd_carte_consommation where type_vente='"+str(self.type_vente)+"')"
            if self.pointvente.id!=False :
                chaine1+=" and point_vente_id="+str(self.pointvente.id)
                chaine2+=" and point_vente_id="+str(self.pointvente.id)
            if self.product.id!=False : 
                chaine1+=" and product_ids="+str(self.product.id)
                chaine2+=" and product_ids="+str(self.product.id)
            if self.agent.id!=False : 
                chaine1+=" and agent_id="+str(self.agent.id)
                chaine2+=" and agent_id="+str(self.agent.id)
            if self.datedebut!=False and self.datefin!=False :
                chaine1+=" and create_date > '"+str(self.datedebut)+"' and create_date < '"+str(self.datefin)+"'"
                chaine2+=" and l.create_date > '"+str(self.datedebut)+"' and l.create_date < '"+str(self.datefin)+"'"
            if self.categ_product.id!=False  :
                chaine1+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
                chaine2+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
            self._cr.execute(chaine1)   
            quantite=0
            montant=0.0
            lent=0
            for rec in self._cr.fetchall():
                if rec[1]!=None:
                    quantite+=rec[1]
                    montant+=rec[0]
                    lent+=rec[2]
            self._cr.execute(chaine2)
            for rec in self._cr.fetchall():
                if rec[1]!=None:
                    quantite+=rec[1]
                    montant+=rec[0]
                    lent+=rec[2]
            data1.append((0,0,{'nbtransaction':lent,'quantite':quantite,'montant':montant}))
        if self.type_vente==False :
            chaine1="select sum(montant),sum(gakd_carte_consommation.quantite),count(id) from gakd_carte_consommation where 1=1"
            chaine2="select sum(gakd_carte_consommation.montant),sum(gakd_carte_consommation.quantite),count(l.transaction_id) from gakd_carte_consommation, gakd_transaction_line l  where l.transaction_id=gakd_carte_consommation.id and l.transaction_id not in (select distinct id from gakd_carte_consommation )"
            if self.pointvente.id!=False :
                chaine1+=" and point_vente_id="+str(self.pointvente.id)
                chaine2+=" and point_vente_id="+str(self.pointvente.id)
            if self.product.id!=False : 
                chaine1+=" and product_ids="+str(self.product.id)
                chaine2+=" and product_ids="+str(self.product.id)
            if self.agent.id!=False : 
                chaine1+=" and agent_id="+str(self.agent.id)
                chaine2+=" and agent_id="+str(self.agent.id)
            if self.datedebut!=False and self.datefin!=False :
                chaine1+=" and create_date > '"+str(self.datedebut)+"' and create_date < '"+str(self.datefin)+"'"
                chaine2+=" and l.create_date > '"+str(self.datedebut)+"' and l.create_date < '"+str(self.datefin)+"'"
            if self.categ_product.id!=False  :
                chaine1+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
                chaine2+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
            self._cr.execute(chaine1)   
            quantite=0
            montant=0.0
            lent=0
            for rec in self._cr.fetchall():
                if rec[1]!=None:
                    quantite+=rec[1]
                    montant+=rec[0]
                    lent+=rec[2]
            self._cr.execute(chaine2)
            for rec in self._cr.fetchall():
                if rec[1]!=None:
                    quantite+=rec[1]
                    montant+=rec[0]
                    lent+=rec[2]
            data1.append((0,0,{'nbtransaction':lent,'quantite':quantite,'montant':montant}))
        return data1

    def remplir(self) :
        data=[]
        
        if self.type_vente!=False :
            chaine3 ="select point_vente_id,agent_id,product_ids,quantite,montant,create_date,id from gakd_carte_consommation where type_vente='"+str(self.type_vente)+"'"
            chaine4="select gakd_carte_consommation.point_vente_id,gakd_carte_consommation.agent_id,gakd_carte_consommation.product_ids,gakd_carte_consommation.quantite,gakd_carte_consommation.montant,gakd_carte_consommation.create_date,gakd_transaction_line.transaction_id from gakd_carte_consommation, gakd_transaction_line  where  gakd_transaction_line.type_vente='"+str(self.type_vente)+"'and gakd_transaction_line.transaction_id=gakd_carte_consommation.id and gakd_transaction_line.transaction_id not in (select distinct id from gakd_carte_consommation where type_vente='"+str(self.type_vente)+"')"
            if self.pointvente.id!=False :
                chaine3+=" and point_vente_id="+str(self.pointvente.id)
                chaine4+=" and point_vente_id="+str(self.pointvente.id)
            if self.product.id!=False : 
                chaine3+=" and product_ids="+str(self.product.id)
                chaine4+=" and product_ids="+str(self.product.id)
            if self.agent.id!=False : 
                chaine3+=" and agent_id="+str(self.agent.id)
                chaine4+=" and agent_id="+str(self.agent.id)
            if self.datedebut!=False and self.datefin!=False :
                chaine3+=" and create_date > '"+str(self.datedebut)+"' and create_date < '"+str(self.datefin)+"'"
                chaine4+=" and gakd_transaction_line.create_date > '"+str(self.datedebut)+"' and gakd_transaction_line.create_date < '"+str(self.datefin)+"'"
            if self.categ_product.id!=False  :
                chaine3+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
                chaine4+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
            chaine3+=" LIMIT 40 OFFSET "+str(self.nombreclique)
            chaine4+=" LIMIT 40 OFFSET "+str(self.nombreclique) 
        
            self._cr.execute(chaine3)  
            for rec in self._cr.fetchall():
                data.append((0,0,{'point_vente_id':rec[0],'agent_id':rec[1],'product_ids':rec[2],'quantite':rec[3],'montant':rec[4],'date_creation':rec[5],'transaction_id':rec[6]}))
            self._cr.execute(chaine4)  
            print chaine4
            for rec in self._cr.fetchall():
                print 11111144444
                data.append((0,0,{'point_vente_id':rec[0],'agent_id':rec[1],'product_ids':rec[2],'quantite':rec[3],'montant':rec[4],'date_creation':rec[5],'transaction_id':rec[6]}))
        if self.type_vente==False :
            chaine3 ="select point_vente_id,agent_id,product_ids,quantite,montant,create_date,id from gakd_carte_consommation where 1=1 "
            chaine4="select gakd_carte_consommation.point_vente_id,gakd_carte_consommation.agent_id,gakd_carte_consommation.product_ids,gakd_carte_consommation.quantite,gakd_carte_consommation.montant,gakd_carte_consommation.create_date,gakd_transaction_line.transaction_id from gakd_carte_consommation, gakd_transaction_line  where gakd_transaction_line.transaction_id=gakd_carte_consommation.id and gakd_transaction_line.transaction_id not in (select distinct id from gakd_carte_consommation) "
            if self.pointvente.id!=False :
                chaine3+=" and point_vente_id="+str(self.pointvente.id)
                chaine4+=" and point_vente_id="+str(self.pointvente.id)
            if self.product.id!=False : 
                chaine3+=" and product_ids="+str(self.product.id)
                chaine4+=" and product_ids="+str(self.product.id)
            if self.agent.id!=False : 
                chaine3+=" and agent_id="+str(self.agent.id)
                chaine4+=" and agent_id="+str(self.agent.id)
            if self.datedebut!=False and self.datefin!=False :
                chaine3+=" and create_date > '"+str(self.datedebut)+"' and create_date < '"+str(self.datefin)+"'"
                chaine4+=" and gakd_transaction_line.create_date > '"+str(self.datedebut)+"' and gakd_transaction_line.create_date < '"+str(self.datefin)+"'"
            if self.categ_product.id!=False  :
                chaine3+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
                chaine4+=" and product_ids in (select id from product_template where categ_id="+str(self.categ_product.id)+")"
            chaine3+=" LIMIT 40 OFFSET "+str(self.nombreclique)
            chaine4+=" LIMIT 40 OFFSET "+str(self.nombreclique) 
            self._cr.execute(chaine3)  
            for rec in self._cr.fetchall():
                data.append((0,0,{'point_vente_id':rec[0],'agent_id':rec[1],'product_ids':rec[2],'quantite':rec[3],'montant':rec[4],'date_creation':rec[5],'transaction_id':rec[6]}))
            self._cr.execute(chaine4)  
            for rec in self._cr.fetchall():
                data.append((0,0,{'point_vente_id':rec[0],'agent_id':rec[1],'product_ids':rec[2],'quantite':rec[3],'montant':rec[4],'date_creation':rec[5],'transaction_id':rec[6]}))
        self.update({
            'trandetail':[(6,0,[])],
        })
        self.sudo().update({
            'trandetail': data,
        })
    # return {'value':{'trandetail':data}}
    @api.one
    def suivantt(self):
        self.nombreclique=0
        if self.nombreclique>=40:
            self.nombreclique+=40
        else :
            self.nombreclique+=40
        self.remplir()
    @api.one
    def precedentt(self):
        if self.nombreclique>=40:
            self.nombreclique-=40
        self.remplir()

