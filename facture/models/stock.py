# -*- coding: utf-8 -*-

from odoo import api, fields, models
from openerp.exceptions import ValidationError
# from urllib.request import urlopen
import requests
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta


# import ApiError
class facture(models.Model):

    _inherit='account.invoice'

    @api.one
    def action_invoice_open(self):
        sale_id=0
        for linne in self.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        so=self.env['sale.order'].search([('id','=',sale_id)])  
        test1="Sénégal"
        test2=test1.decode('utf-8') 
        for s in so.order_line:
            if s.product_id.name!="Gasoil a 15" and "Other" not in s.product_id.categ_id.name and "Fret" not in  s.product_id.categ_id.name and "Lubricants" not in s.product_id.categ_id.name and test2 not in so.stock.name and "DAKAR" not in so.stock.name : 
                if len(self.facture)==0:
                    print '1 1'
                    raise ValidationError("Attention : Il faudrait completer le Détail CIF ! ")   
                else:
                    test=0
                    for factures in self.facture :
                        if factures.name!='FRAIS DE PASSAGE' and factures.name!='TVA':
                            if factures.euro== 0:
                                test=1
                                print '1 2'
                                raise ValidationError("Attention : Une ou plusieurs informations du Détail CIF sont laissées vides, veuillez les renseigner ")
                    if test==0:
                        for fac in self.facture :
                            if fac.name=='CIF':
                                if fac.euro !=so.amount_total :
                                    b="%.2f" % (float(fac.euro)-float("%.2f" % (so.amount_total)))
                                    raise ValidationError("Il y a une différence entre le montant du CIF et le montant de la commande de : "+str(b))
                        super(facture,self).action_invoice_open()
                        self.write({
                            'test3':True
                        })     
        for s in so.order_line:
            test1="Sénégal"
            test2=test1.decode('utf-8')
            if (("DAKAR" in so.stock.name or test2 in so.stock.name) and s.product_id.name=="Gasoil a 15" ) or "Fret" in s.product_id.categ_id.name or  "Lubricants" in s.product_id.categ_id.name or "Other" in s.product_id.categ_id.name:                
                if len(self.facture2)==0:
                    print '2 1'
                    raise ValidationError("Attention : Il faudrait completer le Détail CIF !  ")   
                else:
                    test=0
                    for factures in self.facture2 :
                        if  "Lubricants" in s.product_id.categ_id.name or "Other" in s.product_id.categ_id.name :
                            if factures.name!='FRET':
                                if factures.euro== 0:                          
                                    test=1
                                    raise ValidationError("Attention : Une ou plusieurs informations du Détail CIF sont laissées vides, veuillez les renseigner  ")
                        if  "Lubricants" not in s.product_id.categ_id.name and  "Other" not in s.product_id.categ_id.name :
                            if factures.euro== 0:                          
                                test=1
                                raise ValidationError("Attention : Une ou plusieurs informations du Détail CIF sont laissées vides, veuillez les renseigner  ")
                                
                    if test==0:
                        for fac in self.facture2 :
                            if fac.name=='CIF':
                                if fac.euro !=so.amount_total :
                                    b="%.2f" % (float(fac.euro)-float("%.2f" % (so.amount_total)))
                                    raise ValidationError("Il y a une différence entre le montant du CIF et le montant de la commande de : "+str(b))
                                else :
                                    super(facture,self).action_invoice_open()
                                    self.write({
                                        'test3':True
                                    })

    # on teste si les depots et les produits qu on a sur la commande sont les meme qu on sur la configuration des factures pour afficher une page de Details CIF
    @api.one
    @api.depends('compute', 'facture')
    def chercher_facture(self):
        bool1=False
        bool2=False
        cf=self.env['configuration.facture'].search([])
        sale_id=0
        
        for linne in self.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        so=self.env['sale.order'].search([('id','=',sale_id)])   
        stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
        for p in so.order_line:
            for a in cf :
                for ab in a.listeproduit :
                    if(p.product_id.name==ab.name):
                        bool1=True 
                for ac in a.listedepot :
                    test1=ac.name
                    test2=test1.encode('utf-8')
                    test3=so.stock.name
                    test4=test3.encode('utf-8')                  
                    if(test4==test2):
                        bool2=True
        if(bool1==True and bool2==True):
            self.compute=True
   
    # on teste si le depot qu on a sur la commande est Senegal et le produit qu on a sur la commande est Gasoil a 15 pour afficher une page qui contient le FRET et le CIF 
  
    @api.one
    @api.depends('compute2','facture2')
    def fret(self):
        sale_id=0
        for linne in self.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        so=self.env['sale.order'].search([('id','=',sale_id)])   
        stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
        for s in so.order_line:
            print 's.product_id.name'
            print s.product_id.name
            if s.product_id.name=="Gasoil a 15" :
                
                test1="Sénégal"
                test2=test1.decode('utf-8')
                if test2 in so.stock.name or "DAKAR" in so.stock.name :
                    self.compute2=True
            if "Lubricants" in s.product_id.categ_id.name or "Other" in s.product_id.categ_id.name:
                self.compute2=True
    compute2=fields.Boolean(compute='fret')          
    compute=fields.Boolean(compute='chercher_facture')
    facture=fields.One2many('facture.liste','factures')
    facture2=fields.One2many('facture.liste','factures2')
    test=fields.Boolean(default=False)
    test2=fields.Boolean(default=False)
    test3=fields.Boolean(default=False)

    # dans ce code l'on créé les lignes de la page qui contient seulement le FRET et le CIF 
    @api.one
    def actu(self) :
        self.env["facture.liste"].sudo().create({
            "name":'Produit(s)',
            "euro":0,
            "ead":0,
            "factures2":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'FRET',
            "euro":0,
            "ead":0,
            "factures2":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'CIF',
            "euro":0,
            "ead":0,
            "factures2":self.id
        })
        self.write({
            'test2':True, 
        })
    # dans ce code l'on créé les lignes de la page Details CIF
    @api.one
    def actualisation(self) :
        self.env["facture.liste"].sudo().create({
            "name":'FOB',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'FRET',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'ASSURANCE',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'AUTRE FRAIS',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'TVA',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'FRAIS DE PASSAGE',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.env["facture.liste"].sudo().create({
            "name":'CIF',
            "euro":0,
            "ead":0,
            "factures":self.id
        })
        self.write({
            'test':True
        })
    # cette fonction permet de changer a partir d EURO au AED selon la date de livrison et la date qui se trouve dans le model currency en cas de FRET et CIF
    @api.onchange('facture2')
    def onchange_fret(self):   
        totalEad=0
        totalE=0
        for factures in self.facture2:
            if factures.name != 'CIF' :
                sale_id=0
                for linne in self.invoice_line_ids :
                    for line in linne.sale_line_ids:
                        sale_id = line.order_id.id
                        break
                    break
                stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
                currency=self.env['res.currency'].search([('name','=','EUR')])
                currencyRate=self.env['res.currency.rate'].search([('currency_id','=',currency[0].id)])
                date1=stock.min_date
                date1_obj = datetime.strptime(date1, "%Y-%m-%d %H:%M:%S")
                d1=date1_obj.strftime("%Y-%m-%d") 
                mini=0
                nouveau =0  
                test =0
                cas1=0
                cas2=0
                date3=datetime.now()
                for curr in currencyRate :
                    date2=curr.name
                    date2_obj = datetime.strptime(date2, "%Y-%m-%d %H:%M:%S")
                    d2=date2_obj.strftime("%Y-%m-%d") 
                    # on fait une différence entre la date de liv et la date de curr
                    rd = relativedelta(datetime.strptime(d1,"%Y-%m-%d"),datetime.strptime(d2,"%Y-%m-%d"))
                    # si la diff est negatif on affiche un message  
                    if rd.days < 0:
                        if cas2==0:
                            cas1+=1
                            if cas1==len(currencyRate):
                                raise ValidationError("Veuillez parametrer dans le système le taux de conversion des devises ")   
                   # si la diff est egale 0  
                    elif  rd.days == 0 :
                        factures.ead=factures.euro/curr.rate                 
                        cas2=1
                        factures.write({
                            'ead':factures.ead,    
                        })
                        totalEad+=factures.ead
                        totalE+=factures.euro
                        break 
                    # si la diff est egale 0    
                    else :
                        if cas2==0:
                            if test == 0 :
                                mini=rd.days
                                test=1
                            nouveau=rd.days
                            if nouveau <= mini:
                                mini=rd.days
                                date3=date2
                                factures.ead=factures.euro/curr.rate
                                factures.write({
                                'ead':factures.ead,    
                                })
                                totalEad+=factures.ead
                                totalE+=factures.euro
                                break
            if factures.name=='CIF':
                factures.write({
                    'ead':totalEad,
                    'euro':totalE,
                })
        return {}
   # cette fonction permet de changer a partir d EURO au AED selon la date de livrison et la date qui se trouve dans le model currency en cas de detais CIF
    @api.onchange('facture')
    def onchange_fob(self):
        totalEad=0
        totalE=0
        for factures in self.facture:
            if factures.name != 'CIF' :
                sale_id=0
                for linne in self.invoice_line_ids :
                    for line in linne.sale_line_ids:
                        sale_id = line.order_id.id
                        break
                    break
                stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
                currency=self.env['res.currency'].search([('name','=','EUR')])
                currencyRate=self.env['res.currency.rate'].search([('currency_id','=',currency[0].id)])
                date1=stock.min_date
                date1_obj = datetime.strptime(date1, "%Y-%m-%d %H:%M:%S")
                d1=date1_obj.strftime("%Y-%m-%d") 
                mini=0
                nouveau =0  
                test =0
                cas1=0
                cas2=0
                date3=datetime.now()
                for curr in currencyRate :
                    date2=curr.name
                    date2_obj = datetime.strptime(date2, "%Y-%m-%d %H:%M:%S")
                    d2=date2_obj.strftime("%Y-%m-%d") 
                    rd = relativedelta(datetime.strptime(d1,"%Y-%m-%d"),datetime.strptime(d2,"%Y-%m-%d"))
                    if rd.days < 0:
                        if cas2==0:
                            cas1+=1
                            if cas1==len(currencyRate):
                                raise ValidationError("Veuillez parametrer dans le système le taux de conversion des devises")   
                    elif  rd.days == 0 :
                        # factures.ead=factures.euro/curr.rate                 
                        cas2=1
                        factures.write({
                            'ead':factures.euro/curr.rate ,    
                        })
                        totalEad+=factures.ead
                        totalE+=factures.euro
                        break   
                    else :
                        if cas2==0:
                            if test == 0 :
                                mini=rd.days
                                test=1
                            nouveau=rd.days
                            if nouveau <= mini:
                                mini=rd.days
                                date3=date2
                                # factures.ead=factures.euro/curr.rate
                                factures.write({
                                'ead':factures.euro/curr.rate,    
                                })
                                totalEad+=factures.euro/curr.rate
                                totalE+=factures.euro
                                print 'eeeeeeeeeeeeeeeeeeeeeeennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn'
                                print totalEad
                                print totalE
                                break 
            if factures.name=='CIF':
                factures.write({
                    'ead':totalEad,
                    'euro':totalE,
                })
        return {}
    # on check si le montant CIF est diff de montant de la commande 
    @api.constrains('facture','facture2')
    def _check_CIF(self):
        for linne in self.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        so=self.env['sale.order'].search([('id','=',sale_id)])
        test1="Sénégal"
        test2=test1.decode('utf-8') 
        for s in so.order_line:
            if s.product_id.name!="Gasoil a 15" and "Fret" not in  s.product_id.categ_id.name and "Lubricants" not in s.product_id.categ_id.name and "Other" not in s.product_id.categ_id.name and test2 not in so.stock.name and "DAKAR" not in so.stock.name :   
                for fac in self.facture :
                    if fac.name=='CIF':
                        if fac.euro !=so.amount_total :
                            b="%.2f" % (float(fac.euro)-float("%.2f" % (so.amount_total)))
                            raise ValidationError("Il y a une différence entre le montant du CIF et le montant de la commande de : "+str(b))
        for s in so.order_line:
            test1="Sénégal"
            test2=test1.decode('utf-8')
            if (("DAKAR" in so.stock.name or test2 in so.stock.name) and s.product_id.name=="Gasoil a 15" ) or "Fret" not in  s.product_id.categ_id.name or  "Lubricants" in s.product_id.categ_id.name  or "Other" in s.product_id.categ_id.name :              
                for fac in self.facture2 :
                    if fac.name=='CIF':
                        if fac.euro !=so.amount_total :
                            b="%.2f" % (float(fac.euro)-float("%.2f" % (so.amount_total)))
                            raise ValidationError("Il y a une différence entre le montant du CIF et le montant de la commande de : "+str(b))
            


        