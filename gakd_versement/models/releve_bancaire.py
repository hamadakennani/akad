# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from dateutil.parser import parse
import dateparser
import datetime
import string
import re
from unidecode import unidecode

class Testreleves(models.Model):
    _name = 'gakd.releve_bancaire' 
    _rec_name='reference'

    reference=fields.Char(string='Référence')
    ref = fields.Char(string="Réf")
    date_operation=fields.Date(string='Date de l\'opération')
    date_releve=fields.Char(string='Date releve')
    montant=fields.Char(string='Montant' ,default='0.0')
    state = fields.Selection(string='Etat',
        selection=[('annule', 'Annulé'),('brouillon', 'Brouillon'), ('valideAuto', 'Validé')],
        default='brouillon')
    etat_creation = fields.Selection(string='Etat de creation', selection=[('non', 'Non'),('oui', 'Oui')], default='non')
    libelle=fields.Char(string='Libellé')
    id_versement= fields.Integer()
    bank = fields.Selection([
        ('BAB Produits Blancs', 'BAB Produits Blancs'),
        ('BGFI Produits Blancs', 'BGFI Produits Blancs'),
        ('BOA PRODUITS BLANCS','BOA Produits Blancs'),
        ('BOA LUBRIFIANTS','BOA Lubrifiants'),
        ('BOA GAZ', 'BOA Gaz'),
        ('CASH COLLECTION', 'CASH COLLECTION'),
        ('Eco bank Probuits Blancs', 'ECO Produits Blancs'),
        ('Eco bank Lubrifiants', 'ECO Lubrifiants'),
        ('Eco bank GAZ','ECO Gaz'),
        ('ORABANK PRODUITS BLANCS','ORA Produits Blancs'),
        ('ORABANK LUBRIFIANTS', 'ORA Lubrifiants'),
        ('ORABANK MAINTENANCE', 'ORA Maintenance'),
        ('ORABANK GAZ', 'ORA Gaz'),
        ('BANQUE SOCIETE GENERALE PRODUITS BLANCS', 'SGB Produits Blancs'),
        ('BANQUE SOCIETE GENERALE GAZ', 'SGB Gaz'),
        ], string="banque", required=True)
    day_of_week = fields.Char(string="Jour de semaine")
    ecarts = fields.Float(string="Ecart" , default=0)
    categorie_produit = fields.Selection(
        [('', ''), ('Lubrifiants', 'Lubrifiants'), ('Produits blancs', 'Produits blancs'),('Gaz et accessoire', 'Gaz et accessoire'),
        ('Lavage', 'Lavage'),('CASH COLLECTION', 'CASH COLLECTION')],
        string="Catégorie des produits",
        default=''
    )
    
    #--------- converter la date fr au ang
    def convertDateReleveBancaire(self,dtr,banke):
        #list banques
        boa = ['BOA PRODUITS BLANCS', 'BOA LUBRIFIANTS', 'BOA GAZ']
        ecobank = ['Eco bank Probuits Blancs','Eco bank Lubrifiants', 'Eco bank GAZ']
        ora = ['ORABANK PRODUITS BLANCS', 'ORABANK LUBRIFIANTS','ORABANK GAZ', 'ORABANK MAINTENANCE']
        sgb = ['BANQUE SOCIETE GENERALE PRODUITS BLANCS','BANQUE SOCIETE GENERALE GAZ']

        if dtr :
            date=None
            if u'é' in dtr:
                dtr = dtr.replace(u"é","e")
            elif u'û' in dtr:
                dtr = dtr.replace(u"û", "u")
            if banke == 'BGFI Produits Blancs' or banke in ora or banke in boa or banke in sgb:                
                try:
                    td=dtr.split('/')
                    if td[0].isdigit():
                        if banke in boa:
                            td[2]='20'+td[2]
                        date=datetime.date(day=int(td[0]),month=int(td[1]),year=int(td[2]))
                except:
                    print "error the date "
            elif banke == 'CASH COLLECTION':
                try:
                    dtr=dtr.replace('/','-')
                    dtr=dtr.replace(' ','-')
                    td=dtr.split('-')
                    date=datetime.date(day=int(td[2]),month=int(td[1]),year=int(td[0]))
                except:
                    print "error the date"
            elif banke == 'BAB Produits Blancs' or banke in ecobank:
                try:
                    td=parse(dtr)
                    date=datetime.datetime.strptime(str(td), '%Y-%m-%d %H:%M:%S')
                    date=date.date()
                except:
                    try:
                        dt=dateparser.parse(dtr)
                        date=datetime.datetime.strptime(str(dt), '%Y-%m-%d %H:%M:%S')
                        date=date.date()
                    except:
                        print 'erreur'
                    

            return date
        return None

    @api.model
    def create(self, vals):
        print 'RRRRRRRRR============================================='
        #list banques
        ecobank = ['Eco bank Probuits Blancs','Eco bank Lubrifiants', 'Eco bank GAZ']
        dateChange=self.convertDateReleveBancaire(vals.get('date_releve',False),vals.get('bank',False))
        vals['date_operation']=dateChange

        montantChange=vals.get('montant',False)
        montantChange=str(montantChange).replace(',','')
        montantChange=montantChange.replace(' ','')
        vals['montant']=montantChange
        if dateChange:
            # "Lundi Mardi Mercredi Jeudi Vendredi Samedi Dimanche"
            week_days = ("Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche")
            
            vals['day_of_week'] = week_days[dateChange.weekday()]
        if vals.get('bank',False) not in ecobank:
            if 'reference' in vals.keys() and vals.get('reference',False) :
                splitText = re.findall(r'\d+',str(vals.get('reference',"")))
                if len(splitText) > 0:
                    vals['ref'] = splitText[len(splitText) - 1]
        else :
            if 'libelle' in vals.keys() :
                if vals.get('libelle',"") :
                    c=u'°'
                    libel=vals.get('libelle',"").replace(c,'')
                    splitText = re.findall(r'\d+',libel.replace(' ',''))
                    if len(splitText) > 0:
                        vals['ref'] = splitText[0]
        if vals['date_operation'] and vals['montant'] and vals['montant']!='None':
            return super(Testreleves,self).create(vals)
        return
