# -*- coding: utf-8 -*-
from odoo import fields, api, models

from odoo.exceptions import ValidationError
from funs import date_dure_cul
import datetime
from datetime import date, datetime, timedelta
import dateutil.relativedelta
from dateutil.relativedelta import relativedelta
import math

class ConditionsFinancieres (models.Model):
    _name = 'capcontrat.contrat.conditionsfinancieres'
    _rec_name = 'libelle'
    monnaie="DH"
    taxe="TTC"
    
    @api.one
    @api.depends("fin_theorique","echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence","contrat_id")
    def _get_echeance_text(self):
      self.ensure_one()
      if self.echeance_type=="a une date fixe":
        if self.date_echeance:
          self.echeance_text=str(date_dure_cul(self.date_echeance,1,1,1).date())

      elif   self.echeance_type=="Recurrent":
       
           
                  if self.si_il_est_date_de_debut_de_recurrence and self.si_il_est_date_de_fin_de_recurrence:

                    if  self.date_de_debut_de_recurrence and not self.date_de_fin_de_recurrence:
                          self.echeance_text= self.echeance_text="Tous les "+self.repeter_type.replace("(","").replace(")","") +" à  partir du "+ self.date_de_debut_de_recurrence
                          return  self.echeance_text

                    elif  not self.date_de_debut_de_recurrence and  self.date_de_fin_de_recurrence:
                        if  self.contrat_id.Prise_effet_date_value:


                         self.echeance_text= self.echeance_text="Tous les "+self.repeter_type.replace("(","").replace(")","") +" du "+self.contrat_id.Prise_effet_date_value+" au "+self.date_de_fin_de_recurrence 
                         return  self.echeance_text
                      

                        
                       
                    elif  self.date_de_debut_de_recurrence and  self.date_de_fin_de_recurrence:
                        self.echeance_text="Tous les "+self.repeter_type.replace("(","").replace(")","") +" du "+ self.date_de_debut_de_recurrence+" au "+self.date_de_fin_de_recurrence 
                       
                        return self.echeance_text
                    
                  if self.contrat_id.fin_theorique:
                      self.echeance_text= str(date_dure_cul(self.contrat_id.fin_theorique,1,1,1).date())
                      return  self.echeance_text
    
    @api.one
    @api.depends("echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence")
    def _fin_duree(self):
      self.ensure_one()
      if self.echeance_type=="a une date fixe":
        if self.date_echeance:
          self.fin_theorique= date_dure_cul(self.date_echeance,1,1,1).date()

      elif   self.echeance_type=="Recurrent":
       
           
                  if self.si_il_est_date_de_fin_de_recurrence:

                    if   self.date_de_fin_de_recurrence:
                          self.fin_theorique= date_dure_cul(self.date_de_fin_de_recurrence,1,1,1).date()
                          return  self.fin_theorique

                  
                    
                  else:
                      
                       if self.contrat_id.fin_theorique:
                        self.fin_theorique=date_dure_cul(self.contrat_id.fin_theorique,1,1,1).date()
                       return  self.fin_theorique
      self._get_echeance_text()
      self._get_montant()
     

      
    @api.one
    @api.depends("echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence")
    def _date_debut(self):
      self.ensure_one()
      if self.si_il_est_date_de_debut_de_recurrence:
          if self.date_de_debut_de_recurrence:
            self.date_debut=self.date_de_debut_de_recurrence
            return self.date_debut
      else :
            self.date_debut=self.contrat_id.Signe_le
            return   self.date_debut
     

      
    # @api.one
    # @api.constrains('alertes_repeter','is_Alert',"echeance_type",'date_echeance','si_il_est_date_de_debut_de_recurrence','date_de_debut_de_recurrence','si_il_est_date_de_fin_de_recurrence',"date_de_fin_de_recurrence"
    # ,"repeter_int","repeter_type")
    # def _date_eval(self):
    #   if  self.is_Alert:
    #         echances=self.env['capcontrat.contrat.echeance'].search([("conditions_id","=",self.id)]).unlink()
    #         date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
    #         date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
    #         difference_date =date_f-date_d
    #         i=0
    #         if self.repeter_type=='Jour(s)':
             
    #           conunt=difference_date.days
    #           date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
             
    #           while(conunt>i):
              
    #            date_d=date_d+relativedelta(days=self.repeter_int)
    #            print  " les dates  days  ------------>>>>>>"
    #            print date_d
    #            if isinstance(self.id,(int,long)):
           
               
    #             print " creation de contrat "
           
    #             echeance =self.env['capcontrat.contrat.echeance'].create(
    #             {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
    #             'conditions_id':self.id})
    #             print "  -------- echeance a ete  created "
    #             print echeance
        
              
    #            i=i+1

              
             
          
              
    #         elif self.repeter_type=='Semaine(s)':
    #             date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
    #             date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
    #             difference_date =date_f-date_d
                
    #             conunt=difference_date.days/7
    #             date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
    #             while(conunt>i):
                
    #               date_d=date_d+relativedelta(weeks=self.repeter_int)
    #               print  " les dates  weeks   ------------>>>>>>"
    #               print date_d
    #               if isinstance(self.id,(int,long)):
                    
           
                   
    #                 print " creation de contrat "
    #                 self.env['capcontrat.contrat.echeance'].create(
    #                   {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
    #                    'conditions_id':self.id})
    #               i=i+1

    #         elif self.repeter_type=='Mois':
    #            date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
    #            date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
    #            difference_date =date_f-date_d
    #            conunt=difference_date.days/30
    #            date_d=datetime.strptime(str(self.date_debut), '%Y-%m-%d')

    #            while(conunt>i):
                
    #               date_d=date_d+relativedelta(months=self.repeter_int)
    #               print  " les dates  months   ------------>>>>>>"
    #               print date_d
    #               if isinstance(self.id,(int,long)):
           
                   
    #                 print " creation de contrat "
    #                 self.env['capcontrat.contrat.echeance'].create(
    #                   {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
    #                    'conditions_id':self.id})
    #               i=i+1

    #         elif self.repeter_type=='an(s)':
    #            date_d=datetime.strptime(self.date_debut, '%Y-%m-%d')
    #            date_f=datetime.strptime(self.fin_theorique, '%Y-%m-%d')
    #            difference_date =date_f-date_d
    #            conunt=difference_date.days/365
    #            date_d=datetime.strptime(str(self.date_debut), '%Y-%m-%d')
    #            while(conunt>i):
                
    #               date_d=date_d+relativedelta(years=self.repeter_int)
    #               print  " les dates  years   ------------>>>>>>"
    #               print date_d
    #               if isinstance(self.id,(int,long)):
    #                 print " creation de contrat "
                   
           
    #                 self.env['capcontrat.contrat.echeance'].create(
    #                   {"mailto":self.contrat_id.manage.id,'send_date':date_dure_cul(date_d.date(),0,self.alertes_repeter),"contrat_id":self.contrat_id.id,
    #                    'conditions_id':self.id})
    #               i=i+1

   
      
    def repeter_count(self,difference_date,Montant_init):
       if self.repeter_int:
            
            if self.repeter_type=='Jour(s)':
             
            
              self.montant=(math.floor(difference_date.days/self.repeter_int))*Montant_init
            
              return self.montant
             
            elif self.repeter_type=='Semaine(s)':
             
              
            
              weeks=(difference_date.days/7)
              
              self.montant=(math.floor(weeks/self.repeter_int))*Montant_init
              return self.montant
              
            elif self.repeter_type=='Mois':
             
              months=difference_date.days/30
              self.montant=(math.floor(months/self.repeter_int))*Montant_init
              return self.montant
             
            elif self.repeter_type=='an(s)':
                
                years=difference_date.days/365
                self.montant=(math.floor(years/self.repeter_int))*Montant_init
                return self.montant
               
    def montant_operations(self):

            Montant_init=0
            if self.type_montant=="Pourcentage":
             
              Montant_init=(self.int_montant_Pourcentage*self.int_Pourcentage)/100
                  
            elif self.type_montant=="Montant":
                 
               Montant_init=self.int_montant_cf
              
            if self.echeance_type=='a une date fixe':

             return  Montant_init
            elif self.date_debut and self.fin_theorique:
                date_d=datetime.strptime(str(self.date_debut), '%Y-%m-%d')
                date_f=datetime.strptime(str(self.fin_theorique), '%Y-%m-%d')
                difference_date =date_f-date_d
                
                return self.repeter_count(difference_date,Montant_init)
                       
    @api.one               
    @api.depends("type_montant",'date_de_debut_de_recurrence','date_de_fin_de_recurrence', 'repeter_int', 'repeter_type', 'int_montant_cf',
     'type_operastion')
    def _get_montant(self):
        self.ensure_one()
        if self.type_operastion=="a payer":
            # "******************** a payer ***************"
            res=self.montant_operations()
            res=res * -1.0
            print res * -1.0
            self.montant=res
            return  res


        elif self.type_operastion=="a percevoir":
            #  "******************** a percevoir ***************"
            self.montant=self.montant_operations()
            return self.montant_operations()
          

    
   # @api.onchange("type_montant","int_montant_cf","int_montant_Pourcentage","int_Pourcentage")
    @api.constrains("type_montant","int_montant_cf","int_montant_Pourcentage","int_Pourcentage")
    def date_chake(self):
        if self.type_montant=="Montant" and self.int_montant_cf<=0 :
        
           raise ValidationError("Conditions financiéres :Le montant doit être supérieur à zéro ")


        elif self.type_montant=="Pourcentage":
           if self.int_montant_Pourcentage<=0:
             raise ValidationError(" Conditions financiéres : Le Pourcentage doit être supérieur à zéro ") 
           if self.int_Pourcentage<=0:
             raise ValidationError("Conditions financiéres :Le Pourcentage doit être supérieur à zéro ") 
    
    def _get_devise(self):
      return self.contrat_id.devise
    def _get_text(self):
      return self.taxe
   
    

    libelle=fields.Char(string=u'Libellé', size=128, required=True)
    contrat_id=fields.Many2one('capcontrat.contrat')
    type_operastion=fields.Selection([("a payer","à payer"),("a percevoir","à percevoir")],default="a percevoir",string=u"Type d'opération")
    type_montant=fields.Selection([("Montant","Montant"),("Pourcentage","Pourcentage")],default="Montant",string="Montant")
    # depands  if  type_montant value = Montant
    int_montant_cf=fields.Integer( string="Montant",default=100)
    montant_monnaie=fields.Selection([("DH","DH"),("USD","USD"),("EUR","EUR")],default=lambda self: self.contrat_id.devise or "DH" )
    montant_taxe=fields.Selection([("HT","HT"),("TTC","TTC")],default=lambda self: self.contrat_id.taxe or "TTC")
    # depands  if  type_montant value = Pourcentage
    int_montant_Pourcentage=fields.Integer()
    int_Pourcentage=fields.Integer()
    #	Echéance 
   
    echeance_text=fields.Char(string=u"Echéance fin de contrat",compute='_get_echeance_text', store=True)
    fin_theorique=fields.Date(string=u"Fin théorique",compute=_fin_duree)
    date_debut=fields.Date(compute=_date_debut)
    
   
    echeance_type=fields.Selection([("a une date fixe","à une date fixe"),("Recurrent","Récurrente")],default="Recurrent",string="Echeance")
    # depands  if  echeance_type value = à une date fixe
    date_echeance=fields.Date(string="Date de échéance")
    # depands  if  echeance_type value = "Récurrent"
    si_il_est_date_de_debut_de_recurrence=fields.Boolean(string='Début de la récurrence',default=False)
    #if  si_il_est_date_de_debut_de_recurrence= True
    date_de_debut_de_recurrence=fields.Date(string="Date de début ",default=datetime.today())
    si_il_est_date_de_fin_de_recurrence=fields.Boolean(string=' Fin de la récurrence',default=False)
    #if si_il_est_date_de_fin_de_recurrence = true
    date_de_fin_de_recurrence=fields.Date(string=" Date de fin")
    @api.constrains("echeance_type","date_echeance","si_il_est_date_de_debut_de_recurrence","date_de_debut_de_recurrence","si_il_est_date_de_fin_de_recurrence","date_de_fin_de_recurrence")
    def _chack_echeance(self):
       if self.echeance_type=="a une date fixe":
         if not self.date_echeance:
           raise ValidationError("Conditions financiéres :  la date de échéance est Obligatoir ")
          # datetime.strptime(self.date_de_prise, '%Y-%m-%d')
         elif  self.contrat_id.Signe_le and datetime.strptime(self.date_echeance, '%Y-%m-%d')< datetime.strptime(self.contrat_id.Signe_le, '%Y-%m-%d'):
           raise ValidationError("Conditions financiéres :la date de échéance doit être supérieur à la date du signature")



       elif self.echeance_type=="Recurren":
           if self.si_il_est_date_de_debut_de_recurrence and not self.si_il_est_date_de_debut_de_recurrence:
             if not self.date_de_debut_de_recurrencenot:
               raise ValidationError("La date de Début de la récurrence est Obligatoir")
           if self.si_il_est_date_de_fin_de_recurrence and not self.si_il_est_date_de_fin_de_recurrence :
             if not  self.date_de_fin_de_recurrence :
                raise ValidationError("Conditions financiéres :  la date de Fin de la récurrence est obligatoir")
             elif   datetime.strptime(self.date_de_fin_de_recurrence, '%Y-%m-%d')< datetime.strptime(self.contrat_id.Signe_le, '%Y-%m-%d'):
                raise ValidationError("Conditions financiéres : la date de Fin de la récurrence doit être supérieur à la date du signature")

        


    repeter_int=fields.Integer(string="Répéter tous les ")
    repeter_type=fields.Selection([('Jour(s)','Jour(s)'),('Semaine(s)',' Semaine(s)'),('Mois','Mois')
    ,('an(s)','Année')],default='Mois')
    
    # fin de if 

    is_Alert=fields.Boolean(string=' Fin de la récurrence') 
    alertes_repeter=fields.Selection([(0,0),(1,1),(3,3),(7,7),(15,15),(30,30),(45,45),(90,90)],string="Alertes email",default=1)
    
    #montant
   
    montant=fields.Float(compute='_get_montant', store=True)
    @api.one
    @api.depends("montant_taxe","montant_monnaie")
    def _get_montant_str(self):
      self.ensure_one()
      text= str(self.montant)+" "+self.montant_monnaie +" "+self.montant_taxe
      self.montant_str=text
      return text

     
    montant_str=fields.Char(string="Montant",compute="_get_montant_str")

    
 	
    Information_supplementaire=fields.Text(string="Informations supplémentaires")
    Textinit=fields.Text()
    @api.model
    def create(self,vals):
      #  self.env['capcontrat.contrat.echeance'].create(
      #     {"mailto":vals['manage'],'send_date':date_dure_cul(vals['fin_theorique_value'],0,vals['alertes_email']),"contrat_id":res.id})
      
        

      return super(ConditionsFinancieres,self).create(vals)
    
