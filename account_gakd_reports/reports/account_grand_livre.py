# -*- coding: utf-8 -*-

import time
from odoo import api, models, _
from odoo.exceptions import UserError


class ReportGrandLivre(models.AbstractModel):
    _inherit = 'report.account.report_generalledger'

    def _get_account_move_entry(self, accounts, init_balance, sortby, display_account):
        accounts_res = super(ReportGrandLivre,self)._get_account_move_entry(accounts, init_balance, sortby, display_account)
        return accounts_res

    @api.model
    def render_html(self, docids, data=None):
        super(ReportGrandLivre,self)
        if not data.get('form') or not self.env.context.get('active_model'):
            raise UserError(_("Form content is missing, this report cannot be printed."))

        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))

        init_balance = data['form'].get('initial_balance', True)
        sortby = data['form'].get('sortby', 'sort_date')
        display_account = data['form']['display_account']
        codes = []
        if data['form'].get('journal_ids', False):
            codes = [journal.code for journal in self.env['account.journal'].search([('id', 'in', data['form']['journal_ids'])])]

        accounts = docs if self.model == 'account.account' else self.env['account.account'].search([])
        accounts_res = self.with_context(data['form'].get('used_context',{}))._get_account_move_entry(accounts, init_balance, sortby, display_account)
        print '=========nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn======'
        print data['form']['compte_id']
        print '=========nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn======'
        if data.get('form').get('compte_id',False):
            cm = self.env['account.account'].sudo().search([('id','=',int(str(data['form']['compte_id'][0])))]).code
            print cm
            accounts_res = list(filter(lambda x: int(str(x['code']))== int(cm), accounts_res))
        print accounts_res
        docargs = {
            'doc_ids': docids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'Accounts': accounts_res,
            'print_journal': codes,
        }
        return self.env['report'].render('account.report_generalledger', docargs)