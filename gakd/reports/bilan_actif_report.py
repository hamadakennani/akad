# -*- coding: utf-8 -*-

import time

from odoo import _, api, models
from odoo.exceptions import UserError
import collections
import copy
import math 

data_group = {
    'AD':{'nom':'IMMOBILISATIONS INCORPORELLES'},
    'AE':{},
    'AF':{},
    'AG':{},
    'AH':{'nom':'Autres immobilisations incorporelles '},
    'AI':{'nom':'IMMOBILISATIONS CORPORELLES'},
    'AJ':{},
    'AK':{},
    'AL':{},
    'AM':{},
    'AN':{},
    'AP':{},
    'AQ':{'nom':'IMMOBILISATIONS FINANCIERES '},
    'AR':{},
    'AS':{},
    'AZ':{'nom':'TOTAL ACTIF IMMOBILISE '},
    'BA':{},
    'BB':{},
    'BG':{'nom':'CREANCES ET EMPLOIS ASSIMILES '},
    'BH':{},
    'BI':{},
    'BJ':{'nom':'Autres créances'},
    'BK':{'nom':'TOTAL ACTIF CIRCULANT '},
    'BQ':{},
    'BR':{},
    'BS':{},
    'BT':{'nom':'TOTAL TRESORERIE-ACTIF '},
    'BU':{},
    'BZ':{'nom':'TOTAL GENERAL '},
}
data_group_pf = {
    'CA':{},
    'CB':{},
    'CD':{},
    'CE':{},
    'CF':{},
    'CG':{},
    'CH':{},
    'CJ':{},
    'CL':{},
    'CM':{},
    'CP':{},
    'DA':{},
    'DB':{},
    'DC':{},
    'DD':{},
    'DF':{},
    'DG':{},
    'DJ':{},
    'DK':{},
    'DM':{},
    'DN':{},
    'DP':{},
    'DQ':{},
    'DR':{},
    'DT':{}, 
    'DV':{},
    'DZ':{}
}
class ReportBilanActif(models.AbstractModel):
    _name = 'report.gakd.gakd_bilan_actif'
    def _get_accounts(self, accounts, display_account,year):
        account_result = {}
        # Prepare sql query base on selected parameters from wizard
        tables, where_clause, where_params = self.env['account.move.line']._query_get()
        tables = tables.replace('"','')
        if not tables:
            tables = 'account_move_line'
        wheres = [""]
        if where_clause.strip():
            wheres.append(where_clause.strip())
        # print "==============================================================mmmm"
        # print wheres 
        # wheres.append()
        filters = " AND ".join(wheres)
        # print 'filteeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeer'
        filters += ' AND extract(year from "account_move_line"."create_date") = '+str(year)
        # compute the balance, debit and credit for the provided accounts
        request = ("SELECT account_id AS id, SUM(debit) AS debit, SUM(credit) AS credit, (SUM(debit) - SUM(credit)) AS balance" +\
                   " FROM " + tables + " WHERE account_id IN %s " + filters + " GROUP BY account_id")
        params = (tuple(accounts.ids),) + tuple(where_params)
        # print "================================================="
        # print  request
        print "================================================="
        print params
        self.env.cr.execute(request, params)
        for row in self.env.cr.dictfetchall():
            account_result[row.pop('id')] = row
        account_res = []
        for account in accounts:
            res = dict((fn, 0.0) for fn in ['credit', 'debit', 'balance'])
            currency = account.currency_id and account.currency_id or account.company_id.currency_id
            res['code'] = account.code
            res['name'] = account.name
            if account.id in account_result.keys():
                res['debit'] = account_result[account.id].get('debit')
                res['credit'] = account_result[account.id].get('credit')
                res['balance'] = account_result[account.id].get('balance')
            if display_account == 'all':
                account_res.append(res)
            if display_account == 'not_zero' and not currency.is_zero(res['balance']):
                account_res.append(res)
            if display_account == 'movement' and (not currency.is_zero(res['debit']) or not currency.is_zero(res['credit'])):
                account_res.append(res)
        return account_res

    @api.model
    def render_html(self, docids, data=None): 
        print '666666666666666666666666666666666666'
        account_group = self.env['gakd.account_group'].sudo().search([],order='create_date asc')
        print account_group
        groups = []
        total1 = 0
        for group in account_group:
            total = 0
            request = "select id from account_move_line where extract(month from create_date) = '12' and company_id=1"
            self.env.cr.execute(request)
            summ = len(self.env.cr.dictfetchall())
            request = "select id from account_move_line where extract(month from create_date) = '12' and company_id=1"
            self.env.cr.execute(request)
            i=0 
            for row in self.env.cr.dictfetchall():
                i+= 1
                if True:
                    print str(i)+str('/')+str(summ)
                    #print row["id"]
                #for move_line in self.env['account.move.line'].search([("create_date",'>','08-04-2019')]):
                    move_id = self.env['account.move.line'].search([("id",'=',row["id"])])
                    #print move_id
                    
                    if move_id.account_id.group_id.id == group.id:
                        if move_id.credit != 0:
                            total += move_id.credit
                        if move_id.debit != 0:
                            total -= move_id.debit
                    
            if group.name =='TA' or group.name =='RA' or group.name =='RB':
                total1 += total

                    
                #if move_line.account_id.group_id == 
            if group.name == 'XA':
                res = {"code":group.name,"name":group.nom,"total":total1,"date":group.create_date}
            else:
                res = {"code":group.name,"name":group.nom,"total":total,"date":group.create_date}
            groups.append(res)
        # for group in groups:
        #     if group['code']=='TA' or group['code']=='RA' or group['code']=='RB':
        #         total1 += group['total']
        print "555555555555555555555555" 
        print groups
        print total1
        print "555555555555555555555555"
        #groups = sorted(groups.items(),key=lambda r: r[3])
        docargs = {
            'groups':groups,
        }
        return self.env['report'].render('gakd.gakd_bilan_actif', docargs)

    @api.model
    def render_html1(self, docids, data=None): 
        if not data.get('form') or not self.env.context.get('active_model'):
            raise UserError(_("Form content is missing, this report cannot be printed."))
        self.model = self.env.context.get('active_model')
        docs = self.env[self.model].browse(self.env.context.get('active_ids', []))
        display_account = data['form'].get('display_account')
        accounts = docs if self.model == 'account.account' else self.env['account.account'].search([])
        account_res = self.with_context(data['form'].get('used_context'))._get_accounts(accounts, display_account,data['form'].get('year'))
        account_res_pre = self.with_context(data['form'].get('used_context'))._get_accounts(accounts, display_account,int(data['form'].get('year'))-1)
        global data_group 
        global data_group_pf 
        data_group_pre ={}
        data_group_pre = copy.deepcopy(data_group)
        data_group_pf_pre = copy.deepcopy(data_group_pf)
        # print data_group_pre.get('AD')
        if data['form'].get('bilan') == 'actif' and not data['form'].get('bilan') == 'py':
            
            for ac in account_res : 
                acc = self.env['account.account'].sudo().search([('code','=',ac['code']),('company_id','=',1)])
                if acc.group_id.name in data_group.keys():
                    data_group[acc.group_id.name]['nom'] = acc.group_id.nom
                    if acc.type_compte == 'brut':
                        data_group[acc.group_id.name]['brut'] = round(float(data_group[acc.group_id.name].get('brut',0.00)) + float(ac['balance']),2)
                        data_group[acc.group_id.name]['net'] = round((data_group[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
                        # print data_group[acc.group_id.name]['brut']
                        # print 'iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii'
                    if acc.type_compte == 'ad':
                        data_group[acc.group_id.name]['ad'] = round(float(data_group[acc.group_id.name].get('ad',0.00)) + float(ac['balance']),2)
                        data_group[acc.group_id.name]['net'] = round(float(data_group[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
            

            for a in  data_group:
                # print a
                # print "8888888888888888888888888888888"
                grp=self.env['gakd.account_group'].sudo().search([('name','=',a)])
                # print grp
                if len(grp.account_ids)==0:
                    # print 989999999999999999999999999999999999999 
                    # data_group[a]['oldnet']=0.00
                    # data_group[a]['balance']=0.00 
                    # print data_group[a]['ad']=0.00
                    if a=='AZ':
                        data_group[a]['ad']=data_group['AD'].get('ad',0.00)+data_group['AI'].get('ad',0.00)+data_group['AQ'].get('ad',0.00)
                        data_group[a]['brut']=data_group['AD'].get('brut',0.00)+data_group['AI'].get('brut',0.00)+data_group['AQ'].get('brut',0.00)
                        data_group[a]['net']=data_group['AD'].get('net',0.00)+data_group['AI'].get('net',0.00)+data_group['AQ'].get('net',0.00)
                    if a=='BK':
                        data_group[a]['ad']=data_group['BA'].get('ad',0.00) +data_group['BB'].get('ad',0.00)+data_group['BG'].get('ad',0.00)
                        data_group[a]['brut']=data_group['BA'].get('brut',0.00)+data_group['BB'].get('brut',0.00)+data_group['BG'].get('brut',0.00)
                        data_group[a]['net']=data_group['BA'].get('net',0.00)+data_group['BB'].get('net',0.00)+data_group['BG'].get('net',0.00)
                    if a=='BT':
                        data_group[a]['ad']=data_group['BQ'].get('ad',0.00)+data_group['BS'].get('ad',0.00)+data_group['BR'].get('ad',0.00)
                        data_group[a]['brut']=data_group['BQ'].get('brut',0.00)+data_group['BS'].get('brut',0.00) +data_group['BR'].get('brut',0.00) 
                        data_group[a]['net']=data_group['BQ'].get('net',0.00) +data_group['BS'].get('net',0.00)  +data_group['BR'].get('net',0.00) 
                    if a=='BZ':
                        data_group[a]['ad']=data_group['AZ'].get('ad',0.00)+data_group['BK'].get('ad',0.00)+data_group['BT'].get('ad',0.00)+data_group['BU'].get('net',0.00)
                        data_group[a]['brut']=data_group['AZ'].get('brut',0.00)+data_group['BK'].get('brut',0.00)+data_group['BT'].get('brut',0.00)+data_group['BU'].get('net',0.00)
                        data_group[a]['net']=data_group['AZ'].get('net',0.00)+data_group['BK'].get('net',0.00)+data_group['BT'].get('net',0.00)+data_group['BU'].get('net',0.00)
                    if a=='AD':
                        data_group[a]['ad']=data_group['AH'].get('ad',0.00)+data_group['AE'].get('ad',0.00)+data_group['AF'].get('ad',0.00)+data_group['AG'].get('ad',0.00)
                        data_group[a]['brut']=data_group['AH'].get('brut',0.00)+data_group['AE'].get('brut',0.00)+data_group['AF'].get('brut',0.00)+data_group['AG'].get('brut',0.00)
                        data_group[a]['net']=data_group['AH'].get('net',0.00)+data_group['AE'].get('net',0.00)+data_group['AF'].get('net',0.00)+data_group['AG'].get('net',0.00)
                    if a=='AI':
                        data_group[a]['ad']=data_group['AJ'].get('ad',0.00)+data_group['AK'].get('ad',0.00)+data_group['AL'].get('ad',0.00)+data_group['AM'].get('ad',0.00)+data_group['AN'].get('ad',0.00)+data_group['AP'].get('ad',0.00)
                        data_group[a]['brut']= data_group['AJ'].get('brut',0.00)+data_group['AK'].get('brut',0.00)+data_group['AL'].get('brut',0.00)+data_group['AM'].get('brut',0.00)+data_group['AN'].get('brut',0.00)+data_group['AP'].get('brut',0.00)
                        data_group[a]['net']= data_group['AJ'].get('net',0.00)+data_group['AK'].get('net',0.00)+data_group['AL'].get('net',0.00)+data_group['AM'].get('net',0.00)+data_group['AN'].get('net',0.00)+data_group['AP'].get('net',0.00)
                    if a=='AQ':
                        data_group[a]['ad']=data_group['AR'].get('ad',0.00)+data_group['AS'].get('ad',0.00) 
                        data_group[a]['brut']=data_group['AR'].get('brut',0.00)+data_group['AS'].get('brut',0.00) 
                        data_group[a]['net']=data_group['AR'].get('net',0.00)+data_group['AS'].get('net',0.00) 
                    if a=='BG':
                        data_group[a]['ad']=data_group['BH'].get('ad',0.00)+data_group['BI'].get('ad',0.00) +data_group['BJ'].get('ad',0.00) 
                        data_group[a]['brut']=data_group['BH'].get('brut',0.00)+data_group['BI'].get('brut',0.00) +data_group['BJ'].get('brut',0.00) 
                        data_group[a]['net']=data_group['BH'].get('net',0.00)+data_group['BI'].get('net',0.00) +data_group['BJ'].get('net',0.00) 
                     
            for ac in account_res_pre :
                acc = self.env['account.account'].sudo().search([('code','=',ac['code']),('company_id','=',1)])
                if acc.group_id.name in data_group.keys():
                    if acc.type_compte == 'brut':
                        data_group_pre[acc.group_id.name]['brut'] = round(float(data_group_pre[acc.group_id.name].get('brut',0.00)) + float(ac['balance']),2)
                        data_group_pre[acc.group_id.name]['net'] = round(float(data_group_pre[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
                    if acc.type_compte == 'ad':
                        data_group_pre[acc.group_id.name]['ad'] = round(float(data_group_pre[acc.group_id.name].get('ad',0.00)) + float(ac['balance']),2)
                        data_group_pre[acc.group_id.name]['net'] = round(float(data_group_pre[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
            # print '======================================'
            for g in data_group:
                data_group[g]['oldnet'] = data_group_pre.get(g).get('net')

            
        if data['form'].get('bilan') == 'passif' and not data['form'].get('bilan') == 'py': 
            for ac in account_res : 
                
                acc = self.env['account.account'].sudo().search([('code','=',ac['code']),('company_id','=',1)])
                if acc.group_id.name in data_group_pf.keys():  
                    if acc.group_id.name =='CG': 
                        print "jjjk,kj,skdfnjksdkjfnskj"
                        print data_group_pf[acc.group_id.name].get('brut',0.00)
                        print data_group_pf[acc.group_id.name].get('ad',0.00)
                    if acc.type_compte == 'brut':
                        data_group_pf[acc.group_id.name]['brut'] = round(float(data_group_pf[acc.group_id.name].get('brut',0.00)) + float(ac['balance']),2)
                        data_group_pf[acc.group_id.name]['net'] = round(float(data_group_pf[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
                    if acc.type_compte == 'ad':
                        data_group_pf[acc.group_id.name]['ad'] = round(float(data_group_pf[acc.group_id.name].get('ad',0.00)) + float(ac['balance']),2)
                        data_group_pf[acc.group_id.name]['net'] = round(float(data_group_pf[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
            for ac in account_res_pre :
                acc = self.env['account.account'].sudo().search([('code','=',ac['code']),('company_id','=',1)])
                if acc.group_id.name in data_group_pf.keys():
                    if acc.type_compte == 'brut':
                        data_group_pf_pre[acc.group_id.name]['brut'] = round(float(data_group_pf_pre[acc.group_id.name].get('brut',0.00)) + float(ac.get('balance',0.00)),2)
                        data_group_pf_pre[acc.group_id.name]['net'] = round(float(data_group_pf_pre[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
                    if acc.type_compte == 'ad':
                        data_group_pf_pre[acc.group_id.name]['ad'] = round(float(data_group_pf_pre[acc.group_id.name].get('ad',0.00)) + float(ac.get('balance',0.00)),2)
                        data_group_pf_pre[acc.group_id.name]['net'] = round(float(data_group_pf_pre[acc.group_id.name].get('brut',0.00)) - float(ac.get('ad',0.00)),2)
            # print '======================================'
            for g in data_group_pf:
                data_group_pf[g]['oldnet'] = data_group_pf_pre.get(g).get('net') 
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'data': data['form'],
            'docs': docs,
            'time': time,
            'Groups_a': data_group,
            'Groups_p': data_group_pf,
            'Keys_a':sorted(data_group.keys()),
            'Keys_p':sorted(data_group_pf.keys()),
            'bilan':data['form'].get('bilan')
        }
        if data['form'].get('bilan') == 'py':
            return self.env['report'].render('gakd.gakd_full_bilan', docargs)
        return self.env['report'].render('gakd.gakd_bilan_actif', docargs)
