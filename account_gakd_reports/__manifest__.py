{
    'name': 'gakd_account_reports',
    'version': '1.0',
    'summary': 'Groupe AKAD',
    'description': '',
    'category': 'Groupe AKAD',
    'author': 'IT SHORE',
    'website': '',
    'license': '',
    'depends': ['base','account'],
    'data': [
        'wizard/account_grand_livre_wizard_view.xml'
    ],
    'qweb': [
        
    ],
    'installable': True,
    'auto_install': False,
}
