# -*- coding: utf-8 -*-
import time
import odoo
from datetime import datetime
from odoo import SUPERUSER_ID
from odoo import fields, api, models,_
from odoo.tools.translate import _

class ged_groups(models.Model):
    _name= 'ged.groups'

	company_id= fields.Many2one('res.company', string='Company',states={  'annulee': [('readonly', True)]},default=lambda self: self.env['res.company']._company_default_get('res.users'))
	libelle=fields.Char(string='Libelle')
	users=fields.Many2one('res.users','ged_users_groups','group_id','user_id',string='Utilisateurs')
	status=fields.Selection([('actif','Activer'),('desactiver','Désactiver')],string='Etat',default="actif")
	group_model=fields.Many2one('ged.groups', 'Model')
	#'permission':fields.selection([('lecture',u'Lecture'),('modification',u'Modification'),('controle_total',u'Contrôle total')],'Permission')
    _rec_name = 'libelle'

    def OnChangeModelGroup(self,Model):
        vals = {}
        if Model:
		record = self.browse(Model)
            	if record:
			vals['status'] = record.status
			vals['users'] = [(6, 0, [item.id for item in record.users])]
        	print "ddddddddddddddddddddddddd"
		print vals
		print "ssssssssssssssssssssssssss"
        return {'value':vals}

class ged_groups_documents(models.Model):
    _name= 'ged.groups.documents'
   

	document_id=fields.Many2one('ir.attachment','groups_ids',string='Document')
	group=fields.Many2one('ged.groups',string='Groups')
	permission=fields.Selection([('lecture',u'Lecture'),('modification',u'Modification'),('controle_total',u'Contrôle total')],string='Permission')










