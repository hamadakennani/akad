# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time
from room_disponiblite import is_room_disponible

class  WizRoomState(models.TransientModel):
    _name = 'hotel.room_par_category_wizard'
              

    date_debut = fields.Date(string='Date de début')
    date_fin = fields.Date(string='Date de Fine ')
    @api.multi
    def check_overlap(self):
        date2 = datetime.strptime(self.date_debut, '%Y-%m-%d')
        date1 = datetime.strptime(self.date_fin, '%Y-%m-%d')
        delta = date2 - date1
        return [date1 + timedelta(days=i) for i in range(delta.days + 1)]
    
    @api.onchange('start_date')
    def _onchange_start_date(self):
        dt=datetime.strptime(self.start_date, '%Y-%m-%d')
       
        tm=  date2 = datetime.strptime('08:00:00', '%H:%M:%S').time()
      
 
    

 
    @api.multi
    def print_report(self):
     
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        
        datas = {
                 'form':
                    {
                        
                       
                        'date_debut':self.date_debut,
                        'date_fin':self.date_fin,
                    
                        'id': self.id,
                       
                    }
                }
        return self.env['report'].get_action(self, 'hotel_itshore.room_par_category', data=datas)


class RoomState(models.AbstractModel):
    _name = 'report.hotel_itshore.room_par_category'
    @api.multi
    def check_overlap(self,date_debut,date_fin):
        print "   check_overlap ---------------- >>>>"
        print "--------------->>>>>"
        print date_debut
        print  date_fin
        date2 = datetime.strptime(date_fin, '%Y-%m-%d')
        date1 = datetime.strptime(date_debut, '%Y-%m-%d')
        delta = date2 - date1
        print delta
        dates=[date1 + timedelta(days=i) for i in range(delta.days + 1)]
        print dates
        return dates
    @api.multi
    def room_cat(self):
        return self.env['hotel.room.type'].search([])

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('hotel_itshore.room_par_category')
        self.begining_qty = 0.0
        
        self.total_inventory = []
        self.value_exist = {}
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
            'room_cat':self.room_cat,
            'check_overlap':self.check_overlap,
            'get_rooms':self._get_rooms,
            'date_format':self.date_format
           
            # 'find_room_stats':self.find_room_stats,
            }
       
        return report_obj.render('hotel_itshore.room_par_category', docargs)

    def date_format(self,dt):
        print " date_format ------------>>>"
        print dt
        return dt.strftime('%d-%m-%Y')
    def _get_rooms(self,categ_id,dt):
        print (' hello ')
        print dt
        dtm=datetime.strptime(str(dt), '%Y-%m-%d %H:%M:%S').date()
        count=0
        print ' get rooms fun'
        for   room in  self.env['hotel.room'].search([('categ_id','=',categ_id)]):
            print ' In foor loop '
            print is_room_disponible(self,room.id,str(dtm),str(dtm))
            
            if not is_room_disponible(self,room.id,str(dtm),str(dtm)):

                 count+=1



        return count


    def  fu_reservation_line(self,reserv_id):
        hotel_reservation_line=[]
    
        for hotel_reservation in  self.env['hotel.reservation'].search([]):
               if hotel_reservation.state in ('confirm'):
                    for  reservation_line in hotel_reservation.reservation_line:
                        if reservation_line.reserve_view.id==reserv_id:
                            hotel_reservation_line.append(reservation_line)
        return  hotel_reservation_line
                   
          


    
   

    
