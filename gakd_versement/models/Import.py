# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from dateutil.parser import parse
import datetime
import pandas as pd
import xlrd 
import base64
import io
import re
from unidecode import unidecode
import json
import chardet
import csv
import dateparser

class ImportLine(models.Model):
    _name = 'gakd.import_line'

    file = fields.Binary(string='Fichier',required=True)
    file_name = fields.Char("File Name")
    bank = fields.Selection([
        ('BAB Produits Blancs', 'BAB Produits Blancs'),
        ('BGFI Produits Blancs', 'BGFI Produits Blancs'),
        ('BOA PRODUITS BLANCS', 'BOA Produits Blancs'),
        ('BOA LUBRIFIANTS', 'BOA Lubrifiants'),
        ('BOA GAZ', 'BOA Gaz'),
        ('CASH COLLECTION','CASH COLLECTION'),
        ('Eco bank Probuits Blancs', 'ECO Produits Blancs'),
        ('Eco bank Lubrifiants', 'ECO Lubrifiants'),
        ('Eco bank GAZ', 'ECO Gaz'),
        ('ORABANK PRODUITS BLANCS', 'ORA Produits Blancs'),
        ('ORABANK LUBRIFIANTS', 'ORA Lubrifiants'),
        ('ORABANK MAINTENANCE', 'ORA Maintenance'),
        ('ORABANK GAZ', 'ORA Gaz'),
        ('BANQUE SOCIETE GENERALE PRODUITS BLANCS', 'SGB Produits Blancs'),
        ('BANQUE SOCIETE GENERALE GAZ', 'SGB Gaz'),
        ],string="banque",required=True)
    import_id = fields.Many2one('gakd.import',string = "Import ID")
    categorie_produit = fields.Selection(
        [('', ''), ('Lubrifiants', 'Lubrifiants'), ('Produits blancs', 'Produits blancs'),
        ('Gaz et accessoire', 'Gaz et accessoire'), ('Lavage', 'Lavage'), ('CASH COLLECTION', 'CASH COLLECTION')],
        string="Catégorie des produits",
        default=''
        )

    def ImportData(self) :
        #list banques
        boa = ['BOA PRODUITS BLANCS', 'BOA LUBRIFIANTS', 'BOA GAZ']
        ecobank = ['Eco bank Probuits Blancs','Eco bank Lubrifiants', 'Eco bank GAZ']
        ora = ['ORABANK PRODUITS BLANCS', 'ORABANK LUBRIFIANTS','ORABANK GAZ','ORABANK MAINTENANCE']
        sgb = ['BANQUE SOCIETE GENERALE PRODUITS BLANCS', 'BANQUE SOCIETE GENERALE GAZ']
        #liste categorie produit
        listCategorieProduit = ['Lubrifiants', 'Probuits Blancs','Produits blancs', 'Gaz', 'MAINTENANCE', 'CASH COLLECTION']
        for cp in listCategorieProduit:
            if cp.upper() in self.bank.upper():
                if cp == 'Gaz':
                    self.categorie_produit = 'Gaz et accessoire'
                elif cp == 'MAINTENANCE':
                    self.categorie_produit = 'Lavage'
                elif cp == 'Probuits Blancs':
                    self.categorie_produit = 'Produits blancs'
                else:
                    self.categorie_produit =cp
                break
        toread = io.BytesIO()
        toread.write(base64.b64decode(self.file))  # `decrypted` string as the argument 
        toread.seek(0)  # reset the pointer
        # dict(self._fields['your_selection_field'].selection).get(self.your_selection_field)
        if (self.file_name.endswith('csv') or self.file_name.endswith('CSV') or self.file_name.endswith('txt')) and self.bank in sgb:
            dtar=[]
            for rows in toread:
                dtar.append(rows)
            for rows2 in dtar:
                ro = rows2.split(';')
                if len(ro)>1:
                    stro0 = ro[0].split('/') # re.findall(r'\w+', str(ro[0]))
                    stro00 = re.findall(r'\w+', str(stro0[0]))
                    stro2 = re.findall(r'\w+', str(ro[2]))
                    stro4 = re.findall(r'\w+', str(ro[4]))
                    ro0=''
                    ro2=''
                    ro4=''
                    i=0
                    strd=''
                    for d in stro00:
                        strd=strd+d
                    stro0[0]=strd
                    #24/04/2020
                    for ctr1 in stro0:
                        ro0=ro0+ctr1
                        i+=1
                        if i in [1,2]:
                            ro0 = ro0+'/'
                    for ctr2 in stro2:
                        ro2 = ro2+ctr2
                    for ctr3 in stro4:
                        ro4 = ro4+ctr3
                    datesgb =None
                    try:
                        td=parse(ro0)
                        datesgb=td.strftime('%d/%m/%Y')
                        # datesgb=datesgb.date()
                        # print datesgb
                    except:
                        datesgb=None
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('date_releve', '=',datesgb),
                        ('montant','=',ro2),
                        ('libelle', '=', ro4),
                        ('bank','=',self.bank),
                        ('categorie_produit', '=', self.categorie_produit),
                    ])
                    if len(existData) <= 0:
                        self.env['gakd.releve_bancaire'].sudo().create({
                            'date_releve': datesgb,
                            'montant': ro2,
                            'libelle': ro4,
                            'bank': self.bank,
                            'categorie_produit': self.categorie_produit,
                        })
        if self.bank not in sgb:
            data = None
            if self.file_name.endswith('csv') or self.file_name.endswith('CSV') or self.file_name.endswith('txt'):
                sep = ","
                if self.bank == 'BGFI Produits Blancs' or self.bank in ora :
                    sep = ";"
                try:
                    data = pd.read_csv(toread,sep=sep,encoding='latin1')
                except:
                    print("errors")
                    raise ValidationError("Il y a quelque lignes sur le fichier ne correspondent pas avec le programe ou le format fichier ne corespondant pas avec le programme ")
                    #  CParserError as errors
            else :
                try:
                    data = pd.read_excel(toread,0)
                except:
                    print("errors")
                    raise ValidationError("Il y a quelque lignes sur le fichier ne correspondent pas avec le programe ou le format fichier ne corespondant pas avec le programme ")
            jsonData = json.loads(data.to_json(orient='records'))
            
            if self.bank in ora:
                data = {}
                isFirstTime = True
                for row in jsonData :
                    currentData = {
                        'date_releve':row.get(u'Date',""),
                        'libelle':row.get(u'Libellé de l\'opération',""),
                        'montant':row.get(u'Crédit(XOF)',""),
                        'bank':self.bank,
                        'categorie_produit': self.categorie_produit,
                    }
                    if isFirstTime :
                        data = currentData
                        isFirstTime = False
                    elif currentData.get('date_releve','') == ' ' :
                        data['libelle'] = data.get('libelle') + '\n' + currentData.get('libelle')
                    else :
                        existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','like',data.get(u'libelle',"")),
                        ('date_releve','=',data.get(u'date_releve',"")),
                        ('montant','=',data.get(u'montant',"") if data.get(u'montant',"") != None else False),
                        ('bank','=',self.bank),
                        ('categorie_produit', '=', self.categorie_produit),
                        ])
                        if len(existData) <= 0 :
                            self.env['gakd.releve_bancaire'].sudo().create(data)
                        data = currentData
                self.env['gakd.releve_bancaire'].sudo().create(data)
            if self.bank in ecobank:
                for row in jsonData :
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','=',row.get(u'Description',"")),
                        ('date_releve','=',row.get(u'Transaction Date',"")),
                        ('montant','=',row.get(u'Deposits',"") if row.get(u'Deposits',"") != 0.0 else False  ),
                        ('reference','=',row.get(u'Reference No',"")),
                        ('bank','=',self.bank),
                        ('categorie_produit', '=', self.categorie_produit),
                    ])
                    if len(existData) <= 0 :
                        self.env['gakd.releve_bancaire'].sudo().create({
                            'libelle':row.get(u'Description',""),
                            'reference':row.get(u'Reference No',""),
                            'date_releve':row.get(u'Transaction Date',""),
                            'montant':row.get(u'Deposits',""),
                            'bank':self.bank,
                            'categorie_produit': self.categorie_produit,
                        })
            if self.bank == 'CASH COLLECTION' :
                for row in jsonData :
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','=',row.get(u'Note/Message',"")),
                        ('date_releve','=',row.get(u'Date',"")),
                        ('montant','=',row.get(u'Montant',"")),
                        ('bank','=',self.bank),
                        ('categorie_produit', '=', self.categorie_produit),
                    ])
                    if len(existData) <= 0 :
                        self.env['gakd.releve_bancaire'].sudo().create({
                            'date_releve':row.get(u'Date'),
                            'montant':row.get(u'Montant'),
                            'libelle':row.get(u'Note/Message'),
                            'bank':self.bank,
                            'categorie_produit': self.categorie_produit,
                        })
            if self.bank == 'BGFI Produits Blancs':
                data = {}
                isFirstTime = True
                for row in jsonData :
                    currentData = {
                        'date_releve':row .get(u'Date',""),
                        'libelle':row.get(u'Libellé de l\'opération',""),
                        'montant':row.get(u'Crédit(XOF)',""),
                        'bank': self.bank,
                        'categorie_produit': self.categorie_produit,
                    }
                    if isFirstTime :
                        data = currentData
                        isFirstTime = False
                    elif currentData.get('date_releve','') == ' ' :
                        data['libelle'] = data.get('libelle') + ' ' + currentData.get('libelle')
                    else :
                        existData = self.env['gakd.releve_bancaire'].sudo().search([
                            ('libelle','like',data.get(u'libelle',"")),
                            ('date_releve','=',data.get(u'date_releve',"")),
                            ('montant','=',data.get(u'montant',"")),
                            ('bank','=',self.bank),
                            ('categorie_produit', '=', self.categorie_produit),
                        ])
                        if len(existData) <= 0 :
                            self.env['gakd.releve_bancaire'].sudo().create(data)
                            data = currentData
                existData = self.env['gakd.releve_bancaire'].sudo().search([
                            ('libelle','like',data.get(u'libelle',"")),
                            ('date_releve','=',data.get(u'date_releve',"")),
                            ('montant','=',data.get(u'montant',"")),
                            ('bank','=',self.bank),
                            ('categorie_produit', '=', self.categorie_produit),
                        ])
                if len(existData) <= 0 :
                    self.env['gakd.releve_bancaire'].sudo().create(data)
            if self.bank == 'BAB Produits Blancs':
                for row in jsonData :
                    montant = str(row.get(u'Montant', "")).replace(' ','')
                    print " import"
                    print montant
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','=',row.get(u'Libellé',"")),
                        ('date_releve', '=', row.get(u'Date de l\'opération', "")),
                        ('montant', '=', montant),
                        ('reference', '=', row.get(u'Référence', "")),
                        ('bank','=',self.bank),
                        ('categorie_produit', '=', self.categorie_produit),
                    ])
                    print len(existData)
                    if len(existData) <= 0 :
                        self.env['gakd.releve_bancaire'].sudo().create({
                            'libelle':row.get(u'Libellé',""),
                            'reference':row.get(u'Référence',""),
                            'date_releve':row.get(u'Date de l\'opération',""),
                            'montant':row.get(u'Montant',""),
                            'bank':self.bank,
                            'categorie_produit': self.categorie_produit,
                        })
            elif self.bank in boa:
                for row in jsonData :
                    # montant=row.get(u'Crédit',"").replace(' ', '')
                    existData = self.env['gakd.releve_bancaire'].sudo().search([
                        ('libelle','=',row.get(u'Description',"")),
                        ('date_releve','=',row.get(u'Date op.',"")),
                        ('montant','=',row.get(u'Crédit',"")),
                        ('reference','=',row.get(u'Reference',"")),
                        ('bank','=',self.bank),
                        ('categorie_produit', '=', self.categorie_produit),
                    ])
                    if len(existData) <= 0 :
                        self.env['gakd.releve_bancaire'].sudo().create({
                            'libelle':row.get(u'Description',""),
                            'reference':row.get(u'Reference',""),
                            'date_releve':row.get(u'Date op.',""),
                            'montant':row.get(u'Crédit',""),
                            'bank':self.bank,
                            'categorie_produit': self.categorie_produit,
                        })
            # elif self.bank == 'ECOBANK':
            #     for row in jsonData :
            #         existData = self.env['gakd.releve_bancaire'].sudo().search([
            #             ('libelle','=',row.get(u'TRANSACTIONREFERENCE',"")),
            #             ('date_releve', '=', row.get(u'VALUEDATE', "")),
            #             ('montant','=',row.get(u'AMOUNT',"")),
            #             ('bank','=',self.bank),
            #             ('categorie_produit', '=', self.categorie_produit),
            #         ])
            #         if len(existData) <= 0 :
            #             self.env['gakd.releve_bancaire'].sudo().create({
            #                 'libelle':row.get(u'TRANSACTIONREFERENCE',""),
            #                 'date_releve':row.get(u'VALUEDATE',""),
            #                 'montant':row.get(u'AMOUNT',""),
            #                 'bank':self.bank,
            #                 'categorie_produit': self.categorie_produit,
            #             })
            # elif self.bank == 'SGB':
            #     print "oooooooooooooooooo"
            #     print self.bank+' '+self.categorie_produit
            #     print "oooooooooooooooooo"
            #     for row in jsonData :
            #         existData = self.env['gakd.releve_bancaire'].sudo().search([
            #             ('libelle', '=', row.get(u'LIBELLE', "")),
            #             ('date_releve', '=', row.get(u'DATE OPÉRATION', "")),
            #             ('montant','=',row.get(u'Crédit',"")),
            #             ('bank','=',self.bank),
            #             ('categorie_produit', '=', self.categorie_produit),
            #         ])
            #         if len(existData) <= 0 :
            #             self.env['gakd.releve_bancaire'].sudo().create({
            #                 'libelle': row.get(u'LIBELLE', ""),
            #                 'date_releve': row.get(u'DATE OPÉRATION', ""),
            #                 'montant': row.get(u'Crédit', ""),
            #                 'bank':self.bank,
            #                 'categorie_produit': self.categorie_produit,
            #             })

class Import(models.Model):
    _name = 'gakd.import'
    _rec_name = "id"
    import_lines = fields.One2many('gakd.import_line','import_id',string='Import Lines')

    def ImportAll(self) :
        print 'okokokokop'
        for line in self.import_lines :
            line.ImportData()
