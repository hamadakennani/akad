# -- coding: utf-8 --
from odoo import api, fields, models


class Scrap(models.Model):
    _inherit = 'stock.scrap'

    view_type = fields.Selection(string='Type view', selection=[('bouteilles', 'bouteilles'), ('renovation', 'renovation'),(
        'dedouanement', 'dedouanement')])
    
