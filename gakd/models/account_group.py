from odoo import api, fields, models
class AcountAcount(models.Model):
    _inherit="account.account"
    group_id = fields.Many2one('gakd.account_group',string='Groupe')
    type_compte = fields.Selection([('brut','Brut'),('ad','Amort et deprec')],string="Type de compte")

class AccountGroup(models.Model):
    _name = 'gakd.account_group'

    name = fields.Char(string='Code')
    account_ids = fields.One2many('account.account','group_id',string='Comptes')
    nom = fields.Char(string='Nom')

    def createGroupsToAccounts(self):
        accounts = self.env['account.account'].sudo().search([])
        groups = self.env['gakd.account_group'].sudo().search([])
        print accounts
        for ac in accounts:
            print ac
            if int(ac.code[:3]) == 701 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TA')]).id
                })
                continue
            if int(ac.code[:3]) == 601 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RA')]).id
                })
                continue
            if int(ac.code[:4]) == 6031 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RB')]).id
                })
                continue
            if int(ac.code[:3]) == 702 or int(ac.code[:3]) == 703 or int(ac.code[:3]) == 704 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TB')]).id
                })
                continue
            if int(ac.code[:3]) == 705 or int(ac.code[:3]) == 706 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TC')]).id
                })
                continue
            if int(ac.code[:3]) == 707 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TD')]).id
                })
                continue
            if int(ac.code[:2]) == 73 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TE')]).id
                })
                continue
            if int(ac.code[:2]) == 72 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TF')]).id
                })
                continue
            if int(ac.code[:2]) == 71 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TG')]).id
                })
                continue
            if int(ac.code[:2]) == 75 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TH')]).id
                })
                continue
            if int(ac.code[:3]) == 781 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TI')]).id
                })
                continue
            if int(ac.code[:3]) == 602 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RC')]).id
                })
                continue
            if int(ac.code[:4]) == 6032 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RD')]).id
                })
                continue
            if int(ac.code[:3]) == 604 or int(ac.code[:3]) == 605 or int(ac.code[:3]) == 608 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RE')]).id
                })
                continue
            if int(ac.code[:4]) == 6033 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RF')]).id
                })
                continue
            if int(ac.code[:2]) == 61 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RG')]).id
                })
                continue
            if int(ac.code[:2]) == 62 or int(ac.code[:2]) == 63:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RH')]).id
                })
                continue
            if int(ac.code[:2]) == 64 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RI')]).id
                })
                continue
            if int(ac.code[:2]) == 65:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RJ')]).id
                })
                continue
            if int(ac.code[:2]) == 66:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RK')]).id
                })
                continue
            if int(ac.code[:3]) == 791 or int(ac.code[:3]) == 798 or int(ac.code[:3]) == 799 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TJ')]).id
                })
                continue
            if int(ac.code[:3]) == 681 or int(ac.code[:3]) == 691 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RL')]).id
                })
                continue
            if int(ac.code[:2]) == 77:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TK')]).id
                })
                continue
            if int(ac.code[:3]) == 797:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TL')]).id
                })
                continue
            if int(ac.code[:3]) == 787:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TM')]).id
                })
                continue
            if int(ac.code[:2]) == 67:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RM')]).id
                })
                continue
            if int(ac.code[:3]) == 697:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RN')]).id
                })
                continue
            if int(ac.code[:2]) == 82:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TN')]).id
                })
                continue
            if int(ac.code[:2]) == 84 or int(ac.code[:2]) == 86 or int(ac.code[:2]) == 88:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','TO')]).id
                })
                continue
            if int(ac.code[:2]) == 81:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RO')]).id
                })
                continue
            if int(ac.code[:2]) == 83 or int(ac.code[:2]) == 85:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RO')]).id
                })
                continue
            if int(ac.code[:2]) == 87:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RQ')]).id
                })
                continue
            if int(ac.code[:2]) == 89:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','RS')]).id
                })
                continue
            #3333333333333333333333333333333333
            if int(ac.code[:3]) == 211 or int(ac.code[:4]) == 2181 or int(ac.code[:4]) == 2191:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AE')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2811 or int(ac.code[:4]) == 2818 or int(ac.code[:4]) == 2911 or int(ac.code[:4]) == 2918 or int(ac.code[:4]) == 2919 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AE')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 212 or int(ac.code[:3]) == 213 or int(ac.code[:3]) == 214 or int(ac.code[:4]) == 2193:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AF')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2812 or int(ac.code[:4]) == 2813 or int(ac.code[:4]) == 2814 or int(ac.code[:4]) == 2912 or int(ac.code[:4]) == 2913 or int(ac.code[:4]) == 2914 or int(ac.code[:4]) == 2919:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AF')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 215 or int(ac.code[:3]) == 216 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AG')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2815 or int(ac.code[:4]) == 2816 or int(ac.code[:4]) == 2915 or int(ac.code[:4]) == 2916 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AG')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 22 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AJ')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 282 or int(ac.code[:3]) == 292 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AJ')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 231 or int(ac.code[:3]) == 232 or int(ac.code[:3]) == 233 or int(ac.code[:3]) == 237 or int(ac.code[:4]) == 2391  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AK')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2831 or int(ac.code[:4]) == 2832 or int(ac.code[:4]) == 2833 or int(ac.code[:4]) == 2837 or int(ac.code[:4]) == 2931 or int(ac.code[:4]) == 2932 or int(ac.code[:4]) == 2933 or int(ac.code[:4]) == 2937 or int(ac.code[:4]) == 2939 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AK')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 234 or int(ac.code[:3]) == 235 or int(ac.code[:3]) == 238 or int(ac.code[:4]) == 2397 or int(ac.code[:4]) == 2393  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AL')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2834 or int(ac.code[:4]) == 2835 or int(ac.code[:4]) == 2838 or int(ac.code[:4]) == 2934 or int(ac.code[:4]) == 2935 or int(ac.code[:4]) == 2938 or int(ac.code[:4]) == 2939 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AL')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 24  and not (int(ac.code[:3]) == 245 or int(ac.code[:4]) == 2495) :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AM')]).id,
                    'type_compte':'brut'
                })
                continue
            if (int(ac.code[:3]) == 284 and not int(ac.code[:4]) == 2845 ) or (int(ac.code[:3]) == 294 and not (int(ac.code[:4]) == 2945 or  int(ac.code[:4]) == 2949))  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AM')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 245  or int(ac.code[:4]) == 2495  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AN')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2845  or int(ac.code[:4]) == 2945  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AN')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 251  or int(ac.code[:3]) == 252  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AP')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:4]) == 2951  or int(ac.code[:4]) == 2952  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AP')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 26 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AR')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 296 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AR')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 27 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AS')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 297 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','AS')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 485 or int(ac.code[:3]) == 488 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BA')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 498:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BA')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 31 or int(ac.code[:2]) == 33 or int(ac.code[:2]) == 33 or int(ac.code[:2]) == 34 or int(ac.code[:2]) == 35 or int(ac.code[:2]) == 36 or int(ac.code[:2]) == 37 or int(ac.code[:2]) == 38 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BB')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:2]) == 39 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BB')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 409 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BH')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 490 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BH')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 41 and not  int(ac.code[:2]) == 419:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BI')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 491 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BI')]).id,
                    'type_compte':'ad'
                })
                continue
            if (int(ac.code[:3]) == 185 or int(ac.code[:2]) == 42 or int(ac.code[:2]) == 43 or int(ac.code[:2]) == 44 or int(ac.code[:2]) == 45 or int(ac.code[:2]) == 46 or int(ac.code[:2]) == 47) and not int(ac.code[:3]) == 478 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BJ')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 492 or int(ac.code[:3]) == 493 or int(ac.code[:3]) == 494 or int(ac.code[:3]) == 495 or int(ac.code[:3]) == 496 or int(ac.code[:3]) == 497 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BJ')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 50 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BQ')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 590 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BQ')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 51 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BR')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 591 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BR')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:2]) == 52 or int(ac.code[:2]) == 53 or int(ac.code[:2]) == 54 or int(ac.code[:2]) == 55 or int(ac.code[:2]) == 57 or int(ac.code[:3]) == 581 or int(ac.code[:3]) == 582 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BS')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 592 or int(ac.code[:3]) == 593 or int(ac.code[:3]) == 594  :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BS')]).id,
                    'type_compte':'ad'
                })
                continue
            if int(ac.code[:3]) == 478 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','BU')]).id,
                    'type_compte':'brut'
                })
                continue
            if int(ac.code[:3]) == 101 or int(ac.code[:3]) == 102 or int(ac.code[:3]) == 103 or int(ac.code[:3]) == 104 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CA')]).id,
                })
                continue
            if int(ac.code[:3]) == 109:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CB')]).id,
                })
                continue
            if int(ac.code[:3]) == 105:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CD')]).id,
                })
                continue
            if int(ac.code[:3]) == 106:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CE')]).id,
                })
                continue
            if int(ac.code[:3]) == 111 or int(ac.code[:3]) == 112 or int(ac.code[:3]) == 113:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CF')]).id,
                })
                continue
            if int(ac.code[:3]) == 118:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CG')]).id,
                })
                continue
            if int(ac.code[:3]) == 121 or int(ac.code[:3]) == 129:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CH')]).id,
                })
                continue
            if int(ac.code[:3]) == 131 or int(ac.code[:3]) == 139:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CJ')]).id,
                })
                continue
            if int(ac.code[:2]) == 14:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CL')]).id,
                })
                continue
            if int(ac.code[:2]) == 15:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','CM')]).id,
                })
                continue
            if int(ac.code[:2]) == 16 or int(ac.code[:3]) == 181 or int(ac.code[:3]) == 182 or int(ac.code[:3]) == 183 or int(ac.code[:3]) == 184:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DA')]).id,
                })
                continue
            if int(ac.code[:2]) == 17:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DB')]).id,
                })
                continue
            if int(ac.code[:2]) == 19:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DC')]).id,
                })
                continue
            if int(ac.code[:3]) == 481 or int(ac.code[:3]) == 482 or int(ac.code[:3]) == 484 or int(ac.code[:4]) == 4998 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DH')]).id,
                })
                continue
            if int(ac.code[:3]) == 419:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DI')]).id,
                })
                continue
            if int(ac.code[:2]) == 40 and not int(ac.code[:3]) == 409:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DJ')]).id,
                })
                continue
            if int(ac.code[:2]) == 42 or int(ac.code[:2]) == 43 or int(ac.code[:2]) == 44 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DK')]).id,
                })
                continue
            if (int(ac.code[:2]) == 45 or int(ac.code[:2]) == 46 or int(ac.code[:2]) == 47 or int(ac.code[:3]) == 185 ) and not int(ac.code[:3]) == 479 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DM')]).id,
                })
                continue
            if (int(ac.code[:3]) == 499 or int(ac.code[:3]) == 599) and not int(ac.code[:4]) == 4998:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DN')]).id,
                })
                continue
            if int(ac.code[:3]) == 564 or int(ac.code[:3]) == 565:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DQ')]).id,
                })
                continue
            if int(ac.code[:3]) == 561 or int(ac.code[:3]) == 566 or int(ac.code[:2]) == 52 or int(ac.code[:2]) == 53 :
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DR')]).id,
                })
                continue
            if int(ac.code[:3]) == 479:
                ac.sudo().write({
                    'group_id':self.env['gakd.account_group'].sudo().search([('name','=','DV')]).id,
                })
                continue
            