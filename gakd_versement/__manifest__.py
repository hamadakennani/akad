# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'validation Versements',
    'version': '1.0',
    'summary': 'Groupe AKAD',
    'category': 'Groupe AKAD',
    'author': 'IT SHORE',
    'sequence': 10,
    'description':"",
    'depends': ['base','account','gakd'],
    'installable': True,
    'application': True,
    'auto_install': False,

    'data': [
        'views/validation.xml',
        'views/import_view.xml',
        'views/releve_bancaire_views.xml',        
        'views/rapport.xml',
        'security/res_groups.xml',
        'security/ir.model.access.csv',
        'reports/pdf_raport_validion.xml',
        
        # 'views/versementLine.xml',

        # 'views/rslt.xml',
        # 'views/versement_releveBancaire_view.xml',
        
    ],


    }
