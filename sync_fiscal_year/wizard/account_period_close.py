# -*- coding: utf-8 -*-
# Part of Odoo. See COPYRIGHT & LICENSE files for full copyright and licensing details.

from odoo import api, fields, models
from odoo.exceptions import UserError


class AccountPeriodClose(models.TransientModel):
    _name = "account.period.close"
    _description = "period close"

    sure = fields.Boolean('Check this box')

    def data_save(self):
        account_move_obj = self.env['account.move']
        cr = self.env.cr
        mode = 'done'
        for form in self:
            if form.sure:
                for period_id in self._context.get('active_ids', False):
                    account_move_ids = account_move_obj.search([('period_id', '=', period_id), ('state', '=', "draft")])
                    if account_move_ids:
                        raise UserError('Invalid Action! \nIn order to close a period, you must first post related journal entries.')

                    cr.execute('update account_journal_period set state=%s where period_id=%s', (mode, period_id))
                    cr.execute('update account_period set state=%s where id=%s', (mode, period_id))
                    self.invalidate_cache()

        return {'type': 'ir.actions.act_window_close'}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
