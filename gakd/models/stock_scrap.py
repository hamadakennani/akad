from odoo import fields, api, models,_

class   stock_scrap(models.Model):
    _inherit = "stock.scrap"


    scrap_location_id = fields.Many2one(
        'stock.location', 'Scrap Location',
        domain="[]", states={'done': [('readonly', True)]})
    
