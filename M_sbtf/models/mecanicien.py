# -- coding: utf-8 --

import time
from odoo import api, fields, models, _
from odoo import netsvc
import odoo.addons.decimal_precision as dp


class mro_mecanicien(models.Model):

    _inherit = 'hr.employee'
    
    garage_id=fields.Many2one("mro.garage")
    