# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint

class capcontrat_users_versions(models.Model):
	_name="users.versions"
	_rec_name='users_version'


	version_id= fields.Many2one('capcontrat.versions', string='Version')
	users_version=fields.Many2one('res.users',string='Utilisateur')
	state=fields.Selection([('brouillon','Brouillon'),('valide','Validé'),('annule','Annulé')],'Etat')


	def create(self, vals):
		return super(capcontrat_users_versions, self).create(vals)

