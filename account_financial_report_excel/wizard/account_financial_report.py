 # -*- coding: utf-8 -*-

import xlwt
import cStringIO
import base64
import time
import datetime
from datetime import datetime
from datetime import timedelta
from openerp import tools
from openerp import api, fields, models

class FinanceAccountingReport(models.TransientModel):
    _inherit = "accounting.report"

    @api.multi
    def check_report_excel(self):
        res = super(FinanceAccountingReport, self).check_report()
        return self._print_report_excel(res)

    @api.multi
    def _print_report_excel(self, data):
        workbook = xlwt.Workbook()
        title_style_comp_left = xlwt.easyxf('align: horiz left ; font: name Times New Roman,bold off, italic off, height 450')
        title_style1_table_head = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_headbal = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold on, italic off, height 200; borders: top double, bottom double, left double, right double;')
        title_style1_table_head_bold = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold on, italic off, height 200;')
        title_style1_table_head1 = xlwt.easyxf('font: name Times New Roman,bold on, italic off, height 200')
        title_style1_table_normal = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 200')
        title_style1_table_normal_right = xlwt.easyxf('align: horiz right ;font: name Times New Roman,bold off, italic off, height 200')
        title_style1_table_data_sub = xlwt.easyxf('font: name Times New Roman,bold off, italic off, height 190')

        financial_report_obj = self.env['report.account.report_financial']
        get_account_lines = financial_report_obj.get_account_lines(data['data']['form'])

        sheet_name = 'Financial'
        sheet = workbook.add_sheet(sheet_name)
        comp_id = self.env.user.company_id
        currency_id = comp_id.currency_id
        sheet.write_merge(0, 1, 0, 3, self.account_report_id.name, title_style_comp_left)
        sheet.write(0, 5, 'Printing Date: '+datetime.now().strftime('%Y-%m-%d'), title_style1_table_head1)
        sheet.write(1, 5, comp_id.name, title_style1_table_head1)
        
        if self.date_from:
            sheet.write(3, 2, 'Date from :', title_style1_table_head1)
            sheet.write(4, 2, self.date_from, title_style1_table_data_sub)
        if self.date_to:
            sheet.write(3, 3, 'Date To :', title_style1_table_head1)
            sheet.write(4, 3, self.date_to, title_style1_table_data_sub)
        
        sheet.write(3, 0, 'Target Moves :',title_style1_table_head1)
        if self.target_move == 'all':
            sheet.col(0).width = 256 * 40
            sheet.col(1).width = 256 * 18
            sheet.col(2).width = 256 * 18
            sheet.col(3).width = 256 * 18
            sheet.write(4, 0, 'All Entries',title_style1_table_data_sub)
        if self.target_move == 'posted':
            sheet.col(0).width = 256 * 40
            sheet.col(1).width = 256 * 18
            sheet.col(2).width = 256 * 18
            sheet.col(3).width = 256 * 18
            column = sheet.col(8)
            column.width = 256 * 18
            sheet.write(4, 0, 'All Posted Entries',title_style1_table_data_sub)
        
        if self.debit_credit:
            sheet.write(8, 0, 'Name',title_style1_table_head)
            sheet.write(8, 1, 'Debit',title_style1_table_headbal)
            sheet.write(8, 2, 'Credit',title_style1_table_headbal)
            sheet.write(8, 3, 'Balance',title_style1_table_headbal)

            row_data = 9
            for line in get_account_lines:
                if line.get('level') != 0:
                    if line.get('level') < 3:
                        sheet.write(row_data, 0, tools.ustr(line['name']), title_style1_table_head1)
                        sheet.write(row_data, 1, tools.ustr(line['debit'])+' '+ currency_id.symbol , title_style1_table_head_bold)
                        sheet.write(row_data, 2, tools.ustr(line['credit'])+' '+ currency_id.symbol , title_style1_table_head_bold)
                        sheet.write(row_data, 3, tools.ustr(line['balance'])+' '+ currency_id.symbol , title_style1_table_head_bold)
                    else:
                        sheet.write(row_data, 0, '    '+tools.ustr(line['name']), title_style1_table_normal)
                        sheet.write(row_data, 1, tools.ustr(line['debit'])+' '+ currency_id.symbol , title_style1_table_normal_right)
                        sheet.write(row_data, 2, tools.ustr(line['credit'])+' '+ currency_id.symbol , title_style1_table_normal_right)
                        sheet.write(row_data, 3, tools.ustr(line['balance'])+' '+ currency_id.symbol , title_style1_table_normal_right)
                    row_data+=1
        if not self.debit_credit and not self.enable_filter:
            sheet.write(8, 0, 'Name',title_style1_table_head)
            sheet.write(8, 1, 'Balance',title_style1_table_headbal)

            row_data = 9
            for line in get_account_lines:
                if line.get('level') != 0:
                    if line.get('level') < 3:
                        sheet.write(row_data, 0, tools.ustr(line['name']), title_style1_table_head1)
                        sheet.write(row_data, 1, tools.ustr(line['balance'])+' '+ currency_id.symbol , title_style1_table_head_bold)
                    else:
                        sheet.write(row_data, 0, ' '+tools.ustr(line['name']), title_style1_table_normal)
                        sheet.write(row_data, 1, tools.ustr(line['balance'])+' '+currency_id.symbol, title_style1_table_normal_right)
                    row_data+=1
        
        #data['enable_filter'] == 1 and not data['debit_credit']
        if not self.debit_credit and self.enable_filter:
            sheet.write(8, 0, 'Name',title_style1_table_head)
            sheet.write(8, 1, 'Balance',title_style1_table_headbal)
            sheet.write(8, 2, self.label_filter,title_style1_table_headbal)

            row_data = 9
            for line in get_account_lines:
                if line.get('level') != 0:
                    if line.get('level') < 3:
                        sheet.write(row_data, 0, tools.ustr(line['name']), title_style1_table_head1)
                        sheet.write(row_data, 1, tools.ustr(line['balance'])+' '+currency_id.symbol, title_style1_table_head_bold)
                        sheet.write(row_data, 2, tools.ustr(line['balance_cmp']), title_style1_table_head_bold)
                    else:
                        sheet.write(row_data, 0, '    '+tools.ustr(line['name']), title_style1_table_normal)
                        sheet.write(row_data, 1, tools.ustr(line['balance'])+' '+currency_id.symbol, title_style1_table_normal_right)
                        sheet.write(row_data, 2, tools.ustr(line['balance_cmp']), title_style1_table_normal_right)
                    row_data+=1
        
        stream = cStringIO.StringIO()
        workbook.save(stream)
        attach_id = self.env['accounting.finance.report.output.wizard'].create({'name':'Financial.xls', 'xls_output': base64.encodestring(stream.getvalue())})
        return {
            'context': self.env.context,
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'accounting.finance.report.output.wizard',
            'res_id':attach_id.id,
            'type': 'ir.actions.act_window',
            'target':'new'
        }

class AccountingFinanceReportOutputWizard(models.Model):
    _name = 'accounting.finance.report.output.wizard'
    _description = 'Wizard to store the Excel output'

    xls_output = fields.Binary(string='Excel Output')
    name = fields.Char(string='File Name', help='Save report as .xls format', default='Trial_Balance.xls')

#vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: