# -*- coding: utf-8 -*-
###################################################################################
#
#    This program is free software: you can modify
#    it under the terms of the GNU Affero General Public License (AGPL) as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###################################################################################
{
    'name': 'Access Right Report',
    'version': '1.0',
    'summary': """All user access rights in a single view""",
    'description': """All user access rights in a single view""",
    'category': 'Base',
    'author': 'Kreative',
    'website': "",
    'license': 'AGPL-3',

    'price': 10.0,
    'currency': 'EUR',

    'depends': ['base'],

    'data': [
        'views/report_view.xml',
    ],
    'demo': [

    ],
    'images': ['static/description/banner.png'],
    'qweb': ['static/src/xml/report.xml'],
    'installable': True,
    'auto_install': False,
    'application': False,
}
