# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint
import datetime
import math

class gakd_recharger_carte_client(models.TransientModel):
    _name = 'gakd.wizard.recharger.carte.client'
    _rec_name = 'id'
    _description = 'New Description'

    def _getClient_id(self):
        
        return self.env["gakd.carte"].search([('id','=',self._context.get('active_ids',False)[0])]).owner_id

    def _getsolde(self):
        return self.env["gakd.sous_chargeur"].search([('access','=',self.env.user.id)]).montant_plafond
    
    def _getCarte_id(self):
        
        return self._context.get('active_ids',False)[0]

   

    @api.onchange('client_id')
    def _onchange_client(self):
	self.update({
		'solde': self.client_id.solde_compte_hide
	})

 
    client_id = fields.Many2one("res.partner",string="Client",default=_getClient_id)
    carte_id = fields.Many2one("gakd.carte",string="Carte",default=_getCarte_id)
    solde = fields.Float(string='Solde non affecté',readonly=True ,compute="_getSolde" ,store=True)
    montant = fields.Float(string='Montant à recharger')
    solde_m = fields.Float(string='Votre solde',default=_getsolde)
    journal_id = fields.Many2one(comodel_name='account.journal', string='Journal des règlements',required=True)
    moyen_de_paiement = fields.Selection(string='Moyen de paiement',required=True,selection=[('cheque', 'Chèque'), ('especes', 'Espèces'),('versement','Versement')])
    type_de_recharge= fields.Selection(string='Type  de recharge',
    selection=[(155, 'ESSENCE'), (156, 'GASOIL'),(164,'PETROLE')])
    client_type = fields.Selection([('Oui','Oui'),('Non','Non')],string="TVA retenue à la source",required=True,dafault="Non")

    @api.multi
    def save_recharger(self):
        print "sssssssssssssssssss"
        print "xxxxxxxxxxxxxxxxxx"
        print "sssssssssssssssssss"
        print self.client_id.id
        
        #print self.
        #start oussama's code
        print self.client_type
        if  self.client_type  in ('Oui','Non'):
            montant_bonus = 0
            montant_a_pa = 0
            if self.client_id.client_type == 'client_acc' :
                montant_bonus = self.montant *( self.client_id.valuer_a_accorde/100)
                montant_a_pa = self.montant - montant_bonus

            #end oussama's code
            print self.env["gakd.sous_chargeur"].search([('access','=',self.env.uid)]).id
            # print self.env.uid
            
            print 'TTTTTTTTTTTTTTEST'
            if self.montant <= 0:
                raise ValidationError(_("Merci de renseigner un montant supérieur à zéro"))
            
            po=self.env["gakd.sous_chargeur"].search([('access','=',self.env.user.id)])
            if self.montant<=po.montant_plafond:
                m_init =self.montant
                montant = self.carte_id.solde + self.montant
                self.carte_id.solde = montant
                self.client_id.solde_carte=0
                for carte in self.client_id.carte_ids:
                    self.client_id.solde_carte += carte.solde
                body = """Merci de noter que votre compte vient d'etre credite par un montant de """+str(self.montant)+"""
                Votre solde actuel du compte est : """+str(montant)
                Subject = "JNP BENIN"
                mobile = self.client_id.mobile
                api_instance = swagger_client.SmsApi()
                smsrequest = swagger_client.SmsUniqueRequest("eb717305f6545b5ea0d57a7aba06dc54",None, None, body,mobile,Subject,None,None,None, None, None) # SMSRequest | sms request
                # try:
                #     api_response = api_instance.send_sms(smsrequest)
                # except ApiException as e:
                #     print ("Exception when calling SmsApi->send_sms: %s\n" % e)
                #mailsto = self.client_id.email #self.env['res.partner'].search([["id","=",vals.get('owner_id',False)]]).email
                
                #self.env["gakd.carte"].SendMail(mailsto,Subject,body)
                montant_bonus = 0
                montant_a_pa = 0
                TVA=0
                montant_a_recharge_remise=0
                montant_hor_taxes=0
                qte=0

                if self.client_id.client_type == 'client_acc' :
                    montant_bonus = self.montant * 0.01
                    montant_a_pa = self.montant - montant_bonus
                prix_init=self.env['product.product'].search([('id','=',155)]).lst_price
                qte=1
                montant_hor_taxes=1
            
                tva_l=self.env['account.tax'].search([('name','=','TVA - ESSENCE')])
                if self.type_de_recharge==155:
                    tva_l= self.env['account.tax'].search([('name','=','TVA - ESSENCE')])
                elif  self.type_de_recharge==156:
                    tva_l= self.env['account.tax'].search([('name','=','TVA - GASOIL')])
                elif  self.type_de_recharge==164:
                    tva_l= self.env['account.tax'].search([('name','=','TVA-PETROLE')])
                print self.montant 
                print prix_init
                print tva_l
                TVA = float((self.montant / prix_init))*float(tva_l.amount)

                montant_a_recharge_remise=montant_hor_taxes+TVA+montant_bonus
            
                print '88888888888888888888888aaaa'
                his=self.env['gakd.historique'].create({
                    'montant_init':po.montant_plafond,
                    'montant_fin':po.montant_plafond-m_init,
                    'sous_chargeur':po.id,
                    'type_op':'Recharge compte client',
                    'diff':abs(po.montant_plafond-m_init-po.montant_plafond),
                    'client_id':self.client_id.id,
                    'carte':self.carte_id.id,
                    'moyen_de_paiement':self.moyen_de_paiement
                    })
                po.write({
                        'montant_plafond':po.montant_plafond-self.montant
                    })
                if self.moyen_de_paiement == 'versement':
                    acount_id = 20601
                if self.moyen_de_paiement == 'cheque':
                    acount_id = 20601
                if self.moyen_de_paiement == 'especes':
                    acount_id =20601
                print "2222222222222222222222"
                if self.client_type == "Oui":
                    data = [(0,0 ,{'account_id':self.journal_id.default_debit_account_id.id ,'partner_id':self.client_id.id,'name':'Caisse recette JNP PASS (Recharge)','debit':self.montant-TVA}),
                    (0,0 ,{'account_id':19691 ,'partner_id':self.client_id.id,'name':'TVA retenu à la source ','debit':TVA}),
                    (0,0 ,{'account_id': acount_id,'partner_id':self.client_id.id,'name':'Clients, avances et acomptes reçus SUR JNP PASS','credit':self.montant})]
                else:
                    data = [(0,0 ,{'account_id':self.journal_id.default_debit_account_id.id ,'partner_id':self.client_id.id,'name':'Caisse recette JNP PASS (Recharge)','debit':self.montant}),
                    (0,0 ,{'account_id': acount_id,'partner_id':self.client_id.id,'name':'Clients, avances et acomptes reçus SUR JNP PASS','credit':self.montant})]
                av= self.env['account.move'].create({
            
            'journal_id':self.journal_id.id,
            'date':datetime.datetime.now().date(),
                'ref': self.client_id.name+'-'+str(datetime.datetime.now()),
                
                'line_ids':data
                
                })
                
              
                his.sudo().write({
                    'account_move':av.id,
                })
            
                av.post()
                
                # if self.moyen_de_paiement == 'versement':
                #     acount_id = 19857
                # if self.moyen_de_paiement == 'cheque':
                #     acount_id = 19975
                # if self.moyen_de_paiement == 'especes':
                #     acount_id = 19975
            #     av= self.env['account.move'].create({
                
            #    'journal_id':self.journal_id.id,
            #    'date':datetime.datetime.now().date(),
            #     'ref': self.client_id.name+'-'+str(datetime.datetime.now()),
                
            #     'line_ids':[(0,0 ,{'account_id':acount_id ,'partner_id':self.client_id.id,'name':'Avance JNP PASS','debit':self.montant}),
            #     (0,0 ,{'account_id':20601,
            #     'partner_id':self.client_id.id,'name':'Avance JNP PASS','credit':self.montant})]
            #     })
                
            

            
                # av.post()
                return {'type': 'ir.actions.act_window_close'}
            else:
                raise ValidationError(_("Votre solde est insuffisant pour effectuer l\'opération demandé"))
        else:
                raise ValidationError(_("Merci de remplir la TVA retenu a la source"))
# -------------------------------------------------


# class gakd_recharger_carte(models.TransientModel):
#     _name = 'gakd.wizard.recharger.carte'
#     _rec_name = 'id'
#     _description = 'New Description'


#     def _getClient_id(self):
#         return self._context.get('active_ids',False)[0]

#     @api.onchange('client_id')
#     def _onchange_client(self):
#         self.update({
# 		'solde': self.client_id.solde_compte,
# 		'carte_ids': self.client_id.carte_ids
# 	})



#     client_id = fields.Many2one("res.partner",string="Client",default=_getClient_id)
#     solde = fields.Float(string='Solde non affecté',readonly=True)#,compute="_getSolde", store=True)
#     carte_ids = fields.Many2many("gakd.carte", "wizard_recharger_carte", "recharge_id", "carte_id","Liste des Cartes")


#     @api.multi
#     def save_recharger(self):
#         return {'type': 'ir.actions.act_window_close'}

