{
    'name': 'CapContrat',
    'version': '2.0',
    'summary': '',
    'description': '',
    'category': '',
    'author': 'IT SHORE',
    'website': '',
    'license': '',
    'depends': ['base','report','contrat_final'],
    'data': [
        
       
        'views/contrat.xml',
        'views/res_partner_view.xml',
        'views/echeance_view.xml',
        'views/departement_view.xml',
        'views/modele_notifivarion_view.xml',
        'views/menu_view.xml',
        'views/historique_view.xml',
        'views/crons.xml',
        
        'static/xml/data.xml',
        "security/res_groups.xml",
        "security/ir.model.access.csv",
        
        
       
        #'views/customer_view.xml'
    ],
    'css': ['static/css/style.css'],
    'js': ['static/js/qrcode.js'],
    'installable': True,
    'auto_install': False,
}
