# -- coding: utf-8 --
from odoo import api, fields, models



class vehicle(models.Model):
    _inherit = 'fleet.vehicle'
    
    @api.one
    def default_get(self):
        self.qty1=0
        voyage = self.env['tms.travel'].search([('unit_id', '=', self.id)])
        data =[]
        data = [(4,tr.id) for tr in voyage]
        self.update({
                'voyage_camion': [(6, 0, data)]
                })
        # res['voyage_camion']=data
        # return res



    genre = fields.Char('Genre')
    carrosserie = fields.Char('Carrosserie')
    n_formule = fields.Integer('N° Formule')
    nom_vendeur = fields.Many2one('res.users','Vendeur')
    date_heure_achat = fields.Datetime(u"Date et heure d'achat")
    prix_achat = fields.Float(u"Prix d'achat")
    n_piece = fields.Integer('N° Pièce')
    frais = fields.Char('Frais')
    document = fields.Char('Documents')
    version = fields.Char('Version')
    date_circulation = fields.Datetime("Date de mise en circulation")
    categorie = fields.Char('Catégorie')
    energe = fields.Char('Energie')
    date_certificat = fields.Datetime("Date de certificat")
    type_camion = fields.Selection([('Occasion','Occasion'),('Neuf','Neuf')],"Type")
    prix_vente = fields.Float("Prix de vente")
    dernier_relve_km=fields.Float("Dernier relevé kilomètrique",readonly=True,compute="_get_odometer")
    km_prochaine_vidange = fields.Integer("Kilométrage de la prochaine vidange",readonly=True)
    date_dernier_entretien= fields.Date("Date du dernier entretien technique",readonly=True)
    date_prochain_entretien= fields.Date("Date du prochain entretien technique",readonly=True)
    date_dernier_Jaugeage= fields.Date("Date du dernier jaugeage du camion",readonly=True)
    date_prochain_Jaugeage= fields.Date("Date du prochain jaugeage du camion",readonly=True)
    consomaion_100=fields.Float("Consommation au 100 KM")
    voyage_camion = fields.Many2many("tms.travel",compute=default_get)
    statut=fields.Selection([('En panne','En panne'),('En Réparation','En Réparation'),('En Maintenance','En Maintenance'),('Fonctionnel','Fonctionnel')],readonly=True)
    def _get_odometer(self):
        for record in self:
            odometerss = self.env['fleet.vehicle.odometer'].search([('vehicle_id', '=', record.id)])
            odom=0.0
            idd=1
            for a in odometerss :
                if idd<=a.id:
                    idd=a.id
                    odom=a.value
            record.odometer = odom
            record.dernier_relve_km=odom
            # record.dernier_relve_km=record.odometer

    def _set_odometer(self):
        for record in self:
            record.dernier_relve_km = record.odometer
        super(vehicle,self)._set_odometer()

   
class autre_frais(models.Model):
    _name = "autre.frais"

    types = fields.Char("Type")
    montant =fields.Float("Montant")
    note =fields.Text("Note")
    voyage=fields.Many2one("tms.travel")
    