# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time

class benin_petro_retour_bon_consommation(models.TransientModel):
    _name = 'gakd.wizard.retour_bon_consommation'

    point_vente_id=fields.Many2one('gakd.point.vente', string='Point de  Vente')
    product_id =fields.Many2one('product.product', string='Produit')
    start_date = fields.Datetime(
         string='Date Début',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
    end_date = fields.Datetime(
         string='Date Fin',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )


    @api.multi
    def print_report(self):
        print 555555555555555555555
        ds=datetime.strptime(self.start_date, '%Y-%m-%d %H:%M:%S')
        de=datetime.strptime(self.end_date, '%Y-%m-%d %H:%M:%S')
        res={}
        data = {}
        liste_transactions = self.env['gakd.carte.consommation'].search([('point_vente_id','=',self.point_vente_id.id),('type_vente','=','Vente par TV')])
        for tr in liste_transactions:
            if tr.ticket_id:
                if datetime.strptime(tr.create_date, '%Y-%m-%d %H:%M:%S')>= ds and datetime.strptime(tr.create_date, '%Y-%m-%d %H:%M:%S')<= de:
                    if tr.ticket_id.tv_type.type_name not in res:
                        res[tr.ticket_id.tv_type.type_name] = {
                            'type':tr.ticket_id.tv_type.type_name,
                            'montant':0,
                            'qte':0
                        }
                        
                    else:
                        res[tr.ticket_id.tv_type.type_name]["montant"] = float(res[tr.ticket_id.tv_type.type_name]["montant"]) + float(tr.montant)
                        res[tr.ticket_id.tv_type.type_name]["qte"] = float(res[tr.ticket_id.tv_type.type_name]["qte"]) + float(tr.quantite)
                        #print res
                        #data['qte'] = 
                        #res[tr.ticket_id.tv_type.type_name] = float(res[tr.ticket_id.tv_type.type_name]) + float(tr.montant)
                        
                    
        print res
        #print listes_transactions
        gerant = self.env['gakd.agent'].search([('point_vente_id','=',self.point_vente_id.id),('fonction','=','Gerant')])[0].name
        datas = {
                 'form':
                    {
                        'point_vente': self.point_vente_id.name,
                        'produit': self.product_id.name,
                        'gerant': gerant,
                        'transactions':res
                        
                     }
                }
        print datas
        return self.env['report'].get_action(self, 'gakd.retour_bon_consommation_report', data=datas)

class retour_bon_consommation_report(models.AbstractModel):
    _name = 'report.gakd.retour_bon_consommation_report'
    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('gakd.retour_bon_consommation_report')
        
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
            }
        print data
        return report_obj.render('gakd.retour_bon_consommation_report', docargs)