# -*- coding: utf-8 -*-
from odoo import api, models,fields, _
from num2words import num2words
from datetime import datetime, date, timedelta
from openerp.exceptions import ValidationError
class transaction_wizard(models.TransientModel):
    _name = "transaction.wizard"
    _description = "Transaction"
    def _get_carte_consommation_id(self):
        return self._context.get('active_ids',False)[0]  

    def _getpoint_vente(self):
        data= self._get_carte_consommation_id()
        return self.env["gakd.carte.consommation"].search([('id','=',data)]).point_vente_id
    def _getproduit(self):
        data= self._get_carte_consommation_id()
        return self.env["gakd.carte.consommation"].search([('id','=',data)]).product_ids
    def _getquantite(self):
        data= self._get_carte_consommation_id()
        return self.env["gakd.carte.consommation"].search([('id','=',data)]).quantite
    def _getmontant(self):
        data= self._get_carte_consommation_id()
        return self.env["gakd.carte.consommation"].search([('id','=',data)]).montant
    def _getdate_transaction(self):
        data= self._get_carte_consommation_id()
        return self.env["gakd.carte.consommation"].search([('id','=',data)]).create_date
    def _gettype_vente(self):
        data= self._get_carte_consommation_id()
        for ele in self.env["gakd.carte.consommation"].search([('id','=',data)]) :
            for el in ele.transaction_lines :
                return el.type_vente
    def _getcarte_vente(self):
        data= self._get_carte_consommation_id()
        for ele in self.env["gakd.carte.consommation"].search([('id','=',data)]) :
            for el in ele.transaction_lines :
                return el.carte_id
    point_vente=fields.Many2one("gakd.point.vente",string="Point de vente",default=_getpoint_vente)
    produit=fields.Many2one("product.product",string="Produit",default=_getproduit)
    quantite=fields.Char(string="Quantité",default=_getquantite)
    montant=fields.Char(string="Montant",default=_getmontant)
    date_transaction=fields.Datetime(string="Date transaction",default=_getdate_transaction)
    type_vente=fields.Char(string="Moyens de paiments",default=_gettype_vente)
    carte=fields.Many2one("gakd.carte",string="Carte",default=_getcarte_vente)



    def refund(self):
        print 'refuuuuuuuuuuuuuuuuuuuuuuuuuuund' 
        print 'iiiiiiiiiiiiiiiiiiiin'
        transaction_id= self._get_carte_consommation_id()
        PosOrder = self.env['pos.order']
        current_session = self.env['pos.session'].search([('state', '!=', 'closed'), ('user_id', '=', self.env.uid)], limit=1)
        orders=self.env['pos.order'].search([('transaction_id', '=', transaction_id)])
        for order in orders:
            clone = order.copy({
                # ot used, name forced by create
                'name': order.name + _(' REFUND'),
                'session_id': int(self.quantite),
                'date_order': fields.Datetime.now(),
                'pos_reference': order.pos_reference,
                'transaction_id':transaction_id
            })
            PosOrder += clone
        for clone in PosOrder:
            for order_line in clone.lines:
                order_line.write({'qty': -order_line.qty})
        print 'hhhhhhhhhhhhhhhhhhhhhhhi'
        old_solde=self.carte.solde
        self.carte.write({
            'solde':old_solde+float(self.montant)
        })