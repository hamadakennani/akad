# -*- coding: utf-8 -*-
import time
import odoo
from datetime import datetime
from odoo import SUPERUSER_ID
from odoo import fields, api, models,_
from odoo.tools.translate import _



class document_directory(models.Model):
    _inherit = 'document.directory'

    def _auto_init(self):
        # self._sql_constraints = [
        #     ('dirname_uniq', 'Check(1=1)', 'The directory name must be unique!')
        # ]
	#cr.execute('ALTER TABLE document_directory DROP CONSTRAINT IF EXISTS document_directory_dirname_uniq')
        super(document_directory, self)._auto_init()

    # _sql_constraints = [
    #         ('dirname_uniq', 'Check(1=1)', 'The directory name must be unique!')
    # ]
    # _defaults = {

    # }    
    def _check_duplication(self,vals,op='create'):
	company = self.env["res.users"].read( uid)
        name=vals.get('name',False)
        parent_id=vals.get('parent_id',False)
        ressource_parent_type_id=vals.get('ressource_parent_type_id',False)
        ressource_id=vals.get('ressource_id',0)
        if op=='write':
            for directory in self.browse(SUPERUSER_ID):
                if not name:
                    name=directory.name
                if not parent_id:
                    parent_id=directory.parent_id and directory.parent_id.id or False
                # TODO fix algo
                if not ressource_parent_type_id:
                    ressource_parent_type_id=directory.ressource_parent_type_id and directory.ressource_parent_type_id.id or False
                if not ressource_id:
                    ressource_id=directory.ressource_id and directory.ressource_id or 0
                res=self.search(cr,uid,[('id','<>',directory.id),('name','=',name),('parent_id','=',parent_id),('ressource_parent_type_id','=',ressource_parent_type_id),('ressource_id','=',ressource_id)])
                if len(res):
                    return False
        if op=='create':
            res = self.search([('name','=',name),('company_id','=',company.get('company_id')[0])])
            if len(res):
                return False
        return True
    @api.model
    def create(self,vals):
        return super(document_directory, self).create(vals)

    @api.model
    def write(self,vals):
        return super(document_directory, self).write(vals)

    


