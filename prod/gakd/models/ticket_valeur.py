# -*- coding: utf-8 -*-
from odoo import api, fields, models
from random import randint

class Ticket_valeur(models.Model):
    _name = 'gakd.ticket_valeur'
    _rec_name='num_serie'
    _order = 'create_date desc'

    def _default_serie(self):
	result = True
	serie_proposition = 0
        while result==True:
            serie_proposition = str(randint(111, 999)) + str(randint(111, 999)) + str(randint(111, 999)) + str(randint(111, 999))
            result = self.search([['num_serie', '=', int(serie_proposition)]]).id
        return serie_proposition
    def _getQrCode(self,string):
	import hashlib
	md5 = hashlib.md5()
	qrcode = ""
	result = True
	serie_proposition = 0
        while result==True:
            md5.update(string+str(serie_proposition))
            qrcode = md5.hexdigest()
            serie_proposition += 1 
            result = self.search([['qrcode', '=', qrcode ]]).id
	return qrcode
    tv_type = fields.Many2one('gakd.tv_type',string='Coupure')
    client = fields.Many2one('res.partner',string='Client')
    etat = fields.Selection([('util','Utilisé'),('nonutil','Non utilisé')],default='nonutil')
    qrcode = fields.Char(string="dddd")
    num_serie = fields.Char(string=u'Numéro de serie')
    print_hestory_id = fields.Many2one('tv_print_hestory',string='hestory')
    remise = fields.Many2one('gakd.tv_remise',string='Remise')
    @api.model
    def create(self, vals):
        vals['num_serie'] = self._default_serie()
        qrcode = self._getQrCode(str(vals.get("num_serie"))+'<:>AKAD<:>'+str(vals.get("date_expiration")))
        if qrcode=="":
            raise ValidationError(_("Merci de contacter le prestataire pour la génération du QRCODE"))
        if 'qrcode' not in vals:
            vals['qrcode'] = qrcode
        vals['etat']='nonutil'
        print 'oooooooooooo'
        print vals
        return super(Ticket_valeur, self).create(vals)
    
