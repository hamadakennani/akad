# -- coding: utf-8 --
from odoo import api, fields, models

from openerp.exceptions import ValidationError


class voyage(models.Model):
    _inherit = 'tms.travel'

    trans_id=fields.Many2one('gakd.transfert')
    transfert_list = fields.Many2many('sbtf.transfert')
    capacite = fields.Float("Capacité total",readonly=True)
    capacite_charge =fields.Float("Total Qté Chargée",readonly=True)
    capacite_reste =fields.Float("Reste à charger",readonly=True)
    consomaion_100=fields.Float("Consommation au 100 KM")
    consomaion_theotique=fields.Float("Consommation Théorique total")
    consomaion_real=fields.Float("Consommation réelle total")
    document_joindre = fields.One2many('document.joindre','voyage')
    bon_livraison = fields.One2many('bon.livraison','voyage')
    autre_frais = fields.One2many('autre.frais','voyage',"Autre frais")
    frais=fields.Selection([('Frais 1','Frais 1'),('Frais 2','Frais 2'),('Frais 3','Frais 3'),('Frais 4','Frais 4'),('Frais 5','Frais 5')],string="Frais de route")
    # camion_voyage = fields.Many2one("fleet.vehicle")
    @api.onchange('unit_id')
    def get_chauffeur(self):
        camion=self.env['fleet.vehicle'].search([('id','=',self.unit_id.id)])
        self.sudo().update({
            'employee_id':camion.employee_id.id,
            'capacite':camion.capacite,
            'capacite_reste':camion.capacite,
            'consomaion_100':camion.consomaion_100
        })
    @api.onchange('route_id')
    def get_consomation(self):
        route =self.env['tms.route'].search([('id','=',self.route_id.id)])
        consomation=route.distance
        consomation=(consomation*self.consomaion_100)/100
        self.sudo().update({
            
            'consomaion_theotique':consomation
        })
    @api.onchange('transfert_list')
    def get_quantite(self):
        qte_chargee=0
        qte_reste=self.capacite
        for line in self.transfert_list : 
            qte_chargee+=line.qty
            qte_reste-=line.qty
            line.travel=self.id
        self.sudo().update({
            'capacite_charge':qte_chargee,
            'capacite_reste':qte_reste,
            

        })
    @api.constrains('transfert_list')
    def _check_quantite(self):
        print 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
        print self.capacite_charge
        print self.capacite
        if self.capacite_charge>self.capacite:
            raise ValidationError("La quantité chargé est plus grande que la capacité du camion") 

    @api.model
    def create(self, values):
        print values.get('employee_id')
        print values.get('capacite')
        travel = super(voyage, self).create(values)
        print travel
        print values
        print travel.employee_id
        print travel.capacite
        print 4444444444444444444444444
        travel.employee_id=values.get('employee_id')
        travel.capacite=values.get('capacite')
        travel.capacite_charge=values.get('capacite_charge')
        travel.capacite_reste=values.get('capacite_reste')
        return travel

class document_joindre(models.Model):
    _name="document.joindre"
    _rec_name="commentaire"
    piece=fields.Binary("Piéce jointe")
    commentaire = fields.Char("Commentaire")
    voyage = fields.Many2one("tms.travel")

class bon_livaison(models.Model):
    _name="bon.livraison"
    _rec_name="piece"
    piece=fields.Binary("Piéce jointe")
    sale_id = fields.Many2one("sale.order" ,string="Commande")
    transfert_id = fields.Many2one("gakd.transfert",string="Transfert")
    voyage = fields.Many2one("tms.travel")
    jnp_picking_id = fields.Many2one('stock.picking', "Picking")