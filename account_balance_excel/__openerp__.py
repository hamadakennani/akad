# -*- coding: utf-8 -*-

# Part of Probuse Consulting Service Pvt Ltd. See LICENSE file for full copyright and licensing details.

{
    'name': 'Account Trial Balance Excel Report',
    'version': '1.0',
    'price': 80.0,
    'live_test_url': 'https://youtu.be/Ka3x_TmDOZc',
    'currency': 'EUR',
    'license': 'Other proprietary',
    'category': 'Accounting',
    'summary': 'Excel report for Trial Balance',
    'description': """        
     You can install xlwt library in following link
     https://pypi.python.org/pypi/xlwt
     
Tags:
Account Balance
Account Balance Excel
excel reports
accounting reports
account finance report
Balance report
Balance report in excel
report in excel
odoo community report
odoo community accounting reports
community accounting reports
financial reports community
Trial Balance excel
""",
    'author': 'Probuse Consulting Service Pvt. Ltd.',
    'website': 'www.probuse.com',
    'depends': ['account'],
    'data': [
            'security/ir.model.access.csv',
            'wizard/account_balance_report_view.xml',
             ],
    'installable': True,
    'application': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
