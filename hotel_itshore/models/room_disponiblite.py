# -- coding: utf-8 --
from datetime import date
from odoo import fields, models,api
from datetime import datetime, timedelta
def  fu_reservation_line(self,reserv_id):
        hotel_reservation_line=[]
       
        if self.folio_id.reservation_id.id:
           for hotel_reservation in  self.env['hotel.reservation'].search([('id','!=',self.folio_id.reservation_id.id)]):
                if hotel_reservation.state in ('confirm'):
                    for  reservation_line in hotel_reservation.reservation_line:
                        if reservation_line.reserve_view.id==reserv_id:
                            hotel_reservation_line.append(reservation_line)

                   
        else:
            for hotel_reservation in  self.env['hotel.reservation'].search([]):
               if hotel_reservation.state in ('confirm'):
                    for  reservation_line in hotel_reservation.reservation_line:
                        if reservation_line.reserve_view.id==reserv_id:
                            hotel_reservation_line.append(reservation_line)
        return  hotel_reservation_line
                   
          


@api.multi
def check_overlap( date1, date2):
        date2 = datetime.strptime(date2, '%Y-%m-%d')
        date1 = datetime.strptime(date1, '%Y-%m-%d')
        delta = date2 - date1
        return set([date1 + timedelta(days=i) for i in range(delta.days + 1)])

def is_room_disponible(obj,room_id,date_debut,date_fin):
        print 'is_romm_disponible '
        print ' values '
        print obj
        print room_id
        print date_debut
        print date_fin
     
        hotel_reservation_line=[]
       
        folio__lines=obj.env['hotel.folio.line'].search([])
        
       
        room=obj.env['hotel.room']
        dt='%Y-%m-%d %H:%M:%S'
        
        mytime = "%Y-%m-%d"
        room_bool=False

        if  date_debut and date_fin and room_id :
            room=room.search([('id' ,'=',room_id)])
            
            reserv_checkin=  datetime.strptime(date_debut,'%Y-%m-%d').date()
            reserv_checkout=  datetime.strptime(date_fin,'%Y-%m-%d').date()
           
            for line in room.room_reservation_line_ids.\
                                search([('status', 'in', ('confirm', 'done')),
                                        ('room_id', '=', room_id)]):
                
                    check_in = datetime.strptime(line.check_in, dt).date()
                    check_out = datetime.strptime(line.check_out, dt).date()
                    print "----------oooooooooooooooooooooo----------------"
                    print reserv_checkin
                    print check_out
                    if check_in <= reserv_checkin <= check_out:
                        room_bool = True
                        
                    if check_in <= reserv_checkout <= check_out:
                        room_bool = True
                        
                    if reserv_checkin <= check_in and \
                            reserv_checkout >= check_out:
                        room_bool = True
                        
                    
                    
                    

                    if room_bool  :
                
                        return True
            for folio_line in folio__lines.search([('product_id','=',room.product_id.id)]):
                check_in = datetime.strptime(folio_line.checkin_date, dt).date()
                check_out = datetime.strptime(folio_line.checkout_date, dt).date()
            
                if check_in <= reserv_checkin <= check_out:
                    room_bool = True
                if check_in <= reserv_checkout <= check_out:
                    room_bool = True
                if reserv_checkin <= check_in and \
                        reserv_checkout >= check_out:
                    room_bool = True
                
               
                if room_bool:
                         return True
        return False    
    

def room_state(obj,room_id,date_debut,date_fin):
       
     
        hotel_reservation_line=[]
        print  ' onchange iiiiiii '
        cont=0
        contFloi=0
        print 'room_state ------------- <>><< disponiblity '
        
        folio__lines=obj.env['hotel.folio.line']
        room=obj.env['hotel.room']
        dt='%Y-%m-%d %H:%M:%S'
        da='%Y-%m-%d'
        print 'count days '
        mytime = "%Y-%m-%d"
        room_bool=False
        roomState='free'
        house_keeping='Propre'
        date_fin_maintenance=''
        room_house_keeping=False
        room=room.search([('id' ,'=',room_id)])
        if  date_debut and  date_fin and room_id:
            print ' Opppp ooo if '
            reserv=room.search([('product_id' ,'=',room.product_id.id)])
            print date_debut
            reserv_checkin=  datetime.strptime(date_debut,'%Y-%m-%d')
            reserv_checkout=  datetime.strptime(date_fin,'%Y-%m-%d')
            print 'reserv_checkout'
            print reserv_checkout
            print "obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'maintenance')])"
            print obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'maintenance')])
            print "obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'dirty')])"
            print obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'dirty')])
            for hk in  obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'maintenance')]):
                print "obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'maintenance')])"
                print  hk.end_date
                print obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'maintenance')])
                if hk.end_date and hk.current_date:
                        print 'hk.end_date and hk.current_date:'
                        check_out = datetime.strptime(hk.end_date,  da)
                        check_in = datetime.strptime(hk.current_date,  da)
                        if  check_in >=reserv_checkin<=check_out:
                            room_house_keeping = True
                            print 'room_house_keeping = True'
                        if room_house_keeping:
                            house_keeping='Maintenance '
                            
                            return [house_keeping, hk.end_date]
                elif hk.current_date:
                    check_in = datetime.strptime(hk.current_date,  da)
                    if check_in == reserv_checkin:
                        room_house_keeping = True
                    if room_house_keeping:
                        print 'room_house_keeping = True 2'
                        house_keeping='Maintenance'

                        return [house_keeping,hk.current_date]
            
            for hk in  obj.env['hotel.housekeeping'].search([('room_no','=',room_id),('state','=', 'dirty')]):
                    if hk.end_date and hk.current_date:
                            check_out = datetime.strptime(hk.end_date,  da)
                            check_in = datetime.strptime(hk.current_date,  da)
                            if  check_in >=reserv_checkin<=check_out:
                                room_house_keeping = True
                                date_fin_maintenance
                            if room_house_keeping:
                                house_keeping='Sale'  
                                return [house_keeping,'']
                    elif hk.current_date:
                        check_in = datetime.strptime(hk.current_date,  da)
                        if check_in == reserv_checkin :
                            room_house_keeping = True
                        if room_house_keeping:
                            house_keeping='Sale'
                            return [house_keeping,'']
        return ['Propre','']
               