# -*- coding: utf-8 -*-
##############################################################################
#
# Copyright (c) 2017 brain-tec AG (http://www.braintec-group.com)
# All Right Reserved
#
# See LICENSE file for full licensing details.
##############################################################################
{
    'name': "Read only user",
    'summary': "Enables a user to only have read access rights to all modules",
    'description': "The read only flag for a user can be activated in Settings -> Users",
    'author': "brain-tec",
    'website': "http://www.braintec-group.com/",
    'category': 'Extra Tools',
    'version': '1.0',
    'price': 9.99,
    'currency': 'EUR',
    'license': 'OPL-1',
    'images': ['static/description/main_screenshot.png'],
    'depends': ['base'],
    'data': ['views/res_users_ext.xml'
    ],
}
