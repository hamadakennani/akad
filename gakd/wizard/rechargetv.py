# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import time
from pprint import pprint


class sous_chargeur(models.TransientModel):

    _name="gakd.wizard.soustv"
    _rec_name='id'

    def _getChargeur_id(self):
        return self._context.get('active_ids',False)[0]
    
    def _getsolde(self):
        return self.env["gakd.chargeur"].search([('access','=',self.env.uid)]).montant_plafond_tv

    montant = fields.Float(string='Montant plafond',required=True)
    booltest=fields.Selection([('Augmenter','A augmenter'),('Diminuer','A diminuer')],default='Augmenter')
    sous_chargeur_id = fields.Many2one("gakd.sous_chargeur",string="Trésorier",default=_getChargeur_id)
    solde = fields.Float(string='Votre solde',default=_getsolde)
    
    @api.multi
    def test(self):
        if self.booltest=='Augmenter': 
            p = self.env["gakd.chargeur"].search([('access','=',self.env.user.id)])
            if self.montant <= p.montant_plafond_tv:
                #p=self.env["gakd.chargeur"].search([('id','=',self.sous_chargeur_id.superv.id)])
                nv_montant= p.montant_plafond_tv-self.montant
                self.env['gakd.historique'].create({
                'montant_init':p.montant_plafond_tv,
                'montant_fin':nv_montant,
                'chargeur':p.id,
                'type_op':'Augmentation TV',
                'diff':abs(p.montant_plafond_tv-nv_montant)
                
                })
                
                p.write({
                    'montant_plafond_tv':nv_montant,
                })
                po=self.env["gakd.sous_chargeur"].search([('id','=',self.sous_chargeur_id.id)])
                nv_m= po.montant_plafond_tv+self.montant
                po.montant_super_tv = nv_montant
                p=self.env["gakd.chargeur"].search([('id','=',self.sous_chargeur_id.superv.id)])
                nv_montant= p.montant_plafond_tv+self.montant

                po.write({
                    'montant_plafond_tv':nv_m,
                })

            else :
                raise ValidationError('Votre solde est insuffisant ')
        elif self.booltest=='Diminuer':
            if self.montant <= self.sous_chargeur_id.montant_plafond_tv:
                p = self.env["gakd.chargeur"].search([('access','=',self.env.user.id)])
                #p=self.env["gakd.chargeur"].search([('id','=',self.sous_chargeur_id.superv.id)])
                nv_montant= p.montant_plafond_tv+self.montant
                self.env['gakd.historique'].create({
                'montant_init':p.montant_plafond_tv,
                'montant_fin':nv_montant,
                'chargeur':p.id,
                'type_op':'Diminution TV',
                'diff':abs(p.montant_plafond_tv-nv_montant)
                })
                p.write({
                    'montant_plafond_tv':nv_montant,
                })
                po=self.env["gakd.sous_chargeur"].search([('id','=',self.sous_chargeur_id.id)])
                nv_m = po.montant_plafond_tv-self.montant
                po.montant_super_tv = nv_montant
                po.write({
                    'montant_plafond_tv':nv_m,
                })

            else :
                raise ValidationError('Le  solde est insuffisant ')


# # -------------------------------------------------


class gakd_chargeur(models.TransientModel):
    _name="gakd.wizard.gakd.chargeurtv"
    _rec_name = 'id'

    def _getChargeur_id(self):
        return self._context.get('active_ids',False)[0]

    montant = fields.Float(string='Montant plafond',required=True)
    booltest=fields.Selection([('Augmenter','A augmenter'),('Diminuer','A diminuer')],default='Augmenter')
    chargeur_id = fields.Many2one("gakd.chargeur",string="Trésorier",default=_getChargeur_id)

    @api.multi
    def recharger(self):
        nouveau=0
        typeop='hh'
        if self.montant !=0:
            if self.booltest=='Augmenter':
                nouveau=self.chargeur_id.montant_plafond_tv + self.montant
                typeop='Augmenter TV'

            if self.booltest=='Diminuer':
                nouveau=self.chargeur_id.montant_plafond_tv - self.montant
                typeop='Diminuer TV'
            p=self.env["gakd.chargeur"].search([('id','=',self.chargeur_id.id)])
            if nouveau != p.montant_plafond_tv:
                if nouveau >= 0:                  
                    
                    self.env['gakd.historique'].create({
                        'montant_init':p.montant_plafond_tv,
                        'montant_fin':nouveau,
                        'chargeur':self.chargeur_id.id,
                        'type_op':str(typeop),
                        'diff':abs(p.montant_plafond_tv-nouveau)    
                        })
                    p.write({
                        'montant_plafond_tv':nouveau,
                    })
                else  :
                    raise ValidationError('Solde  insuffisant ')
        else :
            raise ValidationError('Donnez un montant supérieur a zéro')

        
