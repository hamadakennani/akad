# -- coding: utf-8 --
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from unidecode import unidecode
from dateutil.parser import parse
import pytz
import unicodedata
import string
import codecs
import sys
import base64
import re

import datetime
from datetime import datetime as dt
class journal(models.Model):
    _inherit = 'account.journal'
class Versement(models.Model) :
    _inherit = 'gakd.versement'
    day_of_week = fields.Char(string="Jour de semaine",compute='_get_day_of_week',store=True)
    # @api.model
    # def create(self,vals) :
    #     week_days= ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche']
    #     versement = super(Versement,self).create(vals)
    #     day_of_week = week_days[versement.create_date.weekday()]
    #     versement.write({
    #         'day_of_week':day_of_week
    #     })
    #     return versement
    @api.multi
    def _get_day_of_week(self) :
        week_days= ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche']
        for record in self :
            day = dt.strptime(record.create_date, '%Y-%m-%d %H:%M:%S').weekday()
            day_of_week = week_days[day]
            record.update({
                'day_of_week':day_of_week
            })
        return True
            
    
class VersementLine(models.Model):
    _inherit = 'gakd.versement_line'
    _rec_name='num_versement'
    
    # ,('valideManuel', 'Validé manuel')
    state = fields.Selection(string='Etat',
        selection=[('annule', 'Annulé'),('brouillon', 'Brouillon'),  ('valide', 'Validé manuellement'),('valideAuto', 'Validé automatiquement')],
        default='brouillon')
    etat_creation = fields.Selection(string='Etat creation', selection=[('non', 'Non'),('oui', 'Oui')], default='non') 
    versement_line_id = fields.Many2one('gakd.valider.versement')
    day_of_week = fields.Char(string="Jour de semaine",compute='_get_day_of_week')
    
    @api.multi
    def _get_day_of_week(self) :
        week_days= ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi','Dimanche']
        for record in self :
            day = dt.strptime(record.create_date, '%Y-%m-%d %H:%M:%S').weekday()
            day_of_week = week_days[day]
            record.update({
                'day_of_week':day_of_week
            })
        return True
    
class VersementValider(models.Model):
    _name = 'gakd.valider.versement'
    _description = 'New Description'
    _order="create_date desc"

    versement_line=fields.One2many(comodel_name='gakd.versement_line', inverse_name='versement_line_id', string='Lignes de versement')
    etat_creation = fields.Selection(string='Etat creation', selection=[('non', 'Non'),('oui', 'Oui')], default='non') 


    datD=dt.now()-datetime.timedelta(1)
    datF=dt.now()
    date_debut= fields.Date(string='Date debut',   default=datD.strftime('%Y-%m-%d %H:%M:%S'), )
    date_fin=fields.Date(string='Date fin',   default=datF.strftime('%Y-%m-%d %H:%M:%S'), )      

    @api.multi
    def remplireListeVersement(self,dateD,dateF):
        liste_versements1=self.env['gakd.versement_line'].search([('create_date','>=',dateD),('create_date','<=',dateF)])
        return liste_versements1

    @api.multi
    def returnBrouillon(self):
        print('test test test test test test test test test test test test test test ')
        # liste_CB=self.env['account.journal'].search([('type','=','bank')])
        # for nm in liste_CB:
        #     print(nm.name)
        
        date1=self.date_debut    
        datFormatV1=parse(date1)
        date1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')
        date2=self.date_fin    
        datFormatV2=parse(date2)
        date2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
        
        
        # 'journal_id','like','' 
        #  ('state','=','valideAuto')        
        liste_releve=self.env['gakd.releve_bancaire'].search([            
                    # ('date_operation','>=',date1),
                    # ('date_operation','<=',date2)
                    ])
        liste_versementsLine=self.env['gakd.versement_line'].search([
                        # ('create_date','>=',date1),
                        # ('create_date','<=',date2)
                        ])
        liste_versementss=self.env['gakd.versement'].search([
                        ('state','!=','annule'),
                        # ('create_date','>=',date1),
                        # ('create_date','<=',date2)
                        ])
        print ('number of liste versement'+str(len(liste_versementsLine)))
        print ('number of liste releve'+str(len(liste_releve))) 
        for vl in liste_versementsLine:
            print (111111111111111)
            vl.write({
                'state': 'brouillon',
                'etat_creation': 'non',
            })
            # vl.versement_id.write({
            #     'state': 'brouillon',
            # })
        for vs in liste_versementss:
            print (22222222222222)            
            vs.write({
                'state': 'brouillon',
            })
        
        for rb in liste_releve:
            print( 33333333333333)
            rb.write({
                'state': 'brouillon',
                'id_versement':0,
                'etat_creation': 'non',
                'ecarts':0,
            })

    @api.onchange('date_debut')
    def _onchange_datedebut(self):
        if self.date_debut<=self.date_fin:
            datf1=self.date_debut    
            datFormatV1=parse(datf1)
            datf1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')

            datf2=self.date_fin    
            datFormatV2=parse(datf2)
            datf2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')

            data = self.remplireListeVersement(datf1,datf2)
            # print data
            return {'value':{'versement_line':[(6,0,data.ids)]}}

    @api.onchange('date_fin')
    def _onchange_date_fin(self):
        if self.date_debut<=self.date_fin:
            datf1=self.date_debut    
            datFormatV1=parse(datf1)
            datf1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')

            datf2=self.date_fin    
            datFormatV2=parse(datf2)
            datf2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
            
            data = self.remplireListeVersement(datf1,datf2)
            return {'value':{'versement_line':[(6,0,data.ids)]}}

    @api.multi
    def searchInLibelle(self,codePvt,libl):
        bol=False
        if codePvt:
            l=0
            libl=self.replaceCaractere(libl)[0].upper()
            codePvt=self.replaceCaractere(codePvt)[0].upper()
            J0=' '
            if codePvt[0:2]== 'JO':
                J0=codePvt.replace("O",'0')
            if codePvt in libl or J0.upper() in libl:
                code = codePvt if codePvt in libl else J0.upper()
                j=libl.index(code)
                k=len(code)
                try:
                    if not libl[j+k].isdigit():
                        bol=True
                except:
                    bol=False
                
        return bol

    @api.multi
    def valideVersement(self,dateD,dateF):
        # caissier
        listeVersementsGegant=self.env['gakd.versement'].search([('state','!=','valideAuto'),
                                                                ('create_date','>=',dateD),
                                                                ('create_date','<=',dateF),
                                                                ('vers_par','=','gerant')])       
        for v in listeVersementsGegant:
            listVersementLine1=v.versement_line
            for vl in listVersementLine1:
                if vl.state=='valideAuto':
                    v.write({
                        'state': 'valideAuto',
                    })
        listeVersementscaissier=self.env['gakd.versement'].search([('state','!=','valideAuto'),
                                                                ('create_date','>=',dateD),
                                                                ('create_date','<=',dateF),
                                                                ('vers_par','=','caissier')])   
        for v in listeVersementscaissier:
            listVersementLine2=v.versement_line
            vl2=0
            for vl in listVersementLine2:
                if vl.state=='valideAuto':
                    vl2+=1
            if vl2==len(listVersementLine2):
                v.write({
                    'state': 'valideAuto',
                })

    #--- suprimer les espaces et les caractère spéciaux
    @api.one
    def replaceCaractere(self,libele):
        alphabet=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        'à','â','ç','è','é','ê','î','ô','ù','û',
        '0','1','2','3','4','5','6','7','8','9',]
        if not libele:
            libele='null'
        libele=libele.replace(" ","")
        for char in libele:
            if char.lower() not in alphabet:
                libele=libele.replace(char,"")
        return libele

    #   creer les versement line valider
    @api.multi
    def create_versementValide(self,dateD,dateF):
        print('11111111111111111111111111111111111111111111111111111111111111111111111111111111111')
        mn=0
        liste_versementV=self.env['gakd.versement_line'].search([
                ('state','=','valideAuto'),
                ('etat_creation','=','non'),
                ('create_date','>=',dateD),
                ('create_date','<=',dateF)
                ])
        for lvc1 in liste_versementV:
            mn+=1
            print(mn,'================================')
            listeIdRB=[]
            som=0.00
            #get journal
            if len(lvc1.journal_id)>0:
                journal=lvc1.journal_id[0]
            else:
                journal=lvc1.journal_id
            #------get pointe vente,versement
            if len(lvc1.versement_id)>0:
                versement=lvc1.versement_id[0]           
                if len(lvc1.versement_id[0].point_vente_id)>0:
                    pointeventes=lvc1.versement_id[0].point_vente_id[0]
                else:
                    pointeventes=lvc1.versement_id[0].point_vente_id
            else:
                versement=lvc1.versement_id
                if len(lvc1.versement_id.point_vente_id)>0:                    
                    pointeventes=lvc1.versement_id.point_vente_id[0]
                else:
                    pointeventes=lvc1.versement_id.point_vente_id
            if not pointeventes:
                libelle_PV='no'
            else:
                libelle_PV=pointeventes.libelle

            #date versement "format yyyy-mm-dd "
            datFormatV=parse(lvc1.create_date)
            datV_date=datFormatV.strftime('%Y-%m-%d')

            liste_RBv1=self.env['gakd.releve_bancaire'].search([
								('state','=','valideAuto'),
								('etat_creation','=','non'),
								('id_versement','=',lvc1.id),
								('date_operation','>=',dateD),
								('date_operation','<=',dateF)
								])
            for lrbc1 in liste_RBv1:
                listeIdRB.append(lrbc1.id)
                som+=float(lrbc1.montant.replace(" ",""))           
                
            if len(listeIdRB)>1:
                lvc1.write({
                    'etat_creation': 'oui'
                })
                for lrbc2 in liste_RBv1:
                    lrbc2.write({
                        'etat_creation': 'oui'
                    })
                    result=self.env['gakd.liste'].sudo().create({
                        'compte_bancaire':journal.id,
                        'point_vente':pointeventes.id,
                        'categorie_produit':versement.type_versement,
                        'statu_validaion':lvc1.state,
                        })
                    resultvr=self.env['gakd.versementreleve'].sudo().create({
                        'compte_bancaire':journal.name,
                        'point_vente':str(libelle_PV),
                        'categorie_produit':versement.type_versement,
                        'date_versement_releve':datV_date,
                        'num_versement':lvc1.num_versement,
                        'montant_versement':lvc1.montant,
                        'montant_releve': float(lrbc2.montant.replace(" ", "")),
                        'montant_manquant':lrbc2.ecarts,
                        'num_releve':lrbc2.ref,
                        'lebelle_Releve':lrbc2.libelle,
                        'statu_validaion':lvc1.state,
                        # 'liste_versement':result.id,
                        })
                # resultsom=self.env['gakd.versementreleve'].sudo().create({
                #         'compte_bancaire':"somme",
                #         'montant_versement':lvc1.montant,
                #         'montant_releve':som,
                #         'montant_manquant':lrbc2.ecarts,
                #         'lebelle_Releve':"some du produi "+versement.type_versement+" "+lvc1.num_versement,
                #         'statu_validaion':lvc1.state,
                #         })
            elif len(listeIdRB)==1 :
                lvc1.write({
                    'etat_creation': 'oui'
                })                
                rbc1=self.env['gakd.releve_bancaire'].search([('id','=',listeIdRB[0])])
                rbc1.write({
                    'etat_creation': 'oui'
                })              
                result=self.env['gakd.liste'].sudo().create({
                        'compte_bancaire':journal.id,
                        'point_vente':pointeventes.id,
                        'daters':datV_date,
                        'categorie_produit':versement.type_versement,
                        'statu_validaion':lvc1.state,
                    })
                resultvr1=self.env['gakd.versementreleve'].sudo().create({
                    'compte_bancaire':journal.name,
                    'point_vente':str(libelle_PV),
                    'categorie_produit':versement.type_versement,
                    'date_versement_releve':datV_date,
                    'num_versement':lvc1.num_versement,
                    'montant_versement':lvc1.montant,
                    'montant_releve':float(rbc1.montant.replace(" ","") ),
                    'montant_manquant':rbc1.ecarts,
                    'um_releve':rbc1.ref,
                    'lebelle_Releve':rbc1.libelle,
                    'statu_validaion':lvc1.state,
                    # 'liste_versement':result.id,
                    })

    #creer les versement annule
    @api.multi
    def create_versementBrouillon(self,dateD,dateF):
        #------ liste veresement
        print('222222222222222222222222222222222222222222222222222222222222222222')
        m=0
        listes_versement=self.env['gakd.versement_line'].search([
            ('state','=','brouillon'),
            ('etat_creation','=','non'),
            ('create_date','>=',dateD),
            ('create_date','<=',dateF)],
            order="create_date desc")
        print('nombre line versement line anuller=  ',len(listes_versement))
        for lvc2 in listes_versement:
            #get journal
            if len(lvc2.journal_id)>0:
                journal=lvc2.journal_id[0]
            else:
                journal=lvc2.journal_id
            #------get pointe vente,versement
            if len(lvc2.versement_id)>0:
                versement=lvc2.versement_id[0]           
                if len(lvc2.versement_id[0].point_vente_id)>0:
                    pointeventes=lvc2.versement_id[0].point_vente_id[0]
                else:
                    pointeventes=lvc2.versement_id[0].point_vente_id
            else:
                versement=lvc2.versement_id
                if len(lvc2.versement_id.point_vente_id)>0:                    
                    pointeventes=lvc2.versement_id.point_vente_id[0]
                else:
                    pointeventes=lvc2.versement_id.point_vente_id

            #date versement "format yyyy-mm-dd "
            datFormatV=parse(lvc2.create_date)
            datV_date=datFormatV.strftime('%Y-%m-%d')
            m+=1
            print(m,'===================================')
            print(datV_date)

            #----get categorier produit
            # categorierProduitV=versement.type_versement
            # listcategorie=['gasoil','gazoil','essence']

            #deviser le num des versement 'num1/num2/num3' to ['num1','num2','num3']
            c=u'°'
            numver=u''.join(lvc2.num_versement.replace(c,'')).encode('utf-16')
            listNumVersement = re.findall(r'\d+',str(numver.replace(' ','')))
            listeRef=[]
            for n in listNumVersement:
                if len(n)>3:
                    listeRef.append(n)
            #point venete
            if not pointeventes:
                libelle_PV='no'
            else:
                libelle_PV=pointeventes.libelle
            #=-------- test si le nume de versement se forme "num1/num2/num3/..."
            montantVr1=0.0
            montantVr1=lvc2.montant
            nom_journal=journal.name
            #pour un num versement sous la formt "num1/num2/num3....."
            bol0=True
            #test par reference 1
            listes_RB1=self.env['gakd.releve_bancaire'].search([
                        ('state','=','brouillon'),
                        ('etat_creation','=','non'),
                        ('date_operation','>=',dateD),
                        ('date_operation','<=',dateF)
                        ])            
            for lrbc3 in listes_RB1:
                if lrbc3.ref and lrbc3.montant:
                    for numvers in listNumVersement:
                        if lrbc3.ref==numvers :
                            if lrbc3.bank.replace(" ", "").upper() in nom_journal.replace(" ", "").upper():
                                ecart=float(lrbc3.montant.replace(" ",""))-float(montantVr1)
                                print('BBBBBBBBBBBB11111111111111111111111111111')
                                bol0=False
                                lrbc3.write({
                                    'etat_creation': 'oui'
                                    })
                                lvc2.write({
                                    'etat_creation': 'oui'
                                    })
                                mr1=lrbc3.montant.replace(" ","") 
                                result=self.env['gakd.liste'].sudo().create({
                                    'compte_bancaire':journal.id,
                                    'point_vente':pointeventes.id,
                                    'categorie_produit':versement.type_versement,
                                    'statu_validaion':lvc2.state,
                                    })    
                                resultvr=self.env['gakd.versementreleve'].sudo().create({
                                    'compte_bancaire':nom_journal,
                                    'point_vente':libelle_PV,
                                    'categorie_produit':versement.type_versement,
                                    'date_versement_releve':datV_date,
                                    'num_versement':lvc2.num_versement,
                                    'montant_versement':lvc2.montant,
                                    'montant_releve':float(mr1),
                                    'montant_manquant':ecart,
                                    'num_releve':lrbc3.ref,
                                    'lebelle_Releve':lrbc3.libelle,
                                    'statu_validaion':'M.Ref-Mt.Diff',
                                    })
                if bol0==False:
                    break
                #fin test par reference
            #test par date,pointe vente ,categorie
            if bol0:
                if nom_journal.replace(' ', '').upper() != 'CASHCOLLECTION':
                    #-----test si categorier= produit blants
                    listes_RB3=self.env['gakd.releve_bancaire'].search([
                                ('state','=','brouillon'),
                                ('etat_creation','=','non'),
                                ('categorie_produit', '=', versement.type_versement),
                                ('date_operation','>=',dateD),
                                ('date_operation','<=',dateF)],
                                order="date_operation desc")
                    for lrbc4 in listes_RB3:
                        #------get et convertet  date de releve bancaire au yyyy-mm-dd
                        if lrbc4.date_operation:
                            datR2 = parse(str(lrbc4.date_operation))
                            datR_date=datR2.strftime('%Y-%m-%d')
                            #---- suprime les espaces dans montant de releve bancaire
                            if lrbc4.montant:
                                #---delete special characters dans les libelle
                                leb=self.replaceCaractere(lrbc4.libelle)[0].upper()
                                banck2=lrbc4.bank.replace(" ", "")
                                if datR_date==datV_date and (banck2.upper() in nom_journal.replace(" ", "").upper()):
                                    if self.searchInLibelle(libelle_PV,lrbc4.libelle):
                                        bol0=False
                                        ecart = float(lrbc4.montant.replace(" ", ""))-float(lvc2.montant)
                                        lvc2.write({
                                            'etat_creation': 'oui'
                                        })
                                        lrbc4.write({
                                            'etat_creation': 'oui'
                                        })
                                        result=self.env['gakd.liste'].sudo().create({
                                            'compte_bancaire':journal.id,
                                            'point_vente':pointeventes.id,
                                            'categorie_produit':versement.type_versement,
                                            'statu_validaion':lvc2.state,
                                            })
                                        resultvr=self.env['gakd.versementreleve'].sudo().create({
                                            'compte_bancaire':nom_journal,
                                            'point_vente':libelle_PV,
                                            'categorie_produit':versement.type_versement,
                                            'date_versement_releve':datV_date,
                                            'num_versement':lvc2.num_versement,
                                            'montant_versement':lvc2.montant,
                                            'montant_releve':float(lrbc4.montant.replace(" ","")),
                                            'montant_manquant':ecart,
                                            'num_releve':lrbc4.ref,
                                            'lebelle_Releve':lrbc4.libelle,
                                            'statu_validaion':'M.Date-M.PV Mt.Diff',
                                            # 'liste_versement':result.id,
                                            })
                                        print('BBBBBBBB222222222222222222222222222222222222')
                        if bol0==False:
                            break
                #pour un seul  num versement
                elif nom_journal.replace(' ', '').upper() == 'CASHCOLLECTION':
                    listes_RB4=self.env['gakd.releve_bancaire'].search([
                                ('state','=','brouillon'),
                                ('etat_creation','=','non'),
                                ('date_operation','>=',dateD),
                                ('date_operation','<=',dateF)],
                                order="date_operation desc")
                    #test par date  2
                    for lrbc6 in listes_RB4:
                        if lrbc6.date_operation:
                            datR=lrbc6.date_operation
                            datR2=parse(str(datR))
                            datR_date = datR2.strftime('%Y-%m-%d')                            
                            #get mantant
                            if lrbc6.montant:
                                mantantRBd2=lrbc6.montant.replace(" ","")                            
                                montantVL=float(lvc2.montant)
                                ecart2=float(mantantRBd2)-float(montantVL)
                                banck4=lrbc6.bank.replace(" ", "")
                                #---delete special characters dans les libelle
                                leb=self.replaceCaractere(lrbc6.libelle)[0].upper()
                                if  datR_date==datV_date and (banck4.upper() in nom_journal.replace(" ", "").upper()):
                                    if self.searchInLibelle(libelle_PV,lrbc6.libelle):
                                        bol0=False
                                        lvc2.write({
                                            'etat_creation': 'oui'
                                            })
                                        lrbc6.write({
                                            'etat_creation': 'oui'
                                            })
                                        result=self.env['gakd.liste'].sudo().create({
                                            'compte_bancaire':journal.id,
                                            'point_vente':pointeventes.id,
                                            'categorie_produit':versement.type_versement,
                                            'statu_validaion':lvc2.state,
                                            })    
                                        resultvr=self.env['gakd.versementreleve'].sudo().create({
                                            'compte_bancaire':nom_journal,
                                            'point_vente':libelle_PV,
                                            'categorie_produit':versement.type_versement,
                                            'date_versement_releve':datV_date,
                                            'num_versement':listNumVersement[0],
                                            'montant_versement':lvc2.montant,
                                            'montant_releve':float(mantantRBd2),
                                            'montant_manquant':ecart2,
                                            'num_releve':lrbc6.ref,
                                            'lebelle_Releve':lrbc6.libelle,
                                            'statu_validaion':'M.Date-M.PV Mt.Diff',
                                            # 'liste_versement':result.id,
                                            })
                                        print('BBBBBB3333333333333333333333')
                        if bol0==False:
                            break
                        #fin test par date

    # cree les versement et releve n'existe pas
    @api.multi
    def create_versementNotExixte(self,dateD,dateF):
        #create versement
        print('3333333333333333333333333333333333333333333333333333333333')
        n1=0
        n2=0
        listeVersemets=self.env['gakd.versement_line'].search([
                    ('state','=','brouillon'),
                    ('etat_creation','=','non'),
                    ('create_date','>=',dateD),
                    ('create_date','<=',dateF)])
        print('nombre line versement line anuller=  ',len(listeVersemets))
        # create versement n existe pas
        for lvc3 in listeVersemets:            
            #get journal
            if len(lvc3.journal_id)>0:
                journal=lvc3.journal_id[0]
            else:
                journal=lvc3.journal_id
            #------get pointe vente,versement
            if len(lvc3.versement_id)>0:
                versement=lvc3.versement_id[0]           
                if len(lvc3.versement_id[0].point_vente_id)>0:
                    pointeventes=lvc3.versement_id[0].point_vente_id[0]
                else:
                    pointeventes=lvc3.versement_id[0].point_vente_id
            else:
                versement=lvc3.versement_id
                if len(lvc3.versement_id.point_vente_id)>0:                    
                    pointeventes=lvc3.versement_id.point_vente_id[0]
                else:
                    pointeventes=lvc3.versement_id.point_vente_id
            #date versement "format yyyy-mm-dd "
            datV=lvc3.create_date
            datFormatV=parse(datV)
            datV_date=datFormatV.strftime('%Y-%m-%d')
            #code pointe vente
            codepv=pointeventes.libelle            
            if not codepv :
                codepv='-'
            n1+=1
            print(n1,'--==--==---==--===---===---===--===-==--==')
            print (datV_date)
            nBol=True
            dataexist=self.env['gakd.versementreleve'].search([
                    ('compte_bancaire','=',journal.name),
                    ('point_vente','=',codepv),
                    ('categorie_produit','=',versement.type_versement),
                    ('date_versement_releve','=',lvc3.create_date),
                    ('num_versement','=',lvc3.num_versement),
                    ('montant_versement','=',lvc3.montant),
                    ('statu_validaion', '=', 'V.n_existe_pas_dans_R'),
                    ])
            if len(dataexist)>0:
                nBol=False
            if nBol:
                # create veresement n existe pas            
                result=self.env['gakd.liste'].sudo().create({
                    'compte_bancaire':journal.id,
                    'point_vente':pointeventes.id,
                    'categorie_produit':versement.type_versement,
                    'statu_validaion':lvc3.state,
                })
                resultvr=self.env['gakd.versementreleve'].sudo().create({
                    'compte_bancaire':journal.name,
                    'point_vente':codepv,
                    'categorie_produit':versement.type_versement,
                    'date_versement_releve':lvc3.create_date,
                    'num_versement':lvc3.num_versement,
                    'montant_versement':lvc3.montant,
                    'montant_releve':0,
                    'montant_manquant':0,
                    'num_releve':"-",
                    'lebelle_Releve':"versement n'\existe pas dans les relevés bancaires",
                    'statu_validaion':'V.n_existe_pas_dans_R',
                    # 'liste_versement':result.id,
                    })
        #create releve bancaire
        liste_PV=self.env['gakd.point.vente'].search([])
        listeRB=self.env['gakd.releve_bancaire'].search([
            ('state','=','brouillon'),
            ('etat_creation','=','non'),
            ('date_operation','>=',dateD),
            ('date_operation','<=',dateF)])
        print ('nombre ligne releve.B =',len(listeRB))        
        for lrbc7 in listeRB:
            codepv='-'
            idpv=0
            for pv in liste_PV:
                if self.searchInLibelle(pv.libelle,lrbc7.libelle):
                    codepv=pv.libelle
                    idpv=pv.id
            liste_CB=self.env['account.journal'].search([('type','=','bank')])
            idcb=0
            for cb in liste_CB:
                if cb.name.replace(" ", "").upper() in lrbc7.bank.replace(" ", "").upper():
                    idcb=cb.id
            bankreleve=lrbc7.bank
            #------get et convertet  date de releve bancaire au yyyy-mm-dd
            datR=lrbc7.date_operation
            datR2=parse(str(datR))
            datR_date=datR2.strftime('%Y-%m-%d')
            print ('NNNNNNNN  nnnnnnnnn NNNNNNNNNN')
            n2+=1
            print(n2,'--==--==---==--===---===---===--===-==--==')
            print (datR_date)
            if not lrbc7.montant or lrbc7.montant=='None':
                mr1=0
            else:
                mr1=lrbc7.montant.replace(" ","") 
            print mr1
            result2=self.env['gakd.liste'].sudo().create({
                'compte_bancaire':idcb,
                'point_vente':idpv,
                'categorie_produit':"-",
                'statu_validaion':lrbc7.state,
            })    
            resultvr2=self.env['gakd.versementreleve'].sudo().create({
                'compte_bancaire': bankreleve,
                'point_vente':codepv,
                'categorie_produit':lrbc7.categorie_produit,
                'date_versement_releve':datR_date,
                'num_versement':"-",
                'montant_versement':0,
                'montant_releve':float(mr1),
                'montant_manquant':0,
                'num_releve':lrbc7.ref,
                'lebelle_Releve':lrbc7.libelle,
                'statu_validaion':'R.n_existe_pas_dans_V',
            })
            lrbc7.write({
            'etat_creation': 'oui'
            })
            print("99999999999999999999999999999999999")

    #validerles versements week end
    def weekEndsValidator(self):
        date1=self.date_debut    
        datFormatV1=parse(date1)
        date1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')
        date2 = self.date_fin
        datFormatV2=parse(date2)
        date2=datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
        data = []
        
        liste_versements=self.env['gakd.versement_line'].search([
                        ('state','!=','valideAuto'),
                        ('etat_creation','=','non'),
                        ('create_date','>=',date1),
                        ('create_date','<=',date2)])
        
        weekend_versements = [v for v in liste_versements if v.day_of_week in ['Samedi','Dimanche']]
                
        for versement in weekend_versements : 
            relve_date = None
            addedDays = 0
            if versement.day_of_week == 'Samedi' :
                addedDays = 2
            else :
                addedDays = 1
            relve_date = dt.strptime(versement.create_date, "%Y-%m-%d %H:%M:%S") + datetime.timedelta(days=addedDays)
            relve=[]
            if versement.versement_id.point_vente_id.libelle:
                relve = self.env['gakd.releve_bancaire'].search([
                                ('date_operation','=',relve_date),
                                ('montant','>=',(versement.montant)),
                                ('montant','<=',versement.montant),
                                ('libelle','ilike','%'+versement.versement_id.point_vente_id.libelle+'%'),
                                ('state','!=','valideAuto')
                                ])
            if len(relve) > 0 :  
                for r in relve : 
                    if str(versement.journal_id.name).replace(" ","").upper().find(r.bank.replace(" ","").upper()) >= 0  :
                        versement.write({
                            'state':'valideAuto'
                        })
                        r.write({
                            'state':'valideAuto',
                            # 'id_versement': versement.id,
                            # 'ecarts': float(sommontantRB)-float(montantVrRef),
                        })
                        print ("==================Week end===================")
                        data.append((relve,versement))
                        break      
        return True

    def createVersementline_someProduitBlans(self):
        # ------ liste veresement
        print('2222ok2222================================')
        datFormatV1 = parse(self.date_debut)
        date1 = datFormatV1.strftime('%Y-%m-%d %H:%M:%S')

        datFormatV2 = parse(self.date_fin)
        datFormatV2 += datetime.timedelta(1)
        date2 = datFormatV2.strftime('%Y-%m-%d %H:%M:%S')

        liste_versements = self.env['gakd.versement_line'].search([
            ('state', '=', 'brouillon'),
            ('etat_creation', '=', 'non'),
            ('create_date', '>=', date1),
            ('create_date', '<=', date2)],
            order="create_date asc")
        for lv1 in liste_versements:
            #get journal
            if len(lv1.journal_id) > 0:
                journal = lv1.journal_id[0]
            else:
                journal = lv1.journal_id
            #------get pointe vente,versement
            if len(lv1.versement_id) > 0:
                versement = lv1.versement_id[0]
                if len(lv1.versement_id[0].point_vente_id) > 0:
                    pointeventes = lv1.versement_id[0].point_vente_id[0]
                else:
                    pointeventes = lv1.versement_id[0].point_vente_id
            else:
                versement = lv1.versement_id
                if len(lv1.versement_id.point_vente_id) > 0:
                    pointeventes = lv1.versement_id.point_vente_id[0]
                else:
                    pointeventes = lv1.versement_id.point_vente_id

            #date versement "format yyyy-mm-dd "
            datFormatV = parse(lv1.create_date)
            datV_date = datFormatV.strftime('%Y-%m-%d')

            #deviser le num des versement 'num1/num2/num3' to ['num1','num2','num3']
            c = u'°'
            numver = u''.join(lv1.num_versement.replace(c, '')).encode('utf-8')
            listNumVersement = re.findall(r'\d+', str(numver))
            listeRef = []
            for n in listNumVersement:
                if len(n) > 3:
                    listeRef.append(n)
            
            #test par reference
            sommontantRB = 0.00
            boll0 = True
            listIDRef1 = []
            liste_RB1 = self.env['gakd.releve_bancaire'].search([
                ('state', '=', 'brouillon'),
                ('ref', 'in', listeRef),
                ('etat_creation', '=', 'non'),
                ('date_operation', '>=', date1),
                ('date_operation', '<=', date2)
            ])
            for lrb1 in liste_RB1:
                if lrb1.bank.replace(" ", "").upper() in journal.name.replace(" ", "").upper():
                    if lrb1.montant:
                        sommontantRB += float(lrb1.montant.replace(" ", ""))
                        listIDRef1.append(lrb1.id)
                        print('11111111111111111111111111111111111111111111111')
            #----resulta de test par reference
            if len(listIDRef1) == len(listeRef):
                for lrb2 in liste_RB1:
                    if lrb2.id in listIDRef1:
                        bol0 = False
                        lrb2.write({
                            'etat_creation': 'oui'
                        })
                        lv1.write({
                            'etat_creation': 'oui'
                        })
                        result = self.env['gakd.liste'].sudo().create({
                            'compte_bancaire': journal.id,
                            'point_vente': pointeventes.id,
                            'categorie_produit': versement.type_versement,
                            'statu_validaion': lv1.state,
                        })
                        resultvr = self.env['gakd.versementreleve'].sudo().create({
                            'compte_bancaire': journal.name,
                            'point_vente': pointeventes.libelle,
                            'categorie_produit': versement.type_versement,
                            'date_versement_releve': datV_date,
                            'num_versement': lv1.num_versement,
                            'montant_versement': lv1.montant,
                            'montant_releve': float(lrb2.montant.replace(" ", "")),
                            'montant_manquant': sommontantRB-float(lv1.montant),
                            'num_releve': lrb2.ref,
                            'lebelle_Releve': lrb2.libelle,
                            'statu_validaion': 'M.Ref-Mt.Diff',
                        })
            if boll0:
                if journal.name.upper() != 'CASHCOLLECTION':
                    #test 2
                    #-----test si categorier= produit blants
                    boll1 = True
                    if versement.type_versement == 'Produits blancs':
                        liste_RB3 = self.env['gakd.releve_bancaire'].search([
                            ('state', '=', 'brouillon'),
                            ('etat_creation', '=', 'non'),
                            ('categorie_produit', '=', versement.type_versement),
                            ('date_operation', '>=', date1),
                            ('date_operation', '<=', date2)],
                            order="date_operation , montant asc")
                        boll1 = False
                        listIDdt1 = []
                        sommontantRBDT1 = 0.0
                        for lrb3 in liste_RB3:
                            if lrb3.date_operation and lrb3.montant:
                                #------get et convertet  date de releve bancaire au yyyy-mm-dd
                                datR2 = parse(str(lrb3.date_operation))
                                datR_date = datR2.strftime('%Y-%m-%d')
                                #---delete special characters dans les libelle
                                leb = self.replaceCaractere(lrb3.libelle)[0].upper()
                                if datR_date == datV_date and (lrb3.bank.replace(" ", "").upper() in journal.name.upper()):
                                    if self.searchInLibelle(pointeventes.libelle, lrb3.libelle):
                                        listIDdt1.append(lrb3.id)
                                        sommontantRBDT1 += float(lrb3.montant.replace(" ", ""))
                        # if float(sommontantRBDT1) == float(lv1.montant):
                        for lrb4 in liste_RB3:
                            if lrb4.id in listIDdt1:
                                lrb4.write({
                                    'etat_creation': 'oui'
                                })
                                lv1.write({
                                    'etat_creation': 'oui'
                                })
                                result = self.env['gakd.liste'].sudo().create({
                                    'compte_bancaire': journal.id,
                                    'point_vente': pointeventes.id,
                                    'categorie_produit': versement.type_versement,
                                    'statu_validaion': lv1.state,
                                })
                                resultvr = self.env['gakd.versementreleve'].sudo().create({
                                    'compte_bancaire': journal.name,
                                    'point_vente': pointeventes.libelle,
                                    'categorie_produit': versement.type_versement,
                                    'date_versement_releve': datV_date,
                                    'num_versement': lv1.num_versement,
                                    'montant_versement': lv1.montant,
                                    'montant_releve': float(lrb4.montant.replace(" ", "")),
                                    'montant_manquant': sommontantRBDT1-float(lv1.montant),
                                    'num_releve': lrb4.ref,
                                    'lebelle_Releve': lrb4.libelle,
                                    'statu_validaion': 'M.Date-M.PV Mt.Diff',
                                })
                elif versement.type_versement == 'Produits blancs':
                    #liste categorie produits
                    listcategoriePB = []
                    liste_RB4 = self.env['gakd.releve_bancaire'].search([
                        ('state', '!=', 'valideAuto'),
                        ('etat_creation', '=', 'non'),
                        ('date_operation', '>=', date1),
                        ('date_operation', '<=', date2)],
                        order="date_operation , montant asc")
                    listcategoriePB = ['ESSENCE','ESSENC', 'GASOIL', 'GAZOIL']
                    listIDdt1 = []
                    sommontantRBDT1 = 0.0
                    for lrb3 in liste_RB4:
                        if lrb3.date_operation and lrb3.montant:
                            #------get et convertet  date de releve bancaire au yyyy-mm-dd
                            datR2 = parse(str(lrb3.date_operation))
                            datR_date = datR2.strftime('%Y-%m-%d')
                            #---delete special characters dans les libelle
                            leb = self.replaceCaractere(lrb3.libelle)[0].upper()
                            if datR_date == datV_date and (lrb3.bank.replace(" ", "").upper() in journal.name.upper()):
                                if (listcategoriePB[0].upper() in leb or listcategoriePB[1].upper() in leb or listcategoriePB[2].upper() in leb or listcategoriePB[3].upper() in leb):
                                    if self.searchInLibelle(pointeventes.libelle, lrb3.libelle):
                                        listIDdt1.append(lrb3.id)
                                        sommontantRBDT1 += float(lrb3.montant.replace(" ", ""))

                    # if float(sommontantRBDT1) == float(lv1.montant):
                    for lrb4 in liste_RB3:
                        if lrb4.id in listIDdt1:
                            lrb4.write({
                                'etat_creation': 'oui'
                            })
                            lv1.write({
                                'etat_creation': 'oui'
                            })
                            result = self.env['gakd.liste'].sudo().create({
                                'compte_bancaire': journal.id,
                                'point_vente': pointeventes.id,
                                'categorie_produit': versement.type_versement,
                                'statu_validaion': lv1.state,
                            })
                            resultvr = self.env['gakd.versementreleve'].sudo().create({
                                'compte_bancaire': journal.name,
                                'point_vente': pointeventes.libelle,
                                'categorie_produit': versement.type_versement,
                                'date_versement_releve': datV_date,
                                'num_versement': lv1.num_versement,
                                'montant_versement': lv1.montant,
                                'montant_releve': float(lrb4.montant.replace(" ", "")),
                                'montant_manquant': sommontantRBDT1-float(lv1.montant),
                                'num_releve': lrb4.ref,
                                'lebelle_Releve': lrb4.libelle,
                                'statu_validaion': 'M.Date-M.PV Mt.Diff',
                            })

    @api.multi
    def validerVersementline_someProduitBlans(self):
        # ------ liste veresement
        print('ok2222================================')
        k = 0
        date1 = self.date_debut
        datFormatV1 = parse(date1)
        date1 = datFormatV1.strftime('%Y-%m-%d %H:%M:%S')

        date2 = self.date_fin
        datFormatV2 = parse(date2)
        datFormatV2 += datetime.timedelta(1)
        date2 = datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
        print datFormatV2

        liste_versements = self.env['gakd.versement_line'].search([
            ('state', '!=', 'valideAuto'),
            ('etat_creation', '=', 'non'),
            ('create_date', '>=', date1),
            ('create_date', '<=', date2)],
            order="create_date asc")
        print('nombre des line versement', len(liste_versements))
        for lv1 in liste_versements:
            #get journal
            if len(lv1.journal_id) > 0:
                journal = lv1.journal_id[0]
            else:
                journal = lv1.journal_id
            #------pointe vente
            if len(lv1.versement_id) > 0:
                versement = lv1.versement_id[0]
                if len(lv1.versement_id[0].point_vente_id) > 0:
                    codepv = lv1.versement_id[0].point_vente_id[0].libelle
                else:
                    codepv = lv1.versement_id[0].point_vente_id.libelle
            else:
                versement = lv1.versement_id
                if len(lv1.versement_id.point_vente_id) > 0:
                    codepv = lv1.versement_id.point_vente_id[0].libelle
                else:
                    codepv = lv1.versement_id.point_vente_id.libelle

            #date versement "format yyyy-mm-dd "
            datV = lv1.create_date
            datFormatV = parse(datV)
            datV_date = datFormatV.strftime('%Y-%m-%d')
            k += 1
            print(k, ' kk22222--_-_--_-__--__--__--__--__--__--__', lv1.id)
            print(datV_date)

            #deviser le num des versement 'num1/num2/num3' to ['num1','num2','num3']
            c = u'°'
            numver = u''.join(lv1.num_versement.replace(c, '')).encode('utf-8')
            listNumVersement = re.findall(r'\d+', str(numver))
            listeRef = []
            for n in listNumVersement:
                if len(n) > 3:
                    listeRef.append(n)
            #bank
            nom_journal = journal.name.replace(" ", "")
            boll0 = True
            #test par reference
            sommontantRB = 0.00
            listIDRef1 = []
            liste_RB1 = self.env['gakd.releve_bancaire'].search([
                ('state', '!=', 'valideAuto'),
                ('ref', 'in', listeRef),
                ('etat_creation', '=', 'non'),
                ('date_operation', '>=', date1),
                ('date_operation', '<=', date2)
            ])
            for lrb1 in liste_RB1:
                if lrb1.bank.replace(" ", "").upper() in nom_journal.upper():
                    if lrb1.montant:
                        sommontantRB += float(lrb1.montant.replace(" ", ""))
                        listIDRef1.append(lrb1.id)
                        print('11111111111111111111111111111111111111111111111')
            #----resulta de test par reference
            if float(sommontantRB) == float(lv1.montant):
                for lrb2 in liste_RB1:
                    if lrb2.id in listIDRef1:
                        boll0 = False
                        lv1.write({
                            'state': 'valideAuto',
                        })
                        lrb2.write({
                            'state': 'valideAuto',
                            'id_versement': lv1.id,
                            'ecarts': float(sommontantRB)-float(lv1.montant),
                        })
                        if versement.moyen_de_paiement == 'versement':
                            account_id_d = 19857
                            account_id_c = 20607
                            data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                    (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                            av = self.env['account.move'].create({
                                'journal_id': journal.id,
                                'date': dt.now().date(),
                                'ref': 'sssss-'+str(dt.now()),
                                'line_ids': data
                            })
                            av.post()
                        if versement.moyen_de_paiement == 'cheque':
                            account_id_d = 19857
                            account_id_c = 19975
                            data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant}),
                                    (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                            av = self.env['account.move'].create({
                                'journal_id': journal.id,
                                'date': dt.now().date(),
                                'ref': 'sssss-'+str(dt.now()),
                                'line_ids': data
                            })
                            av.post()
                        if versement.moyen_de_paiement == 'especes':
                            account_id_d = 19857
                            account_id_c = 20607
                            data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                    (0, 0, {'account_id': 20143, 'partner_id': '','name': 'Frais de timbre', 'debit': 50}),
                                    (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                            av = self.env['account.move'].create({
                                'journal_id': journal.id,
                                'date': dt.now().date(),
                                'ref': 'sssss-'+str(dt.now()),
                                'line_ids': data
                            })
                            av.post()
                        print ("bank", lrb1.bank)
                        print('vvvvvvvvvvvvvvv  11111111111111111111')
                #fin test reference
            if boll0:
                if nom_journal.upper() != 'CASHCOLLECTION':
                    #test 2
                    #-----test si categorier= produit blants
                    liste_RB3 = self.env['gakd.releve_bancaire'].search([
                        ('state', '!=', 'valideAuto'),
                        ('etat_creation', '=', 'non'),
                        ('categorie_produit', '=', versement.type_versement),
                        ('date_operation', '>=', date1),
                        ('date_operation', '<=', date2)],
                        order="date_operation , montant asc")
                    boll1 = True
                    if versement.type_versement == 'Produits blancs':
                        boll1 = False
                        listIDdt1 = []
                        sommontantRBDT1 = 0.0
                        for lrb3 in liste_RB3:
                            if lrb3.date_operation:
                                #------get et convertet  date de releve bancaire au yyyy-mm-dd
                                datR = lrb3.date_operation
                                datR2 = parse(str(datR))
                                datR_date = datR2.strftime('%Y-%m-%d')
                                if lrb3.montant:
                                    #---- suprime les espaces dans montant de releve bancaire
                                    mantantRd = lrb3.montant.replace(" ", "")
                                    #---delete special characters dans les libelle
                                    leb = self.replaceCaractere(
                                        lrb3.libelle)[0].upper()
                                    banck6 = lrb3.bank.replace(" ", "")
                                    print ('banque releve====================')
                                    print banck6
                                    if datR_date == datV_date and (banck6.upper() in nom_journal.upper()):
                                        if self.searchInLibelle(codepv, lrb3.libelle):
                                            listIDdt1.append(lrb3.id)
                                            sommontantRBDT1 += float(
                                                lrb3.montant.replace(" ", ""))
                                            print(
                                                '2222222222222222222222222222222222222222222222222')
                        if float(sommontantRBDT1) == float(lv1.montant):
                            for lrb4 in liste_RB3:
                                if lrb4.id in listIDdt1:
                                    lv1.write({
                                        'state': 'valideAuto',
                                    })
                                    lrb4.write({
                                        'state': 'valideAuto',
                                        'id_versement': lv1.id,
                                        'ecarts': float(sommontantRBDT1)-float(lv1.montant),
                                    })
                                    if versement.moyen_de_paiement == 'versement':
                                        account_id_d = 19857
                                        account_id_c = 20607
                                        data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                                (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                        av = self.env['account.move'].create({
                                            'journal_id': journal.id,
                                            'date': dt.now().date(),
                                            'ref': 'sssss-'+str(dt.now()),
                                            'line_ids': data
                                        })
                                        av.post()
                                    if versement.moyen_de_paiement == 'cheque':
                                        account_id_d = 19857
                                        account_id_c = 19975
                                        data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant}),
                                                (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                        av = self.env['account.move'].create({
                                            'journal_id': journal.id,
                                            'date': dt.now().date(),
                                            'ref': 'sssss-'+str(dt.now()),
                                            'line_ids': data
                                        })
                                        av.post()
                                    if versement.moyen_de_paiement == 'especes':
                                        account_id_d = 19857
                                        account_id_c = 20607
                                        data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                                (0, 0, {'account_id': 20143, 'partner_id': '', 'name': 'Frais de timbre', 'debit': 50}),
                                                (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                        av = self.env['account.move'].create({
                                            'journal_id': journal.id,
                                            'date': dt.now().date(),
                                            'ref': 'sssss-'+str(dt.now()),
                                            'line_ids': data
                                        })
                                        av.post()
                                    print('vvvvvvvvvvvvv   22222222222222222')
                else:
                    #liste categorie produits
                    listcategoriePB = []
                    liste_RB4 = self.env['gakd.releve_bancaire'].search([
                        ('state', '!=', 'valideAuto'),
                        ('etat_creation', '=', 'non'),
                        ('date_operation', '>=', date1),
                        ('date_operation', '<=', date2)],
                        order="date_operation , montant asc")
                    if versement.type_versement == 'Produits blancs':
                        listcategoriePB = ['ESSENCE','ESSENC', 'GASOIL', 'GAZOIL']
                        listIDdt1 = []
                        sommontantRBDT1 = 0.0
                        for lrb3 in liste_RB4:
                            if lrb3.date_operation and lrb3.montant:
                                #------get et convertet  date de releve bancaire au yyyy-mm-dd
                                datR = lrb3.date_operation
                                datR2 = parse(str(datR))
                                datR_date = datR2.strftime('%Y-%m-%d')
                                #---delete special characters dans les libelle
                                leb = self.replaceCaractere(
                                    lrb3.libelle)[0].upper()
                                banck6 = lrb3.bank.replace(" ", "")
                                if datR_date == datV_date and (banck6.upper() in nom_journal.upper()):
                                    if (listcategoriePB[0].upper() in leb or listcategoriePB[1].upper() in leb or listcategoriePB[2].upper() in leb or listcategoriePB[3].upper() in leb):
                                        if self.searchInLibelle(codepv, lrb3.libelle):
                                            listIDdt1.append(lrb3.id)
                                            sommontantRBDT1 += float(
                                                lrb3.montant.replace(" ", ""))
                                            print(
                                                '222222222222222222222222222222222222222222222222222222222222222222')

                        if float(sommontantRBDT1) == float(lv1.montant):
                            for lrb4 in liste_RB3:
                                if lrb4.id in listIDdt1:
                                    lv1.write({
                                        'state': 'valideAuto',
                                    })
                                    lrb4.write({
                                        'state': 'valideAuto',
                                        'id_versement': lv1.id,
                                        'ecarts': float(sommontantRBDT1)-float(lv1.montant),
                                    })
                                    if versement.moyen_de_paiement == 'versement':
                                        account_id_d = 19857
                                        account_id_c = 20607
                                        data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                                (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                        av = self.env['account.move'].create({
                                            'journal_id': journal.id,
                                            'date': dt.now().date(),
                                            'ref': 'sssss-'+str(dt.now()),
                                            'line_ids': data
                                        })
                                        av.post()
                                    if versement.moyen_de_paiement == 'cheque':
                                        account_id_d = 19857
                                        account_id_c = 19975
                                        data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant}),
                                                (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                        av = self.env['account.move'].create({
                                            'journal_id': journal.id,
                                            'date': dt.now().date(),
                                            'ref': 'sssss-'+str(dt.now()),
                                            'line_ids': data
                                        })
                                        av.post()
                                    if versement.moyen_de_paiement == 'especes':
                                        account_id_d = 19857
                                        account_id_c = 20607
                                        data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                                (0, 0, {'account_id': 20143, 'partner_id': '', 'name': 'Frais de timbre', 'debit': 50}),
                                                (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                        av = self.env['account.move'].create({
                                            'journal_id': journal.id,
                                            'date': dt.now().date(),
                                            'ref': 'sssss-'+str(dt.now()),
                                            'line_ids': data
                                        })
                                        av.post()
                                    print('vvvvvvvvvvvvv   22222222222222222')
    #validerles versements
    @api.multi
    def validerVersementline(self):
        # ------ liste veresement
        print('ok================================')
        k=0
        date1=self.date_debut    
        datFormatV1=parse(date1)
        date1=datFormatV1.strftime('%Y-%m-%d %H:%M:%S')
        
        date2=self.date_fin
        datFormatV2=parse(date2)
        datFormatV2+=datetime.timedelta(1)
        date2 = datFormatV2.strftime('%Y-%m-%d %H:%M:%S')
        print datFormatV2

        liste_versements=self.env['gakd.versement_line'].search([
                        ('state','!=','valideAuto'),
                        ('etat_creation','=','non'),
                        ('create_date','>=',date1),
                        ('create_date','<=',date2)],
                        order="create_date asc")
        print('nombre des line versement',len(liste_versements))
        for lv1 in liste_versements:
            #get journal
            if len(lv1.journal_id)>0:
                journal=lv1.journal_id[0]
            else:
                journal=lv1.journal_id
            #------pointe vente
            if len(lv1.versement_id)>0:
                versement=lv1.versement_id[0]
                if len(lv1.versement_id[0].point_vente_id)>0:
                    codepv=lv1.versement_id[0].point_vente_id[0].libelle
                else:
                    codepv=lv1.versement_id[0].point_vente_id.libelle
            else:
                versement=lv1.versement_id
                if len(lv1.versement_id.point_vente_id)>0:
                    codepv=lv1.versement_id.point_vente_id[0].libelle
                else:
                    codepv=lv1.versement_id.point_vente_id.libelle

            #date versement "format yyyy-mm-dd "
            datV=lv1.create_date
            datFormatV=parse(datV)
            datV_date=datFormatV.strftime('%Y-%m-%d')
            k+=1
            print(k,'--_-_--_-__--__--__--__--__--__--__',lv1.id)
            print(datV_date)

            #deviser le num des versement 'num1/num2/num3' to ['num1','num2','num3']
            c=u'°'
            numver=u''.join(lv1.num_versement.replace(c,'')).encode('utf-8')
            numver = numver.replace(' ', '')
            listNumVersement = re.findall(r'\d+',str(numver))
            listeRef=[]
            for n in listNumVersement:
                if len(n)>3:
                    listeRef.append(n)
            
            #bank
            nom_journal=journal.name.replace(" ", "")
            boll0=True

            #test par reference
            montantVrRef=lv1.montant
            liste_RB1=self.env['gakd.releve_bancaire'].search([
                    ('state','!=','valideAuto'),
                    ('ref','in',listeRef),
                    ('etat_creation','=','non'),
                    ('date_operation','>=',date1),
                    ('date_operation','<=',date2)
                    ])
            for lrb1 in liste_RB1:
                if lrb1.bank.replace(" ", "").upper() in nom_journal.upper():
                    if lrb1.montant:
                        montantRB=lrb1.montant.replace(" ","")
                        if float(montantRB)==float(montantVrRef):
                            boll0 = False
                            lv1.write({
                                'state': 'valideAuto',
                            })
                            lrb1.write({
                                'state': 'valideAuto',
                                'id_versement': lv1.id,
                                'ecarts': float(montantRB)-float(montantVrRef),
                            })
                            if versement.moyen_de_paiement == 'versement':
                                account_id_d = 19857
                                account_id_c = 20607
                                data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                        (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                av = self.env['account.move'].create({
                                    'journal_id': journal.id,
                                    'date': dt.now().date(),
                                    'ref': 'sssss-'+str(dt.now()),
                                    'line_ids': data
                                })
                                av.post()
                            if versement.moyen_de_paiement == 'cheque':
                                account_id_d = 19857
                                account_id_c = 19975
                                data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant}),
                                        (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                av = self.env['account.move'].create({
                                    'journal_id': journal.id,
                                    'date': dt.now().date(),
                                    'ref': 'sssss-'+str(dt.now()),
                                    'line_ids': data
                                })
                                av.post()
                            if versement.moyen_de_paiement == 'especes':
                                account_id_d = 19857
                                account_id_c = 20607
                                data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                        (0, 0, {'account_id': 20143, 'partner_id': '','name': 'Frais de timbre', 'debit': 50}),
                                        (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                av = self.env['account.move'].create({
                                    'journal_id': journal.id,
                                    'date': dt.now().date(),
                                    'ref': 'sssss-'+str(dt.now()),
                                    'line_ids': data
                                })
                                av.post()
                            print ("bank", lrb1.bank)
                            print('vvvvvvvvvvvvvvv  11111111111111111111')
                    if boll0==False:
                        break
            # tester par la date , pointe de vente, bank, categorie produit
            if boll0:
                if nom_journal.upper() != 'CASHCOLLECTION':
                    #test 2
                    # categorie produits
                    categorierProduitV=versement.type_versement
                    liste_RB3=self.env['gakd.releve_bancaire'].search([
                            ('state','!=','valideAuto'),
                            ('etat_creation','=','non'),
                            ('categorie_produit', '=', categorierProduitV),
                            ('date_operation','>=',date1),
                            ('date_operation','<=',date2)],
                            order="date_operation , montant asc")
                    montantVrd1=0.0
                    montantVrd1=lv1.montant
                    for lrb3 in liste_RB3:
                        if lrb3.date_operation:
                            #------get et convertet  date de releve bancaire au yyyy-mm-dd
                            datR=lrb3.date_operation
                            datR2=parse(str(datR))
                            datR_date=datR2.strftime('%Y-%m-%d')
                            print "--------------------------------------------------"
                            print datR_date
                            print datV_date
                            #---- suprime les espaces dans montant de releve bancaire
                            if lrb3.montant:
                                mantantRd=lrb3.montant.replace(" ","")
                                #---delete special characters dans les libelle
                                leb=self.replaceCaractere(lrb3.libelle)[0].upper()
                                banck6=lrb3.bank.replace(" ", "")
                                if datR_date==datV_date and (banck6.upper() in nom_journal.upper()):
                                        if self.searchInLibelle(codepv,lrb3.libelle): 
                                            if float(mantantRd)==float(montantVrd1):
                                                boll0=False
                                                lv1.write({
                                                    'state': 'valideAuto',
                                                })
                                                lrb3.write({
                                                    'state': 'valideAuto',
                                                    'id_versement': lv1.id,
                                                    'ecarts': float(mantantRd)-float(montantVrd1),
                                                })
                                                if versement.moyen_de_paiement == 'versement':
                                                    account_id_d = 19857
                                                    account_id_c = 20607
                                                    data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                                            (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                                    av = self.env['account.move'].create({
                                                        'journal_id': journal.id,
                                                        'date': dt.now().date(),
                                                        'ref': 'sssss-'+str(dt.now()),
                                                        'line_ids': data
                                                    })
                                                    av.post()
                                                if versement.moyen_de_paiement == 'cheque':
                                                    account_id_d = 19857
                                                    account_id_c = 19975
                                                    data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant}),
                                                            (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                                    av = self.env['account.move'].create({
                                                        'journal_id': journal.id,
                                                        'date': dt.now().date(),
                                                        'ref': 'sssss-'+str(dt.now()),
                                                        'line_ids': data
                                                    })
                                                    av.post()
                                                if versement.moyen_de_paiement == 'especes':
                                                    account_id_d = 19857
                                                    account_id_c = 20607
                                                    data = [(0, 0, {'account_id': account_id_d, 'partner_id': '', 'name': 'Versement à la banque', 'debit': lv1.montant-50}),
                                                            (0, 0, {'account_id': 20143, 'partner_id': '', 'name': 'Frais de timbre', 'debit': 50}),
                                                            (0, 0, {'account_id': account_id_c, 'partner_id': '', 'name': 'Versement à la banque', 'credit': lv1.montant})]
                                                    av = self.env['account.move'].create({
                                                        'journal_id': journal.id,
                                                        'date': dt.now().date(),
                                                        'ref': 'sssss-'+str(dt.now()),
                                                        'line_ids': data
                                                    })
                                                    av.post()
                                                print('vvvvvvvvvvvvv   22222222222222222')
                        if boll0==False:
                            break
                elif nom_journal.upper() == 'CASHCOLLECTION':
                    liste_RB4=self.env['gakd.releve_bancaire'].search([
                            ('state','!=','valideAuto'),
                            ('etat_creation','=','non'),
                            ('date_operation','>=',date1),
                            ('date_operation','<=',date2)],
                            order="date_operation , montant asc")
                    montantVr3=0.0
                    montantVr3=lv1.montant
                    for lrb5 in liste_RB4:
                        boll1=True                   
                        if lrb5.state!='valideAuto':
                            if lrb5.date_operation and lrb5.montant:                            
                                #------get et convertet  date releve bancaire au yyyy-mm-dd
                                datR=lrb5.date_operation
                                datR2=parse(str(datR))
                                datR_date=datR2.strftime('%Y-%m-%d')
                                print "--------------------------------------------------"
                                print datR_date
                                print datV_date
                                #---- replace space montant releve bancaire
                                mantantRdd=0.0
                                mantantRdd=lrb5.montant.replace(" ","")
                                #---delete special characters
                                banck8=lrb5.bank.replace(" ","")
                                #---delete special characters dans les libelle
                                leb=self.replaceCaractere(lrb5.libelle)[0].upper() 
                                print ('banque releve====================')
                                print (banck8)
                                if  datR_date==datV_date and (banck8.upper() in nom_journal.upper()):                                                                              
                                    if self.searchInLibelle(codepv,lrb5.libelle):                                                
                                        if float(mantantRdd)==float(montantVr3):
                                            boll1=False
                                            lv1.write({
                                                'state': 'valideAuto',
                                                })
                                            lrb5.write({
                                                'state': 'valideAuto',
                                                'id_versement': lv1.id,
                                                'ecarts':float(mantantRdd)-float(montantVr3),
                                            })
                                            if versement.moyen_de_paiement == 'versement':
                                                account_id_d = 19857
                                                account_id_c = 20607            
                                                data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant-50}),
                                                (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                                                av= self.env['account.move'].create({
                                                    'journal_id':journal.id,
                                                    'date':dt.now().date(),
                                                    'ref': 'sssss-'+str(dt.now()),
                                                    'line_ids':data
                                                })  
                                                av.post()          
                                            if versement.moyen_de_paiement == 'cheque':
                                                account_id_d = 19857
                                                account_id_c = 19975           
                                                data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant}),
                                                (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                                                av= self.env['account.move'].create({
                                                'journal_id':journal.id,
                                                'date':dt.now().date(),
                                                'ref': 'sssss-'+str(dt.now()),                
                                                'line_ids':data
                                                })  
                                                av.post()          
                                            if versement.moyen_de_paiement == 'especes':
                                                account_id_d = 19857
                                                account_id_c = 20607           
                                                data = [(0,0 ,{'account_id':account_id_d ,'partner_id':'','name':'Versement à la banque','debit':lv1.montant-50}),
                                                (0,0 ,{'account_id':20143,'partner_id':'','name':'Frais de timbre','debit':50}),
                                                (0,0 ,{'account_id':account_id_c,'partner_id':'','name':'Versement à la banque','credit':lv1.montant})]
                                                av= self.env['account.move'].create({
                                                'journal_id':journal.id,
                                                'date':dt.now().date(),
                                                'ref': 'sssss-'+str(dt.now()),                
                                                'line_ids':data
                                                })  
                                                av.post()
                                            print ("bank",lrb5.bank)                                                
                                            print('vvvvvvv   33333333333333333333333333333')
                        if  boll1==False:
                            break
        self.validerVersementline_someProduitBlans()
        self.weekEndsValidator()
        self.valideVersement(date1,date2)
        self.create_versementValide(date1,date2)
        self.create_versementBrouillon(date1, date2)
        self.createVersementline_someProduitBlans()
        self.create_versementNotExixte(date1,date2)
        data = self.remplireListeVersement(date1,date2)
        self.versement_line = [(6,0,data.ids)]
        return self.versement_line
