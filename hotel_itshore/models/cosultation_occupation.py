# -- coding: utf-8 --
from odoo import api, fields, models
from datetime import datetime
from datetime import timedelta
from datetime import date
import math

class Consulte(models.Model):
    _name = 'hotel.consult'


    datee = fields.Date(string='Date') 
    consult_prec = fields.One2many('hotel.consult.prec','con',string='avant') 
    consult_suiv = fields.One2many('hotel.consult.prec','con',string='apres') 



    nb_avant=fields.Integer(string='Nombre de jours avant :')
    nb_apres=fields.Integer(string='Nombre de jours aprés :')

    @api.multi
    def date_next(self,date1):
        date1 = datetime.strptime(date1, '%Y-%m-%d')
        data=[]
        nb=50
        if self.nb_apres:
            nb=self.nb_apres
        for  i in range(nb):
            data.append(date1+ timedelta(days=i))
        return  data


    @api.multi
    def date_prec(self,date1):
        date1 = datetime.strptime(date1, '%Y-%m-%d')- timedelta(days=1)
        data=[] 
        nb=50
        if self.nb_avant:
            nb=self.nb_avant
        for  i in range(nb):
            data.append(date1- timedelta(days=i))
        # print reversed(sorted(data.values()))
        # print list(reversed(data))
        for a in data:
            print a
        print 'list(reversed(data))list(reversed(data))'
        return list(reversed(data))

    @api.onchange('datee','nb_avant','nb_apres')
    def _onchange_type(self):
        a= date.today()
        final=[]
        # print self.date_next(a)
        date_prec= self.date_prec(str(a))
        # self._cr.execute("select id  from hotel_folio_line as hr where Date(hr.checkin_date) <= '"+str(a)+"'and Date(hr.checkout_date) >= '"+str(a)+"'")
        # data = self._cr.fetchall()
        # li=[]
        nb_chambre=len(self.env['hotel.room'].search([]))
        #print 'nooooooooooooooooooooombre chambre'
        # print nb_chambre
        # for dat in data:
        #     li.append(dat[0])
        room_lines=self.env['hotel.folio.line'].search([])#('id','in',li)
        #print date_prec
        #print 'date_precdate_precdate_precdate_prec'
        nb=0
        tt_occ=0
        arr_rms=0
        occ_pour=0
        room_revv=0
        average=0
        dep_rm=0
        for dd in date_prec:
            nb+=1
            #print '(hhhhhhbjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj'
            total_occu=0
            arr_room=0
            occu=0
            room_rev=0
            average_rate=0
            dep_room=0
            # print dd.date()
            for room in room_lines:
                # print type(datetime.strptime(room.checkout_date,'%Y-%m-%d %H:%M:%S').date())
                # print type(dd.date())
                if  datetime.strptime(room.checkout_date,'%Y-%m-%d %H:%M:%S').date() == dd.date():
                    dep_room+=1
                    # print 'hhhhhhhhhhbjbhbjhb'
                if   datetime.strptime(room.checkin_date,'%Y-%m-%d %H:%M:%S').date()  == dd.date():
                    arr_room+=1
                if   datetime.strptime(room.checkin_date,'%Y-%m-%d %H:%M:%S').date() <= dd.date() and   datetime.strptime(room.checkout_date,'%Y-%m-%d %H:%M:%S').date() > dd.date():
                    total_occu+=1
                    room_rev+=room.price_unit

            occu=float(float(total_occu) * 100)/float(nb_chambre)
            occu = float("{0:.2f}".format(occu))
            print 888888888888888888
            print total_occu
            print nb_chambre
            print occu
            if total_occu!=0:
                average_rate=room_rev/total_occu
            tt_occ+=total_occu
            arr_rms+=arr_room
            occ_pour+=occu
            room_revv+=room_rev
            average+=average_rate
            dep_rm+=dep_room
            final.append({
                'date':  str(dd.date()),
                'total_occu': str(total_occu),
                'arr_room' :str(arr_room),
                'occu':str(occu)+' %',
                'room_rev': str(room_rev),
                'average_rate': str(average_rate),
                'dep_room': str(dep_room),
            })
        total_occup = float("{0:.2f}".format(occ_pour/nb))
        final.append({
                'date':  'Subtotal',
                'total_occu': str(tt_occ),
                'arr_room' :str(arr_rms),
                'occu':str(total_occup)+' %',
                'room_rev': str(room_revv),
                'average_rate': str(average/nb),
                'dep_room': str(dep_rm),
            })
                # hhhhhhhhhhhhhhhhhhhhhhhhhhhh
        #print final
        #print 'historiiiiiiiiiiiiiii'
        finall=[]
        # print self.date_next(a)
        date_prec= self.date_next(str(a))
        # self._cr.execute("select id  from hotel_reservation_line as hr where Date(hr.checkin) <= '"+str(a)+"'and Date(hr.checkout) >= '"+str(a)+"'")
        # data = self._cr.fetchall()
        # li=[]
        nb_chambre=len(self.env['hotel.room'].search([]))
        # print 'nooooooooooooooooooooombre chambre'
        # print nb_chambre
        # for dat in data:
        #     li.append(dat[0])
        #     print str(dat)+'daaaaaaaaaaaaaaaaaaaaaaaaat'
        room_lines=self.env['hotel_reservation.line'].search([])#('id','in',li)
        nb=0
        tt_occ=0
        arr_rms=0
        occ_pour=0
        room_revv=0
        average=0
        dep_rm=0
        for dd in date_prec:
            nb+=1
            total_occu=0
            arr_room=0
            occu=0
            room_rev=0
            average_rate=0
            dep_room=0
            # print dd.date()
            for room in room_lines:
                if room.checkin and room.checkout:
                    #print datetime.strptime(room.checkin,'%Y-%m-%d %H:%M:%S').date()
                    #print dd.date()
                    #print '66666666666666666'
                    if  datetime.strptime(room.checkout,'%Y-%m-%d %H:%M:%S').date() == dd.date():
                        dep_room+=1
                        # print 'hhhhhhhhhhbjbhbjhb'
                    if   datetime.strptime(room.checkin,'%Y-%m-%d %H:%M:%S').date()  == dd.date():
                        arr_room+=1
                        #print 'cheeeeeeeeeeeeeeeeeeeeeeeeeeecking'
                    if   datetime.strptime(room.checkin,'%Y-%m-%d %H:%M:%S').date() <= dd.date() and   datetime.strptime(room.checkout,'%Y-%m-%d %H:%M:%S').date() > dd.date():
                        total_occu+=1
                        room_rev+=room.prix

            occu=float(float(total_occu) * 100)/float(nb_chambre)
            occu = float("{0:.2f}".format(occu))
            if total_occu!=0:
                average_rate=room_rev/total_occu

            tt_occ+=total_occu
            arr_rms+=arr_room
            occ_pour+=occu
            room_revv+=room_rev
            average+=average_rate
            dep_rm+=dep_room
            finall.append({
                'date':  str(dd.date()),
                'total_occu': str(total_occu),
                'arr_room' :str(arr_room),
                'occu':str(occu)+' %',
                'room_rev': str(room_rev),
                'average_rate': str(average_rate),
                'dep_room': str(dep_room),
            })
        total_prevision = float("{0:.2f}".format(occ_pour/nb))
        finall.append({
                'date':  'Subtotal',
                'total_occu': str(tt_occ),
                'arr_room' :str(arr_rms),
                'occu':str(total_prevision)+' %',
                'room_rev': str(room_revv),
                'average_rate': str(average/nb),
                'dep_room': str(dep_rm),
            })
        #print finall
        #print "finallfinallfinallfinallfinallfinall"
        return {'value':{'consult_prec':final,'consult_suiv':finall}}#


class Produit_stock(models.Model):
    _name="hotel.consult.prec" 

    con = fields.Many2one(comodel_name='hotel.consult', string='con')
    date=fields.Char(string='Date') 
    total_occu=fields.Char(string='Total OCC')
    arr_room=fields.Char(string='Arr Rooms')
    occu=fields.Char(string='Occ. %')
    room_rev=fields.Char(string='Room Revenue')
    average_rate=fields.Char(string='Average rate')
    dep_room=fields.Char(string='Dep Rooms')