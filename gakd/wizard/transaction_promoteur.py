# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import time
from odoo.addons.report_xlsx.report.report_xlsx import  ReportXlsx

class transaction_promoteur(models.TransientModel):
    _name = 'gakd.wizard.transaction_promoteur'

    promoteur=fields.Many2one('gakd.tv_station', string='Promoteur')
    type_vente = fields.Selection([('JNP PASS','JNP PASS'),('Ticket valeur','Ticket valeur'),('MOMO PAYE','MOMO PAYE')])
    start_date = fields.Datetime(
         string='Date Début',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
    end_date = fields.Datetime(
         string='Date Fin',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )


    @api.multi
    def print_report(self,workbook):
        print '88888888888888888888888888888'
        print self.promoteur
        print self.type_vente
        res = []
        if self.type_vente == 'JNP PASS':
            liste_transactions = self.env['gakd.jnppass_station_line'].search([('jnppass_station','=',self.promoteur.id)])
            for tran in liste_transactions:
                print tran.jnppass_station.ref
                res.append({"station":self.promoteur,"montant":tran.montant})
            print res
            # sheet = workbook.add_worksheet()
            # sheet.write(0,0,'Point de vent')
            # sheet.write(0,1,'Produit')
            # sheet.write(0,2,'Quantite')
            # sheet.write(0,3,'Date')
            # sheet.write(0,4,'Description')
            # # add the rest of the report code here
            # row = 1
            # data = self.get_data()
            # for item in liste_transactions:
                # for subitem in data[item] :
                #     sheet.write(row,0,item.name)
                #     sheet.write(row,1,subitem.product_id.name)
                #     sheet.write(row,2,subitem.product_uom_qty)
                #     sheet.write(row,3,subitem.date)
                #     sheet.write(row,4,subitem.name)
                    #row +=1
        if self.type_vente == 'Ticket valeur':
            print 'Ticket valeur'
        
        if self.type_vente == 'MOMO PAYE':
            print 'MOMO PAYE'

        datas = []
        return self.env['report'].get_action(self, 'gakd.promoteur_transaction_report_template', data=res)

class promoteur_transaction_report_template(models.AbstractModel):
    _name = 'report.gakd.promoteur_transaction_report_template'

    @api.model
    def render_html(self, docids, data=None):
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('gakd.promoteur_transaction_report_template')
        
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': data,
            }
       
        return report_obj.render('gakd.promoteur_transaction_report_template', docargs)

























class transaction_promoteur_by_client(models.TransientModel):
    _name = 'gakd.wizard.liste_transaction_promoteur_byclient'

    promoteur=fields.Many2one('gakd.tv_station', string='Promoteur')
    type_vente = fields.Selection([('JNP PASS','JNP PASS'),('Ticket valeur','Ticket valeur'),('MOMO PAYE','MOMO PAYE')])
    start_date = fields.Datetime(
         string='Date Début',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
    end_date = fields.Datetime(
         string='Date Fin',
         required=True,
         default=lambda *a: (parser.parse(datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)))
         )
