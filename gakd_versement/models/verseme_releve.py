# -- coding: utf-8 --
from odoo import api, fields, models,_
from odoo.exceptions import ValidationError
from datetime import date, timedelta




class VersementReleve(models.Model):
    _name = 'gakd.versementreleve'
    _description='versements plus releve'

    yesterday = date.today() - timedelta(days=1)
    hier=yesterday.strftime('%Y-%m-%d')
    
    compte_bancaire=fields.Char(string='compte bancaire')
    point_vente=fields.Char(string='Point de vente')
    categorie_produit=fields.Char(string="Catégorie du produit")
    date_versement_releve=fields.Date(string='Date de versement/Relevé',  default=hier, required=True)
    num_versement=fields.Char(string='N° versement')
    nom_gerant=fields.Char(string='Nom du gérant')
    montant_versement = fields.Float(string='Montant versement')
    montant_releve = fields.Float(string='Montant Relevé')
    montant_manquant = fields.Float(string='Ecart')
    num_releve=fields.Char(string='N° B.V Relevé')
    lebelle_Releve=fields.Char(string='Libellé Relevé')
    statu_validaion=fields.Selection(
        string='Statut de validation',
        selection=[
            ('annule', 'Annulé'),
            ('brouillon', 'Brouillon'), 
            ('valideAuto', 'Validé'),
            ('M.Ref-Mt.Diff', 'Même référence mais momtant différent'),
            ('M.Date-M.PV Mt.Diff', 'Même date Même PV mais montant différent'),
            ('V.n_existe_pas_dans_R',"versement n'existe pas dans les relevés"),
		    ('R.n_existe_pas_dans_V',"relevé n'existe pas dans les versements"),
            ])
    liste_versement=fields.Many2one('gakd.liste')
