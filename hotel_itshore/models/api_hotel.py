# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from collections import OrderedDict
import collections
from unidecode import unidecode
from random import randint
from datetime import datetime, timedelta
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

class api_hotel(models.Model):
    
    @api.multi
    def addFitnessAbonnementline(self,qrcode):
        print 'checkFitnessAbonnementByQrcode'
        
        abonnement = self.env['hotel.fitness.abonnement'].search([['qrcode', '=', qrcode.get("qrcode")]])  
        print abonnement
        if not abonnement.id:
           return 0
        else :
            dt='%Y-%m-%d '
            
            abonnement.abonnement_lines=[(0,0,{
                'date_debut':qrcode.get("date_debut") ,
                'duree':qrcode.get("duree"),
                'type_discount':qrcode.get("type_discount"),
                'discount':float(qrcode.get("discount")),
                'discount_fix':float(qrcode.get("discount_fix")),
               
                 }) ]
         
            return 1
      
    @api.multi
    def checkFitnessAbonnementByQrcode(self,qrcode):
        print 'checkFitnessAbonnementByQrcode'
      
        for ab in  self.env['hotel.fitness.abonnement'].search([]) :
            print 'qr code'
            print ab.qrcode
        abonnement = self.env['hotel.fitness.abonnement'].search([['qrcode', '=', qrcode.get("qrcode")]])  
        if abonnement.nb_jours==0:
           return 0
        elif abonnement.nb_jours>0:
            self.env["hotel.fitness.historique"].create({
                'libelle':'tessssssssssssss',
                'time' : datetime.now(),
                'abonnement_id':abonnement.id
            })
            return 1
      
    @api.multi
    def checkHotelEmByMatricule(self,qrcode):
        print 'checkHotelEmByMatricule'
        print qrcode
        print qrcode.get("matricule")
        emp = self.env['hr.employee'].search([('matricule', '=', qrcode.get("matricule")),('password', '=', qrcode.get("password"))])
        print emp
        if not emp.id:
            return 0
        else:
            return 1
    @api.multi
    def getHotelEmpByMatricule(self,qrcode):
        print 'getHotelEmpByMatricule'
        data = []
        emp = self.env['hr.employee'].search([['matricule', '=', qrcode.get("matricule")]])
        print emp.password
        data = [("id",emp.id),("matricule",unidecode(emp.matricule)),("name",unidecode(emp.name)),
        ("fonction",unidecode(emp.fonction)),("password",emp.password)]
        return data
