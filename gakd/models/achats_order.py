# -*- coding: utf-8 -*-

from odoo import api, fields, models
from lxml import etree
from openerp.exceptions import ValidationError

class Achat_order(models.Model):
    _inherit = 'purchase.order'


    @api.model
    def create(self, vals):

        print 'ordeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeer'
        print vals
        return super(Achat_order, self).create(vals)


class Facture_for(models.Model):
    _inherit = 'account.invoice'

    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
    
        res = super(Facture_for, self).fields_view_get(view_id=view_id, view_type=view_type,toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        if view_type == 'tree' and   self.env.user.has_group('gakd.group_gakd_sous_chargeur'):
            for node_form in doc.xpath("//tree"):
                node_form.set("create", 'false')
                res['arch'] = etree.tostring(doc)

        return res
    # @api.one
    # def getTransfertDomain(self):
    #     print 'hiiiiiidfidiffififidiiiddddddddddddddddddddddddddddd'
    #     print '---------'
    #     location = self.env['stock.location'].search([('name','=','non-d')]).id
    #     transferts = self.env['gakd.transfert'].search(['&',('location_id','=',location),('is_done','=',False)])
    #     print location
    #     print transferts
    #     data = [tr.id for tr in transferts]
    #     print data
    #     return data
    
    # @api.model
    # def default_get(self,fields):
    #     res =  super(Facture_for, self).default_get(fields)
    #     print 'hiiiiiidfidiffififidiiiddddddddddddddddddddddddddddd'
    #     location = self.env['stock.location'].search([('name','=','non-d')]).id
    #     transferts = self.env['gakd.transfert'].search(['&',('location_id','=',location),('is_done','=',False)])
    #     data = [(4,tr.id) for tr in transferts]
    #     data_ids =  [ tr.id for tr in transferts]
    #     print transferts
    #     print location
    #     res['transfert_idss']=data
    #     print data
    #     return res
    
    # def getTransferts(self):
    #     location = self.env['stock.location'].search([('name','=','non-d')]).id
    #     transferts = self.env['gakd.transfert'].search(['&',('location_id','=',location),('is_done','=',False)])
    #     data = [(4,tr.id) for tr in transferts]
    #     data_ids = [tr.id for tr in transferts]
    #     print '======'
    #     print self.transfert_idss.ids
    #     return [('id','in',self.transfert_idss)]

    # transfert_idss = fields.One2many('gakd.transfert',inverse_name='fact_idd',string='transferts')
    transfert_id = fields.Many2one(comodel_name='stock.picking', string='Transfert')
    code_mecef = fields.Char("Code mecef")
    nim = fields.Char("Nim")
    mecef_compteur = fields.Char("Mecef Compteur")
    mecef_date_heure= fields.Char("Date  Mecef")
    qr_code = fields.Char("Code QR")
    montant_ttc_a_payer = fields.Char("Montant TTC")
    # @api.one
    # @api.depends('transfert_idss')
    # def _getTren(self):
    #     location = self.env['stock.location'].search([('name','=','non-d')]).id
    #     transferts = self.env['gakd.transfert'].search(['&',('location_id','=',location),('is_done','=',False)])
    #     data = [(4,tr.id) for tr in transferts]
    #     data_ids = [tr.id for tr in transferts]
    #     self.transfert_idss=[(6,False,data_ids)]
    # @api.model
    # def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
    #     res = super(Facture_for, self).fields_view_get(view_id=view_id, view_type=view_type,toolbar=toolbar, submenu=submenu)
    #     if view_type == 'form':
    #         doc = etree.XML(res['arch'])
    #         nodes = doc.xpath("//field[@name='transfert_id']")
    #         location = self.env['stock.location'].search([('name','=','non-d')]).id
    #         transferts = self.env['gakd.transfert'].search(['&',('location_id','=',location),('is_done','=',False)])
    #         data = [(4,tr.id) for tr in transferts]
    #         data_ids =  [tr.id for tr in transferts]
    #         d = '['
    #         for i in data_ids:
    #             d+=str(i)+','
    #         d+=']'
    #         for node in nodes:
    #             node.set('domain', "[('id', 'in',"+d+")]")
    #         res['arch'] = etree.tostring(doc)
    #     return res
    @api.model
    def create(self, vals):
        transfert = self.env['stock.picking'].search([('id','=',vals.get('transfert_id',False))])
        print transfert
        transfert.write({
            'is_done':'done'
        })
        return super(Facture_for, self).create(vals)
    def fichier(self):
        return self.env['report'].get_action(self, 'res.partner.xlsx')
    def imprimer(self):
        if self.qr_code:
            return self.env['report'].get_action(self, 'gakd.template_report_invoice11')
        else :
            raise ValidationError("Attention, il faudrait d’abord importer les données de signature de la DGI avant d’imprimer la facture normalisée")

    # @api.onchange('id')
    # def _onchange_id(self):
    #     print '-----------------------'
    #     location = self.env['stock.location'].search([('name','=','non-d')]).id
    #     transferts = self.env['gakd.transfert'].search(['&',('location_id','=',location),('is_done','=',False)])
    #     data_ids =  [tr.id for tr in transferts]
    #     return {'domain':{'transfert_id':[('id', 'in',data_ids)]}}


class Facture(models.Model):
    _inherit = 'account.invoice'

    amount_total = fields.Monetary(string='Total',store=True, readonly=True, compute='_compute_amount',digits=(12,2))

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'currency_id', 'company_id', 'date_invoice', 'type')
    def _compute_amount(self):
        round_curr = self.currency_id.round
        self.amount_untaxed =round( sum(line.price_subtotal for line in self.invoice_line_ids),1)
        self.amount_tax =round( sum(round_curr(line.amount) for line in self.tax_line_ids),1)
        self.amount_total = round(self.amount_untaxed + self.amount_tax,1)
        amount_total_company_signed = self.amount_total
        amount_untaxed_signed = self.amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(self.amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(self.amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        self.amount_total_company_signed = round(amount_total_company_signed * sign,1)
        self.amount_total_signed =round( self.amount_total * sign,1)
        self.amount_untaxed_signed = round(amount_untaxed_signed * sign,1)
        self.amount_total=round(self.amount_total,1)
        print '7888888888888888888fdgdddddddddddddddddddddddfg8'



    def group_lines(self, iml, line):
        print '888888888888888888888888888888888888888888888888888888sssssss'
        """Merge account move lines (and hence analytic lines) if invoice line hashcodes are equals"""
        if self.journal_id.group_invoice_lines:
            line2 = {}
            for x, y, l in line:
                tmp = self.inv_line_characteristic_hashcode(l)
                if tmp in line2:
                    am = line2[tmp]['debit'] - line2[tmp]['credit'] + (l['debit'] - l['credit'])
                    line2[tmp]['debit'] = (round(am,2) > 0) and am or 0.0
                    line2[tmp]['credit'] = (round(am,2) < 0) and -am or 0.0
                    line2[tmp]['amount_currency'] += l['amount_currency']
                    line2[tmp]['analytic_line_ids'] += l['analytic_line_ids']
                    qty = l.get('quantity')
                    if qty:
                        line2[tmp]['quantity'] = line2[tmp].get('quantity', 0.0) + qty
                else:
                    line2[tmp] = l
            line = []
            for key, val in line2.items():
                line.append((0, 0, val))
        return line

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total, inv.date_invoice)[0]
                res_amount_currency = total_currency
                ctx['date'] = inv._get_currency_rate_date()
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': round(t[1],1),
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': round(total,1),
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)
            print 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk'
            date = inv.date or inv.date_invoice
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True