# -*- coding: utf-8 -*-

from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
import xlsxwriter

class PartnerXlsx(ReportXlsx):

    def generate_xlsx_report(self, workbook, data, lines):
        sheet = workbook.add_worksheet("Produit")
        sheet.write(0,0,'REFERENCE')
        sheet.write(0,1,"NOM DE L'ARTICLE")
        sheet.write(0,2,"PRIX D'ACHAT TTC")
        sheet.write(0,3,'PRIX DE VENTE HT')
        sheet.write(0,4,'TVA')
        sheet.write(0,5,'ID_TAXE_SPECIFIQUE')
        sheet.write(0,6,'CODE_TAXE_GROUP')
        sheet.write(0,7,u'Réf. Facture ')
        sheet.write(0,8,u'Taxe ASAS ')
        sheet1 = workbook.add_worksheet("Facture")
        sheet1.write(0,0,'Reference Article')
        sheet1.write(0,1,u"Désignation")
        sheet1.write(0,2,u"Quantité")
        sheet1.write(0,3,u'Réduction')
        sheet1.write(0,4,u'Type opération')
        
        sheet1.write(0,5,'Identifiant du client')
        sheet1.write(0,6,'IFU du client')
        sheet1.write(0,7,'Nom du client')
        sheet1.write(0,8,'AIB')
        sheet1.write(0,9,u'Réf. Facture actuelle')
        sheet1.write(0,10,u'Réf. Facture parent')
        row=1
        account = self.env['account.invoice'].search([('qr_code','=',False),('date_invoice','>','30/04/2020')])
        for obj in account:
            for article in obj.invoice_line_ids:
                
                taxe_spec=0
                tva = article.price_unit*article.quantity-article.price_subtotal
                sheet.write(row,0,article.product_id.id)
                sheet.write(row,1,article.product_id.name)
                sheet.write(row,2,round(article.price_unit))
                sheet.write(row,3,round(article.price_subtotal /article.quantity))
                sheet.write(row,7, obj.number )
                ctg='B'
                if article.product_id.name.startswith('GAZ'):
                    ctg='A'
                if  obj.partner_id.property_account_position_id:
                    ctg='D'
                if article.product_id.categories_consomable=='Produits blancs':
                    if len(obj.tax_line_ids )>0:
                        taxe_spec= obj.tax_line_ids[0].amount
                        tva=tva-taxe_spec
                
                sheet.write(row,4, round(tva/article.quantity) )
                sheet.write(row,5, round( taxe_spec/article.quantity) ) 
                sheet.write(row,6,ctg)  
                sheet1.write(row,0,article.product_id.id) 
                sheet1.write(row,1,article.product_id.name)
                sheet1.write(row,2,round(article.quantity))
                sheet1.write(row,3,0)
                sheet1.write(row,4,'FV')
                sheet1.write(row,5, obj.partner_id.id )
                if obj.partner_id.ifu:
                    sheet1.write(row,6, obj.partner_id.ifu)
                sheet1.write(row,7, obj.partner_id.name )
                sheet1.write(row,9, obj.number )
                row=row+1
                # sheet1.write(1,0,obj.id)
                # report_name = obj.partner_id
                # # # One sheet by partner
                # bold = workbook.add_format({'bold': True})
                # sheet.write(0, 0, obj.name, bold)

        # workbook.close()
PartnerXlsx('report.res.partner.xlsx',
            'account.invoice')

