from datetime import datetime, timedelta
from datetime import datetime,date
from dateutil import relativedelta
from dateutil.relativedelta import relativedelta

def date_dure_cul(dateinit,opet,intDure,typeDuree='Jour(s)'):
 if dateinit:
    if typeDuree=="Jour(s)":
        if opet==0:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(days=-intDure)
        else:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(days=intDure)
            

    elif typeDuree=='Semaine(s)':
        if opet==0:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(weeks=-intDure)
        else:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(weeks=intDure)
            

    elif typeDuree=='Mois':
        if opet==0:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(months=-intDure)
        else:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(months=intDure)
            
    elif typeDuree=='an(s)':
        if opet==0:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(years=-intDure)
        else:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')+relativedelta(years=intDure)
    else:
         return datetime.strptime(str(dateinit), '%Y-%m-%d')
     