# -*- coding: utf-8 -*-

from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
import logging
import time
import math
class   bon_commande(models.Model):
    _inherit = "sale.order"

    # stock = fields.Many2one(comodel_name='stock.location', string='Emplacement',required=True)
    transfert = fields.Many2one(comodel_name='gakd.transfert')
    tax_tva = fields.Float(string='TVA',compute="_calc_taxes")
    tax_ts = fields.Float(string='TVA',compute="_calc_taxes")

    def _calc_taxes(self):
        for order in self:
            tva = 0
            ts = 0
            for line in order.order_line:
                if line.product_id.id==155:
                    tva_l= self.env['account.tax'].search([('name','=','TVA - ESSENCE')]).amount
                    ts_l= self.env['account.tax'].search([('name','=','TS - ESSENCE')]).amount
                elif  line.product_id.id==156:
                    tva_l= self.env['account.tax'].search([('name','=','TVA - GASOIL')]).amount
                    ts_l= self.env['account.tax'].search([('name','=','TS - GASOIL')]).amount
                elif  line.product_id.id==164:
                    tva_l= self.env['account.tax'].search([('name','=','TVA-PETROLE')]).amount
                    tva_l= self.env['account.tax'].search([('name','=','Ts-PETROLE')]).amount
                else:
                    continue
                tva += tva_l * line.product_uom_qty
                ts += ts_l * line.product_uom_qty
            order.sudo().update({
                'tax_tva':tva,
                'tax_ts':ts
            })

    # @api.multi
    # def action_view_delivery(self):
    #     '''
    #     This function returns an action that display existing delivery orders
    #     of given sales order ids. It can either be a in a list or in a form
    #     view, if there is only one delivery order to show.
    #     '''
    #     print 'hello'
    #     action = self.env.ref('gakd.stock_transfer_action').read()[0]
    #     print action

       
    #     action['views'] = [(self.env.ref('gakd.stock_transfer_view_form').id, 'form')]
    #     action['res_id'] = self.transfert.id
    #     print action 
    #     return action
    
    def action_confirm(self) :
        data=[]
        p= super(bon_commande,self).action_confirm()
        print self.order_line
        source=0
        destinations=0
        location_id=0
        location_id_for_view=0
        location_dest_id=0
        client=0
        # stock_picking_id=0
        # for tr in self.picking_ids:
        #     stock_picking_id=tr.stock_picking_id
        #     break
        for tr in self.picking_ids:
            source=tr.location_id.id
            destinations=tr.location_dest_id.id
            location_id_for_view=tr.location_id.id
            location_id=tr.location_id.id
            location_dest_id=tr.location_dest_id.id
            client=tr.partner_id.id
        # if len(self.picking_ids) == 1 :
        for tr in self.picking_ids:
            print tr
            tr.write({
                'boolTest':True
            })
                     
                # print 'iohnjhiihoioioiojiojo'
                # print tr.move_lines
                # for a in  tr.move_lines:
                #     print a
                #     data.append((0,0 ,{'product_id':a.product_id.id,'qty':a.product_uom_qty,'source':source,
                #     'destinations':destinations,'location_id_for_view':location_id_for_view,'booltest':True}))
        ids={}
        data=[]
        
        for tr in self.picking_ids:
            for a in  tr.move_lines:
                if a.product_id.id not in ids:
                    ids[a.product_id.id]=a.product_uom_qty
                else :
                    ids[a.product_id.id]+=a.product_uom_qty
        for key,value in ids.iteritems():
            data.append((0,0 ,{'product_id':key,'qty':value,'source':source,
                    'destinations':destinations,'location_id_for_view':location_id_for_view,'booltest':True}))
           
        # else :
        #     ids={}
        #     data=[]
        #     for tr in self.picking_ids:
        #         for a in  tr.move_lines:
        #             if tr.product_id.id not in ids:
        #                 ids[a.product_id.id]=a.product_uom_qty
        #             else :
        #                 ids[a.product_id.id]+=a.product_uom_qty
        #     for key,value in ids.iteritems():
        #         data.append((0,0 ,{'product_id':key,'qty':value,'source':source,
        #                 'destinations':destinations,'location_id_for_view':location_id_for_view,'booltest':True}))
        """av= self.env['gakd.transfert'].create({
                    'typeDesProuduit':'autre',
                    'transfer_type':'d',
                    'Date_prevue':datetime.datetime.now().date(),
                    'location_id':location_id,
                    'move_lines':data,
                    'location_dest_depot_id':location_dest_id,
                    'client':client,
                    # 'stock_picking_id':stock_picking_id,
                    'states':'test',
                    'commande_id':self.id
                        })    
        self.transfert=av.id 
        self.delivery_count=1
        print self.transfert
        print  av.commande_id  
            print 'deleeeeete'"""
        # for tr in self.picking_ids:
        #     for f in tr.pack_operation_ids:
        #         f.unlink()
        #     tr.unlink()
         
        return p
         
    @api.model
    def create(self, vals):
        print 'oooooooooooorder'
        # flag=1
        # print 'VAAAAAAAAAAAAALS bon' de commande'
        # print vals
        # for val in  vals['order_line']:
        #     product_id= val[2]['product_id']
        #     print product_id
        #     qte_demande=val[2]['product_uom_qty']
        #     print qte_demande
        #     qte=0
        #     qte_dispo=self.env["stock.quant"].search([('location_id','=',vals['stock']),('product_id','=',product_id)])
        #     for q in qte_dispo:
        #         qte+=q.qty
        #     print 'qte dispo'
        #     print qte
        #     if qte_demande > qte:
        #         flag=0
        # if flag==1:
        #     return super(bon_commande, self).create(vals)
        # else :
        #     raise ValidationError("La quantité disponible dans l\'emplacement demandé est supérieure à la quantité disponible veuillez la vérifier  ")
        
        d= super(bon_commande, self).create(vals)
        # a=self.env["stock.picking"].search([('origin','=',d.origin)])
        # print 'sele order print'
        # print a
        # print d.partner_id.id
        # a.write({
        #     'boolTest':True,
        #     'partner_id':d.partner_id.id,
            
        #     })
        # print 'ffffffff'
        # print a.sale_id
        # print a.boolTest
        # print 'boool affect'
        return d
  
    # @api.constrains('stock','order_line')
    # def qte_check(self):
    #     flag=1
    #     print 'VAAAAAAAAAAAAALS bon de commande'
    #     for val in  self.order_line:
    #         print val
    #         product_id= val.product_id.id
    #         print product_id
    #         qte_demande=val.product_uom_qty
    #         print qte_demande
    #         qte=0
    #         qte_dispo=self.env["stock.quant"].search([('location_id','=',self.stock.id),('product_id','=',product_id)])
    #         for q in qte_dispo:
    #             qte+=q.qty
    #         print 'qte dispo'
    #         print qte
    #         if qte_demande > qte:
    #             flag=0
    #     if flag==0:
    #         raise ValidationError("La quantité disponible dans l\'emplacement demandé est supérieure à la quantité disponible veuillez la vérifier  ")
             

class stock_picking(models.Model):
    _inherit = 'stock.picking'
    
    boolTest = fields.Boolean(default=False)
    client_id=fields.Many2one(comodel_name='res.partner', string='Client')
    
    @api.model
    def create(self,vals):
        print 'PIIIIIIIIIIIICKING'
        data=[]
        vals['image1'] = 'iVBORw0KGgoAAAANSUhEUgAAAK0AAABqCAIAAABBIEYYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIISURBVHhe7djBjQJBFAPRyT/pBg5IaARUAH579cnlEtN/r+MPgXMuEBB4EuABDV4EeMADHnDgTcDvARf8HnDA7wEHPgn4LvDBd4EDvgsc8F3gwJ2A9wEnvA844H3AAe8DDngfcOAbAe9EXngncsA7kQPeiRzwTuSAdyIHfhFwL3DDvcAB9wIH3AsccC9wwL3AAfcCB/4RcDfyw93IAXcjB9yNHHA3csDdyAF3IwfcjRwoAv5/UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwtHzVfG6U/pf1sAAAAAElFTkSuQmCC'
        vals['image2'] = 'iVBORw0KGgoAAAANSUhEUgAAAK0AAABqCAIAAABBIEYYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIISURBVHhe7djBjQJBFAPRyT/pBg5IaARUAH579cnlEtN/r+MPgXMuEBB4EuABDV4EeMADHnDgTcDvARf8HnDA7wEHPgn4LvDBd4EDvgsc8F3gwJ2A9wEnvA844H3AAe8DDngfcOAbAe9EXngncsA7kQPeiRzwTuSAdyIHfhFwL3DDvcAB9wIH3AsccC9wwL3AAfcCB/4RcDfyw93IAXcjB9yNHHA3csDdyAF3IwfcjRwoAv5/UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwteVCENnIebOxcLXlQhDZyHmzsXC15UIQ2ch5s7FwtHzVfG6U/pf1sAAAAAElFTkSuQmCC'
        return super(stock_picking, self).create(vals)
    
    # @api.multi
    # def write(self,vals):
    #     print 'PIIIIIIIIIIIICKING88888888888'
    #     print vals
    #     data=[]
    #     p=super(stock_picking, self).write(vals)
    #     #location_dest_id
    #     print 'KKKKKKKKKKK'
    #     print self.sale_id
    #     # print vals['order']
    #     print self.boolTest
    #     if 'boolTest' in vals:
    #         print 'iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii'
    #         if vals['boolTest']:
    #             print 'CRAAAAAATE trans'
    #             location_id=self.location_id
    #             # prod=self.env["stock.r"].search([('id','=',self.sale_id.id)]).order_line
    #             # test=self.env["sale.order.line"].search([('id','=',prod)])
    #             data=[]
    #             for a in  self.move_lines:
    #                 print a
    #                 data.append((0,0 ,{'product_id':a.product_id.id,'qty':a.product_uom_qty,'source':location_id.id,
    #                 'destinations':self.location_dest_id.id,'location_id_for_view':location_id.id,'booltest':True}))
    #             print 'said'
    #             # print self.sale_id.pack_operation_product_ids
    #             print self.partner_id
    #             av= self.env['gakd.transfert'].create({
    #                 'typeDesProuduit':'autre',
    #                 'transfer_type':'d',
    #                 'Date_prevue':datetime.datetime.now().date(),
    #                 'location_id':location_id.id,
    #                 'move_lines':data,
    #                 'location_dest_depot_id':self.location_dest_id.id,
    #                 'client':self.partner_id.id,
    #                 # 'stock_picking_id':self.id,
    #                 'states':'test'
    #                     })
    #             print '-----------------*'
    #     return p



class pos_order(models.Model):
    _inherit = 'pos.order'

    transaction_id = fields.Many2one(comodel_name='gakd.carte.consommation', string='Transaction')
    last_id = fields.Float("Last id")
    def _reconcile_payments(self):
        
        print '_reconcile_payments'
        i=0
        for order in self:
            i+= 1
            print str(i)+str('/')+str(len(self))
            print '_reconcile_payments ligne'
            
            aml = order.statement_ids.mapped('journal_entry_ids').mapped('line_ids') | order.account_move.line_ids | order.invoice_id.move_id.line_ids
            aml = aml.filtered(lambda r: not r.reconciled and r.account_id.internal_type == 'receivable' and r.partner_id == order.partner_id.commercial_partner_id)

            # Reconcile returns first
            # to avoid mixing up the credit of a payment and the credit of a return
            # in the receivable account
            aml_returns = aml.filtered(lambda l: (l.journal_id.type == 'sale' and l.credit) or (l.journal_id.type != 'sale' and l.debit))
            try:
                aml_returns.reconcile()
                (aml - aml_returns).reconcile()
            except:
                # There might be unexpected situations where the automatic reconciliation won't
                # work. We don't want the user to be blocked because of this, since the automatic
                # reconciliation is introduced for convenience, not for mandatory accounting
                # reasons.
                # It may be interesting to have the Traceback logged anyway
                # for debugging and support purposes
                _logger.exception('Reconciliation did not work for order %s', order.name)

"""
class pos_session(models.Model):
    _inherit = 'pos.session'

    def _confirm_orders(self):
	super(pos_session, self)
        for session in self:
	    print '555555555555555555555555'
            company_id = session.config_id.journal_id.company_id.id
            orders = session.order_ids.filtered(lambda order: order.state == 'paid')
	    print orders
            journal_id = self.env['ir.config_parameter'].sudo().get_param(
                'pos.closing.journal_id_%s' % company_id, default=session.config_id.journal_id.id)
	    print journal_id
	   
            if not journal_id:
                raise UserError(_("You have to set a Sale Journal for the POS:%s") % (session.config_id.name,))
	    print '111111111111111111'
            move = self.env['pos.order'].with_context(force_company=company_id)._create_account_move(session.start_at, session.name, int(journal_id), company_id)
            
	    orders.with_context(force_company=company_id)._create_account_move_line(session, move)
	    print '111111111111111111111'
            for order in session.order_ids.filtered(lambda o: o.state not in ['done', 'invoiced']):
                if order.state not in ('paid'):
                    raise UserError(_("You cannot confirm all orders of this session, because they have not the 'paid' status"))
                order.action_pos_order_done()
            
            orders_to_reconcile = session.order_ids.filtered(lambda order: order.state in ['invoiced', 'done'] and order.partner_id)
            orders_to_reconcile.sudo()._reconcile_payments()

    @api.multi
    def action_pos_session_close(self):
	super(pos_session, self)
	print '88888888888888888888888888'
        for session in self:
            company_id = session.config_id.company_id.id
            ctx = dict(self.env.context, force_company=company_id, company_id=company_id)
            for st in session.statement_ids:
                
                if (st.journal_id.type not in ['bank', 'cash']):
                    raise UserError(_("The type of the journal for your payment method should be bank or cash "))
                st.with_context(ctx).sudo().button_confirm_bank()
	print '9999999999999999999'
        self.with_context(ctx)._confirm_orders()
	
        self.write({'state': 'closed'})
        return {
            'type': 'ir.actions.client',
            'name': 'Point of Sale Menu',
            'tag': 'reload',
            'params': {'menu_id': self.env.ref('point_of_sale.menu_point_root').id},
        }
"""

from odoo import fields, api, models,_

class   stock_scrap(models.Model):
    _inherit = "stock.scrap"


    scrap_location_id = fields.Many2one(
        'stock.location', 'Scrap Location',
        domain="[]", states={'done': [('readonly', True)]})


class OrderLine(models.Model):
    _inherit = "sale.order.line"

    product_prix_ht = fields.Float(string="Prix HT",compute='_calc_prix_ht')
    def _calc_prix_ht(self):
        for line in self:
            tva_l = 0
            ts_l =0
            if line.product_id.id==155:
                tva_l= self.env['account.tax'].search([('name','=','TVA - ESSENCE')]).amount
                ts_l= self.env['account.tax'].search([('name','=','TS - ESSENCE')]).amount
            elif  line.product_id.id==156:
                tva_l= self.env['account.tax'].search([('name','=','TVA - GASOIL')]).amount
                ts_l= self.env['account.tax'].search([('name','=','TS - GASOIL')]).amount
            elif  line.product_id.id==164:
                tva_l= self.env['account.tax'].search([('name','=','TVA - PETROLE')]).amount
                tva_l= self.env['account.tax'].search([('name','=','TS - PETROLE')]).amount
            print 55555555555555555555555555555
            print line.product_id.lst_price
            line.sudo().update({
                'product_prix_ht':line.product_id.lst_price - (tva_l + ts_l)
            })


class pos(models.Model):
    _inherit = 'pos.session'

    def compta(self):
        logging.info(' POOOOOOOOOOOS SESSIONS HHHHHH')
        all= self.env["pos.session"].search([('state','=','opened')])
        for a in all:
            logging.info('hh555555555555566666666666666666')
            a.action_pos_session_closing_control()

    @api.multi
    def action_pos_session_close(self):
        super(pos, self)
        for session in self:
            company_id = session.config_id.company_id.id
            ctx = dict(self.env.context, force_company=company_id, company_id=company_id)
            for st in session.statement_ids:
                for lined in st.line_ids:
                    print 98989898989898899898989899899
                    if lined.account_id.company_id.id != 1 :
                        acc = self.env["account.account"].search([('code','=',lined.account_id.code),('company_id','=',1)])
                        # acc1 = self.env["account.account"].search([('code','=',acc.code),('company_id','=',1)])
                        # raise UserError(_("hhhhhhhhhhhhhhhhhhhhhh"))
                        lined.write({
                            'account_id':acc.id
                        })
            for st in session.statement_ids:
                logging.info('11111111122222222221111111111')
                print st
                print st.difference
                print st.journal_id.amount_authorized_diff
                if abs(st.difference) > st.journal_id.amount_authorized_diff:
                    # The pos manager can close statements with maximums.
                    if not self.env['ir.model.access'].check_groups("point_of_sale.group_pos_manager"):
                        raise UserError(_("Your ending balance is too different from the theoretical cash closing (%.2f), the maximum allowed is: %.2f. You can contact your manager to force it.") % (st.difference, st.journal_id.amount_authorized_diff))
                if (st.journal_id.type not in ['bank', 'cash']):
                    raise UserError(_("The type of the journal for your payment method should be bank or cash "))
		print'ouiiiiiiiiiiiiiiiiiiiiiii'
                st.with_context(ctx).sudo().button_confirm_bank()
		print '888888888888888888888'
        self.with_context(ctx)._confirm_orders()
        self.write({'state': 'closed'})
        return {
            'type': 'ir.actions.client',
            'name': 'Point of Sale Menu',
            'tag': 'reload',
            'params': {'menu_id': self.env.ref('point_of_sale.menu_point_root').id},
        }

class account_bank_statement(models.Model):
    _inherit = 'account.bank.statement'
    @api.multi
    def button_confirm_bank(self):
        super(account_bank_statement, self)
        logging.info('HH button_confirm_bank')
        self._balance_check()
        statements = self.filtered(lambda r: r.state == 'open')
        print statements
        
        for statement in statements:
            logging.info('HH button_confirm_bank out')
            moves = self.env['account.move']
            logging.info(statement.line_ids)
            i=0
            for st_line in statement.line_ids:
                i+=1
                logging.info('COOOOOteur')
                logging.info(str(i) + str('/')+ str(len(statement.line_ids)))
                logging.info('HH for st_line in statement.line_ids')
                if st_line.account_id and not st_line.journal_entry_ids.ids:
                    st_line.fast_counterpart_creation()
                elif not st_line.journal_entry_ids.ids:
                    raise UserError(_('All the account entries lines must be processed in order to close the statement.'))
                moves = (moves | st_line.journal_entry_ids)
            logging.info('HH for st_line in statement.line_ids out')
            if moves:
                moves.filtered(lambda m: m.state != 'posted').post()
            statement.message_post(body=_('Statement %s confirmed, journal items were created.') % (statement.name,))
        statements.link_bank_to_partner()
        statements.write({'state': 'confirm', 'date_done': time.strftime("%Y-%m-%d %H:%M:%S")})



class pos_order(models.Model):
    _inherit = 'pos.order'

    transaction_id = fields.Many2one(comodel_name='gakd.carte.consommation', string='Transaction')
    last_id = fields.Float("Last id")
    def _reconcile_payments(self):
        super(pos_order, self)
        logging.info('_reconcile_payments')
        i=0
        for order in self:
            i+= 1
            if i<2:
                if True:
                    logging.info(str(i)+str('//')+str(len(self)))
                    logging.info('_reconcile_payments ligne')
                    aml = order.statement_ids.mapped('journal_entry_ids').mapped('line_ids') | order.account_move.line_ids | order.invoice_id.move_id.line_ids
                    aml = aml.filtered(lambda r: not r.reconciled and r.account_id.internal_type == 'receivable' and r.partner_id == order.partner_id.commercial_partner_id)

                    # Reconcile returns first
                    # to avoid mixing up the credit of a payment and the credit of a return
                    # in the receivable account
                    aml_returns = aml.filtered(lambda l: (l.journal_id.type == 'sale' and l.credit) or (l.journal_id.type != 'sale' and l.debit))
                    try:
                        aml_returns.reconcile()
                        (aml - aml_returns).reconcile()
                    except:
                        # There might be unexpected situations where the automatic reconciliation won't
                        # work. We don't want the user to be blocked because of this, since the automatic
                        # reconciliation is introduced for convenience, not for mandatory accounting
                        # reasons.
                        # It may be interesting to have the Traceback logged anyway
                        # for debugging and support purposes
                        logging.exception('Reconciliation did not work for order %s', order.name)

class AccountPayment(models.Model):
    _inherit = 'account.payment'


    @api.model
    def create(self, values):
        # Add code here
       

        print values
        print '###############################'
        print self.env.user
        print '###############################'
        if self.env.user.has_group('gakd.group_gakd_sous_chargeur')  and  values['partner_type']=='supplier':
            journal_id=self.env['account.journal'].search([('id','=',values['journal_id'])])
            if values['amount']>100000:
                raise  ValidationError('Avertissement : vous ne pouvez saisir un règlement supérieur à 100 000 CFA')
        p=super(AccountPayment, self).create(values)
       
      
     

        return p

class stock_operation(models.Model):
    _inherit = 'stock.picking'



    field_return = fields.Boolean(string="return",default=False)
    
    @api.one
    @api.depends('compute_field_vente','compute_field_achat','source','dest')
    def _get_emp(self):
        self.ensure_one()
        self.compute_field_vente = False
        self.compute_field_achat = False
        print 'compuuuuuuuuuuuuute '
        if not self.sale_id and not self.purchase_id:
            self.compute_field_vente = True
            self.compute_field_achat = True
        else :
            if self.sale_id: 
                print 'hhhhhhhhhhhhh'
                self.compute_field_vente = True
                self.compute_field_achat = False
            elif self.purchase_id:
                print 'jjjjjjjjjjjjj'
                self.compute_field_vente = False
                self.compute_field_achat =  True 
        print self.sale_id
        print self.purchase_id
        print self.compute_field_vente
        print self.compute_field_achat

    @api.constrains('source', 'dest')
    def _check_view(self):
        if not self.field_return and not self.backorder_id:
            if self.compute_field_vente:
                print 'sourcesourcesourcesourcesourcesourcesource'
                print self.field_return
                print self.backorder_id
                if not self.source:
                    raise ValidationError("Veuillez saisir l'emplacement d'origine")
            if self.compute_field_achat:
                if not self.dest:
                    raise ValidationError("Veuillez saisir l'emplacement de destination")
    
    compute_field_vente = fields.Boolean(string="check field", compute='_get_emp')
    compute_field_achat = fields.Boolean(string="check field", compute='_get_emp')
    source = fields.Many2one(comodel_name='stock.location', string='Emplacement d\'origine')
    dest = fields.Many2one(comodel_name='stock.location', string='Emplacement de destination')

    @api.multi
    def write(self, vals):
        print 'WRITE 88888888888888888'
        # pa= super(stock_operation, self).write(vals)
        print self.source.id
        print self.location_id
        # if self.source.id:
        if 'source' in vals:
            self.sudo().write({
                'location_id':vals['source'],
            })
        if 'dest' in vals:
            self.sudo().write({
                'location_dest_id':vals['dest'],
            })
        return super(stock_operation, self).write(vals)

    def force_assign(self):
        # super(stock_operation).force_assign()
        print "fooooooooooooooooooooorce assign"
        self.mapped('move_lines').filtered(lambda move: move.state in ['confirmed', 'waiting']).force_assign()
        print "dooooooooooooooooooooooooooo new transfert"
        # print self.location_dest_id.display_name
        # print self.location_id.display_name
        if not self.backorder_id and not self.field_return:
            if self.source.id:
                self.sudo().write({
                    'location_id':self.source.id,
                })
            if self.dest.id:
                self.sudo().write({
                    'location_dest_id':self.dest.id,
                })
        # print self.location_dest_id.display_name
        # print self.location_id.display_name
        # print self.sale_id
        # print self.purchase_id
        # raise ValidationError("Veuillez verifier les unités de mesure")
        return True

    def action_assign(self):
        print "sssssssssssssss assign"
        self.filtered(lambda picking: picking.state == 'draft').action_confirm()
        moves = self.mapped('move_lines').filtered(lambda move: move.state not in ('draft', 'cancel', 'done'))
        if not moves:
            raise UserError('Nothing to check the availability for.')
        moves.action_assign()
        if not self.backorder_id and not self.field_return:
            if self.source.id:
                self.sudo().write({
                    'location_id':self.source.id,
                })
            if self.dest.id:
                self.sudo().write({
                    'location_dest_id':self.dest.id,
                })
        return True

    def do_new_transfer(self):  
        super(stock_operation) 
        print "dooooooooooooooooooooooooooo new transfert2"
        # print self.location_dest_id.display_name
        # print self.location_id.display_name
        # print self.sale_id
        # print self.purchase_id
        if not self.backorder_id and not self.field_return:
            if self.source.id:
                self.sudo().write({
                    'location_id':self.source.id,
                })
            if self.dest.id:
                self.sudo().write({
                    'location_dest_id':self.dest.id,
                })
            for a in self.pack_operation_product_ids: 
                for il in self.pack_operation_ids:
                    if self.source.id:
                        il.sudo().write({
                            'location_id':self.source.id,
                        })
                    if self.dest.id:
                        il.sudo().write({
                            'location_dest_id':self.dest.id,
                        })
                                        

        # raise ValidationError("Veuillez remplir tous les champs pour les produits blancs")
        for pick in self:
            if pick.state == 'done':
                raise UserError('The pick is already validated')
            pack_operations_delete = self.env['stock.pack.operation']
            if not pick.move_lines and not pick.pack_operation_ids:
                raise UserError('Please create some Initial Demand or Mark as Todo and create some Operations. ')
            # In draft or with no pack operations edited yet, ask if we can just do everything
            if pick.state == 'draft' or all([x.qty_done == 0.0 for x in pick.pack_operation_ids]):
                # If no lots when needed, raise error
                picking_type = pick.picking_type_id
                if (picking_type.use_create_lots or picking_type.use_existing_lots):
                    for pack in pick.pack_operation_ids:
                        if pack.product_id and pack.product_id.tracking != 'none':
                            raise UserError('Some products require lots/serial numbers, so you need to specify those first!')
                view = self.env.ref('stock.view_immediate_transfer')
                wiz = self.env['stock.immediate.transfer'].create({'pick_id': pick.id})
                # TDE FIXME: a return in a loop, what a good idea. Really.
                return {
                    'name': ('Immediate Transfer?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.immediate.transfer',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }

            # Check backorder should check for other barcodes
            if pick.check_backorder():
                view = self.env.ref('stock.view_backorder_confirmation')
                wiz = self.env['stock.backorder.confirmation'].create({'pick_id': pick.id})
                # TDE FIXME: same reamrk as above actually
                return {
                    'name': ('Create Backorder?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.backorder.confirmation',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }
            for operation in pick.pack_operation_ids:
                if operation.qty_done < 0:
                    raise UserError('No negative quantities allowed')
                if operation.qty_done > 0:
                    operation.write({'product_qty': operation.qty_done})
                else:
                    pack_operations_delete |= operation
            if pack_operations_delete:
                pack_operations_delete.unlink()
        self.do_transfer()
        print 'THE end'

class SaleAdvancePaymentInv(models.TransientModel):
    _inherit = "sale.advance.payment.inv"


    @api.model
    def _default_product_id(self):
        print '_default_product_id'
        product_id = self.env['ir.values'].get_default('sale.config.settings', 'deposit_product_id_setting')
        print 'proooooooooooooooduct'
        print product_id 
        print self.env['product.product'].sudo().search([('id','=',product_id)]).name
        print 'kkkkkkkkkkkkkkkkkkk'
        return self.env['product.product'].sudo().browse(product_id)
    
    

class StockReturnPicking(models.TransientModel):
    _inherit = 'stock.return.picking'

    @api.multi
    def _create_returns(self):
        super(StockReturnPicking,self)
        picking = self.env['stock.picking'].browse(self.env.context['active_id'])

        return_moves = self.product_return_moves.mapped('move_id')
        unreserve_moves = self.env['stock.move']
        for move in return_moves:
            to_check_moves = self.env['stock.move'] | move.move_dest_id
            while to_check_moves:
                current_move = to_check_moves[-1]
                to_check_moves = to_check_moves[:-1]
                if current_move.state not in ('done', 'cancel') and current_move.reserved_quant_ids:
                    unreserve_moves |= current_move
                split_move_ids = self.env['stock.move'].search([('split_from', '=', current_move.id)])
                to_check_moves |= split_move_ids

        if unreserve_moves:
            unreserve_moves.do_unreserve()
            # break the link between moves in order to be able to fix them later if needed
            unreserve_moves.write({'move_orig_ids': False})

        # create new picking for returned products
        picking_type_id = picking.picking_type_id.return_picking_type_id.id or picking.picking_type_id.id
        new_picking = picking.copy({
            'move_lines': [],
            'picking_type_id': picking_type_id,
            'state': 'draft',
            'origin': picking.name,
            'location_id': picking.location_dest_id.id,
            'location_dest_id': self.location_id.id,
            'field_return':True})
        new_picking.message_post_with_view('mail.message_origin_link',
            values={'self': new_picking, 'origin': picking},
            subtype_id=self.env.ref('mail.mt_note').id)

        returned_lines = 0
        for return_line in self.product_return_moves:
            if not return_line.move_id:
                raise UserError(_("You have manually created product lines, please delete them to proceed"))
            new_qty = return_line.quantity
            if new_qty:
                # The return of a return should be linked with the original's destination move if it was not cancelled
                if return_line.move_id.origin_returned_move_id.move_dest_id.id and return_line.move_id.origin_returned_move_id.move_dest_id.state != 'cancel':
                    move_dest_id = return_line.move_id.origin_returned_move_id.move_dest_id.id
                else:
                    move_dest_id = False

                returned_lines += 1
                return_line.move_id.copy({
                    'product_id': return_line.product_id.id,
                    'product_uom_qty': new_qty,
                    'picking_id': new_picking.id,
                    'state': 'draft',
                    'location_id': return_line.move_id.location_dest_id.id,
                    'location_dest_id': self.location_id.id or return_line.move_id.location_id.id,
                    'picking_type_id': picking_type_id,
                    'warehouse_id': picking.picking_type_id.warehouse_id.id,
                    'origin_returned_move_id': return_line.move_id.id,
                    'procure_method': 'make_to_stock',
                    'move_dest_id': move_dest_id,
                })

        if not returned_lines:
            raise UserError(_("Please specify at least one non-zero quantity."))

        new_picking.action_confirm()
        new_picking.action_assign()
        return new_picking.id, picking_type_id
