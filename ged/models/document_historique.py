# -*- coding: utf-8 -*-
import time
import odoo
from datetime import datetime
from odoo import SUPERUSER_ID
from odoo import fields, api, models,_
from odoo.tools.translate import _
import glob
import random
import errno

class history_attachment(models.Model):
    _name = 'history_attachment'
    


    def set_cancel(self):
	for version in self.browse(cr, uid, ids):
		if version.state == "activated":
			allVersion = sorted(self.search([('document_id','=',version.document_id.id),('state','=','expired')]),reverse=True)
			self.write({'state': 'cancel'})
			self.write({'state': 'activated'})
		else:
			self.write({'state': 'cancel'})
        return True

    def set_activated():
	for version in self.browse():
		if version.state == "expired":
			allVersion = sorted(self.search([('document_id','=',version.document_id.id),('state','=','activated')]),reverse=True)
			self.write({'state': 'expired'})

			vals_document = {
				'version_num': version.version_num,
				'datas_fname': version.datas_fname,
				'datas'      : version.datas,
				}
			self.env['ir.attachment'].write(vals_document)

        return True
# {'type': 'ir.actions.client','tag': 'reload',}


    _order ='id desc'
    document_id=fields.Many2one('ir.attachment',string='Document')
    write_uid=  fields.Many2one('res.users', string='Utilisateur', readonly=True)
    write_date= fields.Datetime(string='Date modification', readonly=True)
    version_num=fields.Char(string='Version',required=True)
    datas_fname= fields.Char(string='File Name')
    datas=fields.Binary(string='Database Data')
    state=fields.Selection([('expired',u'expirée'),('activated',u'Activée'),('cancel',u'Annulée')], string='Statut', readonly=True)


