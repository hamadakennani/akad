# -*- coding: utf-8 -*-
from odoo import api, fields, models
from datetime import datetime
from datetime import timedelta
from datetime import date

class Consulte(models.Model):
    _name = 'gakd.consulte_recharge'


    @api.one
    @api.depends('view_test','sous_chargeur','montant_debut','montant_fin','montant','datej')
    def get_view(self):
        if self.env.user.has_group('gakd.group_gakd_sous_chargeur') :
            self.view_test=False
        else :
            self.view_test=True

    @api.one
    @api.depends('view_test','sous_chargeur','montant_debut','montant_fin','datej')
    def get_montant(self):
        if  len(self.list_recharge) == 0 :
            self.montant=False 
        else :
            self.montant=True

    @api.one
    @api.depends('datej','sous_chargeur','list_recharge','view_test','montant')
    def montants(self):
        self.montant_debut=0
        self.montant_fin=0
        self.montant_init_tv=0
        self.montant_fin_tv=0
        self.totaljnp=0
        self.totaltv=0
        cp1=0
        cp2=0
        for a in self.list_recharge:
            print "list_rechargelist_rechargelist_rechargelist_recharge"
            print a.type_op
            print a.montant_init
            print a.montant_fin
            if a.type_op=='Recharge compte client':
                self.totaljnp+=a.diff
                if cp1==0:
                    self.montant_debut=a.montant_init
                    
                    cp1=1
                self.montant_fin=a.montant_fin
            if a.type_op=='T.V':
                self.totaltv+=a.diff
                if cp2==0:
                    self.montant_init_tv=a.montant_init
                    
                    cp2=1
                self.montant_fin_tv=a.montant_fin

    datej =  fields.Date(string='Date',default=lambda *a: datetime.now())
    list_recharge = fields.One2many('gakd.liste_recharge','con',string='Recharge')
    sous_chargeur = fields.Many2one('gakd.sous_chargeur',string="Caissier")
    montant_debut = fields.Float(string='Solde de début JNP PASS',compute='montants')
    montant_fin = fields.Float(string='Montant à la fin JNP PASS',compute='montants')
    montant_init_tv = fields.Float(string='Solde de début TV',compute='montants')
    montant_fin_tv =  fields.Float(string='Montant à la fin TV',compute='montants')
    view_test=fields.Boolean(compute='get_view')
    montant=fields.Boolean(compute='get_montant')
    totaljnp= fields.Float(string='Total des opérations',compute='montants')
    totaltv= fields.Float(string='Total des opérations',compute='montants')


    @api.onchange('datej','sous_chargeur')
    def _onchange_type(self):
        print 'hiiiiiiiiiiiiiiiiiiiiiiiiiii'
        print self.datej
        data=[]
        datedebut=self.datej
        dd=datetime.strptime(datedebut,'%Y-%m-%d')
        df=datetime.strptime(datedebut,'%Y-%m-%d')+timedelta(days=1)
        tm1=datetime.strptime('08:00:00','%H:%M:%S').time()
        tm2=datetime.strptime('07:59:59','%H:%M:%S').time()
        if self.env.user.has_group('gakd.group_gakd_sous_chargeur'):
            self.sous_chargeur=self.env["gakd.sous_chargeur"].search([("access","=",self.env.user.id)])
        datedebutt=str(datetime.combine(dd.date(),tm1))
        datefin=str(datetime.combine(df.date(),tm2))
        # self.montant_debut=0
        # self.montant_fin=0
        if self.sous_chargeur :
            cp=0
            for a in self.sous_chargeur.historique:                
                if a.create_date >= datedebutt and a.create_date <= datefin:
                    if cp==0:
                        print a.montant_init
                        # self.montant_debut=a.montant_init
                        cp=562
                    data.append((0,0,{'montant_init':a.montant_init,'montant_fin':a.montant_fin,'diff':a.diff,'type_op':a.type_op}))
                    # self.montant_fin=a.montant_fin
        return {'value':{'list_recharge':data}}


class liste_recharge(models.Model):
    _name="gakd.liste_recharge"

    con = fields.Many2one(comodel_name='gakd.consulte_recharge', string='con')
    montant_init = fields.Float(string='Montant initiale')
    montant_fin =  fields.Float(string='Montant finale')
    type_op = fields.Char(string='Type d\'opération')
    diff = fields.Float(string='Montant affecté')