# -*- coding: utf-8 -*-
from odoo import fields, api, models,_
from odoo.exceptions import  ValidationError
import datetime
from random import randint
import time
import datetime
class capcontrat_versions(models.Model):
    _name='capcontrat.versions'
    _rec_name='namer'
    _order='create_date desc'


    contrat_id=fields.Many2one('capcontrat.projets',string='Contrat')
    file=fields.Binary('Document',required=True)
    filename=fields.Char(string='file_name')
    state=fields.Selection((('brouillon','Brouillon'),('encours','Encours'),('valide','Validé'),('annule','Annulé')),string='Statut')
    description=fields.Text(string='Description')
    user_access=fields.One2many('users.versions','version_id',string='Utilisateurs',required=False)
    user_creater = fields.Many2one('res.users',string='Créateur', default=lambda self: self.env.user)
    based_on=fields.Many2one('capcontrat.versions','Basée sur')
    tempepasser = fields.Many2one('capcontrat.temps',string="Temps passé(H)",domain=[('id','=',-1)])
    num = fields.Integer(string="number of version")
    nom_vers = fields.Char(string="Nom de version")
    numberofusers = fields.Float(string="nombre user")
    numbervalide = fields.Float(string="Progrés de validation")
    namer = fields.Char(string="recname")
    nombrehe=fields.Integer(string='Temps passé (H)',required=True)
    dates=fields.Date(string='Date',required=True)

    def SendMail(self,mailsto,Subject,body):
		import smtplib
		from email.MIMEMultipart import MIMEMultipart
		from email.MIMEText import MIMEText
		mail_server = self.env['ir.mail_server'].search([["name","=","localhost"]])
		msg = MIMEMultipart()
		msg.set_charset("utf-8")
		msg['From']    = mail_server.smtp_user
		msg['To']      = mailsto
		msg['Subject'] = Subject
		body = body
		msg.attach(MIMEText(body, 'html'))
		server = smtplib.SMTP(mail_server.smtp_host, 587)
		server.ehlo()
		server.starttls()
		server.ehlo()
		server.login(mail_server.smtp_user,mail_server.smtp_pass)
		text = msg.as_string()
		server.sendmail(mail_server.smtp_host, mailsto.split(','), text)
		return True
    @api.onchange('categorie_id')
    def on_change_fi(self):
        v={}
        if categorie_id:
            contrat_sr=self.env['contrat.categories'].browse([categorie_id])
            v['version_file'] =  contrat_sr.categorie_file
        return {'value': v}
    @api.model
    def create(self, vals):
        print vals
        if vals.get('contrat_id',False):
            print vals.get('contrat_id')
            version_ids = self.search([('contrat_id', '=',vals.get('contrat_id'))])
            print version_ids
            countVersion = len(version_ids)
            print "yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
            print countVersion
            name = vals.get('filename')
            if '.' in name:	
                extension=str((name.split('.')[1]).split('_')[0])
                vals['filename'] =str((name.split('.')[0]).split('_')[0])+"_V"+str(countVersion)+'.'+extension
            else:
                vals['filename'] =str(name.split('_')[0])+"_V"+str(countVersion)
            vals['nom_vers'] = vals.get('filename')+"_V"+str(countVersion)
            if countVersion>=0:	
                vals['state']="brouillon"
            projet = self.env['capcontrat.projets'].browse(vals.get('contrat_id',False))
            vals['nom_vers'] = projet.libelle +"_V"+str(countVersion)
            users_ids=[]
            for users in projet.users_ids:
                users_ids.append((0,0,{'users_version':users.users_id.id,'state':'brouillon'}))
            vals['user_access'] = users_ids
            countVersion = countVersion+1
            projet.write({
                'numberofversion':countVersion
            })
            print "ooooooooooooooooooooooooooooooooooooooooooooo"
            vals["numberofusers"] = float( len(users_ids))
            print len(users_ids)
            print vals["numberofusers"]
            v = float((1/float( len(users_ids)))*100)
            print "vvvvvvvvvvvvvvvvvvvvvvvvvvnumbervalide"
            print v
            vals["numbervalide"] = float(v)
            print vals["numbervalide"]
            vals["namer"] = vals['nom_vers']
            print "--------------------------------------------------------"
            vr = super(capcontrat_versions, self).create(vals)
            print "--------------------------------------------------------"
            tmp = self.env["capcontrat.temps"].create({
                'version_id':int(vr.id),
                'contrat_id':int(vr.contrat_id.id),
                'nombrehe':int(vr.nombrehe),
                'dates':vr.dates,
            })
            print "--------------------------------------------------------"
            vr.write({
                'namer':vr.nom_vers+'-'+vr.user_creater.name+'-'+str(vr.create_date),
                'tempepasser':int(tmp.id)
            })
            print "--------------------------------------------------------"
            user_ver = self.env['users.versions'].search(['&',('version_id','=',vr.id),('users_version','=',self.env.user.id)])
            user_ver.write({
            'state':'valide'
            })
            print "--------------------------------------------------------"
            for user in projet.users_ids:
                mailto = user.users_id.email
                Subject = "CapContract"
                body = "une nouvelle version publier pour le contrat "+str(vr.contrat_id.libelle)+"<br>Publie par : "+str(vr.user_creater.name)+" <br> pour telecharger la version<br> <a style='color: #fff;background-color: #9c27b0;border-color: #9c27b0;box-shadow: 0 2px 2px 0 rgba(156, 39, 176, 0.14), 0 3px 1px -2px rgba(156, 39, 176, 0.2), 0 1px 5px 0 rgba(156, 39, 176, 0.12);}' href='http://localhost:8069/web/content?model=capcontrat.versions&field=file&id="+str(vr.id)+"&download=true&filename_field=filename'>click ici</a>pour pour Consulter le Contrat <a href='http://localhost:8069/web?debug#id="""+str(vr.contrat_id.id)+"&view_type=form&model=capcontrat.projets&menu_id=29&action=77'>click ici</a>"
                #self.SendMail(mailto,Subject,body)

        return vr

    @api.depends('contrat_id')
    def _compute_att(self):
        return self.env.user.id 

    def set_valide(self):
        user_ver = self.env['users.versions'].search(['&',('version_id','=',self.id),('users_version','=',self.env.user.id)])
        user_ver.write({
            'state':'valide'
        })
        print 'mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm'
        print user_ver.state
        ver_users = self.env["users.versions"].search([('version_id','=',self.id)])
        flag = True
        user_valide = 0
        print ver_users
        for u in ver_users:
            print u.state
            if u.state  != 'valide':
                flag = False
            else :
                user_valide = user_valide + 1
        print flag
        v = float((float(user_valide)/float(self.numberofusers))*100)
        self.write({
            'numbervalide':float(v)
        })
        if flag :
            vrs = self.env['capcontrat.versions'].search([])
            for v in vrs:
                v.write({
                    'state':'brouillon'
                })
            self.write({
                'state':'valide',
            })
            contrat = self.env["capcontrat.projets"].search([('id','=',self.contrat_id.id)])
            contrat.write({
                'state':'valide'
            })
