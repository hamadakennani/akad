 openerp.gakd = function (openerp)
{  
//=====================================================================================================================================
openerp.web.form.widgets.add('statistique_total_vente', 'openerp.gakd.total_vente');
    openerp.gakd.total_vente = openerp.web.form.FieldChar.extend({
        template : "statistique_total_vente",
//=======
	//var Model = require('web.Model') 
        init: function (view, code) {
		this._super(view, code);
		var ds = new openerp.web.DataSet(this, 'gakd.statistique', {});
		$(".o_control_panel").css("display","none");
		html = "<table class='table_critere' style='width: 100%%;min-width: 700px;margin:0px auto;'><tr style='text-align: center;'><td>Points de ventes</td><td style='border-bottom: 1px solid #d2caca;'><select id='pointVente' style='padding: 5px;width: 150px;margin: 5px;height: 30px;padding-bottom: 3px;'></select></td><td style='text-align: center;padding: 10px;font-size: 16px;border-radius:5px 0 0 5px;border-bottom: 1px solid #d2caca;padding-bottom: 3px;'>Clients</td><td style='border-bottom: 1px solid #d2caca;'><select id='clients' style='padding: 5px;width: 150px;margin: 5px;height: 30px;padding-bottom: 3px;'></select></td><td class='resultat_by'><span for='pr'>Résultat par produit</span><input type='radio' checked='checked'  value='par produit' name='bb' id='pr'/></td><td  class='resultat_by'><span for='crt'>Résultat par carte</span><input type='radio' value='par carte' name='bb' id='crt'/></td></tr><tr style='text-align: center;'><td style='text-align: center;padding: 10px;font-size: 16px;border-radius:5px 0 0 5px;border-bottom: 1px solid #d2caca;padding-bottom: 3px;'>Année</td><td style='border-bottom: 1px solid #d2caca;'><select id='yearValue' style='padding: 5px;width: 150px;margin: 5px;height: 30px;padding-bottom: 3px;'><option value='All' selected='true'>Tous</option></select></td><td style='text-align: center;padding: 10px;font-size: 16px;border-radius:5px 0 0 5px;border-bottom: 1px solid #d2caca;padding-bottom: 3px;'>Moi</td><td style='border-bottom: 1px solid #d2caca;'><select id='monthValue' style='padding: 5px;width: 150px;margin: 5px;height: 30px;padding-bottom: 3px;'><option value='All' selected='true'>Tous</option></select></td><td style='text-align: center;padding: 10px;font-size: 16px;border-radius:5px 0 0 5px;border-bottom: 1px solid #d2caca;padding-bottom: 3px;'>Date début</td><td style='border-bottom: 1px solid #d2caca;'><input type='date' id='datedebut'  /></td><td style='text-align: center;padding: 10px;font-size: 16px;border-radius:5px 0 0 5px;border-bottom: 1px solid #d2caca;padding-bottom: 3px;'>Date fin</td><td style='border-bottom: 1px solid #d2caca;'><input type='date' id='datefin' /></td></tr></table>";
		//alert(html);

		
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		var ds = new openerp.web.DataSet(this, 'gakd.statistique', {});
		//var Model = require('web.Model') 
	// ***************************************
		url = "http://localhost:8069"
		var point_vente = "All";
		var client = "All";
		var year = "All";
		var month = "All";
		var date_debut = "";
		var date_fin = ""
		var parProduit = "";
		var parCarte = "";
		ds.call('getAllYear', [ds]).done(function(data) {
			$('#yearValue').html(data);
		})
		ds.call('getAllMonth', [ds]).done(function(data) {
			$('#monthValue').html(data);
		})
		ds.call('getResult', [ds,point_vente,client,year,month,date_debut,date_fin,parProduit,parCarte]).done(function(data) {
			$("#table_result").html(data.table);
			$('#result_datatable').dataTable().fnDestroy();
			$('#result_datatable').dataTable({
			      dom: 'Bfrtip',
				buttons: [
				{
					extend: 'pdf',
					title: function(){
							 return "Liste des consommations"
						      },
					  footer: true ,
					   customize: function (doc) {
			   					doc.content[1].table.widths = 
						Array(doc.content[1].table.body[0].length + 1).join('*').split('');
			  		}
				}
				],
 			      "sScrollY": "450px",
			      "sScrollX": "100%",
			      "sScrollXInner": "100%",
			      "bScrollCollapse": true,
			      "fixedHeader": true,
			      "bPaginate": true,
				  "ordering": true,
				  "searching": true,
				  "info": true,
				  "autoWidth": true,
				  "iDisplayLength": 5,
				  "pagingType": "full_numbers",
				  "language": {
				  "search": "Recherche:",
				  "info":"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
				    "paginate": {
				      "next": ">",
				      "previous": "<",
				      "first": "<<",
				      "last": ">>"
				    },
				  "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
				}
			});

		});

		$("body").on("change","#clients",function(){
			if($(this).val() == 'All'){
				$(".resultat_by").css("display","none");
			}else{
				$(".resultat_by").css("display","table-cell");
			}

		});
		ds.call('getPointVente', [ds]).done(function(data) {
			$("#pointVente").html(data);

		});
		ds.call('getClient', [ds]).done(function(data) {
			$("#clients").html(data);

		});

		$("body").on("change","select.select_consommation,.resultat_by,input",function(){
			//$( "#pointVente" ).select2();
			//$( "#clients" ).select2();
		  	point_vente = $("#pointVente").val();
			client = $("#clients").val();
			date_debut = $("#datedebut").val();
			date_fin = $("#datefin").val();
			year = $("#yearValue").val();
			month = $("#monthValue").val();
			var parProduit = $("#pr").is(':checked');
 			var parCarte = $("#crt").is(':checked');
			ds.call('getResult', [ds,point_vente,client,year,month,date_debut,date_fin,parProduit,parCarte]).done(function(data) {
				$("#table_result").html(data.table);
				$('#result_datatable').dataTable().fnDestroy();
				$('#result_datatable').dataTable({
				      dom: 'Bfrtip',
					buttons: [
					{
						extend: 'pdf',
						title: function(){
								 return "Liste des consommations"
							      },
						  footer: true ,
						   customize: function (doc) {
				   					doc.content[1].table.widths = 
							Array(doc.content[1].table.body[0].length + 1).join('*').split('');
				  		}
					}
					],
	 			      "sScrollY": "450px",
				      "sScrollX": "100%",
				      "sScrollXInner": "100%",
				      "bScrollCollapse": true,
				      "fixedHeader": true,
				      "bPaginate": true,
					  "ordering": true,
					  "searching": true,
					  "info": true,
					  "autoWidth": true,
					  "iDisplayLength": 5,
					  "pagingType": "full_numbers",
					  "language": {
					  "search": "Recherche:",
					  "info":"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
					    "paginate": {
					      "next": ">",
					      "previous": "<",
					      "first": "<<",
					      "last": ">>"
					    },
					  "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
					}
				});
				
			});
		})

		

		 
	
	
	// ***************************************
	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });//openerp.notaire_statistique.categories

//=====================================================================================================================================



//=====================================================================================================================================
openerp.web.form.widgets.add('statistique_recharge_client', 'openerp.gakd.recharge_client');
    openerp.gakd.recharge_client = openerp.web.form.FieldChar.extend({
        template : "statistique_recharge_client",
//=======
	//var Model = require('web.Model') 
        init: function (view, code) {
		this._super(view, code);
		var ds = new openerp.web.DataSet(this, 'gakd.statistique', {});
		$(".o_control_panel").css("display","none");
		
		
        },// fiiiiiiiiiiiiiiiiiiiiiiiin init
//======
	start: function() {
		var ds = new openerp.web.DataSet(this, 'gakd.statistique', {});
		//var Model = require('web.Model') 
	// ***************************************
		var client = "All";
		var year = "All";
		var month = "All";
		ds.call('getAllYear', [ds]).done(function(data) {
			$('#yearValue').html(data);
		})
		ds.call('getAllMonth', [ds]).done(function(data) {
			$('#monthValue').html(data);
		})
		
		ds.call('getClient', [ds]).done(function(data) {
			$("#clients_recharge").html(data);

		});
		ds.call('getListeRechargeClient', [ds,client,year,month,$("#datedebut").val(),$("#datefin").val()]).done(function(data) {
			$("#table_result_recharge").html(data.table);
			$('#result_datatable_recharge').dataTable().fnDestroy();
			$('#result_datatable_recharge').dataTable({
			      dom: 'Bfrtip',
				buttons: [
				{
					extend: 'pdf',
					title: function(){
							 return "Liste des recharges clients"
						      },
					  footer: true ,
					   customize: function (doc) {
			   					doc.content[1].table.widths = 
						Array(doc.content[1].table.body[0].length + 1).join('*').split('');
			  		}
				}
				],
 			      "sScrollY": "450px",
			      "sScrollX": "100%",
			      "sScrollXInner": "100%",
			      "bScrollCollapse": true,
			      "fixedHeader": true,
			      "bPaginate": true,
				  "ordering": true,
				  "searching": true,
				  "info": true,
				  "autoWidth": true,
				  "iDisplayLength": 5,
				  "pagingType": "full_numbers",
				  "language": {
				  "search": "Recherche:",
				  "info":"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
				    "paginate": {
				      "next": ">",
				      "previous": "<",
				      "first": "<<",
				      "last": ">>"
				    },
				  "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
				}
			});

		});


		$("body").on("change",".select_recharge",function(){
			
			year = $("#yearValue").val();
			month = $("#monthValue").val();
			ds.call('getListeRechargeClient', [ds,$("#clients_recharge").val(),year,month,$("#datedebut").val(),$("#datefin").val()]).done(function(data) {
				$("#table_result_recharge").html(data.table);
				$('#result_datatable_recharge').dataTable().fnDestroy();
				$('#result_datatable_recharge').dataTable({
				      dom: 'Bfrtip',
					buttons: [
					{
						extend: 'pdf',
						title: function(){
								 return "Liste des recharges clients"
							      },
						  footer: true ,
						   customize: function (doc) {
				   					doc.content[1].table.widths = 
							Array(doc.content[1].table.body[0].length + 1).join('*').split('');
				  		}
					}
					],
	 			      "sScrollY": "450px",
				      "sScrollX": "100%",
				      "sScrollXInner": "100%",
				      "bScrollCollapse": true,
				      "fixedHeader": true,
				      "bPaginate": true,
					  "ordering": true,
					  "searching": false,
					  "info": true,
					  "autoWidth": true,
					  "iDisplayLength": 5,
					  "pagingType": "full_numbers",
					  "language": {
					  "search": "Recherche:",
					  "info":"Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
					    "paginate": {
					      "next": ">",
					      "previous": "<",
					      "first": "<<",
					      "last": ">>"
					    },
					  "emptyTable": "Aucune donn&eacute;e disponible dans le tableau"
					}
				});
				
			});
		})
		

		 
	
	
	// ***************************************
	},// fiiiiiiiiiiiiiiiiiiiiiiiiin start
//======
    });//openerp.notaire_statistique.categories

//=====================================================================================================================================














































}
