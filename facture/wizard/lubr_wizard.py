# -*- coding: utf-8 -*-
from odoo import api, models,fields
from num2words import num2words
from datetime import datetime, date, timedelta
from openerp.exceptions import ValidationError
class companyWizard(models.TransientModel):
    _name = "lubr.wizard"
    _description = "Lubrifiant Wizard"
    
    def _getinvoice_id(self):
        return self._context.get('active_ids',False)[0]  
    company=fields.Many2one('liste.societe')
    invoice_id=fields.Many2one('account.invoice',default=_getinvoice_id)
    devis=fields.Selection(string="Devis", selection=[('euro', 'EURO'),('ead', 'AED')])
    @api.multi
    def print_report(self):

        datas={}
        datas={
            'form':{
                'company':self.company.id,
                'invoice_id':self.invoice_id.id,
                'devis':self.id,
            }            
        }
        return self.env['report'].get_action(self,'facture.lubr_facture', data=datas)
class retour_bon_consommation_report(models.AbstractModel):
    _name = 'report.facture.lubr_facture'
    @api.model
    def render_html(self, docids, data=None):
        
        report_obj = self.env['report']
        report = report_obj._get_report_from_name('facture.lubr_facture')
        invoice=self.env['account.invoice'].search([('id','=',data['form']['invoice_id'])])
        sale_id=0
        for linne in invoice.invoice_line_ids :
            for line in linne.sale_line_ids:
                sale_id = line.order_id.id
                break
            break
        saleorder=self.env['sale.order'].search([('id','=',sale_id)])   
        company=self.env['liste.societe'].search([('id','=',data['form']['company'])])
        invoice=self.env['account.invoice'].search([('id','=',data['form']['invoice_id'])])
        deviss=self.env['lubr.wizard'].search([('id','=',data['form']['devis'])])
        stock=self.env['stock.picking'].search([('sale_id','=',sale_id)])
        Deveuro=[]
        for a in invoice.facture2:
            Deveuro.append({a.name:a.euro})       
        Devead=[]
        for a in invoice.facture2:
            Devead.append({a.name:a.ead})   
        listepro1=[]
        for a in saleorder.order_line:         
            listepro1.append({'product':a.product_id.name,'qte':a.product_uom_qty,'prix':a.price_unit}) 
        if invoice.date_due :
            date_eche = datetime.strptime(invoice.date_due, "%Y-%m-%d")
            date_eche=date_eche.strftime("%d-%m-%Y")
        listeadresse=[]
        for client in saleorder.partner_id :
            listeadresse.append({'adresse1':client.street,'adresse2':client.street2,'city':client.city,'zip':client.zip,'country':client.country_id.name}) 
        test=0
        for line in saleorder.order_line :
            if "Lubricants" in line.product_id.categ_id.name :
                test=1
        if test==1 :
            if company.name=="OCEAN AKAD INTERNATIONAL FZCO":
                banque=self.env['liste.banque'].search([('filt','=','fl')])
                datas={
                    'form':{
                        'company_logo':company.header,
                        'company_fotter':company.fotter,
                        'company_name':company.name,
                        'company_banque':banque.banquee,
                        'company_nomcompte':banque.nomcomptee,
                        'company_numcompte':banque.numcomptee,
                        'company_swift':banque.swiftcodee,
                        'company_iban':banque.ibane,
                        'facture_id':invoice.id,
                        'client':invoice.partner_id.name,
                        'adresse':listeadresse,
                        'tele':invoice.partner_id.phone,
                        'fax':invoice.partner_id.fax,
                        'email':invoice.partner_id.email,
                        'produit':listepro1,
                        'prix_commande':saleorder.amount_total,
                        'date_echeance':date_eche,
                        'facture':Deveuro,
                        'factures':Devead,
                        'lettreeuro':num2words(  Deveuro[2]['CIF'],  to='currency',lang='fr'),
                        'lettreead':num2words(  Devead[2]['CIF'],  to='currency',lang='fr'),
                        'depot':saleorder.stock.name,
                        'portchargement':stock.port.name,
                        'devis':deviss.devis,
                        
                    }            
                }
            else :
                datas={
                    'form':{
                        'company_logo':company.header,
                        'company_fotter':company.fotter,
                        'company_name':company.name,
                        'facture_id':invoice.id,
                        'client':invoice.partner_id.name,
                        'adresse':listeadresse,
                        'tele':invoice.partner_id.phone,
                        'fax':invoice.partner_id.fax,
                        'email':invoice.partner_id.email,
                        'produit':listepro1,
                        'prix_commande':saleorder.amount_total,
                        'date_echeance':date_eche,
                        'facture':Deveuro,
                        'factures':Devead,
                        'lettreeuro':num2words(  Deveuro[2]['CIF'],  to='currency',lang='fr'),
                        'lettreead':num2words(  Devead[2]['CIF'],  to='currency',lang='fr'),
                        'depot':saleorder.stock.name,
                        'portchargement':stock.port.name,
                        'devis':deviss.devis,
                        
                    }            
                }
        else :
            raise ValidationError("Attention : vous n'avez pas choisi la bonne facture dans le menu")
        docargs = {
            'doc_ids': self._ids,
            'doc_model': report.model,
            'docs': self,
            'data': datas,             
            }
        return report_obj.render('facture.lubr_facture',docargs)

