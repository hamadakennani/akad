# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import ValidationError
from itertools import groupby
from datetime import datetime, timedelta


from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.misc import formatLang

import odoo.addons.decimal_precision as dp

class Currency_tarif(models.Model):
    _name = 'currency.tarifs'
    
    @api.onchange('devise_entree','taux')
    def _devise(self):
        print self.env['currency.tarifs'].search([])
        ids=[]
        print 'domaaaaaaain'
        alls=self.env['currency.tarifs'].search([])
        for line in  alls:
            ids.append(line.devise_entree.id)
        return  {'domain':{'devise_entree':[('id','not in',ids)]}}

    devise_entree = fields.Many2one(comodel_name='res.currency', string='Devise en sortie')
    taux = fields.Float(string='Taux')
    
    # @api.constrains('devise_entree')
    # def check_devise(self):
    #     if self.devise_entree in  self.env['currency.tarifs'].search([()]).devise_entree :
    #         raise ValidationError('Vous ne pouvez pas ajouter la meme ligne deux fois')

class Currency_exchange(models.Model):

    _inherit = 'currency.exchange'
    

    out_curr=fields.Many2one('res.currency', track_visibility='never')
    tax = fields.Selection([('0', '2%'), ('5', '5%'), ('10', '10%')],
                           'Service Tax', default='0')

    @api.one
    @api.depends('input_curr', 'out_curr', 'in_amount')
    def _compute_get_currency(self):
        super(Currency_exchange, self)
        print '111111111111'
        print self.env['currency.tarifs'].search([])
        tarifs = self.env['currency.tarifs'].search([('devise_entree','=',self.input_curr.id)])
        print tarifs
        self.rate=tarifs.taux
        self.out_amount=float(self.rate)*float(self.in_amount)
        print '000000000000'
    